package ru.konungstvo.chat.message;

import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;

import java.util.List;

public class GlobalMessage extends RoleplayMessage {
    public GlobalMessage(String playerName, String content, Range range) {
        super(playerName, content, range);
    }

    @Override
    public List<MessageComponent> buildTemplate(List<MessageComponent> template) {
        //String template = ChatColor.NICK.get() + "{playerName}: " + ChatColor.GLOBAL.get() + "(( {content} ))";
        template.add(new MessageComponent("{playerName}: ", ChatColor.NICK));
        template.add(new MessageComponent("(( {content} ", ChatColor.GLOBAL));
        template.add(new MessageComponent("))", ChatColor.GLOBAL));
        return template;
    }
}
