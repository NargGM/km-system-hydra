package ru.konungstvo.chat.message;

import net.minecraft.entity.player.EntityPlayer;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;

import java.util.List;

public class RadioMessage extends RoleplayMessage {
    public RadioMessage(String playerName, String message, Range range) {
        super(playerName, message, range);
        /*
        String template = ChatColor.NICK.get() + "{playerName}" + ChatColor.DEFAULT + " ({rangeDescription} в ООС): " + ChatColor.OOC + "(( {content} ))";
        if (this.getRange() == Range.NORMAL) {
            template = ChatColor.NICK.get() + "{playerName}" + ChatColor.DEFAULT + " (ООС): " + ChatColor.OOC + "(( {content} ))";
        }

         */
    }

    @Override
    public List<MessageComponent> buildTemplate(List<MessageComponent> template) {
        template.add(new MessageComponent("{playerName}", ChatColor.NICK));
        if (getRange() != Range.NORMAL) {
            template.add(new MessageComponent(" ({rangeDescription} в радио): "));
        } else {
            template.add(new MessageComponent(" (в радио): "));
        }
        template.add(new MessageComponent("{content}"));
        return template;
    }
}
