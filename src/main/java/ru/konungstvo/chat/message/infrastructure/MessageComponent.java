package ru.konungstvo.chat.message.infrastructure;

import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.chat.ChatColor;

import java.util.List;

/**
 * Built to suit ingame JSON-format
 */
public class MessageComponent {
    private String text;
    private ChatColor color;
    boolean bold;
    boolean italic;
    boolean strikethrough;
    boolean underlined;
    private String clickCommand;
    private TextComponentString hoverText;

    private List<MessageComponent> siblings;

    public MessageComponent(String str, ChatColor color) {
        this.text = str;
        this.color = color;
        this.bold = false;
        this.italic = false;
        this.strikethrough = false;
        this.underlined = false;
        this.clickCommand = "";
        this.hoverText = null;

        this.siblings = null;
    }

    public MessageComponent(String str) {
        this(str, ChatColor.DEFAULT);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ChatColor getColor() {
        return color;
    }

    public void setColor(ChatColor color) {
        this.color = color;
    }

    public boolean isBold() {
        return bold;
    }

    public void setBold(boolean bold) {
        this.bold = bold;
    }

    public boolean isItalic() {
        return italic;
    }

    public void setItalic(boolean italic) {
        this.italic = italic;
    }

    public boolean isStrikethrough() {
        return strikethrough;
    }

    public void setStrikethrough(boolean strikethrough) {
        this.strikethrough = strikethrough;
    }

    public boolean isUnderlined() {
        return underlined;
    }

    public void setUnderlined(boolean underlined) {
        this.underlined = underlined;
    }

    // CLICK COMMANDS
    public String getClickCommand() {
        return clickCommand;
    }

    public void setClickCommand(String clickCommand) {
        this.clickCommand = clickCommand;
    }

    // HOVER TEXT
    public TextComponentString getHoverText() {
        return hoverText;
    }

    public void setHoverText(TextComponentString hoverText) {
        this.hoverText = hoverText;
    }

    public void setHoverText(String hoverText, TextFormatting color) {
        TextComponentString tcs = new TextComponentString(hoverText.trim());
        tcs.getStyle().setColor(color);
        setHoverText(tcs);
    }

    /*
    public MessageComponent appendSibling(MessageComponent sibling) {
        if (siblings == null) {
            siblings = new LinkedList<>();
        }
        siblings.add(sibling);
        return this;
    }

    public boolean hasSiblings() {
        return siblings != null;
    }

    public List<MessageComponent> getSiblings() {
        return siblings;
    }

     */
}
