package ru.konungstvo.chat.range;

// autodetermine which range should be used for the context
public class RangeFactory {
    public static Range build(String context) {
        for (Range range : Range.values()) {
            if (context.startsWith(range.getSign())) {
                return range;
            }
        }
        return Range.NORMAL;
    }
}
