package ru.konungstvo.player.wounds;

import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.exceptions.WoundsException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class WoundPyramid implements Serializable {
    public final static int CRIT_DEADLY_WEIGHT = 200;
    public final static int SEVERE_WEIGHT = 50;
    public final static int LIGHT_WEIGHT = 25;
    public final static int SCRATCH_WEIGHT = 5;

    public int getAlmostCuredBuff() {
        return almostCuredBuff;
    }

    public void setAlmostCuredBuff(int almostCuredBuff) {
        this.almostCuredBuff = almostCuredBuff;
    }

    private int almostCuredBuff = 0;

    public static WoundPyramid createFromEndurance(int enduranceLevel) {
        switch (enduranceLevel) {
            case 6:
                return new WoundPyramid(4, 5, 6);
            case 5:
                return new WoundPyramid(4, 4, 6);
            case 4:
                return new WoundPyramid(3, 4, 5);
            case 3:
                return new WoundPyramid(2, 4, 5);
            case 2:
                return new WoundPyramid(2, 3, 4);
            case 1:
                return new WoundPyramid(2, 2, 4);
            case 0:
                return new WoundPyramid(1, 2, 3);
            case -1:
                return new WoundPyramid(1, 1, 3);
        }
        return new WoundPyramid(1, 2, 3);
    }

    private List<WoundType> woundList;
    private int deadlyNumber = 1;
    private int criticalNumber = 1;
    private int severeNumber;
    private int lightNumber;
    private int scratchNumber;



    public WoundPyramid(int severeNumber, int lightNumber, int scratchNumber) {
        this.woundList = new ArrayList<>();
        this.severeNumber = severeNumber;
        this.lightNumber = lightNumber;
        this.scratchNumber = scratchNumber;
    }

    public Iterator iterator() {
        return new WoundPyramidIterator();
    }

    private class WoundPyramidIterator implements Iterator {
        private WoundType woundType;
        private int cursor;

        public WoundPyramidIterator() {
            this.cursor = 0;
        }


        @Override
        public boolean hasNext() {
            return cursor < woundList.size();
        }

        @Override
        public WoundType next() {
            if (!hasNext()) return null;
            return woundList.get(this.cursor++);
        }
    }


    public int getNumberOfWounds(WoundType woundType) {
        WoundPyramidIterator iterator = new WoundPyramidIterator();
        int count = 0;
        while (iterator.hasNext()) {
            WoundType wound = iterator.next();
            if (wound == woundType) {
                count++;
            }
        }
        return count;
    }

    public boolean hasSevereSlots() {
        return getNumberOfWounds(WoundType.SEVERE) < severeNumber;
    }

    public boolean onlyDeadlyLeft() {
        if (getNumberOfWounds(WoundType.SCRATCH) == scratchNumber && getNumberOfWounds(WoundType.LIGHT) == lightNumber && getNumberOfWounds(WoundType.SEVERE) == severeNumber && getNumberOfWounds(WoundType.CRITICAL) == criticalNumber) return true;
        return false;
    }

    public void addWound(WoundType woundType) {
        addWound(woundType, null, null);
    }

    public String addWound(WoundType woundType, String name, String description) {
        return addWound(woundType, name, description, false);
    }

    public String addWound(WoundType woundType, String name, String description, boolean ignorelimit) {
        switch (woundType) {
            case SCRATCH:
                if (getNumberOfWounds(WoundType.SCRATCH) == scratchNumber && !ignorelimit) {
                    return addWound(WoundType.LIGHT, name, description);
                }
                break;
            case LIGHT:
                if (getNumberOfWounds(WoundType.LIGHT) == lightNumber && !ignorelimit) {
                    return addWound(WoundType.SEVERE, name, description);
                }
                break;
            case SEVERE:
                if (getNumberOfWounds(WoundType.SEVERE) == severeNumber && !ignorelimit) {
                    return addWound(WoundType.CRITICAL, name, description);
                }
                break;
            case CRITICAL:
                if (getNumberOfWounds(WoundType.CRITICAL) == criticalNumber && !ignorelimit) {
                    return addWound(WoundType.DEADLY, name, description);
                }
                break;
            case DEADLY:
                if (getNumberOfWounds(WoundType.DEADLY) == deadlyNumber) {
//                    throw new WoundsException("Wounds:", "Нельзя добавить смертельную рану! Её слот уже занят.");
                    System.out.println("Нельзя добавить смертельную рану! Её слот уже занят.");
                }
        }

        if ((woundType == WoundType.DEADLY || woundType == WoundType.CRITICAL) && getNumberOfWounds(WoundType.SEVERE) < severeNumber) {
            if (name != null && DataHolder.inst().getPlayer(name).isPersistent()) return addWound(WoundType.SEVERE, name, description);
        }

        try {
            if (woundType != WoundType.DEADLY) {
                DataHolder.inst().getModifiers().sendWound(name, woundType, description);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.woundList.add(woundType);
        return woundType.getDesc();
    }

    public String addWound(WoundType woundType, String name, String description, boolean persisted, boolean forcedDeadly) {
        if (forcedDeadly) woundType = WoundType.DEADLY;
        switch (woundType) {
            case SCRATCH:
                if (getNumberOfWounds(WoundType.SCRATCH) == scratchNumber) {
                    return addWound(WoundType.LIGHT, name, description);
                }
                break;
            case LIGHT:
                if (getNumberOfWounds(WoundType.LIGHT) == lightNumber) {
                    return addWound(WoundType.SEVERE, name, description);
                }
                break;
            case SEVERE:
                if (getNumberOfWounds(WoundType.SEVERE) == severeNumber) {
                    return addWound(WoundType.CRITICAL, name, description);
                }
                break;
            case CRITICAL:
                if (getNumberOfWounds(WoundType.CRITICAL) == criticalNumber) {
                    return addWound(WoundType.DEADLY, name, description);
                }
                break;
            case DEADLY:
                if (getNumberOfWounds(WoundType.DEADLY) == deadlyNumber) {
//                    throw new WoundsException("Wounds:", "Нельзя добавить смертельную рану! Её слот уже занят.");
                    System.out.println("Нельзя добавить смертельную рану! Её слот уже занят.");
                }
        }
        if ((woundType == WoundType.DEADLY || woundType == WoundType.CRITICAL) && getNumberOfWounds(WoundType.SEVERE) < severeNumber) {
            if (persisted) {
                description = description.replace("[нокаут]", "[оглушение]");
                return addWound(WoundType.SEVERE, name, description);
            }
        } else if ((woundType == WoundType.DEADLY)) {
            if (persisted) {
                System.out.println("TEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEESTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT");
               description = description.replace("[нокаут]", "[оглушение]");
               return addWound(WoundType.CRITICAL, name, description);
            }
        }

        try {
            if (woundType != WoundType.DEADLY) {
                DataHolder.inst().getModifiers().sendWound(name, woundType, description);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.woundList.add(woundType);
        return woundType.getDesc();
    }


    public int getWoundsPercentMod() {
        int result = 0;
        for (WoundType w : woundList) {
            switch (w) {
                case SCRATCH:
                    result += SCRATCH_WEIGHT;
                    break;
                case LIGHT:
                    result += LIGHT_WEIGHT;
                    break;
                case SEVERE:
                    result += SEVERE_WEIGHT;
                    break;
                default:
                    result += CRIT_DEADLY_WEIGHT;
                    break;
            }
        }
        return result - almostCuredBuff;
    }

    @Deprecated
    public int getWoundsModOld() {

        if (getNumberOfWounds(WoundType.CRITICAL) > 0 ||
                getNumberOfWounds(WoundType.DEADLY) > 0)
            return -3;
        if (getNumberOfWounds(WoundType.SEVERE) > 0)
            return -2;
        if (getNumberOfWounds(WoundType.LIGHT) > 0)
            return -1;
        return 0;

    }

    public void reset() {
        this.woundList = new ArrayList<>();
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("Deadly: ").append(getNumberOfWounds(WoundType.DEADLY)).append("/").append(deadlyNumber).append('\n');
        result.append("Critical: ").append(getNumberOfWounds(WoundType.CRITICAL)).append("/").append(criticalNumber).append('\n');
        result.append("Severe: ").append(getNumberOfWounds(WoundType.SEVERE)).append("/").append(severeNumber).append('\n');
        result.append("Light: ").append(getNumberOfWounds(WoundType.LIGHT)).append("/").append(lightNumber).append('\n');
        result.append("Scratch: ").append(getNumberOfWounds(WoundType.SCRATCH)).append("/").append(scratchNumber).append('\n');
        return result.toString();
    }

    public List<Integer> getListOfMaximums() {
        List<Integer> result = new ArrayList<>();
        result.add(deadlyNumber);
        result.add(criticalNumber);
        result.add(severeNumber);
        result.add(lightNumber);
        result.add(scratchNumber);
        return result;
    }

    public void changeWoundsNumber(WoundType woundType, int number) {
        System.out.println("changing wounds number " + woundType.getDesc() + " " + number);

        int max = getNumberOfWounds(woundType);
        for (int i = 0; i <= max; i++) {
            woundList.remove(woundType);
        }
        for (int i = 0; i < number; i++) {
            woundList.add(woundType);
        }

    }

    public void setInitialWoundsNumber(WoundType woundType, int number) {
        switch (woundType) {
            case SCRATCH:
                this.scratchNumber = number;
                return;
            case LIGHT:
                this.lightNumber = number;
                return;
            case SEVERE:
                this.severeNumber = number;
                return;
            case CRITICAL:
                this.criticalNumber = number;
                return;
            case DEADLY:
                this.deadlyNumber = number;
        }

    }

    public Message toNiceMessage() {
        Message result = new Message();
        List<Integer> maximums = getListOfMaximums();
        for (int j = 0; j < WoundType.values().length; j++) {
            WoundType woundType = WoundType.values()[j];
            // result.append(woundType.getInt()).append(":");
            for (int i = 0; i < maximums.get(j); i++) {
                if (i < getNumberOfWounds(woundType)) {
                    result.addComponent(new MessageComponent("o", ChatColor.RED));
                } else {
                    result.addComponent(new MessageComponent("o", ChatColor.COMBAT));
                }
            }

            result.addComponent(new MessageComponent("\n"));
        }
        return result;
    }


    public String toNiceString(String name) {
        StringBuilder result = new StringBuilder();
        List<Integer> maximums = getListOfMaximums();
        for (int j = 0; j < WoundType.values().length; j++) {
            WoundType woundType = WoundType.values()[j];
            // result.append(woundType.getInt()).append(":");
            for (int i = 0; i < maximums.get(j); i++) {
                if (i < getNumberOfWounds(woundType)) {
                    result.append("§co");
                } else {
                    result.append("§6o");
                }
            }

            result.append("\n");
        }

        ArrayList<String> injList = DataHolder.inst().getModifiers().getInjuriesList(name);

        for (String inj : injList) {
            result.append(inj).append("\n");
        }

        return result.toString();
    }
}
