package ru.konungstvo.player;

import noppes.npcs.entity.EntityNPCInterface;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import ru.konungstvo.combat.CombatState;
import ru.konungstvo.combat.Weapon;
import ru.konungstvo.combat.equipment.BodyPart;
import ru.konungstvo.combat.movement.MovementTrait;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.Logger;
import ru.konungstvo.exceptions.DataException;
import ru.konungstvo.player.wounds.WoundType;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NPC extends Player {
    private String baseName;
    private String skillsetName;
    private static Logger logger = new Logger("NPC");
    private static final Pattern pattern = Pattern.compile("р\\[.*?\\]");
    private static final Pattern regex = Pattern.compile("([0-9][а-я])");
    double posX, posY, posZ;

    private Player prefferedMaster = null;

    public NPC(String nick) {
        this(nick, nick);
    }

    public NPC(String nick, String baseName) {
        super(nick);
        this.baseName = baseName;
        this.setCombatState(CombatState.SHOULD_WAIT);
    }


    public void updateSkillsOld() throws IOException, ParseException {
        String path = DataHolder.inst().getNpcTemplatesPath() + baseName + ".json";
        JSONObject json = Helpers.readJson(path);
        JSONObject skills = (JSONObject) json.get("навыки");

        if (skills == null) {
            throw new DataException("", "Не найден конфиг-файл для NPC " + baseName);
        }

        // movement
        try {
            addSkill("передвижение", Math.toIntExact((Long) skills.get("передвижение")));
        } catch (NullPointerException e) {
            addSkill("передвижение", 0);
        }

        // alias for attack
        String attack = (String) json.get("атака");
        try {
            addSkill(attack, Math.toIntExact((Long) skills.get(attack)));
            addAlias("атака", attack);
        } catch (NullPointerException ignored) {
        }

        // add other skills
        Object[] skillsArray = skills.keySet().toArray();
        for (Object skillname : skillsArray) {
            if (((String) skillname).isEmpty()) {
                continue;
            }
            int level = Math.toIntExact((Long) skills.get(skillname));
            addSkill((String) skillname, level);
        }

    }

    public void updateTraits() throws IOException, ParseException, NullPointerException {
        String path = DataHolder.inst().getNpcTemplatesPath() + baseName + ".json";
        JSONObject json = Helpers.readJson(path);
        JSONObject traits = (JSONObject) json.get("трейты");

        // add traits
        Object[] traitsArray = traits.keySet().toArray();
        for (Object trait : traitsArray) {
            String traitStr = (String) trait;
            if (traitStr.isEmpty()) {
                continue;
            }
            switch (traitStr) {
                case "толстокожий":
                case "толстокожесть":
                    setHardened(1);
                    continue;
                case "быстрый":
                    setMovementTrait(MovementTrait.FAST);
                    continue;
                case "супербыстрый":
                case "сверхбыстрый":
                    setMovementTrait(MovementTrait.SUPERFAST);
                    continue;
                case "нежить":
                    setInsensitive(true);
                    continue;
            }
        }


    }
    /*
    public void updateArmor() throws IOException, ParseException {
        String path = DataHolder.inst().getNpcTemplatesPath() + baseName + ".json";
        JSONObject json = Helpers.readJson(path);

        String armor = (String) json.get("броня");
        switch (armor) {
            case "лёгкая":
            case "легкая":
                //setArmorMod(0);
                //setDefenseMod(1);
                break;
            case "средняя":
                //setArmorMod(-1);
                //setDefenseMod(2);
                break;
            case "тяжёлая":
            case "тяжелая":
                //setArmorMod(-2);
                //setDefenseMod(3);
                break;
        }
    }

     */

    public void updateRanged() throws IOException, ParseException {
        String path = DataHolder.inst().getNpcTemplatesPath() + baseName + ".json";
        JSONObject json = Helpers.readJson(path);
        JSONObject ranged = (JSONObject) json.get("дальнобой");

        Object[] rangedArray = ranged.keySet().toArray();
        for (Object rangedWeapon : rangedArray) {
            if (((String) rangedWeapon).isEmpty()) {
                continue;
            }
            String type = (String) ranged.get(rangedWeapon);
            getWeapon((String) rangedWeapon).setRangedWeapon(type);
        }


    }

    public void updateWeapons() throws IOException, ParseException {
        String path = DataHolder.inst().getNpcTemplatesPath() + baseName + ".json";
        JSONObject json = Helpers.readJson(path);
        JSONObject weapons = (JSONObject) json.get("оружие");

        Object[] weaponsArray = weapons.keySet().toArray();
        for (Object weapon : weaponsArray) {
            if (((String) weapon).isEmpty()) {
                continue;
            }
            int damage = Math.toIntExact((Long) weapons.get(weapon));
            // consider shield
            if (((String) weapon).equals("блокирование")) {
                //setShield(new Weapon((String) weapon, damage));
                continue;
            }
            addWeapon(new Weapon((String) weapon, damage));
        }

    }

    public void updateSet() throws IOException, ParseException {
        DataHolder.inst().updatePlayerNewSkills(getName());
        DataHolder.inst().updatePlayerShield(getName());
        DataHolder.inst().updatePlayerArmor(getName());
        //updateArmor();
        System.out.println("npcs thorax armor is" + this.getArmor().getArmorPiece(BodyPart.THORAX));
//        updateWeapons();
//        updateTraits();
//        updateRanged();

//        String path = DataHolder.inst().getNpcTemplatesPath() + baseName + ".skills";
//        JSONObject json = Helpers.readJson(path);
//
//        String defense = (String) json.get("защита");
//        Range range = RangeFactory.build(defense);
//        defense = defense.substring(range.getNumberOfSymbolsToStrip());
//
//        setAutoDefense(defense);
//        setAutoDefenseRange(range);
    }

    public void setWoundsInDescription() {
        try {
            String str = "р[";
            if(getWoundPyramid().getNumberOfWounds(WoundType.SCRATCH) > 0) str += getWoundPyramid().getNumberOfWounds(WoundType.SCRATCH) + "ц";
            if(getWoundPyramid().getNumberOfWounds(WoundType.LIGHT) > 0) str += getWoundPyramid().getNumberOfWounds(WoundType.LIGHT) + "л";
            if(getWoundPyramid().getNumberOfWounds(WoundType.SEVERE) > 0) str += getWoundPyramid().getNumberOfWounds(WoundType.SEVERE) + "т";
            if(getWoundPyramid().getNumberOfWounds(WoundType.CRITICAL) > 0) str += getWoundPyramid().getNumberOfWounds(WoundType.CRITICAL) + "к";
            if(getWoundPyramid().getNumberOfWounds(WoundType.DEADLY) > 0) str += getWoundPyramid().getNumberOfWounds(WoundType.DEADLY) + "с";
            str += "]";
            if (str.equals("р[]")) return;
            EntityNPCInterface npcEntity = (EntityNPCInterface) DataHolder.inst().getNpcEntity(getName());
            String desc = npcEntity.wrappedNPC.getDisplay().getTitle();
            Matcher matcher = pattern.matcher(desc);
            String newdesc = "";
            if (matcher.find()) {
                newdesc = matcher.replaceFirst(str);
            } else {
                newdesc = desc + " " + str;
            }
            npcEntity.wrappedNPC.getDisplay().setTitle(newdesc.trim());
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void getWoundsFromDescription() {
        System.out.println("тест11111");
        try {
            EntityNPCInterface npcEntity = (EntityNPCInterface) DataHolder.inst().getNpcEntity(getName());
            String desc = npcEntity.wrappedNPC.getDisplay().getTitle();
            if (desc.contains("[крит]")) getWoundPyramid().changeWoundsNumber(WoundType.CRITICAL, 1);
            Matcher matcher = pattern.matcher(desc);
            if (matcher.find()) {
                System.out.println("тест1234567");

                String wounds = matcher.group();
                System.out.println("тест1 " + wounds);
                Matcher matcher1 = regex.matcher(wounds);
                while (matcher1.find()) {
                    if (matcher1.group().endsWith("ц")) {
                        String test = matcher1.group();
                        System.out.println("тест1 " + test);
                        int num = Integer.parseInt(test.replace("ц", ""));
                        getWoundPyramid().changeWoundsNumber(WoundType.SCRATCH, num);
                    }
                    if (matcher1.group().endsWith("л")) {
                        String test = matcher1.group();
                        System.out.println("тест2 " + test);
                        int num = Integer.parseInt(test.replace("л", ""));
                        getWoundPyramid().changeWoundsNumber(WoundType.LIGHT, num);
                    }
                    if (matcher1.group().endsWith("т")) {
                        String test = matcher1.group();
                        System.out.println("тест3 " + test);
                        int num = Integer.parseInt(test.replace("т", ""));
                        getWoundPyramid().changeWoundsNumber(WoundType.SEVERE, num);
                    }
                    if (matcher1.group().endsWith("к")) {
                        String test = matcher1.group();
                        System.out.println("тест4 " + test);
                        int num = Integer.parseInt(test.replace("к", ""));
                        getWoundPyramid().changeWoundsNumber(WoundType.CRITICAL, num);
                    }
                    if (matcher1.group().endsWith("с")) {
                        String test = matcher1.group();
                        System.out.println("тест5 " + test);
                        int num = Integer.parseInt(test.replace("с", ""));
                        getWoundPyramid().changeWoundsNumber(WoundType.DEADLY, num);
                    }
                }
            } else {
                return;
            }
            //npcEntity.wrappedNPC.getDisplay().setTitle(newdesc.trim());
        } catch (Exception e) {
            System.out.println(e);
        }
    }

//    @Override
//    public int getWoundsMod() {
//        int woundsMod = getWoundPyramid().getWoundsPercentMod();
////        if (isInsensitive()) {
////            if (woundsMod == -1) return 0;
////            if (woundsMod == -2) return -1;
////            return woundsMod;
////        }
//        return woundsMod;
//    }

    public String getBaseName() {
        return baseName;
    }

    public void setBaseName(String baseName) {
        this.baseName = baseName;
    }


    public double getPosX() {
        return posX;
    }

    public void setPosX(double posX) {
        this.posX = posX;
    }

    public double getPosY() {
        return posY;
    }

    public void setPosY(double posY) {
        this.posY = posY;
    }

    public double getPosZ() {
        return posZ;
    }

    public void setPosZ(double posZ) {
        this.posZ = posZ;
    }

    public String getSkillsetName() {
        return skillsetName;
    }

    public void setSkillsetName(String skillsetName) {
        this.skillsetName = skillsetName;
    }

    public Player getPrefferedMaster() {
        return prefferedMaster;
    }

    public void setPrefferedMaster(Player prefferedMaster) {
        this.prefferedMaster = prefferedMaster;
    }

    public void removePrefferedMaster(Player prefferedMaster) {
        this.prefferedMaster = prefferedMaster;
    }
}
