package ru.konungstvo.control.network;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class PacketMessage implements IMessage {
    // A default constructor is always required
    public PacketMessage(){}

    private int customPacket;
    public PacketMessage(int customPacket) {
        this.customPacket = customPacket;
    }

    @Override public void toBytes(ByteBuf buf) {
        // Writes the int into the buf
        buf.writeInt(customPacket);
    }

    @Override public void fromBytes(ByteBuf buf) {
        // Reads the int back from the buf. Note that if you have multiple values, you must read in the same order you wrote.
        customPacket = buf.readInt();
    }
}
