package ru.konungstvo.control.network;

import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import ru.konungstvo.bridge.ClientProxy;

// The params of the IMessageHandler are <REQ, REPLY>
// This means that the first param is the packet you are receiving, and the second is the packet you are returning.
// The returned packet can be used as a "response" from a sent packet.
public class ClickContainerPacketMessageHandler implements IMessageHandler<ClickContainerMessage, IMessage> {
    // Do note that the default constructor is required, but implicitly defined in this case

    @Override public IMessage onMessage(ClickContainerMessage message, MessageContext ctx) {
        System.out.println("Recieving packet on " + FMLCommonHandler.instance().getSide());
        System.out.println(message.name);
        System.out.println(message.content);

        ITextComponent textComponents = ITextComponent.Serializer.jsonToComponent(message.content);
        System.out.println("After deserializing: ");
        System.out.println(textComponents);


        if (FMLCommonHandler.instance().getSide().isClient()) {
            if (!message.delete) {
                if(textComponents == null) return null;
                ClientProxy.addClickContainer(message.name, textComponents);
            } else {
                if (message.name.equals("Modifiers")) {
                    ClientProxy.purgeModifiers();
                    return null;
                } else if (message.name.equals("PURGE")) {
                    ClientProxy.purgeContainers();
                    ClientProxy.purgeModifiers();
                    return null;
                }

                if (message.name.equals("ChooseNpc")) {
                    ClientProxy.purgeContainersChooseNpc();
                } else {
                    ClientProxy.deleteClickContainer(message.name);
                }
            }
        }

        // No response packet
        return null;

    }
}

