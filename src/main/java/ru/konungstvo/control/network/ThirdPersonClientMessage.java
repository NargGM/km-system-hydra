package ru.konungstvo.control.network;

import de.cas_ual_ty.gci.GunCus;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.konungstvo.bridge.ClientProxy;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

public class ThirdPersonClientMessage implements IMessage {
    public int thirdPerson = 0;

    public ThirdPersonClientMessage() {
    }

    //public ModifierMessage(boolean modifiedDamage, int modifier, boolean bodyPart) {
    //    this(modifiedDamage, modifier, bodyPart);
    //}
    public ThirdPersonClientMessage(int thirdPerson) {
        this.thirdPerson = thirdPerson;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        thirdPerson = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(thirdPerson);
    }

    public static class ThirdPersonClientMessageHandler implements IMessageHandler<ThirdPersonClientMessage, IMessage>
    {
        @SideOnly(Side.CLIENT)
        @Override
        public IMessage onMessage(ThirdPersonClientMessage message, MessageContext ctx)
        {
            System.out.println("сообщение " + message.thirdPerson);
            ClientProxy.setThirdPersonBlocked(message.thirdPerson == -666);
            if (message.thirdPerson != 666) {
                Minecraft.getMinecraft().gameSettings.thirdPersonView = 0;
                ClientProxy.setThirdPerson(0);
            }
            return null;
        }

    }

}
