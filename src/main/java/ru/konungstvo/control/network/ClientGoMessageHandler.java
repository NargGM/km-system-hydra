package ru.konungstvo.control.network;

import net.minecraft.client.Minecraft;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import ru.konungstvo.bridge.ClientProxy;

// The params of the IMessageHandler are <REQ, REPLY>
// This means that the first param is the packet you are receiving, and the second is the packet you are returning.
// The returned packet can be used as a "response" from a sent packet.
public class ClientGoMessageHandler implements IMessageHandler<ClientGoMessage, IMessage> {
    // Do note that the default constructor is required, but implicitly defined in this case

    @Override public IMessage onMessage(ClientGoMessage message, MessageContext ctx) {
        System.out.println("Recieving packet on " + FMLCommonHandler.instance().getSide());

        if (FMLCommonHandler.instance().getSide().isClient()) {
            ClientCommandHandler.instance.executeCommand(Minecraft.getMinecraft().player, "/clientgo " + message.blocksToCover);
        }
        // No response packet
        return null;

    }
}

