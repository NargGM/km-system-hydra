package ru.konungstvo.control;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger implements Serializable {
    private String name;
    public Logger(String name) {
        this.name = name;
    }
    public void info(String info) {
        System.out.println("["+name+"/INFO] " + info);
    }

    public void debug(String debug) {
        System.out.println("["+name+"/DEBUG] " + debug);
    }

    public void log(String type, String message) {
        Date date = new Date();
        try {
            message = message.replaceAll("§.", "");
            writeIntoLog(type, "[" + date.toString() + "] " + message + "\n");
                writeIntoLog("whole", "[" + date.toString() + "] " + message + "\n");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    private void writeIntoLog(String logName, String message) throws IOException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String dateString = dateFormat.format(date);

        //File file = new File("./logs/"+logName+"/" + logName + "-" + dateString + ".log");
        File file = new File("./logs/"+logName+"/" + logName + "_current.log");
        file.createNewFile(); // if file already exists will do nothing
        FileOutputStream oFile = new FileOutputStream(file, true);
        oFile.write(message.getBytes());
        oFile.close();

        /*
        File file = new File("./logs/"+logName+"/" + logName + "-" + dateString + ".log");

        if (!file.exists()) {
            file.createNewFile();
        }
        FileWriter fileWriter = new FileWriter(file, true);
        fileWriter.write(message);
//        fileWriter = new FileWriter("./logs/whole/whole-"+ dateString + ".log", true);
 //       fileWriter.write(message);
        fileWriter.close();
        */
    }
}
