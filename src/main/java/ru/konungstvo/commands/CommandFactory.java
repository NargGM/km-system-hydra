package ru.konungstvo.commands;

import ru.konungstvo.player.Player;

public class CommandFactory {
    public static String execute(Player executor, String command, String[] args) {
        /*
        switch (command) {
            case "exec":
                if (!executor.hasPermission(Permission.GM))
                    return "§4Недостаточно прав!";
                return RecursiveExecutor.execute(executor, args);
            case "test":
                return ExampleExecutor.execute(command, args);
            case "combat":
                if (!executor.hasPermission(Permission.GM))
                    return "§4Недостаточно прав!";
                return CombatExecutor.executeCombat(executor, args);
            case "equip":
                //return ChatColor.RED + "Команда ещё не готова. Откуда ты про неё знаешь?";
                return EquipExecutor.execute(executor, args);
            case "queue":
                if (!executor.hasPermission(Permission.GM)) {
                    String answer = "";
                    switch (executor.getCombatState()) {
                        case SHOULD_ACT:
                            answer = "Сейчас ваш ход!";
                            break;
                        case ACTED:
                            answer = "Вы уже ходили!";
                            break;
                        case SHOULD_WAIT:
                            answer = "Ваш ход ещё не наступил, подождите!";
                            break;
                    }
                    return ChatColor.COMBAT + answer;
                }
            case "q":
            case "react":
            case "r":
                if (!executor.hasPermission(Permission.GM))
                    return "§4Недостаточно прав!";
                return QueueExecutor.execute(executor, args);

            case "next":
                if (executor.hasPermission(Permission.GM)) {
                    return QueueExecutor.execute(executor, "next");
                }
                return QueueExecutor.executeAsFighter(executor, command, args);
            case "shift":
                return QueueExecutor.executeAsFighter(executor, command, args);
            case "setrange":
                if (!executor.hasPermission(Permission.GM))
                    return "§4Недостаточно прав!";
                return SetRangeExecutor.execute(executor, args);
            case "me":
                return "§4/me отключено, используйте *§f";
            case "sc":
                return ChatColor.RED + "Команда ещё не готова. Откуда ты про неё знаешь?";
                //return CombatExecutor.executeScoreboard(executor, args);
            case "update":
                return UpdateExecutor.execute(executor, args);
            case "alwaysgm":
                if (!executor.hasPermission(Permission.GM))
                    return "§4Недостаточно прав!";
                return AlwaysGmExecutor.execute(executor, args);
            case "msg":
       return null;     case "tell":
                if (!executor.hasPermission(Permission.TELL))
                    return "§4Недостаточно прав!";
                return TellExecutor.execute(executor, args);
            case "remind":
            case "reminder":
                if (!executor.hasPermission(Permission.GM))
                    return "§4Команда /remind доступна только ГМ-ам!";
                return ReminderExecutor.execute(executor, args);
            case "ingamerestart":
                if (!executor.hasPermission(Permission.GM))
                    return "§4Команда доступна только ГМ-ам!";
                return DiscordBotExecutor.restartBot();
            case "wounds":
                return WoundsExecutor.execute(executor, args);
            case "alias":
                return AliasExecutor.execute(executor, args);
            case "go":
                return MovementExecutor.executePointerCalc(executor, args);
            case "run":
                if (!executor.hasPermission(Permission.GM))
                    return "§4Команда доступна только ГМ-ам!";
                return RunExecutor.execute(executor, args);
            case "trait":
                if (!executor.hasPermission(Permission.GM))
                    return "§4Команда доступна только ГМ-ам!";
                return TraitExecutor.execute(executor, args);
            case "endwalk":
            case "walkend":
              //  PointerCalc.stopCalcWalk(executor.getName());
                if (executor.getWalker() != null) {
                    executor.getWalker().setRemainingBlocks(executor.getRemainingBlocks());
                }
                executor.setWalker(null);
                return ChatColor.COMBAT + "Передвижение остановлено. " +
                        Helpers.getPlural(executor.getRemainingBlocks(), "Остался ", "Осталось ", "Осталось ") +
                        ChatColor.BDCHAT + executor.getRemainingBlocks() + ChatColor.COMBAT +
                        Helpers.getPlural(executor.getRemainingBlocks(), " блок.", " блока.", " блоков.");
            case "npc":
                if (!executor.hasPermission(Permission.GM))
                    return "§4Команда доступна только ГМ-ам!";
                return NpcExecutor.execute(executor, args);
            case "move":
                if (!executor.hasPermission(Permission.GM))
                    return "§4Команда доступна только ГМ-ам!";
                String[] newArgs = new String[args.length + 1];
                newArgs[0] = "move";
                System.arraycopy(args, 0, newArgs, 1, newArgs.length);
                return NpcExecutor.execute(executor, newArgs);
            case "autodefense":
                return AutoDefenseExecutor.execute(executor, args);
            case "defenses":
                if (!executor.hasPermission(Permission.GM))
                    return "§4Команда доступна только ГМ-ам!";
                return DefensesExecutor.execute(executor, args);
            case "debug":
                if (!executor.hasPermission(Permission.GM))
                    return "§4Команда доступна только ГМ-ам!";
                return DebugExecutor.execute(executor, args);
            case "setwounds":
                if (!executor.hasPermission(Permission.GM))
                    return "§4Команда доступна только ГМ-ам!";
                return SetWoundsExecutor.execute(executor, args);
            case "radio":
                return RadioExecutor.execute(executor, args);


        }

         */
        return null;
    }


}
