package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.combat.StatusEnd;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.exceptions.PermissionException;
import ru.konungstvo.kmrp_lore.helpers.WeightHandler;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import java.util.ArrayList;
import java.util.List;

public class EffectsExecutor extends CommandBase {


    @Override
    public String getName() {
        return "geteffects";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public List<String> getAliases() {
        ArrayList<String> result = new ArrayList();
        result.add("effects");
        result.add("seteffects");
        result.add("seteffect");
        return result;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player sub = player;
        if (player.getSubordinate() != null) sub = player.getSubordinate();
        if (args.length > 1 && args[1].equals("clear") && PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get())) {
            String playerName = args[0];
            Player eff = DataHolder.inst().getPlayer(playerName);
            eff.clearEffects();
            return;
        }
        if (args.length > 0 && PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get())) {
            if (args.length > 3 && args[1].equals("remove")) {
                String playerName = args[0];
                String effect = args[2];
                for (StatusType statusType1 : StatusType.values()) {
                    if (statusType1.toString().equals(effect)) {
                        DataHolder.inst().getPlayer(playerName).removeStatusEffect(statusType1);
                    }
                }
                return;
            }
            if (args.length > 2) {
                String playerName = args[0];
                String type = args[1];
                int turns = Integer.parseInt(args[2]);
                String context = "";
                int contextNumber = -666;
                if (args.length > 3) {
                    context = args[3];
                    try {
                        contextNumber = Integer.parseInt(context);
                        context = "";
                        //if (contextNumber != -666) context = "";
                    } catch (Exception ignored) {
                        context = args[3];
                    }
                }
                if (args.length > 4) {
                    contextNumber = Integer.parseInt(args[4]);
                }
                Player eff = DataHolder.inst().getPlayer(playerName);
                StatusType statusType = null;
                for (StatusType statusType1 : StatusType.values()) {
                    if (statusType1.toString().equals(type)) {
                        statusType = statusType1;
                        break;
                    }
                }
                if (eff == null) return;
                if (context.isEmpty() && contextNumber == -666) {
                    eff.addStatusEffect(statusType.toString(), StatusEnd.TURN_END, turns, statusType);
                } else if (!context.isEmpty() && contextNumber != -666)  {
                    eff.addStatusEffect(statusType.toString(), StatusEnd.TURN_END, turns, statusType, context, contextNumber, null);
                } else if (contextNumber != -666) {
                    eff.addStatusEffect(statusType.toString(), StatusEnd.TURN_END, turns, statusType, contextNumber);
                }
                else if (!context.isEmpty()) {
                    eff.addStatusEffect(statusType.toString(), StatusEnd.TURN_END, turns, statusType, context);
                }
                return;
            }
        }

        String json = sub.getEffectMessage(false);
        if (json.isEmpty()) {
            ClickContainer.deleteContainer("Effects", getCommandSenderAsPlayer(sender));
            return;
        }
        ClickContainerMessage result = new ClickContainerMessage("Effects", json);
        KMPacketHandler.INSTANCE.sendTo(result, (EntityPlayerMP) sender);
    }
}
