package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.ClientProxy;
import ru.konungstvo.kmrp_lore.helpers.SkillSaverHandler;

public class PurgeSkillSaverExecutor extends CommandBase {
    public static final String NAME = "skillsaverpurge";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;

    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        SkillSaverHandler skillSaverHandler = new SkillSaverHandler(getCommandSenderAsPlayer(sender).getHeldItemMainhand());
        skillSaverHandler.purge();
        TextComponentString answer = new TextComponentString("Все сохраненные скиллы были удалены из оружия в вашей правой руке.");
        answer.getStyle().setColor(TextFormatting.GRAY);
        sender.sendMessage(answer);
    }

}
