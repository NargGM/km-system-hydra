package ru.konungstvo.commands.executor;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.server.permission.PermissionAPI;
import noppes.npcs.api.NpcAPI;
import org.bukkit.block.BlockState;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.*;
import ru.konungstvo.combat.equipment.Weapon;
import ru.konungstvo.combat.movement.MovementTracker;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.helpers.SkillSaverHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponDurabilityHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import java.util.ArrayList;
import java.util.HashMap;

public class FireExecutor extends CommandBase {


    @Override
    public String getName() {
        return "fire";
    }

    public static final String START_MESSAGE = "Выберите режим огня: \n";

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        boolean forced = false;
        if (player.hasPermission(Permission.GM)) {
            if (args.length > 1) {
                if (!args[1].equals("check")) {
                    forced = true;

                }
            }
        }

        int distance = 4;
        int durdamage = 35;
        int fuel = 0;
        double fireangle = 0;
        boolean check = false;
        int hand = 0;
        Player subordinate = player;
        Weapon weapon = null;
        EntityLivingBase entity = getCommandSenderAsPlayer(sender);

        if (player.getSubordinate() != null) {
            subordinate = player.getSubordinate();
            entity = (EntityLivingBase) DataHolder.inst().getNpcEntity(player.getSubordinate().getName());
            if (!DataHolder.inst().isNpc(player.getSubordinate().getName())) {
                entity = ServerProxy.getForgePlayer(player.getSubordinate().getName());
            }
        }

        WeaponDurabilityHandler durHandler = null;
        if (!forced) {

        ItemStack is = entity.getHeldItem(EnumHand.MAIN_HAND);
        hand = 1;
        if (!WeaponTagsHandler.hasWeaponTags(is)) {
            is = entity.getHeldItem(EnumHand.OFF_HAND);
            hand = 0;
        }
        if (!WeaponTagsHandler.hasWeaponTags(is)) {
            sender.sendMessage(new TextComponentString("Нет огнемёта."));
            return;
        }
        WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(is);
        weapon = new Weapon(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon(), weaponTagsHandler);
        if (weapon.isMelee() || (weapon.isRanged() && !weapon.getFirearm().isFlamer())) {
            is = entity.getHeldItem(EnumHand.OFF_HAND);
            hand = 0;
            if (!WeaponTagsHandler.hasWeaponTags(is)) {
                sender.sendMessage(new TextComponentString("Нет огнемёта."));
                return;
            }
            weaponTagsHandler = new WeaponTagsHandler(is);
            weapon = new Weapon(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon(), weaponTagsHandler);
            if (weapon.isMelee() || (weapon.isRanged() && !weapon.getFirearm().isFlamer())) {
                sender.sendMessage(new TextComponentString("Нет огнемёта."));
                return;
            }
        }

        int loaded = 0;
        try {
            loaded = weaponTagsHandler.getDefaultWeapon().getCompoundTag(WeaponTagsHandler.FIREARM_KEY)
                    .getTagList(WeaponTagsHandler.CHAMBER, 10).tagCount();
        } catch (Exception ignored) {

        }

        System.out.println("loaded " + loaded + " hand " + hand);

        HashMap<String, HashMap<String, Integer>> flamerstats = weapon.getFirearm().getFlamerstats();
        durHandler = subordinate.getDurHandlerForHand(hand);

        if (args.length == 0) {
            ClickContainerMessage remove = new ClickContainerMessage("ChooseSkill", true);
            KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
            Message message = new Message("");
            message.addComponent(new MessageComponent(START_MESSAGE));
            for (String key : flamerstats.keySet()) {
                HashMap<String, Integer> content = flamerstats.get(key);
                distance = content.get("distance");
                durdamage = content.get("durdamage");
                fuel = content.get("fuel");
                fireangle = (int) content.get("angle");

                MessageComponent fire = new MessageComponent("[" + key + "] ", ChatColor.COMBAT);
                fire.setHoverText("§4" + (fuel > loaded ? "§lНЕДОСТАТОЧНО ТОПЛИВА" : (durHandler.getDurability() < durdamage && durHandler.hasDurabilityDict() ? "§lНЕДОСТАТОЧНО ПРОЧНОСТИ" : "Сделать залп!")) + "\n" +
                        "§7Стоимость в топливе: " + fuel + "\n" +
                        "§7Дистанция: " + distance + "\n" +
                        "§7Угол: " + fireangle + "\n" +
                        "§7Урон оружию: " + durdamage + "\n" +
                        "", TextFormatting.RED);
                if (fuel <= loaded && (durHandler.getDurability() >= durdamage || !durHandler.hasDurabilityDict()))
                    fire.setClickCommand("/fire " + key);
                MessageComponent checkm = new MessageComponent("[Проверить цели] ", ChatColor.GRAY);
                checkm.setHoverText("§2Проверить в кого попадёт.\n" +
                        "§7Дистанция: " + distance + "\n" +
                        "§7Угол: " + fireangle + "\n" +
                        "", TextFormatting.RED);
                checkm.setClickCommand("/fire " + key + " check");
                message.addComponent(fire);
                message.addComponent(checkm);
            }
            MessageComponent mod = new MessageComponent("[Мод] ", ChatColor.GRAY);
            mod.setClickCommand("/modifiersbutton Fire");
            MessageComponent back = new MessageComponent("[Назад] ", ChatColor.GRAY);
            back.setClickCommand("/toroot Fire");
            message.addComponent(mod);
            message.addComponent(back);
            Helpers.sendClickContainerToClient(message, "Fire", getCommandSenderAsPlayer(sender), player);
            return;
        }


        check = false;

        if (args.length < 2) {
            ClickContainerMessage remove = new ClickContainerMessage("Fire", true);
            KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
        } else {
            if (args.length == 2) check = true;
        }

        HashMap<String, Integer> content = flamerstats.get(args[0]);
        distance = content.get("distance") + 1;
        durdamage = content.get("durdamage");
        fuel = content.get("fuel");
        fireangle = (int) content.get("angle");

        if (!check && fuel > loaded) {
            player.sendMessage(new Message("Недостаточно топлива."));
            return;
        }

        if (!check && durdamage > durHandler.getDurability() && durHandler.hasDurabilityDict()) {
            player.sendMessage(new Message("Недостаточно прочности."));
            return;
        }
        }


//        if (args.length > 0) {
//            if (args[0].equals("help")) {
//                sender.sendMessage(new TextComponentString("/boom {урон} {тип взрыва} {можно ли защищаться}\n" +
//                        "§eДоступные типы: осколочная (дефолт)\n, зажигательная, газовая, слабеющий_газ, свето-шумовая" +
//                        "§eЧтобы от взрыва нельзя было защищаться третий аргумент должен быть 'нз'" +
//                        ""));
//                return;
//            }
//            if (args[0].equals("check")) {
//                check = true;
//                distance = Integer.parseInt(args[1]);
//                if (args.length > 2) fireangle = Double.parseDouble(args[2]);
//            } else {
//                distance = Integer.parseInt(args[0]);
//                if (args.length > 1) fireangle = Double.parseDouble(args[1]);
//            }
//            distance++;
//        }
        EntityPlayerMP forgePlayer = ServerProxy.getForgePlayer(sender.getName());
        Combat combat = DataHolder.inst().getCombat(player.getAttachedCombatID());
//        if (!player.hasPermission(Permission.GM)) {
//            if (distance > 10) {
//                player.sendMessage("§4Треснет.");
//                return;
//            }
//            if (fireangle > 70) {
//                player.sendMessage("§4Треснет.");
//                return;
//            }
//        }
        if (combat == null) {
            player.sendMessage("§4Нет боя.");
            NpcAPI.Instance().getIWorld(forgePlayer.dimension).playSoundAt(NpcAPI.Instance().getIPos(Math.floor(forgePlayer.posX) + 0.5, Math.floor(forgePlayer.posY), Math.floor(forgePlayer.posZ) + 0.5), "minecraft:entity.creeper.primed",1, 1);
            return;
        }

//        if (distance > 20) {
//            player.sendMessage("§4Треснет.");
//            NpcAPI.Instance().getIWorld(forgePlayer.dimension).playSoundAt(NpcAPI.Instance().getIPos(Math.floor(forgePlayer.posX) + 0.5, Math.floor(forgePlayer.posY), Math.floor(forgePlayer.posZ) + 0.5), "minecraft:entity.creeper.primed",1, 1);
//            return;
//        }
        if (forced) {
            distance = Integer.parseInt(args[0]) + 1;
            fireangle = Integer.parseInt(args[1]);
            if (args.length > 2 && args[2].equals("check")) check = true;
            if (distance > 20) {
                player.sendMessage("§4Треснет.");
                return;
            }
            if (fireangle > 360) {
                player.sendMessage("§4Треснет.");
                return;
            }
        }


        ArrayList<Player> targets = (ArrayList<Player>) ServerProxy.getAllFightersInRangeForCombat(player, distance);
        ArrayList<Player> multitargets = new ArrayList<>();


        String skill = "восприятие";

        try {
            SkillSaverHandler skillSaver;
            if (hand == 0) skillSaver = new SkillSaverHandler(entity.getHeldItemOffhand());
            else skillSaver = new SkillSaverHandler(entity.getHeldItemMainhand());
            skill = skillSaver.getSkillSaverFor(subordinate.getName(), SkillType.MASTERING.toString());
        } catch (Exception ignored) {

        }


        for (Player opponent : targets) {
                if (subordinate.equals(opponent)) continue;
                EntityLivingBase d;
                if (DataHolder.inst().isNpc(opponent.getName())) {
                    d = DataHolder.inst().getNpcEntity(opponent.getName());
                } else {
                    d = ServerProxy.getForgePlayer(opponent.getName());
                }
                try {
                    Vec3d blockvectord = new Vec3d(Math.floor(d.posX) + 0.5, d.posY, Math.floor(d.posZ) + 0.5);
                    Vec3d blockvectorplayer = new Vec3d(Math.floor(forgePlayer.posX) + 0.5, forgePlayer.posY, Math.floor(forgePlayer.posZ) + 0.5);
                    Vec3d vector = blockvectord.subtract(blockvectorplayer);
                    Vec3d playerDirection = forgePlayer.getLookVec();
                    System.out.println("name: " + d.getName());
                    double angleDir = (Math.atan2(vector.z, vector.x) / 2 / Math.PI * 360 + 360) % 360;
                    double angleLook = (Math.atan2(playerDirection.z, playerDirection.x) / 2 / Math.PI * 360 + 360) % 360;
                    double angle = (angleDir - angleLook + 360) % 360;
                    //System.out.println("angle: " + angle);


                    System.out.println(d.getName());
                    double angleX = forgePlayer.rotationPitch;
                    System.out.println("angleX: " + angleX);
                    double c = ServerProxy.getRoundedDistanceBetweenButCountEyeHeightForOne(forgePlayer, d);
                    System.out.println("distance: " + c);
                    double a = (forgePlayer.posY + forgePlayer.getEyeHeight()) - (d.posY);
                    System.out.println("Ydiff: " + a);
                    double angle2 = Math.sin(a/c) * 100;
                    System.out.println("fposY: " + forgePlayer.posY);
                    System.out.println("dposY: " + d.posY);
                    System.out.println("angle2: " + angle2);

                    System.out.println("eye height: " + forgePlayer.getEyeHeight() + " " + d.getEyeHeight());

                    System.out.println("anglediff: " + Math.abs(angle2 - angleX));
                    if (Math.abs(angle2 - angleX) > 40) continue;
                    if ((angle <= fireangle/2 || angle >= (360 - fireangle/2))) {
                        System.out.println("hit " + Math.floor(d.posX) + 0.5 + " " + d.posY + " " + Math.floor(d.posZ) + 0.5);
                        multitargets.add(opponent);
                    } else {
                        System.out.println("not hit " + Math.floor(d.posX) + 0.5 + " " + d.posY + " " + Math.floor(d.posZ) + 0.5);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        Vec3d playerDirection = forgePlayer.getLookVec();
        if (!check) {
            if (ForgeRegistries.BLOCKS.getValue( new ResourceLocation("stewblocks", "effect_fire" )) == null) return;
            IBlockState block = ForgeRegistries.BLOCKS.getValue( new ResourceLocation("stewblocks", "effect_fire" )).getDefaultState();
            for (int x = forgePlayer.getPosition().getX() - distance; x <= forgePlayer.getPosition().getX() + distance; x++) {
                for (int y = forgePlayer.getPosition().getY() - distance; y <= forgePlayer.getPosition().getY() + distance; y++) {
                    for (int z = forgePlayer.getPosition().getZ() - distance; z <= forgePlayer.getPosition().getZ() + distance; z++) {
                        if (!forgePlayer.getEntityWorld().getBlockState(new BlockPos(x, y, z)).getMaterial().isReplaceable())
                            continue;
                        BlockPos bpos = new BlockPos(x, y, z);
                        if (bpos.equals(forgePlayer.getPosition())) continue;
                        if (forgePlayer.getEntityWorld().getBlockState(bpos.offset(EnumFacing.DOWN)).getMaterial().isReplaceable())
                            continue;
                        if (forgePlayer.getEntityWorld().getBlockState(bpos.offset(EnumFacing.DOWN)).equals(block))
                            continue;
                        System.out.println("testing " + x + " " + y + " " + z);
                        Vec3d blockvector = new Vec3d(x + 0.5, y, z + 0.5);
                        Vec3d blockvectorplayer = new Vec3d(Math.floor(forgePlayer.posX) + 0.5, forgePlayer.posY, Math.floor(forgePlayer.posZ) + 0.5);
                        Vec3d vector = blockvector.subtract(blockvectorplayer);
                        double angleDir = (Math.atan2(vector.z, vector.x) / 2 / Math.PI * 360 + 360) % 360;
                        double angleLook = (Math.atan2(playerDirection.z, playerDirection.x) / 2 / Math.PI * 360 + 360) % 360;
                        double angle = (angleDir - angleLook + 360) % 360;
                        double angleX = forgePlayer.rotationPitch;
                        System.out.println("angleX: " + angleX);
                        double c = MovementTracker.getDistanceBetween(
                                Math.floor(forgePlayer.posX) + 0.5, forgePlayer.posY + forgePlayer.getEyeHeight(), Math.floor(forgePlayer.posZ) + 0.5,
                                x + 0.5, y, z + 0.5
                        );
                        if (c > distance) continue;
                        System.out.println("distance: " + c);
                        double a = (forgePlayer.posY + forgePlayer.getEyeHeight()) - (y);
                        System.out.println("Ydiff: " + a);
                        double angle2 = Math.sin(a / c) * 100;
                        System.out.println("fposY: " + forgePlayer.posY);
                        System.out.println("angle2: " + angle2);
                        System.out.println("angle2 - angleX: " + Math.abs(angle2 - angleX));
                        if (Math.abs(angle2 - angleX) > 40) continue;
                        System.out.println("angleforreal: " + angle);
                        if (!(angle <= fireangle / 2 || angle >= (360 - fireangle / 2))) continue;
                        System.out.println("fire at " + (x + 0.5) + " " + y + " " + (z + 0.5));
                        forgePlayer.world.setBlockState(bpos, block);

//
//                        Vec3d startvector = new Vec3d(Math.floor(forgePlayer.posX) + 0.5, forgePlayer.posY + forgePlayer.getEyeHeight(), Math.floor(forgePlayer.posZ) + 0.5);
//                        Vec3d endvector = blockvector.add(startvector);
//                        RayTraceResult result = forgePlayer.world.rayTraceBlocks(startvector, endvector, false, true, true);
//                        System.out.println("эм2 " + (result == null));
//                        if (result == null) continue;
//                        System.out.println("эм " + result.getBlockPos().getX() + " " + result.getBlockPos().getY() + " " + result.getBlockPos().getZ());
//                        System.out.println("эм " + result.sideHit.toString());
//
//                        if (bpos.getX() == result.getBlockPos().getX() && bpos.getY() == result.getBlockPos().getY() && bpos.getZ() == result.getBlockPos().getZ()) {
//                            System.out.println("place fire");
//                            forgePlayer.world.setBlockState(bpos.offset(EnumFacing.UP), Blocks.FIRE.getDefaultState());
//                        }
                    }
                }
            }
        }

        if (multitargets.size() == 0) {
            if (check) {
                sender.sendMessage(new TextComponentString("§4В зоне залпа нет целей."));
                return;
            }
            Message message = new Message("§4Происходит огнемётный залп §8[§c" + (distance - 1) + "/" + fireangle + "°§8]§4)! В зоне залпа нет целей.");
            for (int i = 0; i < fuel; i++) {
                subordinate.fireProjectile(hand);
            }
            if (!forced) durHandler.takeAwayDurability(durdamage);
            ServerProxy.sendMessageFromAndInformMasters(player, message);
            EntityPlayerMP playerMP = ServerProxy.getForgePlayer(sender.getName());
            NpcAPI.Instance().getIWorld(playerMP.dimension).playSoundAt(NpcAPI.Instance().getIPos(Math.floor(playerMP.posX) + 0.5, Math.floor(playerMP.posY), Math.floor(playerMP.posZ) + 0.5), "minecraft:entity.creeper.primed",1, 1);
            if (subordinate.getCombatState() == CombatState.SHOULD_ACT) {
                player.performCommand("/queue next");
            }
            return;
        }
        StringBuilder targetsName = new StringBuilder();
        for (Player target : multitargets) {
            targetsName.append(target.getName()).append(" ");
            if (DataHolder.inst().isNpc(target.getName())) continue;
            if (!check) target.performCommand("/fired");
        }
        if (check) {
            sender.sendMessage(new TextComponentString("§4Затронуты залпом будут:§a " + targetsName.toString().trim()));
            return;
        }
        SkillDiceMessage sdm = new SkillDiceMessage(player.getName(), "% " + skill + " время барбекю");
        sdm.build();
        ServerProxy.sendMessageFromAndInformMasters(player, sdm);
        Fire fire = new Fire(ServerProxy.getForgePlayer(sender.getName()), distance, player.getAttachedCombatID(), ServerProxy.getForgePlayer(sender.getName()).dimension, fireangle, sdm);
        combat.addFire(fire);

        fire.addTargets(multitargets);






        Message message = new Message("§4Происходит огнемётный залп §8[§c" + (distance - 1) + "/" + fireangle + "°§8]§4)! §4Затронуты залпом:\n§с[§a" + targetsName.toString().trim() + "§с]");
        for (int i = 0; i < fuel; i++) {
            subordinate.fireProjectile(hand);
        }
        if (!forced) durHandler.takeAwayDurability(durdamage);
        ServerProxy.sendMessageFromAndInformMasters(player, message);
        for (Player target : multitargets) {
            if (DataHolder.inst().isNpc(target.getName())) {
                Message test = new Message("", ChatColor.BLUE);
                MessageComponent testing = new MessageComponent("/sub " + target.getName(), ChatColor.BLUE);
                testing.setUnderlined(true);
                testing.setClickCommand("/sub " + target.getName());
                test.addComponent(testing);
                ServerProxy.sendMessage(DataHolder.inst().getMasterForCombat(combat.getId()), test);
                break;
            };
        }
        //ServerProxy.informDistantMasters(player, message, Range.NORMAL.getDistance());
    }
}
