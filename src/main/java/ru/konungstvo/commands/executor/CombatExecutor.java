package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.bridge.discord.DiscordBridge;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.FudgeDiceMessage;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.Combat;
import ru.konungstvo.combat.CombatState;
import ru.konungstvo.combat.RangedWeapon;
import ru.konungstvo.combat.Weapon;
import ru.konungstvo.combat.movement.ChargeHistory;
import ru.konungstvo.combat.moves.DeprecatedAttack;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.commands.CommandFactory;
import ru.konungstvo.commands.helpercommands.modifiers.SyncModifiers;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.Logger;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.control.network.ThirdPersonClientMessage;
import ru.konungstvo.control.network.ThirdPersonMessage;
import ru.konungstvo.exceptions.DataException;
import ru.konungstvo.exceptions.RuleException;
import ru.konungstvo.player.NPC;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;
import ru.konungstvo.view.Envelope;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// an upper class for handling combats
public class CombatExecutor extends CommandBase {
    private static Logger logger = new Logger("CombatExecutor");
    private static final String NAME = "combat";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), "km.gm");
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        String answer = executeCombat(player, args);
        Message message = new Message(answer, ChatColor.GRAY);
        player.sendMessage(message);
    }

    /**
     * Used by GMs to manipulate combats
     *
     * @param executor GM who issues a command
     * @param args     arguments
     * @return send the result of the command or an error if occurs
     */
    public static String executeCombat(Player executor, String... args) {
        DataHolder dh = DataHolder.inst();

        // Get combat that GM is attached to
        Combat combat = null;
        if (executor.getAttachedCombatID() >= 0) combat = dh.getCombat(executor.getAttachedCombatID());

        // Give info about attached combat
        if (args.length == 0) {
            if (combat == null) return "§5У вас нет текущего боя.";
            //TODO: just call combat.toString()
            String result = "§5Ваш текущий бой: [" + combat.getId() + "] " + combat.getName() + ".\n Бойцы: ";
            for (Player player : combat.getFighters()) {
                result += player.getName() + ", ";
            }
            return result.substring(0, result.length() - 2) + ".";
        }

        if (args[0].equals("help")) return "§eТут будет хелп";

        // Handle arguments
        switch (args[0]) {
            case "create":
                String newCombatName = executor.getName();
                if (args.length > 1) newCombatName = args[1];

                combat = dh.createCombat(newCombatName);
                executor.setAttachedCombatID(combat.getId());
                return "§5Бой " + newCombatName + " создан!";

            case "end":
                if (combat == null) return "§5Бой " + executor.getAttachedCombatID() + " не существует!";
                combat.removeNPCs();
                Message message = new Message();
                MessageComponent messageComponent0 = new MessageComponent("Бой окончен!", ChatColor.YELLOW);
                messageComponent0.setBold(true);
                message.addComponent(messageComponent0);
                if (combat.getTime() != 0) {
                    long millis = MinecraftServer.getCurrentTimeMillis() - combat.getTime();
                    String time = String.format("%02d:%02d:%02d",
                            TimeUnit.MILLISECONDS.toHours(millis),
                            TimeUnit.MILLISECONDS.toMinutes(millis) % 60,
                            TimeUnit.MILLISECONDS.toSeconds(millis) % 60
                    );

                    MessageComponent messageComponent = new MessageComponent(" Бой занял " + time + ".", ChatColor.YELLOW);
                    message.addComponent(messageComponent);
                    DiscordBridge.sendMessage("Бой (" + combat.getId() + ") окончен! Бой занял " + time + ".");
                }
                for (Player p : combat.getFighters()) {
                    p.setMovementHistory(MovementHistory.NONE);
                    p.setChargeHistory(ChargeHistory.NONE);
                    p.setPreviousDefenses(0);
                    p.getPercentDice().setPreviousDefenses(0);
                    p.setRemainingBlocks(-1);
                    p.clearAllOpportunities();
                    p.setRecoil(0);

                    p.setShifted(false);
                    p.setHoldingOnWill(false);
                    p.setDefensiveStance(false);
                    p.setAimedMode(false);
                    p.setStation(false);
                    p.setBipodsDeployed(false);
                    p.clearEffects();
                    if (!dh.isNpc(p.getName())) {
                        p.purgeModifierSet();
                        p.performCommand(SyncModifiers.NAME + " purge");
                    }
                    if (!dh.isNpc(p.getName()) && !p.hasPermission(Permission.GM)) KMPacketHandler.INSTANCE.sendTo(new ThirdPersonClientMessage(666), ServerProxy.getForgePlayer(p.getName()));
                    if (!dh.isNpc(p.getName())) p.sendMessage(message);
                    p.unfreeze();
                    if (ServerProxy.getForgePlayer(p.getName()) == null) {
                        try {
                            Helpers.writeSerialized("saved_players", p);
                            DataHolder.inst().removePlayer(p.getName());
                            break;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (p instanceof NPC) {
                        DataHolder.inst().removeNpc(p.getName());
                    } else {
                        DataHolder.inst().getModifiers().purgeStunWounds(p);
                    }
                }
                ServerProxy.informMasters(message, executor);
                dh.removeCombat(combat.getId());
                return "§5Бой " + combat.getId() + " (" + combat.getName() + ") удалён!";

            case "list":
                StringBuilder result = new StringBuilder("§5Текущие бои:§7");
                for (Combat c : dh.getCombatList()) {
                    result.append('\n').append("§7[").append(c.getId()).append("]");
                    result.append(c.getName()).append(":§8 ");
                    for (Player player : c.getFighters()) {
                        result.append(player.getName()).append(", ");
                    }
                }
                try {
                    result.deleteCharAt(result.lastIndexOf(","));
                } catch (Exception ignored) {
                }
                if (dh.getCombatList().isEmpty()) result.append("\nПусто!");
                return result.toString();

            case "use":
                int id = Integer.parseInt(args[1]);
                executor.setAttachedCombatID(id);
                combat = dh.getCombat(id);
                return "§5Вы сменили текущий бой: [" + combat.getId() + "] " + combat.getName();

            case "add":
                if (combat == null) return "§5Бой c ID [" + executor.getAttachedCombatID() + "] не существует!";

                String resultStr = "§5Добавлены в бой: ";
                for (String ar : args) {
                    if (dh.getPlayer(ar) != null) {
                        dh.getPlayer(ar).setRemainingBlocks(-1);
                        dh.getPlayer(ar).setMovementHistory(MovementHistory.NONE);
                        dh.getPlayer(ar).setChargeHistory(ChargeHistory.NONE);
                        dh.getPlayer(ar).setHoldingOnWill(false);
                        dh.getCombat(combat.getId()).addFighter(ar);
                        if (!dh.isNpc(ar) && !dh.getPlayer(ar).hasPermission(Permission.GM)) {
                            if (combat.getThirdPersonBlocked()) {
                                KMPacketHandler.INSTANCE.sendTo(new ThirdPersonClientMessage(-666), ServerProxy.getForgePlayer(dh.getPlayer(ar).getName()));
                            } else {
                                KMPacketHandler.INSTANCE.sendTo(new ThirdPersonClientMessage(0), ServerProxy.getForgePlayer(dh.getPlayer(ar).getName()));
                            }
                        }
                        if (!dh.isNpc(ar)) {
                            dh.getPlayer(ar).purgeModifierSet();
                            dh.getPlayer(ar).performCommand(SyncModifiers.NAME + " purge");
                        } else {
                            dh.getPlayer(ar).setBloodloss(0);
                            DataHolder.inst().getModifiers().updateBloodloss(ar, 0);
                        }
                        resultStr += (ar + ", ");
                    }
                }
                resultStr = resultStr.substring(0, resultStr.length() - 2) + ".";
                return resultStr;
            case "addlc":
                if (combat == null) return "§5Бой c ID [" + executor.getAttachedCombatID() + "] не существует!";

                String resultStr1 = "§5Добавлены lc: ";
                for (String ar : args) {
                    if (dh.getPlayer(ar) != null) {
                        if (dh.getCombatForPlayer(ar) == null) {
                            return "Нет боя для " + ar;
                        }
                        dh.getCombatForPlayer(ar).addLethalCompany(dh.getPlayer(ar));
                        resultStr1 += (ar + ", ");
                    }
                }
                resultStr1 = resultStr1.substring(0, resultStr1.length() - 2) + ".";
                return resultStr1;
            case "dellc":
                if (combat == null) return "§5Бой c ID [" + executor.getAttachedCombatID() + "] не существует!";

                String resultStr2 = "§5Убраны lc: ";
                for (String ar : args) {
                    if (dh.getPlayer(ar) != null) {
                        if (dh.getCombatForPlayer(ar) == null) {
                            return "Нет боя для " + ar;
                        }
                        dh.getCombatForPlayer(ar).removeLethalCompany(dh.getPlayer(ar));
                        resultStr2 += (ar + ", ");
                    }
                }
                resultStr2 = resultStr2.substring(0, resultStr2.length() - 2) + ".";
                return resultStr2;
            case "remove":
            case "delete":
                resultStr = "§5";
                for (String ar : args) {
                    if (!ar.equals(executor.getName()) && dh.getPlayer(ar) != null) {
                        dh.getPlayer(ar).clearAllOpportunities();
                        dh.getPlayer(ar).clearEffects();
                        dh.getPlayer(ar).setRecoil(0);
                        dh.getPlayer(ar).setHoldingOnWill(false);
                        dh.getCombat(combat.getId()).removeFighter(ar);

                        dh.getPlayer(ar).setShifted(false);
                        dh.getPlayer(ar).setHoldingOnWill(false);
                        dh.getPlayer(ar).setDefensiveStance(false);
                        dh.getPlayer(ar).setAimedMode(false);
                        dh.getPlayer(ar).setStation(false);
                        dh.getPlayer(ar).setBipodsDeployed(false);
                        dh.getPlayer(ar).clearEffects();
                        resultStr += (ar + ", ");
                        if (DataHolder.inst().getPlayer(ar) instanceof NPC) {
                            DataHolder.inst().removeNpc(ar);
                        } else {
                            dh.getPlayer(ar).purgeModifierSet();
                            dh.getPlayer(ar).performCommand(SyncModifiers.NAME + " purge");
                            DataHolder.inst().getModifiers().purgeStunWounds(dh.getPlayer(ar));
                        }

                    }
                }
                resultStr = resultStr.substring(0, resultStr.length() - 1);
                resultStr += " были удалены из боя!";
                return resultStr;
            case "undo":
                dh.undoCombat(combat);
                return ChatColor.COMBAT + "Откат боя на одно изменение назад";
            case "nof5":
                boolean blocked = false;
                if (combat != null ) {
                    combat.setThirdPersonBlocked(!combat.getThirdPersonBlocked());
                    blocked = combat.getThirdPersonBlocked();
                    for (Player player : combat.getFighters()) {
                        if(!dh.isNpc(player.getName())) {
                            if (blocked && !player.hasPermission(Permission.GM)) {
                                KMPacketHandler.INSTANCE.sendTo(new ThirdPersonClientMessage(-666), ServerProxy.getForgePlayer(player.getName()));
                            } else {
                                KMPacketHandler.INSTANCE.sendTo(new ThirdPersonClientMessage(666), ServerProxy.getForgePlayer(player.getName()));
                            }
                        }
                    }
                }
                return "f5 blocked: " + blocked;

        }

        return "§4Неверный ввод!";
    }

    public static boolean executeAttack(Player executor, String message, Range range) throws Exception {
        logger.info("Creating attack!");

        FudgeDiceMessage diceMessage;
        try {
            diceMessage = new SkillDiceMessage(executor.getName(), message, range);
        } catch (DataException e) {
            logger.debug("SKillDice failed, creating FudgeDice");
            diceMessage = new FudgeDiceMessage(executor.getName(), message, range);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        Combat combat = DataHolder.inst().getCombatForPlayer(executor.getName());
        if (combat == null) throw new RuleException("§4Вы не находитесь в бою!");

        // check if combat is freezed
        if (combat.getReactionList() != null && combat.getReactionList().getQueue().isFreezed()) {
            executor.sendMessage(ChatColor.RED + "Очередь заморожена! Ожидайте решения ГМа.");
            return true;
        }
        logger.debug("HERE WE ARE");

        try {
            // if GM is leading this NPC and wants to attack, stop movement
            if (executor instanceof NPC) {
                CommandFactory.execute(executor.getWalkedBy(), "walkend", null);
            }

            int blocksLeft;
            if (executor instanceof NPC) {
                blocksLeft = executor.getRemainingBlocks();
            } else {
//            blocksLeft = PointerCalc.getBlockLeft(executor.getName());
                blocksLeft = 0;
            }
            logger.debug("Blocks left: " + blocksLeft);

            if (
                //!(executor instanceof NPC) &&
                    blocksLeft < 6 && executor.getMovementHistory() == MovementHistory.NOW) {
                if (executor instanceof NPC) {
                    ServerProxy.informMasters(ChatColor.RED + "Непись " + executor.getName() + " не может атаковать, остаток блоков: " + blocksLeft, executor);
                } else {
                    executor.sendMessage(ChatColor.RED + "Требуется 6 блоков после передвижения для совершения атаки. Ваш остаток: " +
                            ChatColor.GLOBAL + blocksLeft + ChatColor.RED + ".");
                }
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        logger.debug("HERE WE ARE2");

        if (!(executor instanceof NPC)) {
//            PointerCalc.stopCalcWalk(executor.getName());
        }
        logger.debug("HERE WE ARE3");
        executor.setRemainingBlocks(-1);
        executor.setMovementHistory(MovementHistory.NONE);

        Player defender = null;
        for (Player player : combat.getFighters()) {
            if (message.contains(player.getName()) || message.contains(player.getName().toLowerCase())) {
                defender = player;
                break;
            }
        }

        if (defender == null) throw new RuleException("§4Вы не указали, кого собрались атаковать!");
        if (!combat.hasFighter(defender.getName()))
            throw new RuleException("§4Игрок " + defender.getName() + " не находится в том же бою, что и вы!");


        logger.debug("TRES1");

        // consider distances
        for (Weapon weapon : executor.getWeapons()) {
            if (!(diceMessage instanceof SkillDiceMessage)) {
                break;
            }
            if (weapon.getSkillName().equals(diceMessage.getDice().getSkillName())) {
                double distance = ServerProxy.getDistanceBetween(executor.getName(), defender.getName());

                // close combat
                if (weapon.getRangedWeapon() == null) {
                    if (distance > 3) {
                        throw new RuleException("Слишком далеко от цели! Атаки ближнего боя должны проходить в радиусе трёх клеток.");
                    }

                    // ranged combat
                    /*
                } else if (diceMessage.getDice().getResult() < weapon.getRangedWeapon().getAttackDifficulty((int) distance)) {
                    int diff = weapon.getRangedWeapon().getAttackDifficulty((int) distance);
                    String needed = ", требуется: " + DataHolder.getInstance().getSkillTable().get(diff);
                    if (diff == 666) {
                        needed = ", цель за пределами дальности.";
                    }
                    ForgeCore.informMasters(ChatColor.COMBAT + executor.getName() + " стреляет из " + weapon.getRangedWeapon().getName() + " на дистанцию " +
                            distance + " и промахивается. Выпало: " + diceMessage.getDice().getResultAsString() + needed);
                            */
                }
            }
        }

        logger.debug("!!!!!!!!!!!!!!!!!!!");
        logger.debug("TRES2");
        // consider custom weapon mod
        String weaponModStr;
        int weaponMod = -666;
        Pattern pattern = Pattern.compile("\\[([+\\-][0-9]{1,2})]");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            weaponModStr = matcher.group(1);
            weaponMod = Integer.parseInt(weaponModStr);
        }

        // consider ranged without skill
        String ranged = "";
        RangedWeapon rangedType = null;
        Pattern pattern2 = Pattern.compile("\\{(.*)}");
        Matcher matcher2 = pattern2.matcher(message);
        if (matcher2.find()) {
            ranged = matcher2.group(1);
            for (RangedWeapon r : RangedWeapon.values()) {
                if (r.getName().equals(ranged) || r.getAlias().equals(ranged)) {
                    rangedType = r;
                    break;
                }
            }
        }

        // create attack and add it to combat
        DeprecatedAttack attack = null;
        try {
            attack = new DeprecatedAttack(executor, defender, diceMessage, weaponMod, rangedType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        combat.addAttack(attack);
        executor.setHasAttacked(true);

        // check for automatic defense
        if (defender.getAutoDefense() != null) {
            try {
//                executeDefense(defender, "% " + defender.getAutoDefense().getName(), defender.getAutoDefenseRange());
                processCombatDice(defender, "% " + defender.getAutoDefense().getName(), defender.getAutoDefenseRange());
            } catch (Exception e) {
                e.printStackTrace();
                String errorInfo = ChatColor.RED + "Ошибка автозащиты!\n" + e.getCause();
                defender.sendMessage(errorInfo);
                ServerProxy.informMasters(errorInfo);
                return false;
            }
        }
        return true;
    }

    public static boolean executeDefense(Player executor, String message, Range range) throws Exception {
        logger.debug("Executing defense in CombatExecutor");
        Combat combat = DataHolder.inst().getCombatForPlayer(executor.getName());
        if (combat == null) throw new RuleException("§4Вы не находитесь в бою!");
        DeprecatedAttack attack = combat.getAttackByDefender(executor.getName());
        if (attack == null) throw new RuleException("§4Вас никто не атакует!");


        // check if combat is freezed
        if (combat.getReactionList() != null && combat.getReactionList().getQueue().isFreezed()) {
            throw new RuleException("Очередь заморожена! Ожидайте решения ГМа.");
        }

        SkillDiceMessage skillDiceMessage = new SkillDiceMessage(executor.getName(), message, range, executor.getPreviousDefenses());


        if (skillDiceMessage.getDice().getSkillName().equals("рукопашный бой")) {
            if (!attack.getAttackDice().getSkillName().equals("рукопашный бой") && (
                    attack.getRangedType() == null ||
                            (attack.getAttacker().getWeapon(attack.getAttackDice().getSkillName()) != null) &&
                                    attack.getAttacker().getWeapon(attack.getAttackDice().getSkillName()).getRangedWeapon() == null)
            ) {
                throw new RuleException("Рукопашным боем можно защищаться только от рукопашного боя или ближней атаки из дальнобоя.");

            }
        } else if ((!skillDiceMessage.getDice().getSkillName().contains("уклонение") &&
                !skillDiceMessage.getDice().getSkillName().contains("парирование") &&
                !skillDiceMessage.getDice().getSkillName().contains("блокирование"))
        ) { // default defense skills
            throw new RuleException("Нельзя защититься с помощью " + skillDiceMessage.getDice().getSkillName() + "! Используйте уклонение, парирование или блокирование.");
        }

        logger.debug("We got here");
        combat.defendAttack(executor.getName(), skillDiceMessage);
        return true;
    }

    public static String executeScoreboard(Player executor, String[] args) {
        if (args.length == 0 || args[0].equals("help")) {
            return "Здесь будет help по команде /scoreboard";
        }
        switch (args[0]) {
            case "on":
                executor.updateWith(new Envelope<>(executor.getName(), "Пустой скорборд"));
            case "wounds":
                //TODO: this will result in client crash, because line is > 16 symbols
                executor.updateWith(new Envelope<>(executor.getName(), "Здесь будет пирамидка\n с ранами."));
        }


        return null;

    }


    public static boolean processCombatDice(Player executor, String dice, Range range) throws Exception {

        String playerName = executor.getName();
        Combat combat = DataHolder.inst().getCombatForPlayer(playerName);
        if (combat == null) return false;
        if (combat.isAttacked(playerName)) {
            return executeDefense(executor, dice, range);
        } else if (isAttack(combat, executor, dice)) {
            return executeAttack(executor, dice, range);
        }

        // return false if this was not a combat dice
        return false;

    }

    @Deprecated
    private static boolean isAttack(Combat combat, Player executor, String dice) {
        return false;
//        if (!(executor instanceof NPC) && executor.getCombatState() == CombatState.SHOULD_ACT && executor.hasAttacked()) {
//            executor.sendMessage(ChatColor.RED + "Вы уже атаковали в этот ход!");
//            return false;
//        }
//        Pattern pattern = Pattern.compile("\\[([+\\-][0-9]{1,2})]");
//        Matcher matcher = pattern.matcher(dice);
//        if (matcher.find() && executor.getCombatState() == CombatState.SHOULD_ACT) {
//            return true;
//        }
//
//        String skillName;
//        try {
//            skillName = executor.getSkillNameFromContext(dice);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//
//        //TODO: remove when opportunities are ready
//        if (executor.getCombatState() != CombatState.SHOULD_ACT) {
//            return false;
//        }
//
//
//        if (!executor.getWeapons().contains(new Weapon("рукопашный бой", 0))) {
//            executor.getWeapons().add(new Weapon("рукопашный бой", 0));
//        }
//        if (executor.getWeapons().contains(new Weapon(skillName, 0))) {
//            for (Player fighter : combat.getFighters()) {
//                if (dice.contains(fighter.getName())) {
//                    return true;
//                }
//            }
//        }
//        return false;
    }


}
