package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Permission;

public class RemoveSubordinateExecutor extends CommandBase {
    public static final String NAME = "subordinateremove";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        DataHolder.inst().getPlayer(sender.getName()).removeSubordinate();
        DataHolder.inst().getPlayer(sender.getName()).performCommand(ServerPurgeContainersExecutor.NAME);
//        ClickContainer.deleteContainer("Subordinate", getCommandSenderAsPlayer(sender));
        TextComponentString answer = new TextComponentString("Ваш подчиненный был удалён.");
        answer.getStyle().setColor(TextFormatting.GRAY);
        sender.sendMessage(answer);
    }

}
