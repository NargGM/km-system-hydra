package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

public class AdvExecutor extends CommandBase {
    public static final String NAME = "adv";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;

    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        int percent;
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player thesender = DataHolder.inst().getPlayer(sender.getName());
        if (player.getSubordinate() != null) player = player.getSubordinate();

        percent = Integer.parseInt(args[0]);

        switch (percent) {
            case -1:
                player.setCustomAdvantage(-1);
                thesender.sendMessage("Добавлена кастумная помеха");
                break;
            case 0:
                player.setCustomAdvantage(0);
                thesender.sendMessage("Кастумные помеха/преимущество сброшены");
                break;
            case 1:
                player.setCustomAdvantage(1);
                thesender.sendMessage("Добавлено кастумное преимущество");
                break;
        }

        return;

    }
}
