package ru.konungstvo.commands.executor;

import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.combat.Combat;
import ru.konungstvo.combat.moves.DeprecatedAttack;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Player;

@Deprecated
public class DebugExecutor {
    public static String execute(Player executor, String[] args) {
        String result = "";
        switch (args[0]) {
            case "attacks":
                if (args.length == 1) {
                    for (Combat combat : DataHolder.inst().getCombatList()) {
                        for (DeprecatedAttack attack : combat.getAttacks()) {
                            result += attack.toString() + '\n';
                        }
                    }
                    if (result.isEmpty()) {
                        return ChatColor.GRAY + "Атак нет.";
                    }
                    return ChatColor.GRAY + result;
                }
                if (args[1].equals("remove") || args[1].equals("delete")) {
                    Combat combat = DataHolder.inst().getCombatForPlayer(args[2]);
                    combat.removeAttack(combat.getAttackByAttacker(args[2]));
                    return ChatColor.RED + "Атака " + args[2] + " удалена.";

                }
        }
        return null;
    }
}

