package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.Boom;
import ru.konungstvo.combat.BoomType;
import ru.konungstvo.combat.Combat;
import ru.konungstvo.combat.Fire;
import ru.konungstvo.combat.dice.modificator.Modificator;
import ru.konungstvo.combat.dice.modificator.ModificatorType;
import ru.konungstvo.combat.equipment.Weapon;
import ru.konungstvo.commands.helpercommands.player.movement.MoveExecutor;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Player;

import java.util.ArrayList;
import java.util.List;

public class FireDefenseExecutor extends CommandBase {

    public static String START_MESSAGE = "Выберите способ защиты от взрыва:\n";
    public static String NAME = "fired";
    @Override
    public String getName() {
        return "fired";
    }
    @Override
    public List<String> getAliases() {
        List<String> res = new ArrayList();
        res.add("FireDefense");
        return res;
    }
    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player subordinate = player;
        if (player.getSubordinate() != null) {
            player = player.getSubordinate();
            subordinate = player;
        }
        Combat combat =  DataHolder.inst().getCombatForPlayer(player.getName());
        if (combat == null) return;

        Fire fire = combat.getFireForPlayer(player);

        if (fire == null) return;



        if (args.length > 0) {
            ClickContainerMessage remove = new ClickContainerMessage("FireDefense", true);
            KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
            Message def;
            MessageComponent def2;
            String targetsStr = "";
            switch (args[0]) {
//                case "cover":
//                    String oldcontainer = "";
//                    if (args.length > 1) {
//                        try
//                        {
//                            int cover = Integer.parseInt(args[1]);
//                            if (cover < 1) {
//                                boom.removeCover(player);
//                            } else {
//                                boom.addCover(player, cover);
//                            }
//                            if (args.length > 2) {
//                                if (DataHolder.inst().isNpc(player.getName())) {
//                                    DataHolder.inst().getPlayer(sender.getName()).performCommand("/FireDefense " + args[2]);
//                                } else {
//                                    player.performCommand("/FireDefense " + args[2]);
//                                }
//                            } else {
//                                if (DataHolder.inst().isNpc(player.getName())) {
//                                    DataHolder.inst().getPlayer(sender.getName()).performCommand("/FireDefense");
//                                } else {
//                                    player.performCommand("/FireDefense");
//                                }
//                            }
//                            return;
//                        } catch (NumberFormatException ex)
//                        {
//                            oldcontainer = args[1];
//                        }
//                    }
//                    Message message1 = new Message("Выберите прочность укрытия:\n", ChatColor.COMBAT);
//                    MessageComponent oppComponent22 = new MessageComponent("[Непробиваемое] ", ChatColor.COMBAT);
//                    oppComponent22.setClickCommand("/boomd cover 5 " + oldcontainer);
//                    oppComponent22.setHoverText(new TextComponentString("§9Пример: сталь и металлокаркас толщиной в блок"));
//                    message1.addComponent(oppComponent22);
//
//                    oppComponent22 = new MessageComponent("[Высокопрочное] ", ChatColor.YELLOW);
//                    oppComponent22.setClickCommand("/boomd cover 4 " + oldcontainer);
//                    oppComponent22.setHoverText(new TextComponentString("§9Пример: железобетонная поверхность толщиной в блок"));
//                    message1.addComponent(oppComponent22);
//
//                    oppComponent22 = new MessageComponent("[Среднепрочное] ", ChatColor.GLOBAL);
//                    oppComponent22.setClickCommand("/boomd cover 3 " + oldcontainer);
//                    oppComponent22.setHoverText(new TextComponentString("§9Пример: кирпичная поверхность толщиной в блок"));
//                    message1.addComponent(oppComponent22);
//
//                    oppComponent22 = new MessageComponent("[Низкопрочное] ", ChatColor.DARK_AQUA);
//                    oppComponent22.setClickCommand("/boomd cover 2 " + oldcontainer);
//                    oppComponent22.setHoverText(new TextComponentString("§9Пример: деревянная поверхность толщиной в блок"));
//                    message1.addComponent(oppComponent22);
//
//                    oppComponent22 = new MessageComponent("[Ничтожное] ", ChatColor.DARK_GRAY);
//                    oppComponent22.setClickCommand("/boomd cover 1 " + oldcontainer);
//                    oppComponent22.setHoverText(new TextComponentString("§9Пример: ткань или стекло толщиной в блок"));
//                    message1.addComponent(oppComponent22);
//
//                    oppComponent22 = new MessageComponent("[Нет укрытия] ", ChatColor.BLUE);
//                    oppComponent22.setClickCommand("/boomd cover 0 " + oldcontainer);
//                    message1.addComponent(oppComponent22);
//
//                    Helpers.sendClickContainerToClient(message1, "BoomDefense", (EntityPlayerMP) sender, subordinate);
////                    String json22 = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message1));
////                    ClickContainerMessage result22 = new ClickContainerMessage("BoomDefense", json22);
////                    KMPacketHandler.INSTANCE.sendTo(result22, (EntityPlayerMP) sender);
//                    return;
                case "jumped":
                    Message message = new Message("Итог:\n", ChatColor.COMBAT);
                    MessageComponent oppComponent = new MessageComponent("[Готово] ", ChatColor.BLUE);
                    oppComponent.setClickCommand("/fired jump");
                    message.addComponent(oppComponent);

                    oppComponent = new MessageComponent("[В безопасности] ", ChatColor.BLUE);
                    oppComponent.setClickCommand("/fired safej");
                    oppComponent.setHoverText("Выбирайте только если в полной безопасности от взрыва!", TextFormatting.RED);
                    message.addComponent(oppComponent);

//                    if (boom.getBoomType() != BoomType.GAS && boom.getBoomType() != BoomType.FIRE && boom.getBoomType() != BoomType.WEAK_GAS) {
//                        switch (boom.getCover(player)) {
//                            case 1:
//                                oppComponent = new MessageComponent("[Ничтожное укрытие] ", ChatColor.DARK_GRAY);
//                                break;
//                            case 2:
//                                oppComponent = new MessageComponent("[Низкопрочное укрытие] ", ChatColor.DARK_AQUA);
//                                break;
//                            case 3:
//                                oppComponent = new MessageComponent("[Среднепрочное укрытие] ", ChatColor.GLOBAL);
//                                break;
//                            case 4:
//                                oppComponent = new MessageComponent("[Высокопрочное укрытие] ", ChatColor.YELLOW);
//                                break;
//                            case 5:
//                                oppComponent = new MessageComponent("[Непробиваемое укрытие] ", ChatColor.COMBAT);
//                                break;
//                            default:
//                                oppComponent = new MessageComponent("[Выбрать укрытие] ", ChatColor.BLUE);
//                                break;
//                        }
//                        oppComponent.setClickCommand("/boomd cover jumped");
//                        message.addComponent(oppComponent);
//                    }
                    Helpers.sendClickContainerToClient(message, "FireDefense", (EntityPlayerMP) sender, subordinate);
//                    String json = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message));
//                    ClickContainerMessage result = new ClickContainerMessage("BoomDefense", json);
//                    KMPacketHandler.INSTANCE.sendTo(result, (EntityPlayerMP) sender);
                    return;
                case "jump":
                    fire.addDefense(player, "jump");
                    def = new Message("§8[ОГНЕМЁТ] §a" + player.getName() + " ");
                    def2 = new MessageComponent("§4отпрыгнул!");
                    targetsStr = fire.getTargetsString();
                    if (!targetsStr.isEmpty()) def2.setHoverText("§4НЕ ЗАЩИТИЛИСЬ: " + "\n§с[§a" + targetsStr + "§с]", TextFormatting.RED);
                    if (!targetsStr.isEmpty()) def2.setUnderlined(true);
                    def.addComponent(def2);
                    ServerProxy.sendMessageFromAndInformMasters(player, def);
                    for (Player target : fire.getTargetsList()) {
                        if (DataHolder.inst().isNpc(target.getName())) {
                            Message test = new Message("", ChatColor.BLUE);
                            MessageComponent testing = new MessageComponent("/sub " + target.getName(), ChatColor.BLUE);
                            testing.setUnderlined(true);
                            testing.setClickCommand("/sub " + target.getName());
                            test.addComponent(testing);
                            ServerProxy.sendMessage(DataHolder.inst().getMasterForCombat(combat.getId()), test);
                            break;
                        };
                    }
                    break;
                case "nothing":
                    fire.addDefense(player, "nothing");
                    def = new Message("§8[ОГНЕМЁТ] §a" + player.getName() + " ");
                    def2 = new MessageComponent("§4не защищается!");
                    targetsStr = fire.getTargetsString();
                    if (!targetsStr.isEmpty()) def2.setHoverText("§4НЕ ЗАЩИТИЛИСЬ: " + "\n§с[§a" + targetsStr + "§с]", TextFormatting.RED);
                    if (!targetsStr.isEmpty()) def2.setUnderlined(true);
                    def.addComponent(def2);
                    ServerProxy.sendMessageFromAndInformMasters(player, def);
                    for (Player target : fire.getTargetsList()) {
                        if (DataHolder.inst().isNpc(target.getName())) {
                            Message test = new Message("", ChatColor.BLUE);
                            MessageComponent testing = new MessageComponent("/sub " + target.getName(), ChatColor.BLUE);
                            testing.setUnderlined(true);
                            testing.setClickCommand("/sub " + target.getName());
                            test.addComponent(testing);
                            ServerProxy.sendMessage(DataHolder.inst().getMasterForCombat(combat.getId()), test);
                            break;
                        };
                    }
                    break;
                case "blocking":
                    if (args.length < 2) {
                        Message message2 = new Message("Выберите руку:\n", ChatColor.COMBAT);

                        MessageComponent oppComponent2 = new MessageComponent("[Левой рукой] ", ChatColor.BLUE);
                        oppComponent2.setClickCommand("/fired blocking 0");
                        message2.addComponent(oppComponent2);

                        oppComponent2 = new MessageComponent("[Правой рукой] ", ChatColor.BLUE);
                        oppComponent2.setClickCommand("/fired blocking 1");
                        message2.addComponent(oppComponent2);

                        oppComponent2 = new MessageComponent("[Назад] ", ChatColor.BLUE);
                        oppComponent2.setClickCommand("/fired");
                        message2.addComponent(oppComponent2);
                        Helpers.sendClickContainerToClient(message2, "FireDefense", (EntityPlayerMP) sender, subordinate);
//                        String json2 = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message2));
//                        ClickContainerMessage result2 = new ClickContainerMessage("BoomDefense", json2);
//                        KMPacketHandler.INSTANCE.sendTo(result2, (EntityPlayerMP) sender);
                        return;
                    }
                    WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(player.getItemForHand(Integer.parseInt(args[1])));
                    if (!weaponTagsHandler.isShield()) {
                        sender.sendMessage( new TextComponentString("Предмет не является щитом."));
                        DataHolder.inst().getPlayer(sender.getName()).performCommand("/fired");
                        return;
                    }
                    fire.addDefense(player, "blocking:"+args[1]);
                    def = new Message("§8[ОГНЕМЁТ] §a" + player.getName() + " ");
                    def2 = new MessageComponent("§4блокирует!");
                    targetsStr = fire.getTargetsString();
                    if (!targetsStr.isEmpty()) def2.setHoverText("§4НЕ ЗАЩИТИЛИСЬ: " + "\n§с[§a" + targetsStr + "§с]", TextFormatting.RED);
                    if (!targetsStr.isEmpty()) def2.setUnderlined(true);
                    def.addComponent(def2);
                    ServerProxy.sendMessageFromAndInformMasters(player, def);
                    for (Player target : fire.getTargetsList()) {
                        if (DataHolder.inst().isNpc(target.getName())) {
                            Message test = new Message("", ChatColor.BLUE);
                            MessageComponent testing = new MessageComponent("/sub " + target.getName(), ChatColor.BLUE);
                            testing.setUnderlined(true);
                            testing.setClickCommand("/sub " + target.getName());
                            test.addComponent(testing);
                            ServerProxy.sendMessage(DataHolder.inst().getMasterForCombat(combat.getId()), test);
                            break;
                        };
                    }
                    break;
                case "parry":
                    if (args.length < 2) {
                        Message message2 = new Message("Выберите руку:\n", ChatColor.COMBAT);

                        MessageComponent oppComponent2 = new MessageComponent("[Левой рукой] ", ChatColor.BLUE);
                        oppComponent2.setClickCommand("/fired parry 0");
                        message2.addComponent(oppComponent2);

                        oppComponent2 = new MessageComponent("[Правой рукой] ", ChatColor.BLUE);
                        oppComponent2.setClickCommand("/fired parry 1");
                        message2.addComponent(oppComponent2);

                        oppComponent2 = new MessageComponent("[Назад] ", ChatColor.BLUE);
                        oppComponent2.setClickCommand("/fired");
                        message2.addComponent(oppComponent2);
                        Helpers.sendClickContainerToClient(message2, "FireDefense", (EntityPlayerMP) sender, subordinate);
//                        String json2 = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message2));
//                        ClickContainerMessage result2 = new ClickContainerMessage("BoomDefense", json2);
//                        KMPacketHandler.INSTANCE.sendTo(result2, (EntityPlayerMP) sender);
                        return;
                    }
                    WeaponTagsHandler weaponTagsHandler1 = new WeaponTagsHandler(player.getItemForHand(Integer.parseInt(args[1])));
                    Weapon weapon = new Weapon(weaponTagsHandler1.getDefaultWeaponName(), weaponTagsHandler1.getDefaultWeapon(), weaponTagsHandler1);
                    if (!weapon.isMelee()) {
                        sender.sendMessage( new TextComponentString("Оружие не является ближним."));
                        DataHolder.inst().getPlayer(sender.getName()).performCommand("/fired");
                        return;
                    }
                    if (weapon.getReach() + 1 < ServerProxy.getDistanceBetween(fire.getAttackerBlockPos(), player)) {
                        sender.sendMessage( new TextComponentString("Недостаточно длинное оружие для парирования."));
                        DataHolder.inst().getPlayer(sender.getName()).performCommand("/fired");
                        return;
                    }
                    SkillDiceMessage sdm = new SkillDiceMessage(player.getName(), "% парирование мечом");
                    sdm.getDice().getPercentDice().setProficiency(player.getProfMod(weapon.getProficiencies()));
                    sdm.build();
                    ServerProxy.sendMessageFromAndInformMasters(player, sdm);
                    fire.addDefenderDice(player, sdm.getDice().getResult());
                    fire.addDefense(player, "parry:"+args[1]);
                    def = new Message("§8[ОГНЕМЁТ] §a" + player.getName() + " ");
                    if (sdm.getDice().getResult() > fire.getAttackerDice().getDice().getResult()) {
                        def2 = new MessageComponent("§4парирует и §nдаёт штраф к броску§4!");
                        fire.getAttackerDice().getDice().addMod(new Modificator(-1, ModificatorType.POINT_BLANK));
                    } else {
                        def2 = new MessageComponent("§4Парирует§4!");
                    }
                    targetsStr = fire.getTargetsString();
                    if (!targetsStr.isEmpty()) def2.setHoverText("§4НЕ ЗАЩИТИЛИСЬ: " + "\n§с[§a" + targetsStr + "§с]", TextFormatting.RED);
                    if (!targetsStr.isEmpty()) def2.setUnderlined(true);
                    def.addComponent(def2);
                    ServerProxy.sendMessageFromAndInformMasters(player, def);
                    for (Player target : fire.getTargetsList()) {
                        if (DataHolder.inst().isNpc(target.getName())) {
                            Message test = new Message("", ChatColor.BLUE);
                            MessageComponent testing = new MessageComponent("/sub " + target.getName(), ChatColor.BLUE);
                            testing.setUnderlined(true);
                            testing.setClickCommand("/sub " + target.getName());
                            test.addComponent(testing);
                            ServerProxy.sendMessage(DataHolder.inst().getMasterForCombat(combat.getId()), test);
                            break;
                        };
                    }
                    break;
                case "constitution":
                    fire.addDefense(player, "constitution");
                    def = new Message("§8[ОГНЕМЁТ] §a" + player.getName() + " ");
                    def2 = new MessageComponent("§4группируется!");
                    targetsStr = fire.getTargetsString();
                    if (!targetsStr.isEmpty()) def2.setHoverText("§4НЕ ЗАЩИТИЛИСЬ: " + "\n§с[§a" + targetsStr + "§с]", TextFormatting.RED);
                    if (!targetsStr.isEmpty()) def2.setUnderlined(true);
                    def.addComponent(def2);
                    ServerProxy.sendMessageFromAndInformMasters(player, def);
                    for (Player target : fire.getTargetsList()) {
                        if (DataHolder.inst().isNpc(target.getName())) {
                            Message test = new Message("", ChatColor.BLUE);
                            MessageComponent testing = new MessageComponent("/sub " + target.getName(), ChatColor.BLUE);
                            testing.setUnderlined(true);
                            testing.setClickCommand("/sub " + target.getName());
                            test.addComponent(testing);
                            ServerProxy.sendMessage(DataHolder.inst().getMasterForCombat(combat.getId()), test);
                            break;
                        };
                    }
                    break;
                case "safe":
                    fire.addDefense(player, "safe");
                    def = new Message("§8[ОГНЕМЁТ] §a" + player.getName() + " ");
                    def2 = new MessageComponent("§4в безопасности!");
                    targetsStr = fire.getTargetsString();
                    if (!targetsStr.isEmpty()) def2.setHoverText("§4НЕ ЗАЩИТИЛИСЬ: " + "\n§с[§a" + targetsStr + "§с]", TextFormatting.RED);
                    if (!targetsStr.isEmpty()) def2.setUnderlined(true);
                    def.addComponent(def2);
                    ServerProxy.sendMessageFromAndInformMasters(player, def);
                    for (Player target : fire.getTargetsList()) {
                        if (DataHolder.inst().isNpc(target.getName())) {
                            Message test = new Message("", ChatColor.BLUE);
                            MessageComponent testing = new MessageComponent("/sub " + target.getName(), ChatColor.BLUE);
                            testing.setUnderlined(true);
                            testing.setClickCommand("/sub " + target.getName());
                            test.addComponent(testing);
                            ServerProxy.sendMessage(DataHolder.inst().getMasterForCombat(combat.getId()), test);
                            break;
                        };
                    }
                    break;
                case "safej":
                    fire.addDefense(player, "safe");
                    def = new Message("§8[ОГНЕМЁТ] §a" + player.getName() + " ");
                    def2 = new MessageComponent("§4отпрыгнул в безопасность!");
                    targetsStr = fire.getTargetsString();
                    if (!targetsStr.isEmpty()) def2.setHoverText("§4НЕ ЗАЩИТИЛИСЬ: " + "\n§с[§a" + targetsStr + "§с]", TextFormatting.RED);
                    if (!targetsStr.isEmpty()) def2.setUnderlined(true);
                    def.addComponent(def2);
                    ServerProxy.sendMessageFromAndInformMasters(player, def);
                    for (Player target : fire.getTargetsList()) {
                        if (DataHolder.inst().isNpc(target.getName())) {
                            Message test = new Message("", ChatColor.BLUE);
                            MessageComponent testing = new MessageComponent("/sub " + target.getName(), ChatColor.BLUE);
                            testing.setUnderlined(true);
                            testing.setClickCommand("/sub " + target.getName());
                            test.addComponent(testing);
                            ServerProxy.sendMessage(DataHolder.inst().getMasterForCombat(combat.getId()), test);
                            break;
                        };
                    }
                    break;
            }
            if (fire.getTargetsList().size() == 0) {
                System.out.println("test");
                fire.doFire();
            }
        } else {
            Message message = new Message(START_MESSAGE, ChatColor.COMBAT);
            MessageComponent oppComponent;
            oppComponent = new MessageComponent("[Отпрыгнуть] ", ChatColor.BLUE);
            oppComponent.setClickCommand("/" + MoveExecutor.NAME + " " + "jump");
            message.addComponent(oppComponent);

            oppComponent = new MessageComponent("[Блокирование] ", ChatColor.BLUE);
            oppComponent.setClickCommand("/fired blocking");
            message.addComponent(oppComponent);

            oppComponent = new MessageComponent("[Группировка] ", ChatColor.BLUE);
            oppComponent.setClickCommand("/fired constitution");
            message.addComponent(oppComponent);

            oppComponent = new MessageComponent("[Парирование] ", ChatColor.BLUE);
            oppComponent.setClickCommand("/fired parry");
            message.addComponent(oppComponent);

            oppComponent = new MessageComponent("[Ничего] ", ChatColor.BLUE);
            oppComponent.setClickCommand("/fired nothing");
            message.addComponent(oppComponent);

            oppComponent = new MessageComponent("[В безопасности] ", ChatColor.BLUE);
            oppComponent.setClickCommand("/fired safe");
            oppComponent.setHoverText("Выбирайте только если в полной безопасности от взрыва!", TextFormatting.RED);
            message.addComponent(oppComponent);

//            if (boom.getBoomType() != BoomType.GAS && boom.getBoomType() != BoomType.FIRE && boom.getBoomType() != BoomType.WEAK_GAS) {
//                switch (boom.getCover(player)) {
//                    case 1:
//                        oppComponent = new MessageComponent("[Ничтожное укрытие] ", ChatColor.GRAY);
//                        break;
//                    case 2:
//                        oppComponent = new MessageComponent("[Низкопрочное укрытие] ", ChatColor.DARK_AQUA);
//                        break;
//                    case 3:
//                        oppComponent = new MessageComponent("[Среднепрочное укрытие] ", ChatColor.GLOBAL);
//                        break;
//                    case 4:
//                        oppComponent = new MessageComponent("[Высокопрочное укрытие] ", ChatColor.YELLOW);
//                        break;
//                    case 5:
//                        oppComponent = new MessageComponent("[Непробиваемое укрытие] ", ChatColor.COMBAT);
//                        break;
//                    default:
//                        oppComponent = new MessageComponent("[Выбрать укрытие] ", ChatColor.BLUE);
//                        break;
//                }
//                oppComponent.setClickCommand("/boomd cover");
//                message.addComponent(oppComponent);
//            }

            oppComponent = new MessageComponent("[Мод] ", ChatColor.GRAY);
            oppComponent.setClickCommand("/modifiersbutton FireDefense");
            message.addComponent(oppComponent);

            Helpers.sendClickContainerToClient(message, "FireDefense", (EntityPlayerMP) sender, subordinate);
//            String json = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message));
//            ClickContainerMessage result = new ClickContainerMessage("BoomDefense", json);
//            KMPacketHandler.INSTANCE.sendTo(result, (EntityPlayerMP) sender);
        }

    }
}
