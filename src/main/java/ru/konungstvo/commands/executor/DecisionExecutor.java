package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.Combat;
import ru.konungstvo.combat.CombatState;
import ru.konungstvo.combat.StatusEnd;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.combat.dice.modificator.Modificator;
import ru.konungstvo.combat.dice.modificator.ModificatorType;
import ru.konungstvo.combat.duel.DuelMaster;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.player.Player;

public class DecisionExecutor extends CommandBase {
    public static final String NAME = "decision";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;

    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player subordinate = DataHolder.inst().getPlayer(sender.getName());
        if (subordinate.getSubordinate() != null) subordinate = subordinate.getSubordinate();
        if (args.length == 0) {
            Message message = new Message("", ChatColor.COMBAT);
            MessageComponent oppComponent22 = new MessageComponent("[Бороться] ", ChatColor.RED);
            oppComponent22.setClickCommand("/decision holdonwill");
            oppComponent22.setHoverText(new TextComponentString("Попытаться остаться в бою. Произойдет проверка силы воли со сложностью нормально."));
            message.addComponent(oppComponent22);
            MessageComponent oppComponent23 = new MessageComponent("[Перестать дышать] ", ChatColor.GRAY);
            oppComponent23.setClickCommand("/decision quitbreathing");
            oppComponent23.setHoverText(new TextComponentString("Перестать сражаться, зато получить один гарантированный успех на бросок выживания."));
            message.addComponent(oppComponent23);
            Helpers.sendClickContainerToClient(message, "Decision", (EntityPlayerMP) sender, subordinate);
        } else if (args[0].equals("holdonwill")) {
            ClickContainerMessage remove = new ClickContainerMessage("Decision", true);
            KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
            SkillDiceMessage sdm = new SkillDiceMessage(subordinate.getName(), "% сила воли IGNOREWOUNDS");
            sdm.build();
            if (sdm.getDice().getResult() >= 2) {
                ServerProxy.sendMessageFromAndInformMasters(subordinate, sdm);
                Message oppComponent22 = new Message(subordinate.getName() +  " остаётся в строю!", ChatColor.GMCHAT);
                ServerProxy.sendMessageFromAndInformMasters(subordinate, oppComponent22);
                Combat combat = DataHolder.inst().getCombatForPlayer(subordinate.getName());
                if (combat == null) return;
                if (combat.getDecisions().contains(subordinate)) combat.removeDecision(subordinate);
                Player next = combat.getReactionList().getQueue().getNext();
                if (next == null) return;
                if (subordinate.getCombatState().equals(CombatState.ACTED)) {
                    subordinate.setCombatState(CombatState.SHOULD_WAIT);
                    combat.addMustNotActNextRound(subordinate);
                }
                combat.getReactionList().getQueue().shift(subordinate, next.getName(), true, true);
                subordinate.addStatusEffect("HOLD ON WILL", StatusEnd.TURN_START, 1, StatusType.TRAIT_EFFECT, "нечувствительность", !subordinate.getCombatState().equals(CombatState.SHOULD_ACT));
                subordinate.setHoldingOnWill(true);
                if (combat.getDecisions().size() == 0 && !DataHolder.inst().hasRealDuelsInCombat(combat) && !subordinate.getCombatState().equals(CombatState.SHOULD_ACT)) {
                    DataHolder.inst().getMasterForPlayerInCombat(subordinate).performCommand("/" + NextExecutor.NAME + " combat");
                } else if (subordinate.getCombatState().equals(CombatState.SHOULD_ACT)) {
                    player.performCommand("/toolpanel");
                }
            } else {
                ServerProxy.sendMessageFromAndInformMasters(subordinate, sdm);
                Message oppComponent22 = new Message(subordinate.getName() +  " не может остаться в строю!", ChatColor.RED);
                ServerProxy.sendMessageFromAndInformMasters(subordinate, oppComponent22);
                Combat combat = DataHolder.inst().getCombatForPlayer(subordinate.getName());
                if (combat == null) return;
                combat.removeDecision(subordinate);
                DataHolder.inst().getMasterForPlayerInCombat(subordinate).performCommand("/queue remove " + subordinate.getName());
                combat.addLethalCompany(subordinate);
                if (combat.getDecisions().size() == 0 && !DataHolder.inst().hasRealDuelsInCombat(combat)) {
                    DataHolder.inst().getMasterForPlayerInCombat(subordinate).performCommand("/" + NextExecutor.NAME + " combat");
                }
            }
        } else {
            ClickContainerMessage remove = new ClickContainerMessage("Decision", true);
            KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
            Message oppComponent22 = new Message(subordinate.getName() +  " перестает сражаться!", ChatColor.RED);
            ServerProxy.sendMessageFromAndInformMasters(subordinate, oppComponent22);
            Combat combat = DataHolder.inst().getCombatForPlayer(subordinate.getName());
            if (combat == null) return;
            combat.addGuaranteedPlus(subordinate);
            DataHolder.inst().getMasterForPlayerInCombat(subordinate).performCommand("/queue remove " + subordinate.getName());
            combat.removeDecision(subordinate);
            combat.addLethalCompany(subordinate);
            if (combat.getDecisions().size() == 0 && !DataHolder.inst().hasRealDuelsInCombat(combat)) {
                DataHolder.inst().getMasterForPlayerInCombat(subordinate).performCommand("/" + NextExecutor.NAME + " combat");
            }
        }
    }

}
