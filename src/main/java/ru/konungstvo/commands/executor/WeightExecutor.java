package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.exceptions.PermissionException;
import ru.konungstvo.kmrp_lore.helpers.Permissions;
import ru.konungstvo.kmrp_lore.helpers.WeightHandler;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

public class WeightExecutor extends CommandBase {


    @Override
    public String getName() {
        return "weight";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        DataHolder dh = DataHolder.inst();
        if (args.length < 1) {
            //Считать вес
            int weight = player.countWeight();
            if (weight <= player.getOptimalWeight()) {
                player.sendMessage(new Message("§bНагрузка §a" + weight + " §bМаксимальная нагрузка §c" + player.getMaxWeight() + "\n" + "§dПсихическая нагрузка — §5" + player.getPsychweight() + "§d/§5" + player.getMaxPsychweight()));
            } else if (weight <= player.getMaxWeight()) {
                player.sendMessage(new Message("§bНагрузка §e" + weight + " §bМаксимальная нагрузка §c" + player.getMaxWeight() + "\n" + "§dПсихическая нагрузка — §5" + player.getPsychweight() + "§d/§5" + player.getMaxPsychweight()));
            } else {
                player.sendMessage(new Message("§bНагрузка §c" + weight + " §bМаксимальная нагрузка §c" + player.getMaxWeight() + "\n" + "§dПсихическая нагрузка — §5" + player.getPsychweight() + "§d/§5" + player.getMaxPsychweight()));
            }
            return;
        }
        if (!player.hasPermission(Permission.GM) && !player.hasPermission(Permission.CM)) {
            throw new PermissionException("1", "Недостаточно прав!");
        }

        switch (args[0]) {
            case "set":
                if (args.length < 2) return;
                if (args[1].equals("psych")) {
                    if (args.length < 3) return;
                    int weight = Integer.parseInt(args[2]);
                    ItemStack item = ((EntityPlayerMP) sender).getHeldItemMainhand();
                    WeightHandler wh = new WeightHandler(item);
                    wh.setPsychweight(weight);
                    return;
                }
                int weight = Integer.parseInt(args[1]);
                ItemStack item = ((EntityPlayerMP) sender).getHeldItemMainhand();
                WeightHandler wh = new WeightHandler(item);
                wh.setWeight(weight);
                return;
            case "remove":
                ItemStack item1 = ((EntityPlayerMP) sender).getHeldItemMainhand();
                WeightHandler wh1 = new WeightHandler(item1);
                wh1.removeWeight();
                return;
            default:
                String playername = args[0];
                Player thePlayer = DataHolder.inst().getPlayer(playername);
                if (thePlayer == null) return;
                int weight1 = thePlayer.countWeightForced();
                player.sendMessage(new Message("§bВес §a" + thePlayer.getName() + " — " + weight1 + " §bОптимальный вес §e" + thePlayer.getOptimalWeight() + " §bМаксимальный вес §c" + thePlayer.getMaxWeight() + "\n" + "§dПсихическая нагрузка §a" + thePlayer.getName() + "§5 — " + thePlayer.getPsychweight()));

                return;
        }
    }
}
