package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import java.util.ArrayList;
import java.util.List;

public class DefensesExecutor extends CommandBase {


    @Override
    public String getName() {
        return "defenses";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public List<String> getAliases() {
        ArrayList<String> result = new ArrayList<String>();
        result.add("defences");
        return result;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Message result = execute(player, args);
        player.sendMessage(result);

    }

    public static Message execute(Player executor, String[] args) {
        // TODO: error handling
        String npcName = args[0];
        Player npc = DataHolder.inst().getPlayer(npcName);
        npc.setPreviousDefenses(Integer.parseInt(args[1]));
        return new Message("Количество предыдущих защит для " + npcName + " установлено на " + args[1], ChatColor.GRAY);

    }
}
