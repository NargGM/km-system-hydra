package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.Weapon;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Player;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Deprecated
public class EquipExecutor extends CommandBase {

    @Override
    public String getName() {
        return "equip";
    }

    @Override
    public String getUsage(ICommandSender iCommandSender) {
        return "";
    }

    @Override
    public void execute(MinecraftServer minecraftServer, ICommandSender iCommandSender, String[] strings) throws CommandException {
        Player player = DataHolder.inst().getPlayer(iCommandSender.getName());
        Message answer = execute(player, strings);
        player.sendMessage(answer);
    }

    public static Message execute(Player executor, String[] args) {

        if (args.length == 0) {
            List<Weapon> weapons = executor.getWeapons();
            if (weapons.isEmpty() && executor.getShield() == null) {
                return new Message("Боевые навыки не заданы!", ChatColor.COMBAT);
            }
            Message result = new Message("Боевые навыки:\n", ChatColor.COMBAT);
//            StringBuilder res = new StringBuilder(ChatColor.COMBAT + "Боевые навыки:\n" + ChatColor.GRAY);
            for (Weapon weapon : weapons) {
                result.addComponentLn(new MessageComponent(weapon.toString(), ChatColor.GRAY));
//                res.append(weapon.toString()).append('\n');
            }
            if (executor.getShield() != null) {
                result.addComponentLn(new MessageComponent(executor.getShield().toString(), ChatColor.GRAY));
                //res.append(executor.getShield().toString());
            }
            return result;
//            return res.toString();
        }

        String combinedArgs = String.join(" ", args);
        String skillName = executor.getSkillNameFromContext(combinedArgs);


        int mod = 0;
        for (String arg : args) {
            if (arg.startsWith("+") || arg.startsWith("-")) {
                mod = Integer.parseInt(arg);
            }
        }

        Pattern pattern = Pattern.compile("\\{(.*)}");
        Matcher matcher = pattern.matcher(combinedArgs);
        String desc = null;
        if (matcher.find()) {
            desc = matcher.group(1);
        }

        Weapon weapon = new Weapon(skillName, mod);
        if (skillName.equals("блокирование")) {
            //executor.setShield(weapon);
            return new Message("Боевой навык для щита установлен: " + weapon.toString(), ChatColor.COMBAT);
        }

        weapon.setRangedWeapon(desc);
        executor.addWeapon(weapon);
        return new Message("Боевой навык для атаки установлен: " + weapon.toString(), ChatColor.COMBAT);
    }
}
