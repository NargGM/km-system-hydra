package ru.konungstvo.commands.helpercommands.player.defense;

import de.cas_ual_ty.gci.GunCus;
import de.cas_ual_ty.gci.item.ItemGun;
import de.cas_ual_ty.gci.network.MessageSound;
import javafx.util.Pair;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import noppes.npcs.api.constants.AnimationType;
import noppes.npcs.entity.EntityNPCInterface;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.FudgeDiceMessage;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.*;
import ru.konungstvo.combat.duel.*;
import ru.konungstvo.combat.equipment.Weapon;
import ru.konungstvo.combat.movement.ChargeHistory;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.combat.reactions.Queue;
import ru.konungstvo.commands.executor.DecisionExecutor;
import ru.konungstvo.commands.executor.NextExecutor;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponDurabilityHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.NPC;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;
import ru.konungstvo.player.wounds.WoundType;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PerformDefenseCommand extends CommandBase {
    public static final String NAME = "performdefense";
    private static final Pattern pattern = Pattern.compile("\\[.*?\\]");
    private static final Pattern pattern2 = Pattern.compile("\\{.*?\\}");
    private static final Pattern pattern3 = Pattern.compile("\\<.*?\\>");

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public List<String> getAliases() {
        ArrayList<String> result = new ArrayList();
        result.add("performdefence");
        result.add("perfromdefense");
        return result;
    }


    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        if (player.hasPermission(Permission.GM) && player.getSubordinate() != null) {
            player = player.getSubordinate();
        }

        String skillName = args[0];

        // REMOVE PREVIOUS PANEL
        {
            ClickContainerMessage remove = new ClickContainerMessage("SelectDefense", true);
            KMPacketHandler.INSTANCE.sendTo(remove, (EntityPlayerMP) sender);
            Message info = new Message(String.format(SelectDefenseCommand.END_MESSAGE, skillName), ChatColor.GRAY);
            DataHolder.inst().getPlayer(sender.getName()).sendMessage(info);
        }

        Duel duel = DataHolder.inst().getDuelForDefender(player.getName());

        System.out.println("duel " + player.getName() + " " + (duel != null));
        System.out.println("duel get defense " + (duel.getDefense() != null));
        System.out.println("duel get defense type " + (duel.getDefense().getDefenseType() != null));

        // НИЧЕГО
        if (skillName.equals(DefenseType.NOTHING.toString())) {
            duel.getDefense().setDefenseType(DefenseType.NOTHING);


            // УКЛОНЕНИЕ
        } else if (skillName.equals(DefenseType.EVADE.toString())) {

            duel.getDefense().setDefenseType(DefenseType.EVADE);

            // СТАВИМ ДАЙС
            Skill evadeSkill = player.getEvadeSkill();
            if (evadeSkill == null) { //Если навыка уклонения нет, кидаем от плохо
                FudgeDiceMessage fudgeDiceMessage = new FudgeDiceMessage(player.getName(), "% плохо");
                duel.getDefense().setDiceMessage(fudgeDiceMessage);
            } else {
                SkillDiceMessage skillDiceMessage = new SkillDiceMessage(player.getName(), "% " + evadeSkill.getName());
                duel.getDefense().setDiceMessage(skillDiceMessage);
            }

        } else if (duel.getDefense().getDefenseType() == DefenseType.ANTICOUNTER) {
            // handled elsewhere, basically

        } else if (duel.getDefense().getDefenseType() == DefenseType.PARRY) {
            // handled elsewhere, basically

            // РУКОПАШНЫЙ БОЙ
        } else if (skillName.equals(DefenseType.HAND_TO_HAND.toString())) {
            duel.getDefense().setDefenseType(DefenseType.HAND_TO_HAND);
            // СТАВИМ ДАЙС
            Skill handToHandSkill = player.getHandToHandSkill();
            if (handToHandSkill == null) { //Если навыка рукопашного боя нет, кидаем от плохо
                FudgeDiceMessage fudgeDiceMessage = new FudgeDiceMessage(player.getName(), "% плохо");
                duel.getDefense().setDiceMessage(fudgeDiceMessage);
            } else {
                SkillDiceMessage skillDiceMessage = new SkillDiceMessage(player.getName(), "% " + handToHandSkill.getName());
                duel.getDefense().setDiceMessage(skillDiceMessage);
            }

            // БЛОКИРОВАНИЕ
        } else if (duel.getDefense().getDefenseType() == DefenseType.BLOCKING) {
//            duel.getDefense().setDefenseType(DefenseType.BLOCKING);
            // handled elsewhere, basically
            // СТАВИМ ДАЙС
            /*Skill blockSkill = player.getBlockSkill();
            if (blockSkill == null) { //Если навыка блокирования нет, кидаем от плохо
                FudgeDiceMessage fudgeDiceMessage = new FudgeDiceMessage(player.getName(), "% плохо");
                duel.getDefense().setDiceMessage(fudgeDiceMessage);
            } else {
                SkillDiceMessage skillDiceMessage = new SkillDiceMessage(player.getName(), "% " + blockSkill.getName());
                duel.getDefense().setDiceMessage(skillDiceMessage);
            }*/

            // ГРУППИРОВКА
        } else if (skillName.equals(DefenseType.CONSTITUTION.toString())) {

            duel.getDefense().setDefenseType(DefenseType.CONSTITUTION);
            // СТАВИМ ДАЙС
            Skill groupSkill = player.getGroupSkill();
            if (groupSkill == null) { //Если навыка блокирования нет, кидаем от плохо
                FudgeDiceMessage fudgeDiceMessage = new FudgeDiceMessage(player.getName(), "% плохо");
                duel.getDefense().setDiceMessage(fudgeDiceMessage);
            } else {
                SkillDiceMessage skillDiceMessage = new SkillDiceMessage(player.getName(), "% " + groupSkill.getName());
                duel.getDefense().setDiceMessage(skillDiceMessage);
            }
        } else if (skillName.equals(DefenseType.WILLPOWER.toString())) {

            duel.getDefense().setDefenseType(DefenseType.WILLPOWER);
            // СТАВИМ ДАЙС
            Skill willpowerSkill = player.getWillpowerSkill();
            if (willpowerSkill == null) { //Если навыка блокирования нет, кидаем от плохо
                FudgeDiceMessage fudgeDiceMessage = new FudgeDiceMessage(player.getName(), "% плохо");
                duel.getDefense().setDiceMessage(fudgeDiceMessage);
            } else {
                SkillDiceMessage skillDiceMessage = new SkillDiceMessage(player.getName(), "% " + willpowerSkill.getName());
                duel.getDefense().setDiceMessage(skillDiceMessage);
            }
        }

        // GET ARMOR FROM ITEMS
        player.purgeArmor();
        DataHolder.inst().updatePlayerArmor(player.getName());
//        for (ItemStack armorPiece :
//                ((EntityPlayerMP) sender).getArmorInventoryList()) {
//            WeaponTagsHandler armorHandler = new WeaponTagsHandler(armorPiece);
//            if (armorHandler.isArmor()) {
//                for (NBTTagCompound piece : armorHandler.getAllWeapons()) {
//                    System.out.println("piece : " + piece.toString());
//                    try {
//                        player.addToArmor(piece.getCompoundTag("armor"));
//                    } catch (ParseException e) {
//                        sender.sendMessage(new TextComponentString("Ошибка при добавлении брони! " + e.getMessage()));
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }
        Player attacker = duel.getAttacker();
        Combat combat = DataHolder.inst().getCombatForPlayer(attacker.getName());

        boolean isRanged = false;
        int recoil = 0;
        boolean multishot = duel.hasMultishot();
        boolean multishot2 = duel.hasMultishot2();
        for (Attack currentAttack : duel.getAttacks()) {
            if (currentAttack.getWeapon().isRanged()) {
                isRanged = true;
                if ((duel.getAttacks().size() > 1 && duel.getActiveHand() != 2) || currentAttack.getWeapon().getFirearm().hasSingleRecoil() || multishot) {
                    recoil = currentAttack.getWeapon().getFirearm().getRecoil();
                    if (currentAttack.getWeapon().getFirearm().getWeaponType().equals("штурмовая винтовка") && duel.getAttacks().size() == 1) recoil = Math.min(0, recoil+1);
                    System.out.println("recoil " + recoil);
                }
            }
//            System.out.println("Current attack: " + currentAttack.toString());
            try {
//                System.out.println(player.getName() + "'s thorax:");
//                player.getArmor().getArmorPiece(BodyPart.THORAX);
                duel.performDefense(currentAttack);
                duel.getDefender().getPercentDice().setPreviousDefenses(0);
            } catch (Exception e) {
                player.sendError(e);
                e.printStackTrace();
            }
            if (duel.mustEnd && duel.successfulCounter) {
                //duel.getDefender().setRecoil(recoil);
                attacker.getPercentDice().setBipod(0);
                attacker.getPercentDice().setAmmo(0);
                attacker.getPercentDice().setLimbInjury(0);
                attacker.getPercentDice().setBackStab(0);
                attacker.getPercentDice().setStock(0);
                attacker.getPercentDice().setHighground(0);
                attacker.getPercentDice().setRipost(0);
                duel.getDefender().getPercentDice().setHighground(0);
                attacker.setMovementHistory(MovementHistory.NONE);
                attacker.setChargeHistory(ChargeHistory.NONE);
                attacker.riposted = null;
                attacker.processTurnEnd(attacker, "combat", DataHolder.inst().getCombatForPlayer(attacker.getName()));
                duel.getDefender().processTurnStart(duel.getDefender(), DataHolder.inst().getCombatForPlayer(attacker.getName()));
                DuelMaster.end(duel);
                if (DataHolder.inst().isNpc(attacker.getName())) {
                    Player master = DataHolder.inst().getMasterForNpcInCombat(attacker);
                    master.setSubordinate(attacker);
                    master.performCommand(String.format("/%s %s", PerformDefenseCommand.NAME, DefenseType.ANTICOUNTER));
                } else {
                    attacker.performCommand(String.format("/%s %s", PerformDefenseCommand.NAME, DefenseType.ANTICOUNTER));
                }
                return;
            }
            Player actualDefender = duel.getDefender(); // this is here because of CounterAttacks
            Player actualAttacker = duel.getAttacker();
            /*COUNTER*/
//            if (duel.successfulCounter) {
//                actualDefender = duel.getAttacker();
//                actualAttacker = duel.getDefender();
//            }
            // INFORM PLAYERS
            duel.originalAttack.getDiceMessage().build();
//            System.out.println("trying to send " + duel.originalAttack.getDiceMessage().toString()) ;
            if (!duel.mustBreak) {
                if (duel.successfulCounter) {
                    //ServerProxy.informDistantMasters(actualAttacker, duel.originalAttack.getDiceMessage(), actualAttacker.getDefaultRange().getDistance());

//            System.out.println("trying to send " + duel.getDefense().getDiceMessage().toString());
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(), duel.getOldAttackResult());
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(), duel.getOldDefenseResult());
                    Message counter = new Message("Контратака!", ChatColor.RED);
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(), counter);
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(), duel.originalAttack.getDiceMessage());
                    if (duel.getDefense().getDefenseType() != DefenseType.NOTHING) {
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(),
                                duel.getDefense().getDiceMessage());
                        //ServerProxy.informDistantMasters(actualAttacker, duel.getDefense().getDiceMessage(), actualAttacker.getDefaultRange().getDistance());

                    } else {
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(),
                                new Message(actualDefender.getName() + " не защищается.", ChatColor.COMBAT));
                        //ServerProxy.informDistantMasters(actualAttacker, new Message(actualDefender.getName() + " не защищается.", ChatColor.COMBAT), actualAttacker.getDefaultRange().getDistance());
                    }
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(), duel.getDamageInfo());
                    //ServerProxy.informDistantMasters(actualAttacker, duel.getDamageInfo(), actualAttacker.getDefaultRange().getDistance());

                    System.out.println(duel.getRangeInfo());
                    if (duel.getRangeInfo() != null) {
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(), duel.getRangeInfo());
                        //ServerProxy.informDistantMasters(actualAttacker, duel.getRangeInfo(), actualAttacker.getDefaultRange().getDistance());
                    }
                } else {
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(), duel.originalAttack.getDiceMessage());
                    //ServerProxy.informDistantMasters(actualAttacker, duel.originalAttack.getDiceMessage(), actualAttacker.getDefaultRange().getDistance());

//            System.out.println("trying to send " + duel.getDefense().getDiceMessage().toString());
                    if (duel.getDefense().getDefenseType() != DefenseType.NOTHING) {
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(),
                                duel.getDefense().getDiceMessage());
                        //ServerProxy.informDistantMasters(actualAttacker, duel.getDefense().getDiceMessage(), actualAttacker.getDefaultRange().getDistance());

                    } else {
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(),
                                new Message(actualDefender.getName() + " не защищается.", ChatColor.COMBAT));
                        //ServerProxy.informDistantMasters(actualAttacker, new Message(actualDefender.getName() + " не защищается.", ChatColor.COMBAT), actualAttacker.getDefaultRange().getDistance());
                    }
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(), duel.getDamageInfo());
                    //ServerProxy.informDistantMasters(actualAttacker, duel.getDamageInfo(), actualAttacker.getDefaultRange().getDistance());

                    System.out.println(duel.getRangeInfo());
                    if (duel.getRangeInfo() != null) {
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(), duel.getRangeInfo());
                        //ServerProxy.informDistantMasters(actualAttacker, duel.getRangeInfo(), actualAttacker.getDefaultRange().getDistance());
                    }
                }
            }

            if (duel.shouldBreak()) {
                System.out.println("123 SHOULD BREAK " + duel.shouldBreak() + " MUST BREAK " + duel.mustBreak + " SHOULD NOT BREAK " + duel.shouldNotBreak());
                if (duel.mustBreak) break;
                if (currentAttack.getWeapon().isRanged()) {
                    boolean station = actualAttacker.hasStation() || (actualAttacker.hasBipods() && currentAttack.weapon.getFirearm().isLMG()) || currentAttack.weapon.getFirearm().isLSMG();
                    if (!duel.shouldNotBreak()) {
                        if (
                                (multishot && (!station || duel.getActiveAttackNumber() > 0)) || (currentAttack.weapon.getFirearm() != null && !station)
                        ) {
                            break;
                        }
                    }
                } else {
                    if (!duel.shouldNotBreak()) break;
                }
            }
            duel.incrementAttackNumber();
        }

        Player actualDefender1 = duel.getDefender(); // this is here because of CounterAttacks. And yes, it's copypaste. Sue me.
        Player actualAttacker1 = duel.getAttacker();

        if ((multishot || duel.mustBreak || duel.shouldBreak()) && !(duel.jammed() && duel.getActiveHand() != 2)) {
            System.out.println("attacks: " + duel.getAttacks().size());
            System.out.println("active: " + duel.getActiveAttackNumber());
            int attacksLeft = duel.getAttacks().size() - duel.getActiveAttackNumber() - 1;
            for (int i = 0; i < attacksLeft; i++) {
                System.out.println("i: " + i);
                int attack = duel.getActiveAttackNumber() + i + 1;
                if (multishot && attack > 2) break; //не тратим 5 патрон у пулемёта
                int hand = duel.getActiveHand();
                if (duel.getActiveHand() == 2) hand = 0; //вторая атака в парной всегда левая
                if (!duel.getAttacks().get(attack).weapon.isRanged()) continue;
                int energy = duel.getAttacks().get(attack).weapon.getFirearm().getEnergy();
                if (energy != -666) {
                    actualAttacker1.fireEnergy(hand, energy);
                } else {
                    actualAttacker1.fireProjectile(hand);
                }
                System.out.println("ACTIVE ATTACK " + (duel.getActiveAttackNumber() + i + 1));
                System.out.println(duel.mustBreak + " duel.mustBreak duel.mustBreak duel.mustBreak duel.mustBreak");
                System.out.println((actualAttacker1.getItemForHand(hand).getItem() instanceof ItemGun) + " actualAttacker1.getItemForHand(hand).getItem() instanceof ItemGunactualAttacker1.getItemForHand(hand).getItem() instanceof ItemGunactualAttacker1.getItemForHand(hand).getItem() instanceof ItemGun");
                    if (!duel.mustBreak && actualAttacker1.getItemForHand(hand).getItem() instanceof ItemGun) {
                        System.out.println("TRUE TRUE TRUE TRUE TRUE");
                        if (duel.getAttacks().get(attack).weapon.isRanged() && duel.getAttacks().get(attack).weapon.getFirearm().shouldBoom()) {
                            System.out.println("TRUE TRUE TRUE TRUE TRUE");
                            ItemStack is = actualAttacker1.getItemForHand(hand);
                            ItemGun ig = (ItemGun) is.getItem();
                            ig.doShoot(actualAttacker1.getEntity(), is, false, false, duel.getAttacks().get(attack).weapon.getFirearm().getSort());
                        }
                    } else if (!duel.mustBreak) {
                        if (duel.getAttacks().get(attack).weapon.isRanged() && duel.getAttacks().get(attack).weapon.getFirearm().shouldBoom()) {
                            EntityLivingBase attEnt = actualAttacker1.getEntity();
                            switch (duel.getAttacks().get(attack).weapon.getFirearm().getSort()) {
                                case 3:
                                    GunCus.channel.sendToAllAround(new MessageSound(attEnt, GunCus.SOUND_HIGHCALSHOT, 8F, 1F), new NetworkRegistry.TargetPoint(attEnt.world.provider.getDimension(), attEnt.posX, attEnt.posY, attEnt.posZ, 150F));
                                    break;
                                case 2:
                                    GunCus.channel.sendToAllAround(new MessageSound(attEnt, GunCus.SOUND_MEDCALSHOT, 7F, 1F), new NetworkRegistry.TargetPoint(attEnt.world.provider.getDimension(), attEnt.posX, attEnt.posY, attEnt.posZ, 100F));
                                    break;
                                case 1:
                                    GunCus.channel.sendToAllAround(new MessageSound(attEnt, GunCus.SOUND_SMALLCALSHOT, 6F, 1F), new NetworkRegistry.TargetPoint(attEnt.world.provider.getDimension(), attEnt.posX, attEnt.posY, attEnt.posZ, 80F));
                                    break;
                            }
                        }
                    }
            }
            if (!multishot && attacksLeft > 0 && duel.getActiveAttackNumber() == 0) { //отбираем прочность за второй выстрел, но не за расстрел
                int hand = duel.getActiveHand();
                if(duel.getActiveHand() == 2) hand = 0; //вторая атака в парной всегда левая\
                try {
                    WeaponDurabilityHandler weaponDurabilityHandler = actualAttacker1.getDurHandlerForHand(hand);
                    if (weaponDurabilityHandler.hasDurabilityForTag(duel.getAttacks().get(1).getWeapon().getName()))
                        weaponDurabilityHandler.takeAwayDurabilityForTag(duel.getAttacks().get(1).getWeapon().getName(), 1);
                } catch (Exception ignored) {

                }

            }
        }

        actualAttacker1.getPercentDice().setModules(0);
        actualDefender1.getPercentDice().setModules(0);

        if (duel.hasReachedDifficulty() && duel.getDefense().getDefenseType() != DefenseType.NOTHING && !duel.getDefender().isCollected() && !duel.dontGivePreviousDebuff()) {
            duel.getDefender().setPreviousDefenses(duel.getDefender().getPreviousDefenses() + 1);
        }

        // MANAGE WOUNDS
        boolean alreadyFallen = false;


        List<Pair<Integer, String>> wounds = duel.getWounds();

        if (duel.suppressing) {
            Pair<Integer, String> bestPair = null;
            Integer dam = 0;
            for (Pair<Integer, String> pair : wounds) {
                Integer damage = (Integer) pair.getKey();
                if (damage > dam) {
                    bestPair = pair;
                    dam = damage;
                }
            }
            if (bestPair != null) {
                wounds.removeAll(wounds);
                wounds.add(bestPair);
            }
        }
        boolean dontprogress = false;
        boolean rip = false;
        boolean critCheck = false;
        boolean removeFromCombat = false;
        for (Pair<Integer, String> pair : wounds) {
            Player actualDefender = duel.getDefender(); // this is here because of CounterAttacks. And yes, it's copypaste. Sue me.
            Player actualAttacker = duel.getAttacker();
//            if (duel.successfulCounter) {
//                actualDefender = duel.getAttacker();
//                actualAttacker = duel.getDefender();
//            }
            Integer damage = (Integer) pair.getKey();
            if (damage < 0) continue;
            String description = (String) pair.getValue();

            if (actualDefender.hasConcentration() && damage > 6) {
                description += " <потеря концентрации>";
                actualDefender.removeStatusEffect(StatusType.CONCENTRATED);
            }
            if (actualDefender.hasStatusEffect(StatusType.PREPARING) && damage > 6) {
                description += " <потеря подготовки>";
                actualDefender.removeStatusEffect(StatusType.PREPARING);
            }

            StringBuilder modsStr = new StringBuilder();
            StringBuilder effectsStr = new StringBuilder();
            StringBuilder statusEffectsStr = new StringBuilder();

            boolean forcedDeadly = false;
            if (description.contains("смертельная рана")) forcedDeadly = true;

            if (description.contains("[")) {
                Matcher matcher = pattern.matcher(description);
                while (matcher.find()) {
                    if (modsStr.toString().isEmpty()) modsStr.append("§4На рану наложены модификаторы:\n");
                    modsStr.append("§c").append(matcher.group()).append("\n");
                }
            }
            if (description.contains("{")) {
                Matcher matcher = pattern2.matcher(description);
                while (matcher.find()) {
                    if (effectsStr.toString().isEmpty()) effectsStr.append("§6На атаку воздействовали эффекты:\n");
                    effectsStr.append("§5").append(matcher.group()).append("\n");
                }
                if(description.contains("{иссушение}")) {
                    actualDefender.setBloodloss(actualDefender.getBloodloss() + 5);
                    DataHolder.inst().getModifiers().updateBloodloss(actualDefender.getName(), actualDefender.getBloodloss());
                }
                if(description.contains("{вампиризм}")) {
                    actualAttacker.vampiricHeal(damage, actualDefender.getName());
                }
                if(description.contains("{кража маны}")) {
                    if(DataHolder.inst().useMana(actualDefender.getName(), 5)) DataHolder.inst().addMana(actualAttacker.getName(), 5);
                }
                description = matcher.replaceAll("");
            }
            if (description.contains("<")) {
                Matcher matcher = pattern3.matcher(description);
                while (matcher.find()) {
                    if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                    statusEffectsStr.append("§3").append(matcher.group()).append("\n");
                }
                description = matcher.replaceAll("");
            }
            boolean persisted = false;
            SkillDiceMessage persistentDice = null;
            if (actualDefender.isFragile()) forcedDeadly = true;
            if (damage > 9 || forcedDeadly) {
                if (actualDefender.isPersistent() && !actualDefender.isIrresolute()) {
                    persisted = true;
                } else if (!actualDefender.isPersistent() && actualDefender.isIrresolute()) {
                    persisted = false;
                } else {
                    persistentDice = new SkillDiceMessage(actualDefender.getName(), "% выносливость");
                    persistentDice.build();
                    if ((damage > 12 || forcedDeadly) && persistentDice.getDice().getResult() > 4) {
                        persisted = true;
                    } else if (!forcedDeadly && damage < 13 && persistentDice.getDice().getResult() > 1) {
                        persisted = true;
                    }
                }
            }

            if (damage > 9 && damage < 13 && !forcedDeadly && !actualDefender.getWoundPyramid().hasSevereSlots()) {
                persistentDice = null;
                persisted = false;
            }

            String type = actualDefender.inflictDamage(damage, description, persisted, forcedDeadly);
            System.out.println("Inflicted " + damage + " damage");

            //Отнимаем разбег за раны
            Message fellOver = null;
            if(damage > 3) {
                try {
                    boolean defriding = false; //не отбираем разгон у всадников
                    if (DataHolder.inst().isNpc(actualDefender.getName())) {
                        Entity npc = DataHolder.inst().getNpcEntity(actualDefender.getName());
                        if (npc.getRidingEntity() != null) defriding = true;
                    } else {
                        Entity playerMP = ServerProxy.getForgePlayer(actualDefender.getName());
                        if (playerMP.getRidingEntity() != null) defriding = true;
                    }
                    if (!defriding && actualDefender.getMovementHistory() != MovementHistory.NONE) {
                        if (actualDefender.getMovementHistory() == MovementHistory.RUN_UP || actualDefender.getMovementHistory() == MovementHistory.LAST_TIME) {
                            actualDefender.setMovementHistory(MovementHistory.NONE);
                            //lostRun = new Message(" " + actualDefender.getName() + " теряет разбег.", ChatColor.RED);
                            if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                            statusEffectsStr.append("§3").append("<потеря разбега>").append("\n");
                        } else if (damage > 6 && actualDefender.getMovementHistory() == MovementHistory.NOW) {
                            actualDefender.setMovementHistory(MovementHistory.NONE);
                            if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                            statusEffectsStr.append("§3").append("<потеря разбега>").append("\n");
                            //lostRun = new Message(" " + actualDefender.getName() + " теряет разбег.", ChatColor.RED);
                        }
                    }
                    if (duel.hasCharged() && !alreadyFallen) {
                        boolean attriding = false; //всадник бьет круче.
                        if (DataHolder.inst().isNpc(actualAttacker.getName())) {
                            Entity npc = DataHolder.inst().getNpcEntity(actualAttacker.getName());
                            if (npc.getRidingEntity() != null) attriding = true;
                        } else {
                            Entity playerMP = ServerProxy.getForgePlayer(actualAttacker.getName());
                            if (playerMP.getRidingEntity() != null) attriding = true;
                        }
                        if (attriding || damage > 6) {
                            if (!actualDefender.isLying() && !actualDefender.cantBeCharged()) {
                                actualDefender.makeFall();
                                if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                                statusEffectsStr.append("§3").append("<падение>").append("\n");
                                if (type.equals(WoundType.CRITICAL.getDesc())) {
                                    fellOver = new Message(" Натиск успешен! " + actualDefender.getName() + " сражен!", ChatColor.RED);
                                } else if (type.equals(WoundType.DEADLY.getDesc())) {
                                    fellOver = new Message(" Натиск успешен! " + actualDefender.getName() + " повержен! Ура!", ChatColor.RED);
                                } else {
                                    fellOver = new Message(" Натиск успешен! " + actualDefender.getName() + " падает!", ChatColor.RED);
                                }
                                if (actualDefender.getEngagedWith() != null) {
                                    if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                                    statusEffectsStr.append("§3").append("<потеря связывания боем>").append("\n");
                                    actualDefender.clearOpportunities();
                                }
                                if (actualDefender.hasConcentration()) {
                                    if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                                    statusEffectsStr.append("§3").append("<потеря концентрации>").append("\n");
                                    actualDefender.removeStatusEffect(StatusType.CONCENTRATED);
                                }
                                if (actualDefender.hasStatusEffect(StatusType.PREPARING)) {
                                    if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                                    statusEffectsStr.append("§3").append("<потеря подготовки>").append("\n");
                                    actualDefender.removeStatusEffect(StatusType.PREPARING);
                                }
                            }
                            alreadyFallen = true;
                        }

                    } else if (effectsStr.toString().contains("{сногсшибательная сила}")) {
                        if (damage > 6 && !actualDefender.isLying() && !actualDefender.cantBeCharged()) {
                            actualDefender.makeFall();
                            if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                            statusEffectsStr.append("§3").append("<падение>").append("\n");
                            if (actualDefender.getEngagedWith() != null) {
                                if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                                statusEffectsStr.append("§3").append("<потеря связывания боем>").append("\n");
                                actualDefender.clearOpportunities();
                            }
                            if (actualDefender.hasConcentration()) {
                                if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                                statusEffectsStr.append("§3").append("<потеря концентрации>").append("\n");
                                actualDefender.removeStatusEffect(StatusType.CONCENTRATED);
                            }
                            if (actualDefender.hasStatusEffect(StatusType.PREPARING)) {
                                if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                                statusEffectsStr.append("§3").append("<потеря подготовки>").append("\n");
                                actualDefender.removeStatusEffect(StatusType.PREPARING);
                            }
                            alreadyFallen = true;
                        }
                    } else {
                        if (actualDefender.hasConcentration() && damage > 6) {
                            if (statusEffectsStr.toString().isEmpty())
                                statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                            statusEffectsStr.append("§3").append("<потеря концентрации>").append("\n");
                            actualDefender.removeStatusEffect(StatusType.CONCENTRATED);
                        }
                        if (actualDefender.hasStatusEffect(StatusType.PREPARING) && damage > 6) {
                            if (statusEffectsStr.toString().isEmpty())
                                statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                            statusEffectsStr.append("§3").append("<потеря подготовки>").append("\n");
                            actualDefender.removeStatusEffect(StatusType.PREPARING);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

//            String type = WoundType.getDescFromDamage(damage);
            Message wound = new Message("Игроку " + actualDefender.getName() + " нанесена ", ChatColor.RED);
            MessageComponent typeC = new MessageComponent(type, ChatColor.RED);

            String ccc = (modsStr.toString().isEmpty() ? "" : modsStr.toString()) + (effectsStr.toString().isEmpty() ? "" : (modsStr.toString().isEmpty() ? "" : "\n") + effectsStr.toString()) + (statusEffectsStr.toString().isEmpty() ? "" : (modsStr.toString().isEmpty() || effectsStr.toString().isEmpty() ? "" : "\n") + statusEffectsStr.toString());
            if (!(type.equals(WoundType.SEVERE.getDesc()) || (type.equals(WoundType.CRITICAL.getDesc()) && (damage > 12 || forcedDeadly)))) persisted = false;
            if (!type.equals(WoundType.DEADLY.getDesc())) ccc = ccc.replace("§c[смертельная рана]\n", "");
            if (!type.equals(WoundType.DEADLY.getDesc()) && !type.equals(WoundType.CRITICAL.getDesc())) ccc = ccc.replace("§c[нокаут]\n", "§c[оглушение]\n");
            StringBuilder persString = new StringBuilder();
            System.out.println((persistentDice == null) + (" MEGATEST"));
            if (damage < 13 && !forcedDeadly && type.equals(WoundType.DEADLY.getDesc())) persistentDice = null;
            if (persistentDice != null) {
                System.out.println(persistentDice.toString() + (" TEST"));
                persString.append("§6§nПроверка выносливости:\n");
                if (persisted) {
                    persString.append("§e").append(persistentDice.toString()).append("\n§eСложность была ").append(damage > 12 || forcedDeadly ? "§lпревосходно§e.\n" : "§lнормально§e.\n");
                } else {
                    persString.append("§e").append(persistentDice.toString()).append("\n§eСложность была ").append(damage > 12 || forcedDeadly ? "§lпревосходно§e.\n" : "§lнормально§e.\n")
                            .append("§4Не удалось выстоять перед §c").append(damage > 12 || forcedDeadly ? "смертельной раной" : "критической раной").append("§4!").append(damage > 12 || forcedDeadly ? " §4§lМгновенная смерть." : "");;
                }
            } else if (type.equals(WoundType.DEADLY.getDesc())) {
                persistentDice = new SkillDiceMessage(actualDefender.getName(), "% выносливость");
                persistentDice.build();
                if (persistentDice.getDice().getResult() > 4) {
                    persisted = true;
                    persString.append("§e").append(persistentDice.toString()).append("\n§eСложность была ").append("§lпревосходно§e.\n");
                } else {
                    persString.append("§e").append(persistentDice.toString()).append("\n§eСложность была ").append("§lпревосходно§e.\n")
                            .append("§4Не удалось выстоять перед §c").append("смертельной раной").append("§4!").append(" §4§lМгновенная смерть.");
                }
            }
            if (persisted) {
                if (!persString.toString().isEmpty()) persString.append("\n");
                if (type.equals(WoundType.DEADLY.getDesc())) persString.append("§2§lУдалось выстоять перед §c§l").append("смертельной раной").append("§2§l! §2Рана остается критической и стабилизируется.");
                else if (type.equals(WoundType.CRITICAL.getDesc()))  persString.append("§2§lУдалось выстоять перед §c§l").append(damage > 12 || forcedDeadly ? "смертельной раной" : "критической раной").append("§2§l! §2Рана снижена до §cкритической§2.");
                else persString.append("§2§lУдалось выстоять перед §c§l").append(damage > 12 || forcedDeadly ? "смертельной раной" : "критической раной").append("§2§l! §2Рана снижена до §cтяжелой§2.");
            }

            if (!persString.toString().isEmpty()) {
                if (!ccc.isEmpty()) {
                    ccc += "\n";
                    ccc += persString.toString();
                } else {
                    ccc = persString.toString();
                }
            }
            if(!ccc.isEmpty()) {
                typeC.setHoverText(new TextComponentString(ccc.trim()));
                typeC.setUnderlined(true);
            }

            MessageComponent dot = new MessageComponent(".", ChatColor.RED);
            wound.addComponent(typeC);
            wound.addComponent(dot);
            if(fellOver != null) {
                wound.addComponent(fellOver);
            }
            ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(), wound);
            //ServerProxy.informDistantMasters(actualAttacker, wound, actualAttacker.getDefaultRange().getDistance());
            if (type.equals(WoundType.CRITICAL.getDesc()) || type.equals(WoundType.DEADLY.getDesc())) {
                try {
                    if (DataHolder.inst().isNpc(duel.getDefender().getName())) {
                        EntityNPCInterface npc = (EntityNPCInterface) DataHolder.inst().getNpcEntity(duel.getDefender().getName());
                        npc.wrappedNPC.getAi().setAnimation(AnimationType.SLEEP);
                        String desc = npc.wrappedNPC.getDisplay().getTitle();
                        npc.wrappedNPC.getDisplay().setTitle((desc + " [крит]").trim());
                        npc.wrappedNPC.getDisplay().setHasLivingAnimation(false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (type.equals(WoundType.CRITICAL.getDesc())) critCheck = true;
                if (critCheck && (ccc.contains("оглушение") || ccc.contains("нокаут"))) {
                    critCheck = false;
                    removeFromCombat = true;
                }
                if (type.equals(WoundType.DEADLY.getDesc())) removeFromCombat = true;

            }


        }

        // RIPOSTE
        if(duel.successfulRiposte()) {
            Player actualDefender = duel.getDefender();
            Player actualAttacker = duel.getAttacker();
            if (actualDefender.getCombatState() == CombatState.SHOULD_WAIT && !actualDefender.getModifiersState().isModifier("noriposte")) {
                Message riposte = new Message("Успешный рипост! " + actualAttacker.getName() + " открыт для ответного удара!", ChatColor.BLUE);
                ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, actualDefender, actualAttacker.getDefaultRange(), riposte);
                //ServerProxy.informDistantMasters(attacker, riposte, actualAttacker.getDefaultRange().getDistance());
                try {
                    Queue queue = DataHolder.inst().getCombatForPlayer(actualDefender.getName()).getReactionList().getQueue();
                    queue.shift(actualDefender, actualAttacker.getName(), true);
                    //ServerProxy.sendMessage(actualDefender, "§9Вы можете атаковать " + actualAttacker.getName() + " с +1 от концентрации (не стакается).");
                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }
                actualDefender.riposted = actualAttacker;
            } else {
                Message riposte = new Message("Атака отбита!", ChatColor.BLUE);
                ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, actualDefender, actualAttacker.getDefaultRange(), riposte);
                //ServerProxy.informDistantMasters(attacker, riposte, actualAttacker.getDefaultRange().getDistance());
            }
        }

        if ((!DataHolder.inst().isNpc(duel.getDefender().getName()) || true) && critCheck && !removeFromCombat) {
            DataHolder.inst().getCombatForPlayer(duel.getDefender().getName()).addDecision(duel.getDefender());
            Player commandUser = duel.getDefender();
            if (DataHolder.inst().isNpc(duel.getDefender().getName())) {
                commandUser = DataHolder.inst().getMasterForNpcInCombat(duel.getDefender());
            }
            Message message = new Message("Выстоит ли " + duel.getDefender().getName() + "?", ChatColor.RED);
            ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker1, actualDefender1, actualAttacker1.getDefaultRange(), message);
            commandUser.performCommand("/" + DecisionExecutor.NAME);
            dontprogress = true;
        }

        //actualDefender.makeFall();
        if ((critCheck || removeFromCombat) && !dontprogress) {
            try {
                Player gm = DataHolder.inst().getMasterForPlayerInCombat(actualDefender1);
                gm.performCommand("/queue remove " + actualDefender1.getName());
                gm.performCommand("/combat remove " + actualDefender1.getName());
                rip = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // SET RECOIL
        if (!duel.noRecoil) {
            if (isRanged) {
//            if (attacker.getModifiersState().getModifier(ModifiersState.serial) != null && attacker.getModifiersState().getModifier(ModifiersState.serial) == 1) {
                System.out.println("setting recoil to " + recoil);
                if (!multishot && !multishot2) attacker.setRecoil(recoil);
//            } else {
//                attacker.setRecoil(0);
//            }
            } else {
                attacker.reduceRecoil();
            }
        } else {
            attacker.reduceRecoil();
        }

        attacker.getPercentDice().setBackStab(0);
        attacker.getPercentDice().setBipod(0);
        attacker.getPercentDice().setAmmo(0);
        attacker.getPercentDice().setLimbInjury(0);
        attacker.getPercentDice().setStock(0);
        attacker.getPercentDice().setHighground(0);
        attacker.getPercentDice().setRipost(0);
        actualDefender1.getPercentDice().setHighground(0);
        attacker.setMovementHistory(MovementHistory.NONE);
        attacker.setChargeHistory(ChargeHistory.NONE);
        attacker.riposted = null;
        if (DataHolder.inst().isNpc(duel.getDefender().getName())) {
            NPC npc = (NPC) DataHolder.inst().getPlayer(duel.getDefender().getName());
            npc.setWoundsInDescription();
        }
        if (duel.unsuccessfulCounter) {
            actualDefender1.setCombatState(CombatState.ACTED);
            try {
                int recoil1 = ((Weapon) duel.getDefense().getDefenseItem()).getFirearm().getRecoil();
                if (((Weapon) duel.getDefense().getDefenseItem()).getFirearm().getWeaponType().equals("штурмовая винтовка")) recoil1 = Math.min(0, recoil1+1);
                if (((Weapon) duel.getDefense().getDefenseItem()).getFirearm().hasSingleRecoil()) actualDefender1.setRecoil(recoil1);
                int hand = duel.getDefense().getActiveHand();
                actualDefender1.fireProjectile(duel.getDefense().getActiveHand());
                Weapon weapon = (Weapon) duel.getDefense().getDefenseItem();
                EntityLivingBase forgePlayer = actualDefender1.getEntity();
                WeaponDurabilityHandler weaponDurabilityHandler = new WeaponDurabilityHandler(actualDefender1.getItemForHand(hand));
                if (weaponDurabilityHandler.hasDurabilityForTag(weapon.getName()))
                    weaponDurabilityHandler.takeAwayDurabilityForTag(weapon.getName(), 1);
                ServerProxy.sendMessageFromAndInformMasters(actualDefender1, new Message(player.getName() + " " + "стреляет!", ChatColor.RED));
                if (weapon.getFirearm().shouldBoom()) {
                    if (actualDefender1.getItemForHand(hand).getItem() instanceof ItemGun) {
                        try {
                            ItemStack is = actualDefender1.getItemForHand(hand);
                            ItemGun gun = (ItemGun) is.getItem();
                            //System.out.println(gun.hasOptic(is));

                            gun.doShoot(forgePlayer, is, false, false, weapon.getFirearm().getSort());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        switch (weapon.getFirearm().getSort()) {
                            case 3:
                                GunCus.channel.sendToAllAround(new MessageSound(forgePlayer, GunCus.SOUND_HIGHCALSHOT, 8F, 1F), new NetworkRegistry.TargetPoint(forgePlayer.world.provider.getDimension(), forgePlayer.posX, forgePlayer.posY, forgePlayer.posZ, 150F));
                                break;
                            case 2:
                                GunCus.channel.sendToAllAround(new MessageSound(forgePlayer, GunCus.SOUND_MEDCALSHOT, 7F, 1F), new NetworkRegistry.TargetPoint(forgePlayer.world.provider.getDimension(), forgePlayer.posX, forgePlayer.posY, forgePlayer.posZ, 100F));
                                break;
                            case 1:
                                GunCus.channel.sendToAllAround(new MessageSound(forgePlayer, GunCus.SOUND_SMALLCALSHOT, 6F, 1F), new NetworkRegistry.TargetPoint(forgePlayer.world.provider.getDimension(), forgePlayer.posX, forgePlayer.posY, forgePlayer.posZ, 80F));
                                break;
                        }
                    }
                }
            } catch (Exception ignored) {

            }
        } else if (duel.successfulCounter) {
            try {
                WeaponTagsHandler defWeaponh = new WeaponTagsHandler(duel.getDefender().getItemForHand(duel.getDefense().getActiveHand()));
                Weapon weapon = new Weapon(defWeaponh.getDefaultWeaponName(), defWeaponh.getDefaultWeapon());
                if (weapon.isRanged()) {
                    int recoil1 = weapon.getFirearm().getRecoil();
                    if (((Weapon) duel.getDefense().getDefenseItem()).getFirearm().getWeaponType().equals("штурмовая винтовка")) recoil1 = Math.min(0, recoil1+1);
                    if (weapon.getFirearm().hasSingleRecoil()) actualDefender1.setRecoil(recoil1);
                }
            } catch (Exception exception) {

            }
        }
        boolean splash = duel.hasSplash();
        boolean multicast = duel.getMulticast() > 0;
        boolean isOpportunity = duel.isOpportunity();
        List<Pair<String, Integer>> cooldowns = duel.getCooldowns();
        boolean mustbr = duel.mustBreak;
        DuelMaster.end(duel);

        if (multishot && mustbr) {
            if (DataHolder.inst().hasRealDuelAttacker(attacker.getName())) {
                ArrayList<Duel> attackerDuels = DataHolder.inst().getAllDuelsForAttacker(attacker.getName());
                for (Duel duel1 : attackerDuels) {
                    DuelMaster.end(duel1);
                }
                Message riposte = new Message("Все последующие атаки отменены!", ChatColor.RED);
                ServerProxy.sendMessageFromAndInformMasters(attacker, riposte);
            }
        }

        if (combat != null) {
            System.out.println("Combat state of " + attacker.getName() + " " + attacker.getCombatState());
            if (isOpportunity && !rip) {
                boolean proced = false;
                if (actualDefender1.getEngagedBy().size() > 0) {
                        ArrayList<Player> opp = new ArrayList<Player>(actualDefender1.getEngagedBy());
                        for (Player opponent : opp) {
                            proced = opponent.executeOpportunity();
                            if (proced) break;
                        }
                }
                if (!proced) {
                    if (actualDefender1.getCombatState() == CombatState.SHOULD_ACT) {
                        if (DataHolder.inst().isNpc(actualDefender1.getName())) {
                            if (actualDefender1.hasStatusEffect(StatusType.STUNNED)) {
                                if (!dontprogress) DataHolder.inst().getMasterForNpcInCombat(actualDefender1).performCommand("/" + NextExecutor.NAME + " combat");
                            } else {
                                DataHolder.inst().getMasterForNpcInCombat(actualDefender1).performCommand("/toolpanel");
                            }
                        } else {
                            actualDefender1.performCommand("/toolpanel");
                            if (actualDefender1.hasStatusEffect(StatusType.STUNNED)) {
                                if (!dontprogress) actualDefender1.performCommand("/" + NextExecutor.NAME + " combat");
                            } else {
                                actualDefender1.performCommand("/toolpanel");
                            }
                        }
                    }
                }
            } else if (attacker.getCombatState() == CombatState.SHOULD_ACT && ((!splash && !multicast && !multishot && !multishot2) || !DataHolder.inst().hasRealDuelAttacker(attacker.getName()))) {
                for (Pair<String, Integer> cooldown : cooldowns) {
                    attacker.addStatusEffect("Восстановление", StatusEnd.TURN_END, cooldown.getValue(), StatusType.COOLDOWN, cooldown.getKey(), (attacker.getCombatState() == CombatState.SHOULD_ACT));
                }
                if (multishot || multishot2) attacker.setRecoil(recoil);
                if (!dontprogress && !isOpportunity) DataHolder.inst().getMasterForCombat(combat.getId()).performCommand("/" + NextExecutor.NAME + " combat");
            } else if (attacker.getCombatState() != CombatState.SHOULD_ACT && ((!splash && !multicast && !multishot && !multishot2) || !DataHolder.inst().hasRealDuelAttacker(attacker.getName()))) {
                for (Pair<String, Integer> cooldown : cooldowns) {
                    attacker.addStatusEffect("Восстановление", StatusEnd.TURN_END, cooldown.getValue(), StatusType.COOLDOWN, cooldown.getKey(), (attacker.getCombatState() == CombatState.SHOULD_ACT));
                }
                if (multishot || multishot2) attacker.setRecoil(recoil);
            }
        }
        if (duel.successfulCounter) {
            actualAttacker1.setCombatState(CombatState.ACTED);
            actualAttacker1.processTurnEnd(actualAttacker1, "combat", DataHolder.inst().getCombatForPlayer(actualAttacker1.getName()));
            if (actualDefender1.getCombatState() == CombatState.SHOULD_ACT && !dontprogress) {
                DataHolder.inst().getMasterForPlayerInCombat(actualDefender1).performCommand("/" + NextExecutor.NAME + " combat");
            }
        }
    }


}
