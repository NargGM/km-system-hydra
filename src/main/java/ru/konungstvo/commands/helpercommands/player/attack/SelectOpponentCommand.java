package ru.konungstvo.commands.helpercommands.player.attack;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.SkillType;
import ru.konungstvo.combat.duel.Duel;
import ru.konungstvo.combat.duel.DuelMaster;
import ru.konungstvo.combat.equipment.FirearmDistance;
import ru.konungstvo.combat.equipment.Weapon;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.player.turn.ToRoot;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.helpers.SkillSaverHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.ModifiersState;
import ru.konungstvo.player.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SelectOpponentCommand extends CommandBase {
    public static final String NAME = "selectopponent";
    public static final String START_MESSAGE = "Выберите цель: \n";
    public static final String END_MESSAGE = "Вы атакуете %s с помощью %s.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        //String skillName = String.join(" ", args);

        // GET PLAYER OR NPC
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player attacker = DataHolder.inst().getPlayer(sender.getName());
        EntityLivingBase forgePlayer = getCommandSenderAsPlayer(sender);
        if (attacker.getSubordinate() != null) {
            attacker = attacker.getSubordinate();
            forgePlayer = (EntityLivingBase) DataHolder.inst().getNpcEntity(attacker.getName());
            if (!DataHolder.inst().isNpc(attacker.getName())) {
                forgePlayer = ServerProxy.getForgePlayer(attacker.getName());
            }
        }


        Duel duel = DataHolder.inst().getDuelForAttacker(attacker.getName());

        if (args.length > 0 && args[0].equals("multicast")) {
            ClickContainer.deleteContainer("ChooseSkill", getCommandSenderAsPlayer(sender));
            ClickContainerMessage remove = new ClickContainerMessage("MagicButton", true);
            KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
            int maxtargets = Integer.parseInt(args[1]);
            int radius = -1;
            int chained = -1;
            if (args.length > 2) {
                if (args[2].startsWith("0123радиус=")) radius = Integer.parseInt(args[2].split("=")[1]);
                if (args[2].startsWith("0123цепь=")) chained = Integer.parseInt(args[2].split("=")[1]);
            }
            int n = 2;
            if (chained > -1 || radius > -1) n = 3;


            // CREATE CLICK PANEL
            Message message = new Message("");
            MessageComponent close = new MessageComponent("[Назад] ", ChatColor.GRAY);
            close.setClickCommand("/" + ToRoot.NAME + " SelectOpponent" + " ToolPanel");

            MessageComponent start = new MessageComponent("Выберите цели:\n", ChatColor.COMBAT);
            message.addComponent(start);

            String[] targets = Arrays.copyOfRange(args, n, args.length);
            String targetsUntil = "";
            String targetsString = String.join(" ", targets);
            System.out.println("targ " + targetsString);
            for (String target : targets) {
                MessageComponent oppComponent = new MessageComponent("[" + target + "] ", ChatColor.DARK_BLUE);
                if (radius > 0) {
                    oppComponent.setClickCommand(
                            //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                            String.format("/%s multicast %s %s %s", NAME, maxtargets, "0123радиус=" + radius, targetsString.replaceFirst("(\\s|^)" + target + "(\\s|$)", " ")).replaceAll("\\s+", " ")
                    );
                } else if (chained > 0) {
                    oppComponent.setClickCommand(
                            //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                            String.format("/%s multicast %s %s %s", NAME, maxtargets, "0123цепь=" + chained, String.join(" ", targetsUntil))
                    );
                } else {
                    oppComponent.setClickCommand(
                            //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                            String.format("/%s multicast %s %s", NAME, maxtargets, targetsString.replaceFirst("(\\s|^)" + target + "(\\s|$)", " ")).replaceAll("\\s+", " ")
                    );
                }
                message.addComponent(oppComponent);
                if (targetsUntil.isEmpty()) targetsUntil += target;
                else targetsUntil += (" " + target);
            }
            Player ult = attacker;
            int lrange = 999;
            if (radius > 0 && targets.length > 0) {
                ult = DataHolder.inst().getPlayer(targets[0]);
                lrange = radius;
            } else if (chained > 0 && targets.length > 0) {
                ult = DataHolder.inst().getPlayer(targets[targets.length-1]);
                lrange = chained;
            }

            if (targets.length < maxtargets) {
                List<Player> newtargets = new ArrayList<>();
                newtargets = ServerProxy.getAllFightersInRange(ult, lrange);

                for (Player opponent : newtargets) {
                    if ((" " + targetsString + " ").contains(" " + opponent.getName() + " ")) continue;
                    if (opponent.equals(attacker)) continue;
                    //if (opponent.getName().equals(sender.getName())) continue;
                    MessageComponent oppComponent = new MessageComponent("[" + opponent.getName() + "] ", ChatColor.BLUE);
                    if (radius > 0) {
                        oppComponent.setClickCommand(
                                //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                                String.format("/%s multicast %s %s %s", NAME, maxtargets, "0123радиус=" + radius, (targetsString + " " + opponent.getName()).trim())
                        );
                    } else if (chained > 0) {
                        oppComponent.setClickCommand(
                                //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                                String.format("/%s multicast %s %s %s", NAME, maxtargets, "0123цепь=" + chained, (targetsString + " " + opponent.getName()).trim())
                        );
                    } else {
                        oppComponent.setClickCommand(
                                //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                                String.format("/%s multicast %s %s", NAME, maxtargets, (targetsString + " " + opponent.getName()).trim())
                        );
                    }
                    message.addComponent(oppComponent);

                }
            }

            if (targets.length > 0) {
                MessageComponent ready = new MessageComponent("[Готово] ", ChatColor.RED);
                ready.setClickCommand(
                        String.format("/%s %s", PerformAttackCommand.NAME, "%MULTICAST%:" + targetsString.replaceAll(" ", ":"))
                );
                message.addComponent(ready);
            }

            message.addComponent(close);

            // SEND PACKET TO CLIENT
            Helpers.sendClickContainerToClient(message, "SelectOpponent", getCommandSenderAsPlayer(sender), attacker);
            return;

        }



        double weaponRange = 1;
        // GET WEAPON RANGE
        List<Player> targets = new ArrayList<>();
        int activeHand = duel.getActiveHand();
        if (activeHand == 2) activeHand = 1;
        ItemStack itemStack = forgePlayer.getHeldItem(
                Helpers.getEnumHand(activeHand)
        );
        WeaponTagsHandler weaponTagsHandler = null;
        if (WeaponTagsHandler.hasWeaponTags(itemStack)) {
            weaponTagsHandler = new WeaponTagsHandler(itemStack);
            weaponRange = weaponTagsHandler.getWeaponRange();
            weaponRange += 1.1;
        } else {
            weaponRange = 2;
        }


        if (weaponTagsHandler != null && weaponTagsHandler.isFirearm()) {
            if (DataHolder.inst().getCombatForPlayer(attacker.getName()) != null) {
                FirearmDistance fd = FirearmDistance.getByName(weaponTagsHandler.getDefaultWeapon().getCompoundTag("rangedFirearm").getString("range"));
                if (fd == null) return;
                System.out.println("MAX DISTANCE WHY DONT YOU WORK " + fd.getValueForDifficuly(6));
                targets = ServerProxy.getAllFightersInRange(attacker, fd.getValueForDifficuly(6));
            } else {
                targets = new ArrayList<>(DataHolder.inst().getCombatForPlayer(attacker.getName()).getFighters());
                targets.remove(attacker);
            }
//            targets = ServerProxy.getVisibleTargets(attacker);
        } else {
            targets = ServerProxy.getAllFightersInRange(attacker, weaponRange);
        }


        if (targets.isEmpty()) {
            TextComponentString error;
            if (weaponTagsHandler != null && weaponTagsHandler.isFirearm()) {
                error = new TextComponentString("Ошибка! Нет врагов в поле зрения.");
            } else {
                error = new TextComponentString("Ошибка! В радиусе дальности вашего оружия (<" + (weaponRange) + ") нет врагов.");

            }
            error.getStyle().setColor(TextFormatting.RED);
            sender.sendMessage(error);
            player.performCommand(ToRoot.NAME + " ToolPanel");
            DuelMaster.end(duel);
            return;
        }


        // CREATE CLICK PANEL
        Message message = new Message("");
        MessageComponent close = new MessageComponent("[Назад] ", ChatColor.GRAY);
        //close.setBold(true);
        close.setClickCommand("/" + ToRoot.NAME + " SelectOpponent" + " ToolPanel");
        MessageComponent start = new MessageComponent(START_MESSAGE, ChatColor.COMBAT);
        message.addComponent(start);

        if (weaponTagsHandler != null && weaponTagsHandler.isFirearm() && weaponTagsHandler.getDefaultWeapon().hasKey("rangedFirearm")) {
            Weapon weapon = new Weapon(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon(), weaponTagsHandler);
            if (weapon.getFirearm().isShotgun() && !weapon.getFirearm().isPlasma() && weapon.getFirearm().getLoadedProjectile() != null && ((weapon.getFirearm().getLoadedProjectile().getCaliber() != null && (weapon.getFirearm().getLoadedProjectile().getCaliber().endsWith("рд") || weapon.getFirearm().getLoadedProjectile().getCaliber().equals("энергодробь"))) || !weapon.isLoaded())) {
                ArrayList<String> multitargets = new ArrayList<>();
                StringBuilder multitargetsstr = new StringBuilder();
                FirearmDistance fd = FirearmDistance.getByName(weapon.getFirearm().getRangeType());
                if (fd != null) {
                    int maxrange = fd.getValueForDifficuly(-1);
                    List<Player> ntargets = ServerProxy.getAllFightersInRange(attacker, maxrange);
                    for (Player opponent : ntargets) {
                        EntityLivingBase d;
                        if (DataHolder.inst().isNpc(opponent.getName())) {
                            d = DataHolder.inst().getNpcEntity(opponent.getName());
                        } else {
                            d = ServerProxy.getForgePlayer(opponent.getName());
                        }
                        try {
                            Vec3d blockvectord = new Vec3d(Math.floor(d.posX) + 0.5, d.posY, Math.floor(d.posZ) + 0.5);
                            Vec3d blockvectorplayer = new Vec3d(Math.floor(forgePlayer.posX) + 0.5, forgePlayer.posY, Math.floor(forgePlayer.posZ) + 0.5);
                            Vec3d vector = blockvectord.subtract(blockvectorplayer);
                            Vec3d playerDirection = forgePlayer.getLookVec();
                            double angleDir = (Math.atan2(vector.z, vector.x) / 2 / Math.PI * 360 + 360) % 360;
                            double angleLook = (Math.atan2(playerDirection.z, playerDirection.x) / 2 / Math.PI * 360 + 360) % 360;
                            double angle = (angleDir - angleLook + 360) % 360;
                            //System.out.println("angle: " + angle);


                            System.out.println(d.getName());
                            double angleX = forgePlayer.rotationPitch;
                            System.out.println("angleX: " + angleX);
                            double c = ServerProxy.getRoundedDistanceBetweenButCountEyeHeight(forgePlayer, d);
                            System.out.println("distance: " + c);
                            double a = (forgePlayer.posY + forgePlayer.getEyeHeight()) - (d.posY + d.getEyeHeight());
                            System.out.println("Ydiff: " + a);
                            double angle2 = Math.sin(a/c) * 100;
                            System.out.println("fposY: " + forgePlayer.posY);
                            System.out.println("dposY: " + d.posY);
                            System.out.println("angle2: " + angle2);

                            System.out.println("eye height: " + forgePlayer.getEyeHeight() + " " + d.getEyeHeight());

                            if (Math.abs(angle2 - angleX) > 30) continue;
                            if ((angle <= 15 || angle >= 345)) { //40 и 20
                                multitargets.add(opponent.getName());
                                multitargetsstr.append(opponent.getName()).append(":");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    MessageComponent refresh = new MessageComponent("[Обновить] ", ChatColor.BLUE);
                    //refresh.setHoverText("splashtargets.toString()", TextFormatting.BLUE);
                    refresh.setClickCommand("/" + NAME + " " + String.join(" ", Arrays.copyOfRange(args, 0, args.length)));
                    message.addComponent(refresh);

                    if (multitargets.size() > 1) {
                        MessageComponent splash;
                        splash = new MessageComponent("[Разнос] ", ChatColor.RED);
                        splash.setHoverText(multitargets.toString(), TextFormatting.BLUE);
                        splash.setClickCommand(String.format("/%s %s", PerformAttackCommand.NAME, "%MULTISHOT3%:" + multitargetsstr.toString()));
                        message.addComponent(splash);
                    }
                }
            }
        }

        if (weaponTagsHandler != null && weaponTagsHandler.isFirearm() && weaponTagsHandler.getDefaultWeapon().hasKey("rangedFirearm") && weaponTagsHandler.getDefaultWeapon().getCompoundTag("rangedFirearm").hasKey("serial") && weaponTagsHandler.getDefaultWeapon().getCompoundTag("rangedFirearm").getBoolean("serial") &&
                !weaponTagsHandler.getDefaultWeapon().getCompoundTag("rangedFirearm").hasKey("double") && attacker.getModifiersState().isModifier("serial") && !weaponTagsHandler.getDefaultWeapon().getCompoundTag("rangedFirearm").getString("type").equals("штурмовая винтовка")) {
            ArrayList<String> multitargets = new ArrayList<>();
            StringBuilder multitargetsstr = new StringBuilder();
            FirearmDistance fd = FirearmDistance.getByName(weaponTagsHandler.getDefaultWeapon().getCompoundTag("rangedFirearm").getString("range"));

            if (fd != null) {
                String type = weaponTagsHandler.getDefaultWeapon().getCompoundTag("rangedFirearm").getString("type");
                boolean machinegun = type.equals("пулемёт");
                boolean pistol = type.equals("пистолет") || type.equals("автоматический пистолет") || type.equals("револьвер");
                int maxrange = fd.getValueForDifficuly(1);
                if (machinegun) maxrange = fd.getValueForDifficuly(2);
                if (pistol) maxrange = fd.getValueForDifficuly(0);
                List<Player> ntargets = ServerProxy.getAllFightersInRange(attacker, maxrange);
                for (Player opponent : ntargets) {
                    EntityLivingBase d;
                    if (DataHolder.inst().isNpc(opponent.getName())) {
                        d = DataHolder.inst().getNpcEntity(opponent.getName());
                    } else {
                        d = ServerProxy.getForgePlayer(opponent.getName());
                    }
                    try {
                        Vec3d blockvectord = new Vec3d(Math.floor(d.posX) + 0.5, d.posY, Math.floor(d.posZ) + 0.5);
                        Vec3d blockvectorplayer = new Vec3d(Math.floor(forgePlayer.posX) + 0.5, forgePlayer.posY, Math.floor(forgePlayer.posZ) + 0.5);
                        Vec3d vector = blockvectord.subtract(blockvectorplayer);
                        Vec3d playerDirection = forgePlayer.getLookVec();
                        double angleDir = (Math.atan2(vector.z, vector.x) / 2 / Math.PI * 360 + 360) % 360;
                        double angleLook = (Math.atan2(playerDirection.z, playerDirection.x) / 2 / Math.PI * 360 + 360) % 360;
                        double angle = (angleDir - angleLook + 360) % 360;
                        //System.out.println("angle: " + angle);


                        System.out.println(d.getName());
                        double angleX = forgePlayer.rotationPitch;
                        System.out.println("angleX: " + angleX);
                        double c = ServerProxy.getRoundedDistanceBetweenButCountEyeHeight(forgePlayer, d);
                        System.out.println("distance: " + c);
                        double a = (forgePlayer.posY + forgePlayer.getEyeHeight()) - (d.posY + d.getEyeHeight());
                        System.out.println("Ydiff: " + a);
                        double angle2 = Math.sin(a/c) * 100;
                        System.out.println("fposY: " + forgePlayer.posY);
                        System.out.println("dposY: " + d.posY);
                        System.out.println("angle2: " + angle2);

                        System.out.println("eye height: " + forgePlayer.getEyeHeight() + " " + d.getEyeHeight());

                        if (Math.abs(angle2 - angleX) > 30) continue;
                        if ((!machinegun && !pistol && (angle <= 25 || angle >= 335)) || (angle <= 15 || angle >= 345)) { //50 и 30
                            multitargets.add(opponent.getName());
                            multitargetsstr.append(opponent.getName()).append(":");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                MessageComponent refresh = new MessageComponent("[Обновить] ", ChatColor.BLUE);
                //refresh.setHoverText("splashtargets.toString()", TextFormatting.BLUE);
                refresh.setClickCommand("/" + NAME + " " + String.join(" ", Arrays.copyOfRange(args, 0, args.length)));
                message.addComponent(refresh);

                if (multitargets.size() > 1) {
                    MessageComponent splash;
                    splash = new MessageComponent("[Расстрел] ", ChatColor.RED);
                    splash.setHoverText(multitargets.toString(), TextFormatting.BLUE);
                    splash.setClickCommand(String.format("/%s %s", PerformAttackCommand.NAME, "%MULTISHOT%:" + multitargetsstr.toString()));
                    message.addComponent(splash);
                }
            }
        }

        if (weaponTagsHandler != null && !weaponTagsHandler.isFirearm() && weaponTagsHandler.getDefaultWeapon().hasKey("melee")
                && weaponTagsHandler.getDefaultWeapon().getCompoundTag("melee").getTagList("factors", 8).toString().contains("двуручное")) {
            ArrayList<String> splashtargets = new ArrayList<>();
            StringBuilder splashstring = new StringBuilder();
            List<Player> ntargets = new ArrayList<>();

            if (!weaponTagsHandler.getDefaultWeapon().getCompoundTag("melee").getString("category").equals("ударно-рассекающее") && !weaponTagsHandler.getDefaultWeapon().getCompoundTag("melee").getString("category").equals("колющее"))
                ntargets = ServerProxy.getAllFightersInRange(attacker, weaponRange - 1);

            for (Player opponent : ntargets) {
                EntityLivingBase d;
                if (DataHolder.inst().isNpc(opponent.getName())) {
                    d = DataHolder.inst().getNpcEntity(opponent.getName());
                } else {
                    d = ServerProxy.getForgePlayer(opponent.getName());
                }
                try {
                    Vec3d blockvectord = new Vec3d(Math.floor(d.posX) + 0.5, d.posY, Math.floor(d.posZ) + 0.5);
                    Vec3d blockvectorplayer = new Vec3d(Math.floor(forgePlayer.posX) + 0.5, forgePlayer.posY, Math.floor(forgePlayer.posZ) + 0.5);
                    Vec3d vector = blockvectord.subtract(blockvectorplayer);
                    Vec3d playerDirection = forgePlayer.getLookVec();
                    double angleDir = (Math.atan2(vector.z, vector.x) / 2 / Math.PI * 360 + 360) % 360;
                    double angleLook = (Math.atan2(playerDirection.z, playerDirection.x) / 2 / Math.PI * 360 + 360) % 360;
                    double angle = (angleDir - angleLook + 360) % 360;

                    System.out.println(d.getName());
                    double angleX = forgePlayer.rotationPitch;
                    System.out.println("angleX: " + angleX);
                    double c = ServerProxy.getRoundedDistanceBetweenButCountEyeHeight(forgePlayer, d);
                    System.out.println("distance: " + c);
                    double a = (forgePlayer.posY + forgePlayer.getEyeHeight()) - (d.posY + d.getEyeHeight());
                    System.out.println("Ydiff: " + a);
                    double angle2 = Math.sin(a/c) * 100;
                    System.out.println("fposY: " + forgePlayer.posY);
                    System.out.println("dposY: " + d.posY);
                    System.out.println("angle2: " + angle2);

                    System.out.println("eye height: " + forgePlayer.getEyeHeight() + " " + d.getEyeHeight());


                    int maxangle2 = (weaponTagsHandler.getDefaultWeapon().getCompoundTag("melee").getString("category").equals("колющее") ? 30 : 45);
                    if (Math.abs(angle2 - angleX) > maxangle2) continue;
                    if ((!weaponTagsHandler.getDefaultWeapon().getCompoundTag("melee").getString("category").equals("колющее") && (angle <= 67.5 || angle >= 292.5)) || (angle <= 10 || angle >= 350)) {
                        splashtargets.add(opponent.getName() + (weaponRange > 3 && ServerProxy.getDistanceBetween(attacker, opponent) <= 1.5 ? " -1" : ""));
                        splashstring.append(opponent.getName()).append(":");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            MessageComponent refresh = new MessageComponent("[Обновить] ", ChatColor.BLUE);
            //refresh.setHoverText("splashtargets.toString()", TextFormatting.BLUE);
            refresh.setClickCommand("/" + NAME + " " + String.join(" ", Arrays.copyOfRange(args, 0, args.length)));
            message.addComponent(refresh);

            if (splashtargets.size() > 1) {
                MessageComponent splash;
                if (weaponTagsHandler.getDefaultWeapon().getCompoundTag("melee").getString("category").equals("колющее")) {
                    splash = new MessageComponent("[Пронзание] ", ChatColor.RED);
                } else {
                    splash = new MessageComponent("[Размах] ", ChatColor.RED);
                }
                splash.setHoverText(splashtargets.toString(), TextFormatting.BLUE);
                splash.setClickCommand(String.format("/%s %s", PerformAttackCommand.NAME, "%SPLASH%:" + splashstring.toString()));
                message.addComponent(splash);
            }
        }


        for (Player opponent : targets) {
            if (opponent.getName().equals(sender.getName())) continue;
            boolean meh = false;
            boolean multishot2 = false;
            MessageComponent oppComponent;
            System.out.println("TEST " + (weaponRange > 3) + " " + (ServerProxy.getDistanceBetween(attacker, opponent)));
            if (weaponTagsHandler.isPlasmaFirearm()) {
                oppComponent = new MessageComponent("[" + opponent.getName() + "] ", ChatColor.BLUE);
                List<Player> splashtargets = ServerProxy.getAllFightersInRange(opponent, 2);
                splashtargets.remove(attacker);
                if (splashtargets.size() > 0) {
                    oppComponent = new MessageComponent("[" + opponent.getName() + "] ", ChatColor.DARK_GREEN);
                    StringBuilder splashstring = new StringBuilder();
                    ArrayList<String> splashstringlist = new ArrayList<>();
                    splashstring.append(opponent.getName());
                    splashstring.append(":");
                    splashstringlist.add(opponent.getName());
                    for (Player opp : splashtargets) {
                        splashstring.append(opp.getName());
                        splashstring.append(":");
                        splashstringlist.add(opp.getName());
                    }
                    oppComponent.setHoverText("Всплеск плазмы заденет " + splashstringlist.toString() + "!", TextFormatting.DARK_GREEN);
                    multishot2 = true;



                    oppComponent.setClickCommand(String.format("/%s %s", PerformAttackCommand.NAME, "%MULTISHOT2%:" + splashstring.toString()));
                }
            } else if (weaponRange > 3 && ServerProxy.getDistanceBetween(attacker, opponent) <= 1.5) {
                oppComponent = new MessageComponent("[" + opponent.getName() + "] ", ChatColor.GRAY);
                if (attacker.riposted == opponent) {
                    oppComponent.setHoverText("Рипост доступен!\nБонус выставится автоматически.\nНе стакается с концентрацией.\nПротивник находится слишком близко, при атаке оружием такой длины будет штраф.", TextFormatting.DARK_RED);
                } else {
                    oppComponent.setHoverText("Противник находится слишком близко, при атаке оружием такой длины будет штраф.", TextFormatting.DARK_RED);
                }
                oppComponent.setUnderlined(true);
            } else {
                oppComponent = new MessageComponent("[" + opponent.getName() + "] ", ChatColor.BLUE);
            }
            if (!multishot2) oppComponent.setClickCommand(
//                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                    String.format("/%s %s", PerformAttackCommand.NAME, opponent.getName())
            );
            if (attacker.riposted == opponent && !meh && !multishot2) {
                oppComponent.setColor(ChatColor.RED);
                oppComponent.setHoverText("Рипост доступен!\nБонус выставится автоматически.\nНе стакается с концентрацией.", TextFormatting.DARK_RED);
            }
            message.addComponent(oppComponent);

        }





        message.addComponent(close);

        // SEND PACKET TO CLIENT
        Helpers.sendClickContainerToClient(message, "SelectOpponent", getCommandSenderAsPlayer(sender), attacker);

    }
}
