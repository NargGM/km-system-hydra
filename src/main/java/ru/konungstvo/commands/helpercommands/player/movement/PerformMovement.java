package ru.konungstvo.commands.helpercommands.player.movement;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.combat.movement.MovementTrait;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.player.turn.ToRoot;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.player.Player;

public class PerformMovement extends CommandBase {
    public static final String NAME = "performmovement";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;

    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        ClickContainer.deleteContainer("ToolPanel", getCommandSenderAsPlayer(sender));

        // GET PLAYER OR NPC
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player defender = DataHolder.inst().getPlayer(sender.getName());
        //EntityLivingBase forgePlayer = getCommandSenderAsPlayer(sender);
        if (player.getSubordinate() != null) {
            defender = player.getSubordinate();
            //forgePlayer = (EntityLivingBase) DataHolder.getInstance().getNpcEntity(defender.getName());
        }

        String startStr = "start";
//        if (defender.getMovementTrait().equals(MovementTrait.FAST)) {
//            startStr = "run";
//        } else if (defender.getMovementTrait().equals(MovementTrait.SUPERFAST)) {
//            startStr = "fast";
//        }

        String runStr = "run";
//        if (defender.getMovementTrait().equals(MovementTrait.FAST)) {
//            runStr = "fast";
//        } else if (defender.getMovementTrait().equals(MovementTrait.SUPERFAST)) {
//            runStr = "superfast";
//        }

        Message panel = new Message("Выберите тип передвижения:\n");
        panel.dim();

        if (!player.hasAimedMode() && !player.hasBipods() && !player.hasDefensiveStance() && !player.hasStation()) {
            MessageComponent crawl = new MessageComponent("[Ползком] ");
            crawl.setColor(ChatColor.BLUE);
            crawl.setClickCommand("/" + MoveExecutor.NAME + " crawl");
            panel.addComponent(crawl);

            MessageComponent crouch = new MessageComponent("[Гуськом] ");
            crouch.setColor(ChatColor.BLUE);
            crouch.setClickCommand("/" + MoveExecutor.NAME + " crouch");
            panel.addComponent(crouch);
        }

        if (!player.hasBipods() && !player.hasStation()) {
            MessageComponent free = new MessageComponent("[Свободное] ");
            free.setColor(ChatColor.BLUE);
            free.setClickCommand("/" + MoveExecutor.NAME + " free");
            panel.addComponent(free);
        }

        if (!player.hasBipods() && !player.hasAimedMode() && !player.hasStation()) {
            MessageComponent walk = new MessageComponent("[Шаг] ");
            walk.setColor(ChatColor.BLUE);
            walk.setClickCommand("/" + MoveExecutor.NAME + " walk");
            panel.addComponent(walk);
        }

        if (!player.hasAimedMode() && !player.hasBipods() && !player.hasDefensiveStance() && !player.hasStation()) {
            MessageComponent start = new MessageComponent("[Разбег] ");
            start.setColor(ChatColor.BLUE);
            start.setClickCommand("/" + MoveExecutor.NAME + " " + startStr);


            panel.addComponent(start);
            if (defender.getMovementHistory() == MovementHistory.RUN_UP || defender.getMovementHistory() == MovementHistory.NOW || defender.getMovementHistory() == MovementHistory.LAST_TIME) {
                MessageComponent run = new MessageComponent("[Бег] ");
                run.setColor(ChatColor.BLUE);
                run.setClickCommand("/" + MoveExecutor.NAME + " " + runStr);
                panel.addComponent(run);
            }
        }
        if (player.hasDefensiveStance()) {
            MessageComponent stopdef = new MessageComponent("[Прервать глухую оборону] ", ChatColor.RED);
            stopdef.setClickCommand("/toolpanel stopdef");
            panel.addComponent(stopdef);
        }

        if (player.hasAimedMode()) {
            MessageComponent stopdef = new MessageComponent("[Прекратить прицеливание] ", ChatColor.RED);
            stopdef.setClickCommand("/toolpanel stopaim");
            panel.addComponent(stopdef);
        }

        if (player.hasBipods()) {
            MessageComponent stopdef = new MessageComponent("[Убрать сошки] ", ChatColor.RED);
            stopdef.setClickCommand("/toolpanel stopbipod");
            panel.addComponent(stopdef);
        }

        if (player.hasStation()) {
            MessageComponent stopdef = new MessageComponent("[Бросить станок] ", ChatColor.RED);
            stopdef.setClickCommand("/toolpanel stopstation");
            panel.addComponent(stopdef);
        }

        MessageComponent modifiers = new MessageComponent("[Мод] ", ChatColor.GRAY);
        modifiers.setClickCommand("/modifiersbutton MovementPanel");
        panel.addComponent(modifiers);

        MessageComponent toRoot = new MessageComponent("[Назад]", ChatColor.GRAY);
        //toRoot.setBold(true);
        toRoot.setClickCommand("/" + ToRoot.NAME + " MovementPanel" + " ToolPanel");
        panel.addComponent(toRoot);



        Helpers.sendClickContainerToClient(panel, "MovementPanel", getCommandSenderAsPlayer(sender), defender);

    }

}
