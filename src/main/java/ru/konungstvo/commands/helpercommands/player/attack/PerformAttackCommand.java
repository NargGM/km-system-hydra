package ru.konungstvo.commands.helpercommands.player.attack;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.combat.duel.Duel;
import ru.konungstvo.combat.duel.DuelMaster;
import ru.konungstvo.combat.equipment.BodyPart;
import ru.konungstvo.combat.equipment.Projectile;
import ru.konungstvo.commands.executor.NextExecutor;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.player.turn.ToRoot;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Player;

import javax.management.remote.rmi._RMIConnection_Stub;
import java.util.ArrayList;
import java.util.List;

public class PerformAttackCommand extends CommandBase {
    public static final String NAME = "performattack";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }
    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        String opponentName = args[0];

        // GET PLAYER OR NPC
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player attacker = DataHolder.inst().getPlayer(sender.getName());
        if (attacker.getSubordinate() != null) {
            attacker = attacker.getSubordinate();
        }

        Duel duel = DataHolder.inst().getDuelForAttacker(attacker.getName());


//        //DOUBLE CHECK
//        double weaponRange = 1;
//        // GET WEAPON RANGE
//        List<Player> targets = new ArrayList<>();
//        int activeHand = duel.getActiveHand();
//        if (activeHand == 2) activeHand = 1;
//        EntityLivingBase forgePlayer = attacker.getEntity();
//        ItemStack itemStack = forgePlayer.getHeldItem(
//                Helpers.getEnumHand(activeHand)
//        );
//
//        WeaponTagsHandler weaponTagsHandler = null;
//        if (WeaponTagsHandler.hasWeaponTags(itemStack)) {
//            weaponTagsHandler = new WeaponTagsHandler(itemStack);
//            weaponRange = weaponTagsHandler.getWeaponRange();
//            weaponRange++;
//        } else {
//            weaponRange = 2;
//        }
//
//
//        if (weaponTagsHandler != null && weaponTagsHandler.isFirearm()) {
//            targets = new ArrayList<>(DataHolder.inst().getCombatForPlayer(attacker.getName()).getFighters());
////            targets = ServerProxy.getVisibleTargets(attacker);
//        } else {
//            targets = ServerProxy.getAllFightersInRange(attacker, weaponRange);
//        }
//
//        if(!targets.contains(DataHolder.inst().getPlayer(opponentName))) {
//            TextComponentString error;
//            if (weaponTagsHandler != null && weaponTagsHandler.isFirearm()) {
//                error = new TextComponentString("Ошибка! " + opponentName + " нет в поле вашего зрения!");
//            } else {
//                error = new TextComponentString("Ошибка! В радиусе дальности вашего оружия (<" + weaponRange + ") нет " + opponentName + ".");
//
//            }
//            error.getStyle().setColor(TextFormatting.RED);
//            sender.sendMessage(error);
//            player.performCommand(ToRoot.NAME + " ToolPanel");
//            DuelMaster.end(duel);
//            return;
//        }
//
//        //


        if (opponentName.contains("%SPLASH%")) {
            if (!player.tryShootMelee(duel.getActiveHand())) {
                ClickContainer.deleteContainer("SelectOpponent", getCommandSenderAsPlayer(sender));
                player.performCommand("/" + NextExecutor.NAME);
                return;
            }
            String[] opponents = opponentName.split(":");
            for (String opponent : opponents) {
                System.out.println(opponentName);
                System.out.println(opponent);
                if (opponent.equals("%SPLASH%") || opponent.isEmpty()) continue;
                Duel newDuel = duel.copy();
                DataHolder.inst().registerDuel(newDuel);
                newDuel.setSplash(true);
                newDuel.setAlreadyShot(true);
                newDuel.setDefender(DataHolder.inst().getPlayer(opponent));
                newDuel.initDefense();
            }
            DuelMaster.end(duel);
        } else if (opponentName.contains("%MULTICAST%")) {
            System.out.println("MULTIREEE");
            System.out.println("MULTIREEE" + duel != null);
            String[] opponents = opponentName.split(":");
            int counter = 0;
            for (String opponent : opponents) {
                if (opponent.equals("%MULTICAST%") || opponent.isEmpty()) continue;
                counter++;
                System.out.println(opponentName);
                System.out.println(opponent);
                Duel newDuel = duel.copy();
                DataHolder.inst().registerDuel(newDuel);
                newDuel.setMulticast(counter);
                newDuel.setDefender(DataHolder.inst().getPlayer(opponent));
                newDuel.initDefense();
            }
        } else if (opponentName.contains("%MULTISHOT%")) {
            System.out.println("MULTIREEE");
            System.out.println("MULTIREEE" + duel != null);
            String[] opponents = opponentName.split(":");
            for (String opponent : opponents) {
                System.out.println(opponentName);
                System.out.println(opponent);
                if (opponent.equals("%MULTISHOT%") || opponent.isEmpty()) continue;
                Duel newDuel = duel.copy();
                DataHolder.inst().registerDuel(newDuel);
                newDuel.setMultishot(true);
                newDuel.setDefender(DataHolder.inst().getPlayer(opponent));
                newDuel.initDefense();
            }
        } else if (opponentName.contains("%MULTISHOT2%")) {
            if (!player.tryShoot(duel.getActiveHand(), duel.getActiveAttack().weapon.getFirearm().getEnergy())) {
                ClickContainer.deleteContainer("SelectOpponent", getCommandSenderAsPlayer(sender));
                player.performCommand("/" + NextExecutor.NAME);
                return;
            }
            System.out.println("MULTIREEE");
            System.out.println("MULTIREEE" + duel != null);
            String[] opponents = opponentName.split(":");
            for (String opponent : opponents) {
                System.out.println(opponentName);
                System.out.println(opponent);
                if (opponent.equals("%MULTISHOT2%") || opponent.isEmpty()) continue;
                Duel newDuel = duel.copy();
                DataHolder.inst().registerDuel(newDuel);
                newDuel.setMultishot2(true);
                newDuel.setAlreadyShot(true);
                newDuel.setDefender(DataHolder.inst().getPlayer(opponent));
                newDuel.initDefense();
            }
        } else if (opponentName.contains("%MULTISHOT3%")) {
            Projectile projectile = player.tryShoot3(duel.getActiveHand());
            if (projectile == null) {
                ClickContainer.deleteContainer("SelectOpponent", getCommandSenderAsPlayer(sender));
                player.performCommand("/" + NextExecutor.NAME);
                return;
            }
            System.out.println("MULTIREEE");
            System.out.println("MULTIREEE" + duel != null);
            String[] opponents = opponentName.split(":");
            for (String opponent : opponents) {
                System.out.println(opponentName);
                System.out.println(opponent);
                if (opponent.equals("%MULTISHOT3%") || opponent.isEmpty()) continue;
                Duel newDuel = duel.copy();
                DataHolder.inst().registerDuel(newDuel);
                newDuel.setMultishot2(true);
                newDuel.setMultishot3(true);
                newDuel.setForcedProjectile(projectile);
                newDuel.setAlreadyShot(true);
                newDuel.setDefender(DataHolder.inst().getPlayer(opponent));
                newDuel.initDefense();
            }
        } else {
            System.out.println(DataHolder.inst().getPlayer(opponentName));
            duel.setDefender(DataHolder.inst().getPlayer(opponentName));
//        System.out.println(opponentName + "'s thorax:");
//        DataHolder.inst().getPlayer(opponentName).getArmor().getArmorPiece(BodyPart.THORAX);
//        ClickContainer.deleteContainer("Modifiers", getCommandSenderAsPlayer(sender));
            duel.initDefense();

        }

        // REMOVE PREVIOUS PANEL
        ClickContainer.deleteContainer("SelectOpponent", getCommandSenderAsPlayer(sender));


    }
}
