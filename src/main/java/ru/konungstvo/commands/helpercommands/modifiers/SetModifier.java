package ru.konungstvo.commands.helpercommands.modifiers;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.control.network.ModifierMessage;
import ru.konungstvo.player.ModifiersState;
import ru.konungstvo.player.Player;

public class SetModifier extends CommandBase {
    public static final String NAME = "setmodifier";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        String modifierType = args[0];
        int modifier = Integer.parseInt(args[1]);
        String toRoot = "";
        String oldContainer = "";
        if (args.length > 2) toRoot = args[2];
        if (args.length > 3) oldContainer = args[3];
//        System.out.println(NAME + " is being executed. Type: " + modifierType + ", mod: " + modifier);

        Player player = DataHolder.inst().getPlayer(sender.getName());
        if (player.getSubordinate() != null) {
            player = player.getSubordinate();
        }
        player.getModifiersState().setModifier(modifierType, modifier);
//        System.out.println("modifiersState for " + player.getName() + ": " + player.getModifiersState());

        /*
        Duel duel = DataHolder.getInstance().getDuelForPlayer(sender.getName());
        if (duel != null) {
            duel.setCustomModifierFor(sender.getName(), modifierType, modifier);
        }
         */

        // REMOVE PREVIOUS PANEL
        ClickContainerMessage remove = new ClickContainerMessage("ChooseModifier", true);
        KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));

        // SEND PACKET TO CLIENT
        boolean modifiedDamage = false;
        boolean bodyPart = false;
        boolean serial = false;
        boolean percent = false;
        boolean stationary = false;
        boolean cover = false;
        boolean optic = false;
        boolean bipod = false;
        boolean stunning = false;
        boolean capture = false;
        boolean noriposte = false;
        boolean shieldwall = false;
        boolean suppressing = false;
        switch (modifierType) {
            case ModifiersState.damage:
                modifiedDamage = true;
                break;
            case ModifiersState.bodypart:
                bodyPart = true;
                break;
            case ModifiersState.serial:
                serial = true;
                break;
            case ModifiersState.percent:
                percent = true;
                break;
            case ModifiersState.stationary:
                stationary = true;
                break;
            case ModifiersState.cover:
                cover = true;
                break;
            case ModifiersState.optic:
                optic = true;
                break;
            case ModifiersState.bipod:
                bipod = true;
                break;
            case ModifiersState.stunning:
                stunning = true;
                break;
            case ModifiersState.capture:
                capture = true;
                break;
            case ModifiersState.noriposte:
                noriposte = true;
                break;
            case ModifiersState.shieldwall:
                shieldwall = true;
                break;
            case ModifiersState.suppressing:
                suppressing = true;
                break;
        }


//        if (modifiedDamage) System.out.println("Modified Damage True");
//        if (bodyPart) System.out.println("Body Part True");
//        if (serial) System.out.println("Serial True");

        ModifierMessage result = new ModifierMessage(modifiedDamage, modifier, bodyPart, serial, percent, stationary, cover, optic, bipod, stunning, capture, noriposte, shieldwall, suppressing);
        KMPacketHandler.INSTANCE.sendTo(result, getCommandSenderAsPlayer(sender));

        System.out.println("Set " + modifierType + " to " + modifier + " for " + player.getName());

        TextComponentString endResult = new TextComponentString("Модификаторы обновлены.");
        endResult.getStyle().setColor(TextFormatting.GRAY);
//        sender.sendMessage(endResult);

        ClickContainer.deleteContainer("ModifiersButton", (EntityPlayerMP) sender);
        System.out.println(toRoot);
        if(!toRoot.equals("")) DataHolder.inst().getPlayer(sender.getName()).performCommand("/" + toRoot + " " + oldContainer);
    }
}
