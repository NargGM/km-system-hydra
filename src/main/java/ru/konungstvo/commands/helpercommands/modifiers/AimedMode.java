package ru.konungstvo.commands.helpercommands.modifiers;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.combat.CombatState;
import ru.konungstvo.combat.StatusEnd;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.combat.equipment.Weapon;
import ru.konungstvo.combat.movement.ChargeHistory;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.commands.executor.NextExecutor;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Player;

public class AimedMode extends CommandBase {
    private static String START_MESSAGE = "Выберите %s!\n";
    public static final String NAME = "setstance";
    @Override
    public String getName() {
        return "setstance";
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        //String modifierType = args[0];
        String oldContainer = "";
        String oldest = "";
        String stance = args[0];
        if(args.length > 1) oldContainer = args[1];
        if(args.length > 2) oldest = args[2];
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player subordinate = DataHolder.inst().getPlayer(sender.getName());

        EntityLivingBase forgePlayer = getCommandSenderAsPlayer(sender);
        if (subordinate.getSubordinate() != null) {
            subordinate = subordinate.getSubordinate();
            //forgePlayer = (EntityLivingBase) DataHolder.inst().getNpcEntity(subordinate.getName());
            if (!DataHolder.inst().isNpc(subordinate.getName())) {
                //forgePlayer = ServerProxy.getForgePlayer(subordinate.getName());
            }
        }

//        ItemStack leftHand = forgePlayer.getHeldItemOffhand();
//        ItemStack rightHand = forgePlayer.getHeldItemMainhand();
//        WeaponTagsHandler leftWeapon = new WeaponTagsHandler(leftHand);
//        WeaponTagsHandler rightWeapon = new WeaponTagsHandler(rightHand);
//        Weapon weapon = null;
//        Message message;



        if (stance.equals("aim") && oldContainer.equals("off")) {
            oldContainer = "";
            if(args.length > 2) oldContainer = args[2];
            if(args.length > 3) oldest = args[3];
            subordinate.setAimedMode(false);
            if(!oldContainer.equals("")) DataHolder.inst().getPlayer(sender.getName()).performCommand("/" + oldContainer + " " + oldest);
            return;
        }

        if (stance.equals("bipod") && oldContainer.equals("off")) {
            oldContainer = "";
            if(args.length > 2) oldContainer = args[2];
            if(args.length > 3) oldest = args[3];
            subordinate.setBipodsDeployed(false);
            if(!oldContainer.equals("")) DataHolder.inst().getPlayer(sender.getName()).performCommand("/" + oldContainer + " " + oldest);
            return;
        }

        if (stance.equals("station") && oldContainer.equals("off")) {
            oldContainer = "";
            if(args.length > 2) oldContainer = args[2];
            if(args.length > 3) oldest = args[3];
            subordinate.setStation(false);
            if(!oldContainer.equals("")) DataHolder.inst().getPlayer(sender.getName()).performCommand("/" + oldContainer + " " + oldest);
            return;
        }


        // REMOVE PREVIOUS PANEL
        if (oldContainer.equals("continue")) {
            ClickContainerMessage remove = new ClickContainerMessage("PerformAction", true);
            KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
        }
            if (!oldContainer.isEmpty() &&  !oldContainer.equals("continue")) {
                ClickContainerMessage remove = new ClickContainerMessage(oldContainer, true);
                KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
            }
            System.out.println(subordinate.getAttachedCombatID() != -1);
            System.out.println(DataHolder.inst().getCombat(subordinate.getAttachedCombatID()) != null);
            //System.out.println(DataHolder.inst().getCombat(subordinate.getAttachedCombatID()).getReactionList().getQueue().getNext() == subordinate);
            if (DataHolder.inst().getCombatForPlayer(subordinate.getName()) != null && DataHolder.inst().getCombatForPlayer(subordinate.getName()).getReactionList().getQueue().getNext() == subordinate) {
                subordinate.setMovementHistory(MovementHistory.NONE);
                subordinate.setChargeHistory(ChargeHistory.NONE);
                if (stance.equals("bipod")) subordinate.setBipodsDeployed(true);
                else if (stance.equals("aim")) subordinate.setAimedMode(true);
                else if (stance.equals("station")) subordinate.setStation(true);
                if (subordinate.getCombatState().equals(CombatState.SHOULD_ACT)) player.performCommand("/"+ NextExecutor.NAME +" " + stance);
            } else {
                subordinate.setMovementHistory(MovementHistory.NONE);
                subordinate.setChargeHistory(ChargeHistory.NONE);
                if (stance.equals("bipod")) subordinate.setBipodsDeployed(true);
                else if (stance.equals("aim")) subordinate.setAimedMode(true);
                else if (stance.equals("station")) subordinate.setStation(true);
                if(!oldContainer.equals("")) DataHolder.inst().getPlayer(sender.getName()).performCommand("/" + oldContainer + " " + oldest);
            }
            return;

    }
}
