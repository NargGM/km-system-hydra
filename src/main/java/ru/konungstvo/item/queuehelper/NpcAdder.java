package ru.konungstvo.item.queuehelper;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import noppes.npcs.api.IWorld;
import noppes.npcs.api.NpcAPI;
import noppes.npcs.api.constants.EntityType;
import noppes.npcs.api.entity.IEntity;
import noppes.npcs.entity.EntityNPCInterface;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.commands.helpercommands.gm.npcadder.ChooseNpcCommand;
import ru.konungstvo.commands.helpercommands.gm.npcadder.NpcCommand;
import ru.konungstvo.control.DataHolder;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NpcAdder extends Item {
    public NpcAdder() {
        super();
    }

    @Override
    @SideOnly(Side.SERVER)
    public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity) {
        String coords = "";
        coords += entity.posX + " " + entity.posY + " " + entity.posZ;

        if (entity instanceof EntityNPCInterface) {
            EntityNPCInterface npc = (EntityNPCInterface) entity;
            if (npc.wrappedNPC.getDisplay().getName().contains(" ")) {
                npc.wrappedNPC.getDisplay().setName(npc.wrappedNPC.getDisplay().getName().replace(" ", "_"));
            }
        }

        IWorld iWorld = NpcAPI.Instance().getIWorld(entity.dimension);
        IEntity[] list = iWorld.getAllEntities(EntityType.NPC);
        int i = 0;
        for (IEntity iEntity : list) {
            if (iEntity.getName().equals(entity.getName())) i++;
            if (i > 1) {
                TextComponentString str = new TextComponentString("На сервере больше одного НПЦ с именем " + entity.getName() + "!");
                player.sendMessage(str);
                return false;
            }
        }

        if (stack.hasDisplayName()) {
            boolean react = stack.getDisplayName().startsWith("§6");
            String stat = stack.getDisplayName().replaceAll("§.", "");
            if (getListOfNpcStats().contains(stat)) {
                if (react) DataHolder.inst().getPlayer(player.getName()).performCommand("/" + NpcCommand.NAME + " addr " + stat + " " + entity.getDisplayName().getUnformattedText() + " " + coords);
                else DataHolder.inst().getPlayer(player.getName()).performCommand("/" + NpcCommand.NAME + " add " + stat + " " + entity.getDisplayName().getUnformattedText() + " " + coords);
            } else {
                DataHolder.inst().getPlayer(player.getName()).sendMessage(new Message("Не найден скиллсет " + stat + "!", ChatColor.RED));
                String command = ChooseNpcCommand.NAME + " " + entity.getDisplayName().getUnformattedText() + " " + coords;
                DataHolder.inst().getPlayer(player.getName()).performCommand(command);
            }
        } else {
            String command = ChooseNpcCommand.NAME + " " + entity.getDisplayName().getUnformattedText() + " " + coords;
            DataHolder.inst().getPlayer(player.getName()).performCommand(command);
        }

        // КОСТЫЛЬ?
        try {
            DataHolder.inst().registerNpc(entity.getDisplayName().getUnformattedText(), (EntityNPCInterface) entity);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (entity instanceof EntityNPCInterface) {
            EntityNPCInterface npc = (EntityNPCInterface) entity;
            npc.wrappedNPC.getAi().setStandingType(1);
        }

        return true;
    }

    @Override
    public boolean canDestroyBlockInCreative(World world, BlockPos pos, ItemStack stack, EntityPlayer player)
    {
        return false;
    }

    private List<String> getListOfNpcStats() {
        ArrayList<String> result = new ArrayList<>();
        File folder = new File(DataHolder.getNpcTemplatesPath());
        File[] listOfFiles = folder.listFiles();

        for (File listOfFile : listOfFiles) {
            System.out.println("File " + listOfFile.getName());
            if (listOfFile.getName().endsWith(".proficiencies")) continue;
            result.add(listOfFile.getName().replace(".skills", ""));
        }

        Collections.sort(result);
        return result;
    }
}
