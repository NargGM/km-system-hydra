package ru.konungstvo.item.queuehelper;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.konungstvo.control.DataHolder;

public class QueueAdder extends Item {
    public QueueAdder() {
        super();
    }

    @Override
    @SideOnly(Side.SERVER)
    public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity) {
        System.out.println("Left clicking!");
        System.out.println(entity.toString());

        System.out.println("SIDE: " + FMLCommonHandler.instance().getSide());
        DataHolder.inst().getPlayer(player.getName()).performCommand("queue add " + entity.getName());
        /*
        MyMessage myMessage = new MyMessage(3);
        KMPacketHandler.INSTANCE.registerMessage(MyMessageHandler.class, MyMessage.class, KMPacketHandler.getDiscriminatorId(), Side.SERVER);
        KMPacketHandler.INSTANCE.sendToServer(myMessage);
        KMPacketHandler.INSTANCE.registerMessage(MyMessageHandler.class, MyMessage.class, KMPacketHandler.getDiscriminatorId(), Side.SERVER);

         */
        return true;
    }

    @Override
    public boolean canDestroyBlockInCreative(World world, BlockPos pos, ItemStack stack, EntityPlayer player)
    {
        return false;
    }

    /*
    @Override
    public boolean onEntitySwing(EntityLivingBase entityLiving, ItemStack stack) {
        System.out.println("Swinging!");
        System.out.println("Entity: " + entityLiving.toString());
        System.out.println("Entity: " + entityLiving.getName());
        return super.onEntitySwing(entityLiving, stack);
    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        System.out.println("Left clicking into air!");
        System.out.println("pos: " + pos.toString());
        System.out.println("coords: " + hitX + ", " + hitY + ", " + hitZ);
        return super.onItemUse(player, worldIn, pos, hand, facing, hitX, hitY, hitZ);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
        System.out.println("Right clicking!");
        return super.onItemRightClick(worldIn, playerIn, handIn);
    }

     */
}
