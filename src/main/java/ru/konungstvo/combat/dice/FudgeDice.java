package ru.konungstvo.combat.dice;

import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.dice.modificator.Modificator;
import ru.konungstvo.combat.dice.modificator.ModificatorSet;
import ru.konungstvo.combat.dice.modificator.ModificatorType;
import ru.konungstvo.combat.dice.modificator.PercentDice;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Logger;

import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Dice such as % poor.
 */
public class FudgeDice {
    public static String regex = "^%\\s?(ужасно|плохо|посредственно|нормально|хорошо|отлично|превосходно|легендарно)(\\s[+-][0-9][0-9]?[0-9]?%?)?(.*)?";
    private String initial;
    private int initialInt;
    private int base;
    private String info;
    private int result;
    private int firstResult;
    private static Logger logger = new Logger("FudgeDice");
    private int defenseMod;
    private PercentDice percentDice;
    private int persentResult;
    private boolean wasChangedByPercentDice = false;
    private boolean alreadyCast = false;
    private boolean rerolled = false;
    private ArrayList<String> advantages = new ArrayList<>();
    private ArrayList<String> disadvantages = new ArrayList<>();
    public ModificatorSet mods;

    public FudgeDice(String initial) {
        this(initial, 0);
    }

    public FudgeDice(String initial, int mod) {
        this(initial, mod, 0, 0, 0, new PercentDice());

    }

    public FudgeDice(String initial, int mod, int woundsMod) {
        this(initial, mod, woundsMod, 0, 0, new PercentDice());
    }

    public FudgeDice(String initial, int mod, int woundsMod, int armorMod, int coverMod, PercentDice percentDice) {
        this(initial,
                new ModificatorSet()
                        .add(new Modificator(mod, ModificatorType.CUSTOM))
                        .add(new Modificator(woundsMod, ModificatorType.WOUNDS))
                        .add(new Modificator(armorMod, ModificatorType.ARMOR))
                        .add(new Modificator(coverMod, ModificatorType.COVER))
        );
        this.percentDice = percentDice;

    }

    public FudgeDice(String initial, ModificatorSet set) {

        this.initial = initial;
        this.initialInt = DataHolder.inst().getDiceAsInteger(initial);
        this.info = "(";
        this.mods = set;

        this.base = this.initialInt;
        for (Modificator mod : mods.getMods()) {
            this.base += mod.getMod();
        }
        this.result = this.base;
        this.firstResult = 666;
    }

    public void cast() {
        cast(false);
    }

    public void cast(boolean reroll) {
        if (isAlreadyCast() && !reroll) return;
        if (isAlreadyCast() && reroll) rerolled = true;
        result = this.base;
        info = "(";
        Random random = new Random();
        for (int i = 0; i < 4; i++) {
            int dice = random.nextInt(4);
            switch (dice) {
                case 0:
                    info += "-";
                    result--;
                    break;
                case 1:
                    info += "=";
                    break;
                case 2:
                    info += "≡";
                    break;
                case 3:
                    info += "+";
                    result++;
                    break;
            }
        }
        info += ")";
        String newInfo = "";

        int percentDiceGet = percentDice.get();
        int percentResult = 0;
        if (percentDiceGet  >= 100) {
            if (!isAlreadyCast()) addMod(new Modificator(percentDiceGet /100, ModificatorType.PERCENT));
            percentDiceGet = percentDiceGet % 100;
        }

        if (percentDiceGet  <= -100) {
            if (!isAlreadyCast()) addMod(new Modificator(percentDiceGet /100, ModificatorType.PERCENT));
            percentDiceGet = percentDiceGet % 100;
        }

        if (percentDiceGet != 0) {
            Random random2 = new Random();
            percentResult = random2.nextInt(100) + 1; //69
            setPersentResult(percentResult);

            if (percentDiceGet > 0 && percentResult <= percentDiceGet) {
                setWasChangedByPercentDice(true);
                // +5%, result 6/100, skip
                // +5%, result 5/100, success
                // +70%, result 70/100, skip
                // +70%, result 69/100, success
                newInfo = info.replaceFirst("\\=", TextFormatting.DARK_GREEN + "\\+" + TextFormatting.YELLOW);
                if (newInfo.equals(info)) {
                    newInfo = info.replaceFirst("\\≡", TextFormatting.DARK_GREEN + "\\+" + TextFormatting.YELLOW);
                }
                if (newInfo.equals(info)) {
                    newInfo = info.replaceFirst("\\-", TextFormatting.DARK_GREEN + "=" + TextFormatting.YELLOW);
                }
                if (!newInfo.equals(info)) {
                    System.out.println("Dice result increased from " + result + " to " + (result+1));
                    result++;
                }

            } else if (percentDiceGet < 0 && percentResult <= -percentDiceGet) {
                setWasChangedByPercentDice(true);
                // -5%, result 5/100, reducing
                // -5%, result 6/100, skip
                // -70%, result 70/100, reducing
                // -70%, result 71/100, skip
                newInfo = info.replaceFirst("\\=", TextFormatting.RED + "\\-" + TextFormatting.YELLOW);
                if (newInfo.equals(info)) {
                    newInfo = info.replaceFirst("\\≡", TextFormatting.RED + "\\-" + TextFormatting.YELLOW);
                }
                if (newInfo.equals(info)) {
                    newInfo = info.replaceFirst("\\+", TextFormatting.RED + "\\=" + TextFormatting.YELLOW);
                }
                if (!newInfo.equals(info)) {
                    System.out.println("Dice result decreased from " + result + " to " + (result-1));
                    result--;
                }
            }

        }
        if (!newInfo.isEmpty()) {
            info = newInfo;
        }

        if (disadvantages.size() > 0 || advantages.size() > 0) {
            int secResult = this.base;
            String secInfo = "(";
            Random random3 = new Random();
            for (int i = 0; i < 4; i++) {
                int dice = random3.nextInt(4);
                switch (dice) {
                    case 0:
                        secInfo += "-";
                        secResult--;
                        break;
                    case 1:
                        secInfo += "=";
                        break;
                    case 2:
                        secInfo += "≡";
                        break;
                    case 3:
                        secInfo += "+";
                        secResult++;
                        break;
                }
            }
            secInfo += ")";
            String newSecInfo = "";

            if (percentDiceGet != 0) {
                if (percentDiceGet > 0 && percentResult <= percentDiceGet) {
                    setWasChangedByPercentDice(true);
                    // +5%, result 6/100, skip
                    // +5%, result 5/100, success
                    // +70%, result 70/100, skip
                    // +70%, result 69/100, success
                    newSecInfo = newSecInfo.replaceFirst("\\=", TextFormatting.DARK_GREEN + "\\+" + TextFormatting.YELLOW);
                    if (newSecInfo.equals(newSecInfo)) {
                        newSecInfo = newSecInfo.replaceFirst("\\≡", TextFormatting.DARK_GREEN + "\\+" + TextFormatting.YELLOW);
                    }
                    if (newSecInfo.equals(newSecInfo)) {
                        newSecInfo = newSecInfo.replaceFirst("\\-", TextFormatting.DARK_GREEN + "=" + TextFormatting.YELLOW);
                    }
                    if (!newSecInfo.equals(newSecInfo)) {
                        System.out.println("Dice result increased from " + secResult + " to " + (secResult+1));
                        secResult++;
                    }

                } else if (percentDiceGet < 0 && percentResult <= -percentDiceGet) {
                    setWasChangedByPercentDice(true);
                    // -5%, result 5/100, reducing
                    // -5%, result 6/100, skip
                    // -70%, result 70/100, reducing
                    // -70%, result 71/100, skip
                    newSecInfo = secInfo.replaceFirst("\\=", TextFormatting.RED + "\\-" + TextFormatting.YELLOW);
                    if (newSecInfo.equals(secInfo)) {
                        newSecInfo = secInfo.replaceFirst("\\≡", TextFormatting.RED + "\\-" + TextFormatting.YELLOW);
                    }
                    if (newSecInfo.equals(secInfo)) {
                        newSecInfo = secInfo.replaceFirst("\\+", TextFormatting.RED + "\\=" + TextFormatting.YELLOW);
                    }
                    if (!newSecInfo.equals(secInfo)) {
                        System.out.println("Dice result decreased from " + secResult + " to " + (secResult-1));
                        secResult--;
                    }
                }

            }
            if (!newSecInfo.isEmpty()) {
                secInfo = newSecInfo;
            }

            if (disadvantages.size() > advantages.size()) {
                if (result < secResult) {
                    info = info + TextFormatting.DARK_RED + TextFormatting.BOLD + " < " + TextFormatting.YELLOW + secInfo;
                } else if (result > secResult) {
                    info = info + TextFormatting.DARK_RED + TextFormatting.BOLD + " > " + TextFormatting.YELLOW + secInfo;
                    result = secResult;
                } else {
                    info = info + TextFormatting.GRAY + " == " + TextFormatting.YELLOW + secInfo;
                }
            } else if (disadvantages.size() < advantages.size()) {
                if (result < secResult) {
                    info = info + TextFormatting.DARK_GREEN + TextFormatting.BOLD + " < " + TextFormatting.YELLOW + secInfo;
                    result = secResult;
                } else if (result > secResult) {
                    info = info + TextFormatting.DARK_GREEN + TextFormatting.BOLD + " > " + TextFormatting.YELLOW + secInfo;
                } else {
                    info = info + TextFormatting.GRAY + " == " + TextFormatting.YELLOW + secInfo;
                }
            }
        }

        if (firstResult == 666) this.firstResult = result;
        setAlreadyCast(true);
        System.out.println("Cast dice with result " + result);
    }

    public String getResultAsString() {
        //return DataHolder.getInstance().getSkillTable().get(result) + " [" + DataHolder.getInstance().getSkillTable().get(firstResult) + "]";
        if (result < -4) result = -5;
        if (result > 9) result = 10;
        return DataHolder.inst().getSkillTable().get(result);

    }

    public String getFirstResultAsString() {
        //return DataHolder.getInstance().getSkillTable().get(result) + " [" + DataHolder.getInstance().getSkillTable().get(firstResult) + "]";
        if (firstResult != 666) {
            if (firstResult < -4) firstResult = -5;
            if (firstResult > 9) firstResult = 10;
        return DataHolder.inst().getSkillTable().get(firstResult);
        } else {
            if (result < -4) result = -5;
            if (result > 9) result = 10;
            return DataHolder.inst().getSkillTable().get(result);
        }
    }

    public String getInfo() {
        return info;
    }

    public String getInitial() {
        return initial;
    }

    public void addAdvantage(String advantage) {
        System.out.println("adding advantage " + advantage);
        advantages.add(advantage);
        if (this.alreadyCast) cast(true);
    }

    public void addDisadvantage(String disadvantage) {
        disadvantages.add(disadvantage);
        if (this.alreadyCast) cast(true);
    }

    public String getAdvDisadvString() {
        StringBuilder res = new StringBuilder();
        if (advantages.size() > 0) {
            if (advantages.size() > 1) res.append("§2§lПреимущества:\n");
            else res.append("§2§lПреимущество:\n");
            for (String advantage : advantages) {
                res.append(advantage).append("\n");
            }
        }
        if (disadvantages.size() > 0) {
            if (disadvantages.size() > 1) res.append("§4§lПомехи:\n");
            else res.append("§4§lПомеха:\n");
            for (String advantage : advantages) {
                res.append(advantage).append("\n");
            }
        }

        return res.toString();
    }

    @Deprecated
    public Message getModAsMessage() {
        Message mod = new Message("");
        Modificator custom = this.mods.getMod(ModificatorType.CUSTOM);
        if (custom != null && custom.getMod() > 0) mod = new Message(" +" + custom.getMod());
        else if (custom.getMod() < 0) mod = new Message(" " + custom.getMod());

        for (Modificator modificator : mods.getMods()) {
            MessageComponent comp = new MessageComponent(" " + modificator.getMod());
            mod.addComponent(comp);
        }

        /*
        MessageComponent armorModComponent =  new MessageComponent("");
        if (armorMod != 0) {
            armorModComponent = new MessageComponent(" " + armorMod, ChatColor.BLUE);
        }
        MessageComponent woundsModComponent =  new MessageComponent("");
        if (woundsMod != 0) {
            woundsModComponent = new MessageComponent(" " + woundsMod, ChatColor.RED);
        }
        MessageComponent defenseModComponent =  new MessageComponent("");
        if (defenseMod != 0) {
            defenseModComponent = new MessageComponent(" " + defenseMod, ChatColor.DARK_GREEN);
        }
        MessageComponent coverModComponent =  new MessageComponent("");
        if (coverMod != 0) {
            coverModComponent = new MessageComponent(" " + coverMod + "y", ChatColor.COMBAT);
        }
//        mod += armorModStr + woundsModStr + defenseModStr + coverModStr;
        mod.addComponent(armorModComponent);
        mod.addComponent(woundsModComponent);
        mod.addComponent(defenseModComponent);
        mod.addComponent(coverModComponent);

         */

        return mod;
    }

    public String getFinalModAsString() {
        if (getModAsString().isEmpty()) return "";
        String resultStr = "";
        if (!getModAsStringOrig().isEmpty()) {
            int result = 0;
            for (Modificator modificator : mods.getMods()) {
                result += modificator.getMod();
            }
            resultStr = String.valueOf(result);
            if (result >= 0) resultStr = "+" + resultStr;
        }
        if (percentDice.get() != 0) {
            int percentDiceInt = percentDice.get();
            if (percentDiceInt >= 100) percentDiceInt = percentDiceInt % 100;
            if (percentDiceInt <= -100) percentDiceInt = percentDiceInt % 100;
            if (percentDiceInt != 0) resultStr += (resultStr.isEmpty() ? "" : " ") + (percentDiceInt > 0 ? "+" : "") + percentDiceInt + "%";
        }
        return " " + resultStr;
    }

    public String getModAsString() {
        String result = "";
        for (Modificator modificator : mods.getMods()) {
            if (modificator.getMod() == 0) continue;
            String modStr = String.valueOf(modificator.getMod());

            if (modificator.getMod() > 0) modStr = TextFormatting.DARK_GREEN + "+" + modStr;
            else modStr = TextFormatting.RED + modStr;

            result += modStr + " " + modificator.getType().getDefaultName() + "\n";
        }
        String percentString = percentDice.toString();
        //if (!result.isEmpty() && !percentString.isEmpty()) result += "\n";
        result += percentString;
        return result.trim();
        /*
        String modStr = "";
        if (mod > 0) modStr = " +" + mod;
        else if (mod < 0) modStr = " " + mod;
        String armorModStr = "";
        if (armorMod < 0) armorModStr = "§8 " + armorMod;
        String woundsModStr = "";
        if (woundsMod < 0) woundsModStr = "§4 " + woundsMod;
        String defenseModStr = "";
        if (defenseMod < 0) defenseModStr = "§3 " + defenseMod;
        String coverModStr = "";
        if (coverMod != 0) coverModStr = " " + coverMod + "у";
        modStr += armorModStr + woundsModStr + defenseModStr + coverModStr;

        if (!modStr.equals("")) modStr += "§e";
        return modStr.trim();

         */
    }

    public String getModAsStringOrig() {
        String result = "";
        for (Modificator modificator : mods.getMods()) {
            if (modificator.getMod() == 0) continue;
            String modStr = String.valueOf(modificator.getMod());

            if (modificator.getMod() > 0) modStr = TextFormatting.DARK_GREEN + "+" + modStr;
            else modStr = TextFormatting.RED + modStr;

            result += modStr + " " + modificator.getType().getDefaultName() + "\n";
        }
        return result.trim();
        /*
        String modStr = "";
        if (mod > 0) modStr = " +" + mod;
        else if (mod < 0) modStr = " " + mod;
        String armorModStr = "";
        if (armorMod < 0) armorModStr = "§8 " + armorMod;
        String woundsModStr = "";
        if (woundsMod < 0) woundsModStr = "§4 " + woundsMod;
        String defenseModStr = "";
        if (defenseMod < 0) defenseModStr = "§3 " + defenseMod;
        String coverModStr = "";
        if (coverMod != 0) coverModStr = " " + coverMod + "у";
        modStr += armorModStr + woundsModStr + defenseModStr + coverModStr;

        if (!modStr.equals("")) modStr += "§e";
        return modStr.trim();

         */
    }

    public int getMod() {
        int mod = 0;
        for (Modificator modificator : mods.getMods()) {
            mod += modificator.getMod();
        }
        return mod;
    }

    public String getBaseAsString() {
        System.out.println("base: " + base);
        if (base < -4) base = -4;
        return DataHolder.inst().getSkillTable().get(base);
    }

    public static boolean matches(String context) {
        logger.debug("Searching for match in context: " + context);
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(context);
        if (matcher.find()) {
            logger.debug("Success!");
            return true;
        } else {
            logger.debug("Failure!");
            return false;
        }
    }

    public int getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "FudgeDice of initial value " + initial + " and result " + getResultAsString();
    }

    public void reroll() {
        String snapshot = this.toString();
        this.cast(true);
        System.out.println ("Rerolled \"" + snapshot + "\" --> \"" + this.toString() + "\"");
    }

    public void considerDefenseMod(int previousDefenses) {
        if (previousDefenses <= 0) return;
        this.base -= previousDefenses;
        this.defenseMod = -previousDefenses;
        logger.debug("Set defenseMod to " + defenseMod);
    }

    public int getDefenseMod() {
        if (defenseMod < -3) return -3;
        return defenseMod;
    }

    public void setDefenseMod(int defenseMod) {
        this.defenseMod = defenseMod;
    }

    public String getSkillName() {
        return "";
    }

    public int getBase() {
        return base;
    }

    public void addMod(Modificator modificator) {
        System.out.println("FudgeDice: adding " + modificator);
        this.mods.add(modificator);
        this.base += modificator.getMod();
        System.out.println("old result: " + result);
        if (firstResult != 666) {
            firstResult += modificator.getMod();
        }
        this.result += modificator.getMod();
        System.out.println("new result: " + result);
    }

    public void setPercentDice(PercentDice percentDice) {
        this.percentDice = percentDice;
    }

    public PercentDice getPercentDice() {
        return percentDice;
    }

    public boolean wasChangedByPercentDice() {
        return wasChangedByPercentDice;
    }

    public void setWasChangedByPercentDice(boolean wasChangedByPercentDice) {
        this.wasChangedByPercentDice = wasChangedByPercentDice;
    }

    public int getPersentResult() {
        return persentResult;
    }

    public void setPersentResult(int persentResult) {
        this.persentResult = persentResult;
    }

    public boolean isAlreadyCast() {
        return alreadyCast;
    }

    public boolean isRerolled() {
        return rerolled;
    }

    public int getFirstResult() {
        if (firstResult == 666) return result;
        return firstResult;
    }

    public void setAlreadyCast(boolean alreadyCast) {
        this.alreadyCast = alreadyCast;
    }
}