package ru.konungstvo.combat.dice.modificator;

public enum ModificatorType {
    WOUNDS("Штраф от ран"),
    ARMOR("Штраф от брони"),
    SHIELD("Штраф от щита"),
    WEIGHT("Штраф от нагрузки"),
    PSYCH_WEIGHT("Психическая нагрузка"),
    EQUIPMENT("От снаряжения"),
    COVER("Модификатор укрытия"),
    PARRY("Штраф от длины оружия"),
    BLOCK("Модификатор щита"),
    POINT_BLANK("Стрельба вблизи"),
    RUNNING("После бега"),
    OFFHAND("Стрельба навскидку"),
    CONCENTRATED("Концентрация"),
    NOTCONCENTRATED("Антиконцентрация (серийный огонь)"),
    MULTISHOT("Расстрел"),
    MULTISHOT3("Разнос"),
    SHIFTED("Откладывал ход"),
    SAFE("Цель в безопасности"),
    RECOIL("Отдача"),
    SERIAL("Серийный огонь"),
    DOUBLE("Двойной выстрел"),
    DOUBLE_STRIKE("Двойной удар"),
    PREVIOUS_DEFENSES("Предыдущие защиты"),
    TWOHANDED("Двуручное оружие"),
    FIREARM_STRENGTH("Требования к силе"),
    PERCENT("От процентных бонусов/штрафов"),
    LYING("Лежа"),
    NOMANA("Нет маны"),
    RIPOSTE("Рипост"),
    CUSTOM("Кастомный модификатор"),
    PREPARING("Подготовка заклинания"),
    OPPORTUNITY("Атака по возможности");



    private String name;
    ModificatorType(String name) {
        this.name = name;
    }
    public String getDefaultName() {
        return name;
    }
}
