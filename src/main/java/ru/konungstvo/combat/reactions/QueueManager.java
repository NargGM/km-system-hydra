package ru.konungstvo.combat.reactions;

import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Player;
import ru.konungstvo.view.ChatView;
import ru.konungstvo.view.Envelope;
import ru.konungstvo.view.View;

import java.util.List;
import java.util.Observable;

//give this class a reactionList (optional) and a command, and it will do the magic
@Deprecated
public class QueueManager extends Observable {
    private ReactionList reactionList;
    private Player player;
    private String command;
    private String[] args;
    private Envelope<String> responseEnvelope;

    public QueueManager(Player executor, String command, String[] args) {
        this.player = executor;
        this.command = command;
        this.args = args;
        process();
    }

    public QueueManager(ReactionList reactionList, Player executor, String command, String[] args) {
        this(reactionList, executor, command, args, new ChatView());
    }

    public QueueManager(ReactionList reactionList, Player executor, String command, String[] args, View view) {
        this.reactionList = reactionList;
        this.command = command;
        this.args = args;
        this.player = executor;
        this.addObserver(view);
    }


    public void process() {
        if (args.length == 0) {
            args = new String[1];
            args[0] = "help"; //TODO This may be a horrible way to handle things! Test more.
        }
        Envelope<String> env = null;
        switch (args[0]) {
            case "help":
                env = new Envelope<>(player.getName(), "Тут будет справка по команде /react.");
                break;

            case "list":
                List<ReactionList> reactionListList = DataHolder.inst().getQueueList();
                String answer;
                if (reactionListList.isEmpty()) answer = "Не найдено ни одного активного боя с очередями!";
                else {
                    StringBuilder answerBuilder = new StringBuilder("Список активных очередей: ");
                    answer = answerBuilder.toString();
                    answer = answer.substring(0, answer.length()-2) + ".";
                }
                env = new Envelope<>(player.getName(), answer);
                break;

            case "end":

                break;
            case "show":


                break;
            case "add":


                break;
            case "edit":

                break;
            case "remove":

                break;
            case "next":

                break;
            default:
        }
        responseEnvelope = env;
        setChanged();
        notifyObservers(env);
    }

    public Envelope<String> getResponseEnvelope() {
        return responseEnvelope;
    }
}
