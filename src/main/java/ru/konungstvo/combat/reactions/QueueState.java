package ru.konungstvo.combat.reactions;

@Deprecated
public enum QueueState {
    WAITING,
    ACTING,
    ATTACKING,
    DEFENDING,
    OPPORTUNITY,
    ACTED;
}
