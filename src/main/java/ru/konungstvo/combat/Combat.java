package ru.konungstvo.combat;

import javafx.util.Pair;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.moves.DeprecatedAttack;
import ru.konungstvo.combat.moves.Opportunity;
import ru.konungstvo.combat.reactions.ReactionList;
import ru.konungstvo.commands.executor.QueueExecutor;
import ru.konungstvo.control.CombatMemento;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Logger;
import ru.konungstvo.exceptions.CombatException;
import ru.konungstvo.exceptions.DataException;
import ru.konungstvo.player.NPC;
import ru.konungstvo.player.Player;
import ru.konungstvo.player.wounds.WoundType;


import java.io.Serializable;
import java.util.*;

/**
 * Represents a combat between players and/or npcs.
 */
public class Combat implements Serializable, Cloneable {
    private String name;
    private int id;
    private int stateID;
    private ReactionList reactionList;
    private List<Player> fighters;
    private List<DeprecatedAttack> attackList;
    private List<Opportunity> opportunityList;

    private List<Player> waitToMakeDecision;

    private List<Player> mustNotActNextRound;

    private HashMap<Player, Pair<Integer, Integer>> lethalCompany;
    private HashMap<Player, String> lethalCompany2;

    private List<Player> guaranteedPlus;


    private List<Boom> booms;

    private List<Fire> fires;
    private static Logger logger = new Logger("COMBAT");
    private boolean thirdPersonBlocked = false;
    private long startingTime = 0;


    public Combat(String name, int id) {
        this.name = name;
        this.id = id;
        this.stateID = 0;
        this.reactionList = null;
        this.attackList = new ArrayList<>();
        this.opportunityList = new ArrayList<>();
        this.fighters = new ArrayList<>();
        this.waitToMakeDecision = new ArrayList<>();
        this.mustNotActNextRound = new ArrayList<>();
        this.lethalCompany = new HashMap<>();
        this.lethalCompany2 = new HashMap<>();
        this.fires = new ArrayList<>();
        this.booms = new ArrayList<>();
        this.guaranteedPlus = new ArrayList<>();
        this.startingTime = MinecraftServer.getCurrentTimeMillis();
        logger.info("Combat with name " + this.name + " was created");
    }

    public Object clone() throws CloneNotSupportedException {
        return (Combat) super.clone();
    }

    public static Combat getCopy(Combat other) {
        Combat result = new Combat(other.getName(), other.getId());
        result.stateID = other.stateID;
        result.reactionList = ReactionList.getCopy(other.reactionList);
        result.attackList = other.attackList;
        result.opportunityList = other.opportunityList;
        result.fighters = other.fighters;
        return result;
    }

    public Player addFighter(String name) {
        Player fighter = DataHolder.inst().getPlayer(name);
        if (hasFighter(name)) {
            System.out.println("Fighter " + name + " existed, skipping.");
            return fighter;
        }
        this.fighters.add(fighter);
        System.out.println("Fighter " + fighter.getName() + " joined Combat " + this.name);
        return fighter;
    }

    public void removeFighter(String fighterName) {
        fighters.remove(new Player(fighterName));
        reactionList.removeReaction(fighterName);
        reactionList.getQueue().removePlayer(fighterName);
    }

    public List<Player> getFighters() {
        return this.fighters;
    }


    public boolean isAttacked(String playerName) {
        return getAttackByDefender(playerName) != null;
    }

    public boolean isAttacker(String attackerName) {
        return getAttackByAttacker(attackerName) != null;
    }

    public boolean hasFighter(String fighterName) {
        if (getFighters().isEmpty()) return false;
        for (Player fighter : getFighters()) {
//            logger.debug("Found " + fighter.getName());
            if (fighter.getName().equals(fighterName))
                return true;
        }
        return false;
    }

    public ReactionList getReactionList() {
        return reactionList;
    }

    public void setReactionList(ReactionList reactionList) {
        this.reactionList = reactionList;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Combat)) return false;
        Combat other = (Combat) obj;
        return id == other.id;
    }

    public static Logger getLogger() {
        return logger;
    }

    public void addAttack(DeprecatedAttack attack) {
        logger.info("Attack was added to Combat " + name);
        this.attackList.add(attack);
    }


    public void defendAttack(String defenderName, SkillDiceMessage skillDiceMessage) throws Exception {
        DeprecatedAttack attack = getAttackByDefender(defenderName);
        if (attack == null) throw new CombatException("Вы пытаетесь защититься.", "На вас никто не нападает!");
        if (skillDiceMessage.getDice().getSkillName().equals("блокирование") && attack.getDefender().getShield() == null) {
            throw new CombatException("", "Сначала снарядите щит с помощью /equip! Например: /equip блокирование +1");
        }

        logger.info(defenderName + " is defending himself with " + skillDiceMessage.getDice().toString());

        attack.defend(skillDiceMessage);

        String previousDefenses = "";
        Player defender = DataHolder.inst().getPlayer(defenderName);
        if (defender.getPreviousDefenses() != 0) {
            previousDefenses = ChatColor.COMBAT + " с доп. штрафом " + defender.getPreviousDefenses() + ChatColor.DICE;
        }
        attack.setResultNotification(attack.getDefender().getDisplayedName() + ChatColor.COMBAT + " защищается от атаки " +
                ChatColor.NICK + attack.getDefender().getDisplayedName() + previousDefenses + ChatColor.COMBAT);
        this.attackList.remove(attack);
        if (!attack.getAttacker().isSuperFighter() && !(attack.getAttacker() instanceof NPC)) {
            QueueExecutor.execute(DataHolder.inst().getMasterForCombat(id), "next");
        }
        logger.debug("We got here 2222222");
    }


    //TODO: this might be really heavy on perfomance
    @Deprecated
    private List<Player> getResponsibleMasters() {
        List<Player> result = new ArrayList<>();
        for (Player player : DataHolder.inst().getPlayerList()) {
            if (player.getAttachedCombatID() == this.id) {
                result.add(player);
            }
        }
        return result;
    }

    public DeprecatedAttack getAttackByDefender(String defenderName) {
        for (DeprecatedAttack attack : attackList) {
            if (attack.getDefender().getName().equals(defenderName)) {
                return attack;
            }
        }
        return null;
    }

    public DeprecatedAttack getAttackByAttacker(String attackerName) {
        for (DeprecatedAttack attack : attackList) {
            if (attack.getAttacker().getName().equals(attackerName)) {
                return attack;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void sendNotification(String notification, List<Player> players) {
        Set<Player> receivers = new LinkedHashSet<>(players);
        for (Player receiver : receivers) {
            receiver.sendMessage("§5" + notification);
        }
    }

    public int getStateID() {
        return stateID;
    }

    public void setStateID(int stateID) {
        this.stateID = stateID;
    }

    public void incrementStateID() {
        this.stateID++;
    }

    public CombatMemento saveStateToMemento() {
        try {
            return new CombatMemento(this);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean getThirdPersonBlocked() {
        return thirdPersonBlocked;
    }

    public void setThirdPersonBlocked(boolean blocked) {
        this.thirdPersonBlocked = blocked;
    }


    public void removeAttack(DeprecatedAttack attack) {
        this.attackList.remove(attack);
    }

//    public NPC spawnNpc(String npcName) {
//       return spawnNpc(npcName, npcName);
//    }

    // create an NPC with unique (to the combat) name
    public NPC spawnNpc(String npcName, String customName, boolean magicName) throws DataException {
        int number = 1;

        for (Player pl : DataHolder.inst().getNpcList()) {
            if (pl.getName().startsWith(customName)) {
                String numberStr = pl.getName().substring(customName.length());
                try {
                    number = Integer.parseInt(numberStr) + 1;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        String displayName = customName + number;
        if (magicName) {
            displayName = customName;
        }
        NPC npc = DataHolder.inst().createNPC(displayName, npcName);

        this.addFighter(npc.getName());
        return npc;
    }

    public void removeNPCs() {
        List<Player> toRemove = new ArrayList<>();
        for (Player fighter : fighters) {
            if (fighter instanceof NPC) {
                logger.debug("Found " + fighter.getName() + " to remove.");
                toRemove.add(fighter);
            }
        }
        for (Player p : toRemove) {
            fighters.remove(p);
            DataHolder.inst().removePlayer(p.getName());
        }
    }

    public long getTime() {return startingTime;}

    public  List<DeprecatedAttack> getAttacks() {
        return attackList;
    }

    public void addBoom(Boom boom) {
        booms.add(boom);
    }

    public void addFire(Fire fire) {
        fires.add(fire);
    }


    public void removeBoom(Boom boom) {
        booms.remove(boom);
    }

    public Boom getBoomForPlayer(Player player) {
        for (Boom boom : booms) {
            if (boom.hasTarget(player)) return boom;
        }
        return null;
    }

    public Fire getFireForPlayer(Player player) {
        for (Fire fire : fires) {
            if (fire.hasTarget(player)) return fire;
        }
        return null;
    }

    public void addDecision(Player player) {
        waitToMakeDecision.add(player);
    }

    public void removeDecision(Player player) {
        waitToMakeDecision.remove(player);
    }

    public List<Player> getDecisions() {
        return waitToMakeDecision;
    }

    public void addMustNotActNextRound(Player player) {
        mustNotActNextRound.add(player);
    }

    public void removeMustNotActNextRound(Player player) {
        mustNotActNextRound.remove(player);
    }

    public List<Player> getMustNotActNextRound() {
        return mustNotActNextRound;
    }

    public void clearMustNotActNextRound() {
        mustNotActNextRound = new ArrayList<>();
    }

    public void addLethalCompany(Player player) {
        lethalCompany.put(player, new Pair<>(0, 0));
        lethalCompany2.put(player, "");
    }

    public void removeLethalCompany(Player player) {
        lethalCompany.remove(player);
    }

    public
    HashMap
            <Player,
                    Pair<Integer,Integer>> getLethalCompany() {
        return lethalCompany;
    }

//    public void proceedLethalCompany() {
//        for (Player player : lethalCompany.keySet()) {
//            if (DataHolder.inst().getCombatForPlayer(player.getName()) == null) {
//                continue;
//            }
//            if (lethalCompany.get(player).getKey() >= 3 || lethalCompany.get(player).getValue() >= 3) continue;
//            SkillDiceMessage sdm = new SkillDiceMessage(player.getName(), "% выносливость");
//            sdm.build();
//            System.out.println("testic " + sdm.getDice().getResult() + " " + sdm);
//            if (sdm.getDice().getResult() > 2) {
//                lethalCompany.put(player, new Pair<>(lethalCompany.get(player).getKey() + 1, lethalCompany.get(player).getValue()));
//            } else {
//                lethalCompany.put(player, new Pair<>(lethalCompany.get(player).getKey(), lethalCompany.get(player).getValue() + 1));
//            }
//
//            if (lethalCompany.get(player).getValue() >= 3) {
//                ServerProxy.sendMessageFromAndInformMasters(player, new Message(player.getName() + " не выживает.", ChatColor.RED));
//                Player gm = DataHolder.inst().getMasterForPlayerInCombat(player);
//                gm.performCommand("/combat remove " + player.getName());
//            } else if (lethalCompany.get(player).getKey() >= 3) {
//                ServerProxy.sendMessageFromAndInformMasters(player, new Message(player.getName() + " выживает.", ChatColor.DARK_GREEN));
//                Player gm = DataHolder.inst().getMasterForPlayerInCombat(player);
//                gm.performCommand("/combat remove " + player.getName());
//            }
//        }
//    }

    public String proceedLethalCompanyAndToString() {
        String result = "";
        for (Player player : lethalCompany.keySet()) {
            boolean inCombat = getFighters().contains(player);
            if (inCombat) player.processStatusEffects(StatusEnd.TURN_START);
            boolean shouldGetMinusFromBurn = true;
            if (inCombat) {
                player.checkIfEndedTurnOnFire();
            }
            if (inCombat && player.hasStatusEffect(StatusType.BURNING) && !player.cantBurn() && !player.getWoundPyramid().onlyDeadlyLeft()) {
                player.inflictDamage(3, "ожог");
                player.takeAwayDurFromAllArmor(1);
                if (player.getWoundPyramid().onlyDeadlyLeft()) shouldGetMinusFromBurn = false;
            }


            if (inCombat) DataHolder.inst().getModifiers().updateBloodlossAndStunning(player, player.getName());
            SkillDiceMessage sdm = null;
            String prefix = "§a";
            String postfix = "";
            if (lethalCompany.get(player).getKey() >= 3 && lethalCompany.get(player).getValue() >= 3) {
                prefix = "§2";
                postfix = " §2§l[ЧУДОМ ВЫЖИВАЕТ]";
            }
            else if (lethalCompany.get(player).getKey() >= 3) {
                prefix = "§2";
                postfix = " §2[РАНЫ СТАБИЛЬНЫ]";
            }
            else if (lethalCompany.get(player).getValue() >= 3) {
                prefix = "§4";
                postfix = " §4[НЕ ДЫШИТ]";
            }
            if (!inCombat && !(lethalCompany.get(player).getKey() >= 3 || lethalCompany.get(player).getValue() >= 3)) {
                result+= "\n§7" + player.getName() + " вне боя, пропускаем.\n";
            }
            if (!(lethalCompany.get(player).getKey() >= 3 || lethalCompany.get(player).getValue() >= 3) && inCombat) {
                sdm = new SkillDiceMessage(player.getName(), "% выносливость");
                sdm.build();
                System.out.println("testic " + sdm.getDice().getResult() + " " + sdm);
                 if (guaranteedPlus.contains(player)) {
                    lethalCompany.put(player, new Pair<>(lethalCompany.get(player).getKey() + 1, lethalCompany.get(player).getValue()));
                    lethalCompany2.put(player, lethalCompany2.get(player) + "§2§l§n+");
                    guaranteedPlus.remove(player);
                    result+= "\n§2" + "Гарантированный успех" + "\n";
                } else if (player.hasStatusEffect(StatusType.BURNING) && !player.cantBurn() && player.getWoundPyramid().onlyDeadlyLeft() && shouldGetMinusFromBurn) {
                    lethalCompany.put(player, new Pair<>(lethalCompany.get(player).getKey(), lethalCompany.get(player).getValue() + 1));
                    lethalCompany2.put(player, lethalCompany2.get(player) + "§6§l-");
                    result+= "\n§4" + player.getName() + " горит!\n";
                } else if (player.hasStatusEffect(StatusType.BLEEDING) && player.getBloodloss() > 99 && !player.cantBleed()) {
                    lethalCompany.put(player, new Pair<>(lethalCompany.get(player).getKey(), lethalCompany.get(player).getValue() + 1));
                    lethalCompany2.put(player, lethalCompany2.get(player) + "§c§l-");
                    result+= "\n§4" + player.getName() + " истекает кровью!\n";
                } else if (sdm.getDice().getResult() > 2 && player.hasStatusEffect(StatusType.BURNING) && !player.cantBurn()) {
                     //lethalCompany.put(player, new Pair<>(lethalCompany.get(player).getKey(), lethalCompany.get(player).getValue() + 1));
                     lethalCompany2.put(player, lethalCompany2.get(player) + "§7§l=");
                     result+= "\n§2" + sdm.toString() + "\n";
                     result+= "§7Горение отменяет положительный результат...\n";
                 } else if (sdm.getDice().getResult() > 2) {
                    lethalCompany.put(player, new Pair<>(lethalCompany.get(player).getKey() + 1, lethalCompany.get(player).getValue()));
                    lethalCompany2.put(player, lethalCompany2.get(player) + "§2§l+");
                    result+= "\n§2" + sdm.toString() + "\n";
                } else {
                    lethalCompany.put(player, new Pair<>(lethalCompany.get(player).getKey(), lethalCompany.get(player).getValue() + 1));
                    lethalCompany2.put(player, lethalCompany2.get(player) + "§4§l-");
                    result+= "\n§4" + sdm.toString() + "\n";
                }

                if (lethalCompany.get(player).getValue() >= 3) {
                    SkillDiceMessage sdm2 = new SkillDiceMessage(player.getName(), "% выносливость против смертельной");
                    sdm2.build();
                    if (sdm2.getDice().getResult() > 4) {
                        lethalCompany.put(player, new Pair<>(3, lethalCompany.get(player).getValue()));
                        result+= "§2" + sdm2.toString().replaceAll("§.", "") + "\n";
                        ServerProxy.sendMessageFromAndInformMasters(player, new Message(player.getName() + " чудом выживает.", ChatColor.DARK_GREEN));
                    } else {
                        result+= "§4" + sdm2.toString().replaceAll("§.", "") + "\n";
                        ServerProxy.sendMessageFromAndInformMasters(player, new Message(player.getName() + " перестает дышать.", ChatColor.DARK_RED));
                        player.getWoundPyramid().addWound(WoundType.DEADLY);
                    }
                    Player gm = DataHolder.inst().getMasterForPlayerInCombat(player);
                    gm.performCommand("/combat remove " + player.getName());
                    inCombat = false;
                } else if (lethalCompany.get(player).getKey() >= 3) {
                    ServerProxy.sendMessageFromAndInformMasters(player, new Message(player.getName() + " стабилизируется.", ChatColor.DARK_GREEN));
                    Player gm = DataHolder.inst().getMasterForPlayerInCombat(player);
                    gm.performCommand("/combat remove " + player.getName());
                    inCombat = false;
                }
            }

            result += prefix + player.getName() + " ";
            result += lethalCompany2.get(player);
            result += postfix + "\n";
            if (inCombat) player.processStatusEffects(StatusEnd.TURN_END);
        }
        return result.trim();
    }

    public String lethalCompanyToString() {
        String result = "";
        for (Player player : lethalCompany.keySet()) {
            result += "§a" + player.getName() + " ";
            for (int i = 0; i < lethalCompany.get(player).getKey(); i++) {
                result += "§2§l+";
            }
            for (int i = 0; i < lethalCompany.get(player).getValue(); i++) {
                result += "§4§l-";
            }
            result += "\n";
        }
        return result.trim();
    }

    public void addGuaranteedPlus(Player player) {
        guaranteedPlus.add(player);
    }
}
