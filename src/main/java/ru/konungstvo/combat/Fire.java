package ru.konungstvo.combat;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextFormatting;
import noppes.npcs.api.NpcAPI;
import noppes.npcs.api.constants.AnimationType;
import noppes.npcs.entity.EntityNPCInterface;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.FudgeDiceMessage;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.equipment.Shield;
import ru.konungstvo.combat.movement.MovementTracker;
import ru.konungstvo.commands.executor.NextExecutor;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.kmrp_lore.helpers.WeaponDurabilityHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.NPC;
import ru.konungstvo.player.Player;
import ru.konungstvo.player.wounds.WoundType;

import java.io.Serializable;
import java.util.*;

public class Fire implements Serializable, Cloneable {
    private int combatid;
    private ArrayList<Player> targetsList;
    private HashMap<Player, String> defenseMap;
    private HashMap<Player, Integer> defendersDices;
    private HashMap<Player, Integer> coverMap;
    private FudgeDiceMessage attackerDice;
    private BlockPos attackerBlockPos;
    private Vec3d attackerVec;
    private Vec3d lookVec;
    private int fireDistance;

    private double fireAngle;


    private double posX;
    private double posY;
    private double posZ;

    private double eyeHeight;

    private double angleX;


    private int dimension;



    public Fire(EntityPlayerMP playerMP, int fireDistance, int id, int dimension, double fireAngle, SkillDiceMessage sdm) {
        this.targetsList = new ArrayList<>();
        this.defenseMap = new HashMap<Player, String>();
        this.defendersDices = new HashMap<Player, Integer>();
        this.coverMap = new HashMap<Player, Integer>();
        this.combatid = id;
        this.dimension = dimension;
        this.posX = Math.floor(playerMP.posX) + 0.5;
        this.posY = playerMP.posY;
        this.posZ = Math.floor(playerMP.posZ) + 0.5;
        this.attackerBlockPos = new BlockPos(posX, posY, posZ);
        this.attackerVec = new Vec3d(posX, posY, posZ);
        this.lookVec = playerMP.getLookVec();
        this.eyeHeight = playerMP.getEyeHeight();
        this.angleX = playerMP.rotationPitch;
        this.fireDistance = fireDistance;
        this.attackerDice = sdm;
        this.fireAngle = fireAngle;
    }

    public void addTarget(Player player) {
        if (!targetsList.contains(player)) targetsList.add(player);
    }

    public void addTargets(ArrayList<Player> targets) {
        targetsList = targets;
    }

    public void removeTarget(Player player) {
        targetsList.remove(player);
    }

    public ArrayList<Player> getTargetsList() {
        return targetsList;
    }

    public boolean hasTarget(Player player) {
        return targetsList.contains(player);
    }

    public void addDefense(Player player, String defense) {
        removeTarget(player);
        defenseMap.put(player, defense);
    }

    public void addCover(Player player, Integer cover) {
        //removeTarget(player);
        coverMap.put(player, cover);
    }

    public void removeCover(Player player) {
        //removeTarget(player);
        coverMap.remove(player);
    }

    public Integer getCover(Player player) {
        //removeTarget(player);
        if (!coverMap.containsKey(player)) return 0;
        return coverMap.get(player);
    }

    public String getTargetsString() {
        StringBuilder targetsName = new StringBuilder();
        for (Player target : targetsList) {
            targetsName.append(target.getName()).append(" ");
            if (DataHolder.inst().isNpc(target.getName())) continue;
            target.performCommand("/fired");
        }

        return targetsName.toString().trim();
    }

    public void setAttackerDice(FudgeDiceMessage fdm) {
        this.attackerDice = fdm;
    }


    public void doFire() {
        for (Map.Entry<Player, String> defense : defenseMap.entrySet()) {
            int armor = 0;
            int shield = 0;
            int fulldamage = 8;
            int partdamage = 5;
            int tinydamage = 3;
            if (defense.getValue().equals("safe")) {
                continue;
            } else {
                Player player = defense.getKey();
                System.out.println(player.getName());

                if (player.cantBurn()) {
                    Message msg = new Message("Игрок " + player.getName() + " невосприимчив к огню.", ChatColor.RED);
                    ServerProxy.sendMessageFromAndInformMasters(player, msg);
                    continue;
                }

                Message wound = new Message("", ChatColor.RED);
                EntityLivingBase forgePlayer = player.getEntity();
                Vec3d blockvectorplayer = new Vec3d(Math.floor(forgePlayer.posX) + 0.5, forgePlayer.posY, Math.floor(forgePlayer.posZ) + 0.5);
                Vec3d vector = blockvectorplayer.subtract(attackerVec);
                double angleDir = (Math.atan2(vector.z, vector.x) / 2 / Math.PI * 360 + 360) % 360;
                double angleLook = (Math.atan2(lookVec.z, lookVec.x) / 2 / Math.PI * 360 + 360) % 360;
                double angle = (angleDir - angleLook + 360) % 360;

                String type = "";
                MessageComponent wound1 = null;
                boolean failBlock = false;
                boolean failConst = false;
                int magicShield = player.getMagicShield();

                double c = MovementTracker.getDistanceBetween(
                        posX, posY + eyeHeight, posZ,
                        Math.floor(forgePlayer.posX) + 0.5, forgePlayer.posY, Math.floor(forgePlayer.posZ) + 0.5);
                System.out.println("distance: " + c);
                double a = posY + eyeHeight - forgePlayer.posY;
                System.out.println("Ydiff: " + a);
                double angle2 = Math.sin(a/c) * 100;

                if (defense.getValue().contains("blocking") || defense.getValue().contains("constitution") || defense.getValue().contains("nothing")) {
                    if ((!(angle <= fireAngle/2 || angle >= (360-fireAngle/2)) || Math.abs(angle2 - angleX) > 40 || fireDistance < c)) {
                        wound1 = new MessageComponent("Игрок " + player.getName() + " вне зоны поражения!", ChatColor.RED);
                        wound.addComponent(wound1);
                        ServerProxy.sendMessageFromAndInformMasters(player, wound);
                        continue;
                    }
                }

                if (defense.getValue().contains("blocking")) {

                    int hand = Integer.parseInt(defense.getValue().split(":")[1]);
                    ItemStack shieldIs = player.getItemForHand(hand);
                    if (WeaponTagsHandler.hasWeaponTags(shieldIs)) {
                        WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(player.getItemForHand(hand));
                        if (weaponTagsHandler.isShield()) {
                            Shield shieldS = new Shield(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon());
                            if (!shieldS.getType().equals("малый")) {
                                SkillDiceMessage sdm = new SkillDiceMessage(player.getName(), "% блокирование");
                                sdm.build();
                                ServerProxy.sendMessageFromAndInformMasters(player, sdm);
                                //ServerProxy.informDistantMasters(player, sdm, Range.NORMAL.getDistance());
                                if (sdm.getDice().getResult() >= attackerDice.getDice().getResult()) {
                                    shield = shieldS.getDamage();
                                    WeaponDurabilityHandler defenderDurHandler = player.getDurHandlerForHand(hand);
                                    if (shieldS.getType().equals("большой")) shield++;
                                    if (defenderDurHandler != null && defenderDurHandler.hasDurabilityDict()) {
                                        if (defenderDurHandler.getPercentageRatio() <= 25) {
                                            int ratio = defenderDurHandler.getPercentageRatio();
                                            if (ratio < 0) {
                                                shield = 0;
                                            } else if (ratio == 0) {
                                                shield = shield / 2;
                                            } else {
                                                Random random = new Random();
                                                int randomResult = random.nextInt(100) + 1;
                                                System.out.println(randomResult);
                                                if (randomResult <= (26 - ratio)) {
                                                    shield -= 1;
                                                }
                                            }
                                        }
                                        if (fulldamage > shield) {
                                            defenderDurHandler.takeAwayDurability(Math.max(1, Math.min(fulldamage - shield, shield)));
                                        }
                                    }

                                    type = player.inflictDamage(fulldamage - shield - magicShield, "Огнемёт");
                                    if (magicShield != 0 && (fulldamage - shield - magicShield) > 0) {
                                        player.adjustMagicShield(fulldamage - shield + magicShield);
                                    }
                                    wound1 = new MessageComponent("Игрок " + player.getName() + " блокирует огнемётный залп! (§4"+fulldamage+"§6-§9" + magicShield + "§6-§1" + shield + "§6=§4" + (fulldamage - shield - magicShield) + "§c). " + (type == null ? "Игрок не получает урона и не загорается!" : "Нанесена " + type + ", игрок загорается!") , ChatColor.RED);
                                    wound1.setHoverText("Успешное блокирование.", TextFormatting.GRAY);
                                    if (type != null) player.takeAwayDurFromAllArmor(Math.max(fulldamage - shield - magicShield, 0));
                                    if (type != null) player.addStatusEffect("горение", StatusEnd.TURN_END, 5, StatusType.BURNING);
                                } else if (player.hasDefensiveStance()) {
                                    Vec3d vector1 = attackerVec.subtract(blockvectorplayer);
                                    Vec3d playerDirection = forgePlayer.getLookVec();
                                    double angleDir1 = (Math.atan2(vector1.z, vector1.x) / 2 / Math.PI * 360 + 360) % 360;
                                    double angleLook1 = (Math.atan2(playerDirection.z, playerDirection.x) / 2 / Math.PI * 360 + 360) % 360;
                                    double angle1 = (angleDir1 - angleLook1 + 360) % 360;

                                    System.out.println("angle1: " + angle);
                                    if (angle1 <= 67.5 || angle1 >= 292.5) {
                                        int diff = attackerDice.getDice().getResult() - sdm.getDice().getResult();
                                        shield = shieldS.getDamage();
                                        if (shieldS.getType().equals("большой")) shield++;
                                        WeaponDurabilityHandler defenderDurHandler = player.getDurHandlerForHand(hand);

                                        if (defenderDurHandler != null && defenderDurHandler.hasDurabilityDict()) {
                                            if (defenderDurHandler.getPercentageRatio() <= 25) {
                                                int ratio = defenderDurHandler.getPercentageRatio();
                                                if (ratio < 0) {
                                                    shield = 0;
                                                } else if (ratio == 0) {
                                                    shield = shield / 2;
                                                } else {
                                                    Random random = new Random();
                                                    int randomResult = random.nextInt(100) + 1;
                                                    System.out.println(randomResult);
                                                    if (randomResult <= (26 - ratio)) {
                                                        shield -= 1;
                                                    }
                                                }
                                            }
                                            if (fulldamage > shield) {
                                                defenderDurHandler.takeAwayDurability(Math.max(1, Math.min(fulldamage - shield, shield)));
                                            }
                                        }

                                        if (diff >= shield) failBlock = true;
                                        else {
                                            type = player.inflictDamage(fulldamage - (shield - diff) - magicShield, "Огнемёт");
                                            if (magicShield != 0 && (fulldamage - (shield - diff)) > 0) {
                                                player.adjustMagicShield(fulldamage - (shield - diff) + magicShield);
                                            }
                                            wound1 = new MessageComponent("Игрок " + player.getName() + " блокирует огнемётный залп! (§4"+fulldamage+"§6-§9" + magicShield + "§6-§1(" + shield + "-" + diff +")§6=§4" + (fulldamage - (shield - diff) - magicShield) + "§c). " + (type == null ? "Игрок не получает урона и не загорается!" : "Нанесена " + type + ", игрок загорается!") , ChatColor.RED);
                                            wound1.setHoverText("Успешное блокирование благодаря глухой обороне.", TextFormatting.GRAY);
                                            if (type != null) player.takeAwayDurFromAllArmor(Math.max((fulldamage - (shield - diff) - magicShield) / 2, 0));
                                            if (type != null) player.addStatusEffect("горение", StatusEnd.TURN_END, 5, StatusType.BURNING);

                                        }
                                    }
                                } else {
                                    failBlock = true;
                                }
                            }
                        }
                    }
                } else if (defense.getValue().contains("constitution")) {
                    SkillDiceMessage sdm = new SkillDiceMessage(player.getName(), "% группировка");
                    sdm.build();
                    ServerProxy.sendMessageFromAndInformMasters(player, sdm);
                    if (sdm.getDice().getResult() > attackerDice.getDice().getResult()) {
                        type = player.inflictDamage(partdamage - magicShield, "Огнемёт");
                        if (magicShield != 0) {
                            player.adjustMagicShield(partdamage);
                        }
                        if (type == null) {
                            //(Не удалось покинуть зону поражения)
                            wound1 = new MessageComponent("Игрок " + player.getName() + " сдерживает огнемётный залп магическим щитом! (§4" + partdamage + "§6-§9" + magicShield + "§6=§4" + (partdamage - shield - magicShield) + "§c). " + (type == null ? "Игрок не получает урона и не загорается!" : "Нанесена " + type + ", игрок загорается!"), ChatColor.RED);
                        } else {
                            player.addStatusEffect("горение", StatusEnd.TURN_END, 5, StatusType.BURNING);
                            //(Не удалось покинуть зону поражения)
                            wound1 = new MessageComponent("Игроку " + player.getName() + " огнеметом нанесена " + type + ", игрок загорается!", ChatColor.RED);
                            wound1.setHoverText("Успешная группировка.", TextFormatting.GRAY);
                            player.takeAwayDurFromAllArmor(tinydamage/2);
                        }
                    } else {
                        failConst = true;
                    }
                } else if (defense.getValue().contains("parry")) {
                    if (defendersDices.get(player) > attackerDice.getDice().getResult()) {
                        type = player.inflictDamage(tinydamage - magicShield, "Огнемёт");
                        if (magicShield != 0) {
                            player.adjustMagicShield(tinydamage);
                        }
                        if (type == null) {
                            //(Не удалось покинуть зону поражения)
                            wound1 = new MessageComponent("Игрок " + player.getName() + " сдерживает огнемётный залп магическим щитом! (§4" + partdamage + "§6-§9" + magicShield + "§6=§4" + (partdamage - shield - magicShield) + "§c). " + (type == null ? "Игрок не получает урона и не загорается!" : "Нанесена " + type + ", игрок загорается!"), ChatColor.RED);
                        } else {
                            player.addStatusEffect("горение", StatusEnd.TURN_END, 5, StatusType.BURNING);
                            //(Не удалось покинуть зону поражения)
                            wound1 = new MessageComponent("Игроку " + player.getName() + " огнеметом нанесена " + type + ", игрок загорается!", ChatColor.RED);
                            wound1.setHoverText("Успешное парирование.", TextFormatting.GRAY);
                            player.takeAwayDurFromAllArmor(tinydamage/2);
                        }
                    } else {
                        failConst = true;
                    }
                }


                if (wound1 == null) {
                    if ((!(angle <= fireAngle/2 || angle >= (360-fireAngle/2)) || Math.abs(angle2 - angleX) > 40 || fireDistance < c)) {
                        if (defendersDices.containsKey(player) && attackerDice.getDice().getResult() < defendersDices.get(player)) {
                            wound1 = new MessageComponent("Игрок " + player.getName() + " успешно уходит от залпа!", ChatColor.RED);
                            wound1.setHoverText("Удалось покинуть зону поражения и бросок отпрыгивания выше броска атаки.", TextFormatting.GRAY);
                            wound.addComponent(wound1);
                            ServerProxy.sendMessageFromAndInformMasters(player, wound);
                            continue;
                        }
                        type = player.inflictDamage(partdamage - magicShield, "Огнемёт");
                        if (magicShield != 0) {
                            player.adjustMagicShield(partdamage);
                        }
                        if (type == null) {
                            //(Не удалось покинуть зону поражения)
                            wound1 = new MessageComponent("Игрок " + player.getName() + " сдерживает огнемётный залп магическим щитом! (§4"+partdamage+"§6-§9" + magicShield + "§6=§4" + (partdamage - shield - magicShield) + "§c). " + (type == null ? "Игрок не получает урона и не загорается!" : "Нанесена " + type + ", игрок загорается!") , ChatColor.RED);
                            wound1.setHoverText("Удалось покинуть зону поражения, но не удалось преодолеть бросок атаки.", TextFormatting.GRAY);
                        } else {
                            player.addStatusEffect("горение", StatusEnd.TURN_END, 5, StatusType.BURNING);
                            //(Не удалось покинуть зону поражения)
                            wound1 = new MessageComponent("Игроку " + player.getName() + " огнеметом нанесена " + type + ", игрок загорается!", ChatColor.RED);
                            wound1.setHoverText("Удалось покинуть зону поражения, но не удалось преодолеть бросок атаки.", TextFormatting.GRAY);
                            player.takeAwayDurFromAllArmor(partdamage/2);
                        }
                    } else {
                        if (!defense.getValue().contains("nothing") && defendersDices.containsKey(player) && attackerDice.getDice().getResult() < defendersDices.get(player)) {
                            if (magicShield != 0) {
                                player.adjustMagicShield(tinydamage);
                            }
                            if (magicShield >= tinydamage) {
                                //(Не удалось покинуть зону поражения)
                                wound1 = new MessageComponent("Игрок " + player.getName() + " сдерживает огнемётный залп магическим щитом!" , ChatColor.RED);
                                wound1.setHoverText("Не удалось покинуть зону поражения, но бросок отпрыгивания выше броска атаки.", TextFormatting.GRAY);
                            } else {
                                type = player.inflictDamage(tinydamage, "Огнемёт");
                                player.addStatusEffect("горение", StatusEnd.TURN_END, 5, StatusType.BURNING);
                                //(Бросок защиты ниже броска атаки)
                                wound1 = new MessageComponent("Игроку " + player.getName() + " огнеметом нанесена " + type + ", игрок загорается!", ChatColor.RED);
                                wound1.setHoverText("Не удалось покинуть зону поражения, но бросок отпрыгивания выше броска атаки.", TextFormatting.GRAY);
                                player.takeAwayDurFromAllArmor(tinydamage /2);

                            }
                        } else {
                            type = player.inflictDamage(fulldamage - magicShield, "Огнемёт");
                            if (magicShield != 0) {
                                player.adjustMagicShield(fulldamage);
                            }
                            if (type == null) {
                                wound1 = new MessageComponent("Игрок " + player.getName() + " сдерживает огнемётный залп магическим щитом! (§47§6-§9" + magicShield + "§6=§4" + (7 - shield - magicShield) + "§c). " + (type == null ? "Игрок не получает урона и не загорается!" : "Нанесена " + type + ", игрок загорается!") , ChatColor.RED);
                                wound1.setHoverText("Не удалось покинуть зону поражения и не удалось преодолеть бросок атаки.", TextFormatting.GRAY);
                            } else {
                                player.addStatusEffect("горение", StatusEnd.TURN_END, 5, StatusType.BURNING);
                                //(Не удалось покинуть зону поражения и бросок защиты ниже броска атаки)
                                wound1 = new MessageComponent("Игроку " + player.getName() + " огнеметом нанесена " + type + ", игрок загорается!", ChatColor.RED);
                                wound1.setHoverText("Не удалось покинуть зону поражения и не удалось преодолеть бросок атаки.", TextFormatting.GRAY);
                                player.takeAwayDurFromAllArmor(fulldamage/2);
                                if (failBlock) wound1 = new MessageComponent("Игроку " + player.getName() + " огнеметом нанесена " + type + ", игрок загорается!", ChatColor.RED);
                                if (failConst) wound1 = new MessageComponent("Игроку " + player.getName() + " огнеметом нанесена " + type + ", игрок загорается!", ChatColor.RED);
                            }
                        }
                    }
                }
                wound.addComponent(wound1);
                ServerProxy.sendMessageFromAndInformMasters(player, wound);
                if (type != null && (type.equals(WoundType.CRITICAL.getDesc()) || type.equals(WoundType.DEADLY.getDesc()))) {
                    try {
                        if (DataHolder.inst().isNpc(player.getName())) {
                            EntityNPCInterface npc = (EntityNPCInterface) DataHolder.inst().getNpcEntity(player.getName());
                            npc.wrappedNPC.getAi().setAnimation(AnimationType.SLEEP);
                            String desc = npc.wrappedNPC.getDisplay().getTitle();
                            npc.wrappedNPC.getDisplay().setTitle((desc + " [крит]").trim());
                            npc.wrappedNPC.getDisplay().setHasLivingAnimation(false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    //actualDefender.makeFall();
                    try {
                        Player gm = DataHolder.inst().getMasterForPlayerInCombat(player);
                        gm.performCommand("/queue remove " + player.getName());
                        //gm.performCommand("/combat remove " + player.getName());
                        Combat combat = DataHolder.inst().getCombatForPlayer(player.getName());
                        if (player.getWoundPyramid().getNumberOfWounds(WoundType.DEADLY) == 0) {
                            if (combat != null) combat.addLethalCompany(player);
                        } else {
                            gm.performCommand("/combat remove " + player.getName());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    if (DataHolder.inst().isNpc(player.getName())) {
                        NPC npc = (NPC) DataHolder.inst().getPlayer(player.getName());
                        npc.setWoundsInDescription();
                    }
                }
            }
        }
        //DataHolder.inst().getCombat(combatid).removeBoom(this);
        NpcAPI.Instance().getIWorld(dimension).playSoundAt(NpcAPI.Instance().getIPos(posX, posY, posZ), "minecraft:entity.creeper.primed",1, 1);
        targetsList = new ArrayList<>();

        Player firer = DataHolder.inst().getPlayer(attackerDice.getPlayerName());
        if (firer.getCombatState() == CombatState.SHOULD_ACT) {
            if (DataHolder.inst().isNpc(firer.getName())) {
                DataHolder.inst().getMasterForNpcInCombat(firer).performCommand("/" + NextExecutor.NAME);
            } else {
                firer.performCommand("/" + NextExecutor.NAME);
            }
        }
    }

    public void addDefenderDice(Player player, int result) {
        defendersDices.put(player, result);
    }

    public BlockPos getAttackerBlockPos() {
        return attackerBlockPos;
    }

    public FudgeDiceMessage getAttackerDice() {
        return attackerDice;
    }

}
