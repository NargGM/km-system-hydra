package ru.konungstvo.combat;

public enum SkillType {

    SIMPLE("обычный"),
    MASTERING("владение"),
    PARRY("парирование"),
    BLOCK("блокирование"),
    EVADE("уклонение"),
    CONSTITUTION("группировка"),
    MISSILE("дальнобой"),
    MAGIC("магия");

    private String desc;
    SkillType(String desc) {
        this.desc = desc;
    }

    public static SkillType getType(String skill) {
        for (SkillType type : SkillType.values()) {
            if (type.toString().contains(skill))
                return type;
        }
        return SIMPLE;
    }

    @Override
    public String toString() {
        return desc;
    }
}
