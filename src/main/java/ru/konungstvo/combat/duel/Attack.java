package ru.konungstvo.combat.duel;

import ru.konungstvo.chat.message.FudgeDiceMessage;
import ru.konungstvo.combat.dice.modificator.Modificator;
import ru.konungstvo.combat.dice.modificator.ModificatorType;
import ru.konungstvo.combat.equipment.Projectile;
import ru.konungstvo.combat.equipment.Weapon;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.ModifiersState;
import ru.konungstvo.player.Player;

public class Attack {
    private FudgeDiceMessage diceMessage;
    private boolean clack = false;


    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public Weapon weapon;
    private int diceMod;
    private int damageMod;



    //public Attack(FudgeDiceMessage diceMessage, Equipment weapon) {
    //    this(diceMessage, weapon, false, null);
    //}
    public Attack(FudgeDiceMessage diceMessage, Weapon weapon) {
        this.diceMessage = diceMessage;
        this.weapon = weapon;
        this.diceMod = 0;
        this.damageMod = 0;
        checkWeaponReqs();
        try {
            checkModules();
        } catch (Exception ignored) {}
        try {
            checkBipod();
        } catch (Exception ignored) {}
        try {
            checkAmmo();
        } catch (Exception ignored) {}
    }

    public void checkModules() {
        /* МОДУЛИ */
        int modulesMod = this.getWeapon().getFirearm().getModulesMods();
        if (modulesMod != 0) {
            diceMessage.getDice().getPercentDice().setModules(modulesMod);
        }
    }

    public void checkBipod() {
        Player player = DataHolder.inst().getPlayer(diceMessage.getPlayerName());
        if (player.hasBipods()) {
            diceMessage.getDice().getPercentDice().setBipod(20);
        }
    }

    public void checkAmmo() {
        Player player = DataHolder.inst().getPlayer(diceMessage.getPlayerName());
        if (this.getWeapon().getFirearm().isModern()) {
            Projectile ammo = this.getWeapon().getFirearm().getLoadedProjectile();
            if (ammo.getMod().contains("дротик")) {
                diceMessage.getDice().getPercentDice().setAmmo(10);
            } else if (ammo.getMod().contains("флешетты")) {
                diceMessage.getDice().getPercentDice().setAmmo(-20);
            }
        }
    }

    private void checkWeaponReqs() {
        Player player = DataHolder.inst().getPlayer(diceMessage.getPlayerName());
        int strength =  player.getSkillLevelAsInt("сила");
        if (weapon.isMelee()) {
            int req = weapon.getMelee().getStrengthReq();
            if (strength < req) {
                diceMessage.getDice().addMod(new Modificator(strength-req, ModificatorType.TWOHANDED));
            }
        } else if (weapon.isRanged()) {
            int req = weapon.getFirearm().getStrengthReq();
            if (player.hasStation()) {
                req -= 1;
            }
            if (strength < req) {
                diceMessage.getDice().addMod(new Modificator(strength-req, ModificatorType.FIREARM_STRENGTH));
            }
        }
    }

    public int getDiceMod() {
        return diceMod;
    }

    public void setDiceMod(int diceMod) {
        this.diceMod = diceMod;
    }

    public int getDamageMod() {
        return damageMod;
    }

    public void setDamageMod(int damageMod) {
        this.damageMod = damageMod;
    }

    public FudgeDiceMessage getDiceMessage() {
        return diceMessage;
    }

    public void setDiceMessage(FudgeDiceMessage diceMessage) {
        this.diceMessage = diceMessage;
    }


    public boolean isClack() {
        return clack;
    }

    public void setClack(boolean clack) {
        this.clack = clack;
    }

    @Override
    public String toString() {
        return "Attack{" +
                "diceResult=" + diceMessage.getDice().getResult() +
                '}';
    }
}
