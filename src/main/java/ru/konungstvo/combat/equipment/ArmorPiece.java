package ru.konungstvo.combat.equipment;

import java.io.Serializable;

public class ArmorPiece implements Serializable {
    private BodyPart part;
    private boolean modern;
    private ArmorType type;

    public ArmorPiece(BodyPart part, boolean modern, ArmorType type) {
        this.part = part;
        this.modern = modern;
        this.type = type;
    }

    public BodyPart getPart() {
        return part;
    }

    public void setPart(BodyPart part) {
        this.part = part;
    }

    public boolean isModern() {
        return modern;
    }

    public void setModern(boolean modern) {
        this.modern = modern;
    }

    public ArmorType getType() {
        return type;
    }

    public void setType(ArmorType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ArmorPiece{" +
                "part=" + part +
                ", modern=" + modern +
                ", type=" + type +
                '}';
    }

    public String toNiceString() {
        System.out.println(type);
        if (type == null )return "§f[" + getPart().getName() + "]";
        switch (type) {
               case LIGHT:
                    if (isModern()) return "§a[" + getPart().getName() + "]";
                    return "§e[" + getPart().getName() + "]";
                case MEDIUM:
                    if (isModern()) return "§9[" + getPart().getName() + "]";
                    return "§7[" + getPart().getName() + "]";
                case HEAVY:
                    if (isModern()) return "§5[" + getPart().getName() + "]";
                    return "§8[" + getPart().getName() + "]";
                default:
                    return "§f[" + getPart().getName() + "]";
        }
    }
}
