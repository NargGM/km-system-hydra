package ru.konungstvo.combat.equipment;

public enum MeleeCategory {

    CHOPPING("рубящее"),
    PIERCING("колющее"),
    SLASHING("режущее"),
    CRUSHING("ударно-дробящее"),
    PENETRATING("ударно-пробойное"),
    FLEXIBLE("гибко-суставчатое"),
    DISSECTING("ударно-рассекающее");




    String desc;

    MeleeCategory(String desc) {
        this.desc = desc;
    }

    public static MeleeCategory get(String categoryStr) {
        for (MeleeCategory meleeCategory : MeleeCategory.values()) {
            if (meleeCategory.desc.equals(categoryStr)) {
                return meleeCategory;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return desc;
    }

    public String get() {
        return desc;
    }


}
