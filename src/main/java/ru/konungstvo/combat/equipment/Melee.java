package ru.konungstvo.combat.equipment;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;

public class Melee implements WeaponTag {
    private int reach;
    private int damage;
    private int baseDamage;
    private boolean twohanded;
    private boolean small;
    private boolean sharp;
    private boolean magic;
    private boolean nonlethal;
    private boolean polearm;
    private MeleeCategory category;
    private String modsplus = "";
    private int cost = -1;

    boolean needAmmo = false;
    int energy = -666;
    private ArrayList<String> proficiencies = new ArrayList<String>();

    private int cooldown = -1;
    private int difficulty = -666;
    private boolean prepare = false;

    protected Melee(String name) {
        //super(name);
    }

    public void fillFromNBT(NBTTagCompound nbtTagCompound, int damage, ArrayList<String> proficiencies) throws ParseException {
        System.out.println("!!!fillFromNBT");
        System.out.println("!!!nbtTagCompound " + nbtTagCompound.toString());
        this.reach = nbtTagCompound.getInteger("reach") + 1;
        this.damage = damage;
        this.proficiencies = proficiencies;
        this.baseDamage = nbtTagCompound.getInteger("baseDamage");
        String categoryStr = nbtTagCompound.getString("category");
        this.category = MeleeCategory.get(categoryStr);
        NBTTagList factors = nbtTagCompound.getTagList("factors", 8);
        System.out.println("!!!does even have factors??? " + nbtTagCompound.hasKey("factors"));
        System.out.println("!!!how many??? " + factors.tagCount());
        System.out.println("!!!factors " + factors.toString());
        for (int i = 0; i < factors.tagCount(); i++) {
            NBTBase factor = factors.get(i);
            System.out.println(factor.toString());
            if (factor.toString().equals("\"двуручное\"")) {
                this.twohanded = true;
                System.out.println("двуручное: " + twohanded);
            }
            if (factor.toString().equals("\"короткое\"")) {
                this.small = true;
                System.out.println("короткое: " + small);
            }
            if (factor.toString().equals("\"острое\"")) {
                System.out.println("factor.toString().equals(\"острое\")");
                this.sharp = true;
                System.out.println("острое: " + sharp);
            }
            if (factor.toString().equals("\"магия\"")) {
                this.magic = true;
            }
            if (factor.toString().equals("\"древковое\"")) {
                this.polearm = true;
            }
            if (factor.toString().equals("\"нелетальное\"")) {
                this.nonlethal = true;
            }
        }
        if(nbtTagCompound.hasKey("modsplus")) this.modsplus = nbtTagCompound.getString("modsplus");
        if(nbtTagCompound.hasKey("cost")) this.cost = nbtTagCompound.getInteger("cost");
        if (nbtTagCompound.hasKey("cooldown")) cooldown = nbtTagCompound.getInteger("cooldown");
        if (nbtTagCompound.hasKey("diff")) difficulty = nbtTagCompound.getInteger("diff");
        if (nbtTagCompound.hasKey("prep")) prepare = nbtTagCompound.getBoolean("prep");

        if (nbtTagCompound.hasKey("energy")) energy = nbtTagCompound.getInteger("energy");
        if (nbtTagCompound.hasKey("needAmmo")) needAmmo = nbtTagCompound.getBoolean("needAmmo");
        //if (nbtTagCompound.hasKey("spellname")) spellname = nbtTagCompound.getString("spellname");
    }

    /*
    @Override
    public void fillFromJson(String jsonStr) throws ParseException {
        super.fillFromJson(jsonStr);
        JSONParser parser = new JSONParser();
        JSONObject json;
        json = (JSONObject) parser.parse(jsonStr);
        json = (JSONObject) json.get("melee");
        this.reach = ((Long) json.get("reach")).intValue();
        this.baseDamage = ((Long) json.get("baseDamage")).intValue();

        String categoryStr = (String) json.get("category");
        this.category = MeleeCategory.get(categoryStr);

    }

     */

    public MeleeCategory getCategory() {
        return category;
    }

    public void setCategory(MeleeCategory category) {
        this.category = category;
    }

    @Override
    public int getDamage() {
        return damage;
    }

    public int getBaseDamage() {
        return baseDamage;
    }

    public double getReach() {
        return reach;
    }

    @Override
    public ArrayList<String> getProficiencies() {
        return proficiencies;
    }

    public void setReach(int reach) {
        this.reach = reach;
    }

    public MeleeCategory getMeleeCategory() {
        return category;
    }

    public void setMeleeCategory(MeleeCategory meleeCategory) {
        this.category = meleeCategory;
    }

    public int getStrengthReq() {
        if (twohanded) return 2;
        return -10;
    }

    public boolean isTwohanded() {return twohanded;}
    public boolean isSmall() {return small;}
    public boolean isSharp() {return sharp;}
    public boolean isMagic() {return magic;}
    public boolean isNonlethal() {return nonlethal;}
    public boolean isPolearm() {return polearm;}

    public String getModsplus() {
        return modsplus;
    }

    public int getCost() {
        return cost;
    }

    public int getCooldown() {
        return cooldown;
    }

    public int getDifficulty() { return difficulty; }
    public boolean needPrepare() { return prepare; }
    //public String getSpellname() { return spellname; }

    public boolean spendsAmmo() { return needAmmo || energy > -666; }
    public boolean needAmmo() {return needAmmo; }
    public boolean isEnergy() {
        return energy > -666;
    }
    public Integer getEnergy() {
        return energy;
    }
}
