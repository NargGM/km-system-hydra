package ru.konungstvo.combat.equipment;

import ru.konungstvo.control.Logger;

import java.util.HashMap;
import java.util.Map;

public enum FirearmDistance {
    fd1("Пистолет", "п", new HashMap<>()),
    fd2("Револьвер", "р", new HashMap<>()),
    fd3("Пистолет-пулемет", "пп", new HashMap<>()),
    fd4("Ружье дробь/пуля", "рд", new HashMap<>()),
    fd5("Ружье пуля", "рп", new HashMap<>()),
    fd6("Обрез дробь/пуля", "од", new HashMap<>()),
    fd7("Обрез пуля", "оп", new HashMap<>()),
    fd8("Винтовка", "в", new HashMap<>()),
    fd9("Пулемет", "пул", new HashMap<>()),
    fd10("Гранатомет ручной", "гр", new HashMap<>()),
    fd11("Гранатомет реактивный", "гре", new HashMap<>()),
    fd12("Гранатомет наствольный", "гн", new HashMap<>()),
    fd13("Арбалет/аркебуз", "а", new HashMap<>()),
    fd14("Лук", "л", new HashMap<>()),
    fd15("Длинный лук", "дл", new HashMap<>()),
    fd16("Дульнозарядный пистолет", "дп", new HashMap<>()),
    fd17("Дульнозарядное ружье гладкоствол", "дрг", new HashMap<>()),
    fd18("Дульнозарядное ружье нарезное", "дрн", new HashMap<>()),
    fd19("Метательное холодное (легкое)", "мхл", new HashMap<>()),
    fd20("Метательное холодное (среднее)", "мхс", new HashMap<>()),
    fd21("Метательное холодное (тяжелое)", "мхт", new HashMap<>()),
    fd22("Метательное подручное", "мп", new HashMap<>()),
    fd23("Духовая трубка", "дт", new HashMap<>()),
    fd24("Праща", "пра", new HashMap<>()),
    fd25("Самопал пистолет", "сп", new HashMap<>()),
    fd26("Самопал револьвер", "ср", new HashMap<>()),
    fd27("Самопал ПП", "спп", new HashMap<>()),
    fd28("Самопал ружье дробь/пуля", "срд", new HashMap<>()),
    fd29("Самопал ружье пуля", "срп", new HashMap<>()),
    fd30("Самопал обрез дробь/пуля", "сод", new HashMap<>()),
    fd31("Самопал обрез пуля", "сор", new HashMap<>()),
    fd32("Самопал винтовка", "св", new HashMap<>()),
    fd33("Самопал пулемет", "спул", new HashMap<>()),
    ;

    private String name;
    private String alias;
    private int damage;
    private HashMap<Integer, Integer> distance;

    FirearmDistance(String name, String alias, HashMap<Integer, Integer> distance) {
        this.name = name;
        this.alias = alias;
        this.distance = distance;
        fillTables();
    }

    private void fillTables() {
        switch (this.name) {
            case "Пистолет":
                distance.put(-1, 3);
                distance.put(0, 10);
                distance.put(1, 17);
                distance.put(2, 28);
                distance.put(3, 39);
                distance.put(4, 53);
                distance.put(5, 67);
                distance.put(6, 81);
                break;
            case "Револьвер":
                distance.put(-1, 3);
                distance.put(0,11);
                distance.put(1, 19);
                distance.put(2, 31);
                distance.put(3, 43);
                distance.put(4, 59);
                distance.put(5, 75);
                distance.put(6, 99);
                break;
            case "Пистолет-пулемет":
                distance.put(-1, 3);
                distance.put(0, 12);
                distance.put(1, 21);
                distance.put(2, 35);
                distance.put(3, 49);
                distance.put(4, 67);
                distance.put(5, 85);
                distance.put(6, 112);
                break;
            case "Ружье дробь/пуля":
                distance.put(-1, 5);
                distance.put(0, 11);
                distance.put(1, 19);
                distance.put(2, 31);
                distance.put(3, 43);
                distance.put(4, 59);
                distance.put(5, 75);
                distance.put(6, 99);
                break;
            case "Ружье пуля":
                distance.put(-1, 3);
                distance.put(0, 12);
                distance.put(1, 21);
                distance.put(2, 35);
                distance.put(3, 49);
                distance.put(4, 67);
                distance.put(5, 85);
                distance.put(6, 112);
                break;
            case "Обрез дробь/пуля":
                distance.put(-1, 4);
                distance.put(0, 9);
                distance.put(1, 15);
                distance.put(2, 23);
                distance.put(3, 31);
                distance.put(4, 37);
                distance.put(5, 42);
                distance.put(6, 46);
                break;
            case "Обрез пуля":
                distance.put(-1, 3);
                distance.put(0, 11);
                distance.put(1, 18);
                distance.put(2, 26);
                distance.put(3, 34);
                distance.put(4, 42);
                distance.put(5, 51);
                distance.put(6, 61);
                break;
            case "Винтовка":
                distance.put(-1, 3);
                distance.put(0, 15);
                distance.put(1, 27);
                distance.put(2, 45);
                distance.put(3, 63);
                distance.put(4, 87);
                distance.put(5, 111);
                distance.put(6, 159);
                break;
            case "Пулемет":
                distance.put(-1, 3);
                distance.put(0, 13);
                distance.put(1, 23);
                distance.put(2, 38);
                distance.put(3, 53);
                distance.put(4, 73);
                distance.put(5, 93);
                distance.put(6, 122);
                break;
            case "Гранатомет ручной":
                distance.put(-1, 3);
                distance.put(0, 10);
                distance.put(1, 20);
                distance.put(2, 30);
                distance.put(3, 40);
                distance.put(4, 50);
                break;
            case "Гранатомет реактивный":
                distance.put(-1, 3);
                distance.put(0, 12);
                distance.put(1, 24);
                distance.put(2, 36);
                distance.put(3, 48);
                distance.put(4, 60);
                distance.put(5, 72);
                distance.put(6, 84);
                break;
            case "Гранатомет наствольный":
                distance.put(-1, 3);
                distance.put(0, 8);
                distance.put(1, 16);
                distance.put(2, 24);
                distance.put(3, 32);
                distance.put(4, 40);
                break;
            case "Арбалет/аркебуз":
                distance.put(-1, 3);
                distance.put(0, 11);
                distance.put(1, 18);
                distance.put(2, 29);
                distance.put(3, 40);
                distance.put(4, 54);
                distance.put(5, 68);
                distance.put(6, 89);
                break;
            case "Лук":
                distance.put(-1, 3);
                distance.put(0, 9);
                distance.put(1, 15);
                distance.put(2, 24);
                distance.put(3, 33);
                distance.put(4, 45);
                distance.put(5, 57);
                distance.put(6, 73);
                break;
            case "Длинный лук":
                distance.put(-1, 3);
                distance.put(0, 11);
                distance.put(1, 18);
                distance.put(2, 29);
                distance.put(3, 40);
                distance.put(4, 54);
                distance.put(5, 68);
                distance.put(6, 89);
                break;
            case "Дульнозарядный пистолет":
                distance.put(-1, 3);
                distance.put(0, 8);
                distance.put(1, 13);
                distance.put(2, 21);
                distance.put(3, 29);
                distance.put(4, 39);
                distance.put(5, 49);
                distance.put(6, 64);
                break;
            case "Дульнозарядное ружье гладкоствол":
                distance.put(-1, 3);
                distance.put(0, 11);
                distance.put(1, 19);
                distance.put(2, 31);
                distance.put(3, 43);
                distance.put(4, 55);
                distance.put(5, 67);
                distance.put(6, 79);
                break;
            case "Дульнозарядное ружье нарезное":
                distance.put(-1, 3);
                distance.put(0, 13);
                distance.put(1, 23);
                distance.put(2, 38);
                distance.put(3, 53);
                distance.put(4, 73);
                distance.put(5, 93);
                distance.put(6, 123);
                break;
            case "Метательное холодное (легкое)":
                distance.put(-1, 3);
                distance.put(0, 6);
                distance.put(1, 9);
                distance.put(2, 14);
                distance.put(3, 19);
                distance.put(4, 25);
                distance.put(5, 31);
                distance.put(6, 40);
                break;
            case "Метательное холодное (среднее)":
                distance.put(-1, 3);
                distance.put(0, 7);
                distance.put(1, 11);
                distance.put(2, 17);
                distance.put(3, 23);
                distance.put(4, 31);
                distance.put(5, 39);
                distance.put(6, 51);
                break;
            case "Метательное холодное (тяжелое)":
                distance.put(-1, 8);
                distance.put(0, 8);
                distance.put(1, 13);
                distance.put(2, 21);
                distance.put(3, 29);
                distance.put(4, 39);
                distance.put(5, 49);
                distance.put(6, 54);
                break;

            case "Метательное подручное":
                distance.put(-1, 3);
                distance.put(0, 5);
                distance.put(1, 7);
                distance.put(2, 9);
                distance.put(3, 11);
                distance.put(4, 13);
                distance.put(5, 15);
                break;

            case "Духовая трубка":
                distance.put(-1, 3);
                distance.put(0, 6);
                distance.put(1, 9);
                distance.put(2, 14);
                distance.put(3, 19);
                distance.put(4, 25);
                distance.put(5, 31);
                distance.put(6, 40);
                break;
            case "Праща":
                distance.put(-1, 3);
                distance.put(0, 8);
                distance.put(1, 13);
                distance.put(2, 20);
                distance.put(3, 27);
                distance.put(4, 36);
                distance.put(5, 45);
                distance.put(6, 58);
                break;
            case "Самопал пистолет":
                distance.put(-1, 3);
                distance.put(0, 9);
                distance.put(1, 15);
                distance.put(2, 24);
                distance.put(3, 33);
                distance.put(4, 45);
                distance.put(5, 57);
                distance.put(6, 75);
                break;
            case "Самопал револьвер":
                distance.put(-1, 3);
                distance.put(0, 11);
                distance.put(1, 18);
                distance.put(2, 29);
                distance.put(3, 40);
                distance.put(4, 54);
                distance.put(5, 68);
                distance.put(6, 89);
                break;
            case "Самопал ПП":
                distance.put(-1, 3);
                distance.put(0, 11);
                distance.put(1, 19);
                distance.put(2, 31);
                distance.put(3, 43);
                distance.put(4, 59);
                distance.put(5, 75);
                distance.put(6, 99);
                break;
            case "Самопал ружье дробь/пуля":
                distance.put(-1, 4);
                distance.put(0, 9);
                distance.put(1, 15);
                distance.put(2, 24);
                distance.put(3, 33);
                distance.put(4, 45);
                distance.put(5, 57);
                distance.put(6, 75);
                break;
            case "Самопал ружье пуля":
                distance.put(-1, 3);
                distance.put(0, 11);
                distance.put(1, 19);
                distance.put(2, 31);
                distance.put(3, 43);
                distance.put(4, 59);
                distance.put(5, 75);
                distance.put(6, 99);
                break;
            case "Самопал обрез дробь/пуля":
                distance.put(-1, 4);
                distance.put(0, 7);
                distance.put(1, 11);
                distance.put(2, 17);
                distance.put(3, 23);
                distance.put(4, 31);
                distance.put(5, 39);
                distance.put(6, 51);
                break;
            case "Самопал обрез пуля":
                distance.put(-1, 3);
                distance.put(0, 9);
                distance.put(1, 15);
                distance.put(2, 24);
                distance.put(3, 33);
                distance.put(4, 45);
                distance.put(5, 57);
                distance.put(6, 75);
                break;
            case "Самопал винтовка":
                distance.put(-1, 3);
                distance.put(0, 12);
                distance.put(1, 21);
                distance.put(2, 35);
                distance.put(3, 49);
                distance.put(4, 67);
                distance.put(5, 85);
                distance.put(6, 112);
                break;
            case "Самопал пулемет":
                distance.put(-1, 3);
                distance.put(0, 11);
                distance.put(1, 19);
                distance.put(2, 31);
                distance.put(3, 43);
                distance.put(4, 59);
                distance.put(5, 75);
                distance.put(6, 99);
                break;

        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getAttackDifficulty(double realDistance) {
//        System.out.println("RangedWeapon: realDistance is " + realDistance);
        for (Map.Entry<Integer, Integer> entry : distance.entrySet()) {
//            System.out.println("Max distance for this tier (" + entry.getKey() + ") is " + entry.getValue() + "=" + entry.getValue());
            if (entry.getValue() >= realDistance) {
                return entry.getKey();
            }
        }
        return 666;
    }

    public int getValueForDifficuly(int dif) {
        return distance.get(dif);
    }

    public static FirearmDistance getByName(String str) {
        for (FirearmDistance fd : FirearmDistance.values()) {
            if (fd.getName().equals(str)) return fd;
        }
        return null;
    }

    public int getFalloff(String bullet, int difficulty) {
        switch (bullet) {
            case "мпп":
            case "спп":
            case "кпп":
            case "травматический":
            case "пневматический":
            case "сигнальный заряд":
                if (difficulty == 2) return 1;
                if (difficulty >= 3) return difficulty - 2;
                return 0;
            case "мрд":
            case "срд":
            case "крд":
            case "энергодробь":
                if (difficulty == 1) return 1;
                if (difficulty > 2) return 5;
                if (difficulty == 2) return 3;
                return 0;
            case "мрп":
            case "срп":
            case "крп":
            case "пуля":
            case "патрон":
                if (difficulty >= 2) return difficulty - 1;
                return 0;
            case "болт":
            case "стрела":
            case "длинная стрела":
                if (difficulty >= 3) return difficulty - 2;
                return 0;
            default:
                return 0;
        }
    }

}
