package ru.konungstvo.combat.equipment;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;

import java.util.ArrayList;

public class Weapon extends Equipment {
    //    private int damage = 0;
//    private int reach = 1;
    private String weaponTagName = "";
    private WeaponTag weaponTag;

    // generally shouldn't be used!! except for weapons with no tags, such as Fist and Trash; also except for Shield
    public Weapon(String name) {
        this(name, null, null);
    }

    public Weapon(String name, NBTTagCompound nbtTagCompound) {
        this(name, nbtTagCompound, null);
    }

    public Weapon(String name, NBTTagCompound nbtTagCompound, WeaponTagsHandler handler) {
        super(name);
        if (nbtTagCompound == null) return;
        NBTTagList list = null;
        try {
            if (nbtTagCompound.hasKey(WeaponTagsHandler.MELEE_KEY)) {
                System.out.println("ОРУЖИЕ БЛИЖНЕГО БОЯ");
                weaponTagName = WeaponTagsHandler.MELEE_KEY;
                weaponTag = new Melee("оружие");
                int damage = nbtTagCompound.getInteger("damage");
                System.out.println("damage: " + damage);
                ArrayList<String> proficiencies = new ArrayList<String>();
                if (nbtTagCompound.hasKey("proficiency")) {
                    list = nbtTagCompound.getTagList("proficiency", 8);
                    for (int i = 0; i < list.tagCount(); i++) {
                        proficiencies.add(list.getStringTagAt(i));
                    }
                }
                ((Melee) weaponTag).fillFromNBT(nbtTagCompound.getCompoundTag(WeaponTagsHandler.MELEE_KEY), damage, proficiencies);

            } else if (nbtTagCompound.hasKey(WeaponTagsHandler.FIREARM_KEY)) {
                weaponTagName = WeaponTagsHandler.FIREARM_KEY;

                Projectile projectile = new Projectile("патрон");
                if (nbtTagCompound.getCompoundTag(WeaponTagsHandler.FIREARM_KEY) // для метательного и магии
                        .hasKey("sProj")) {
                    projectile.fillFromNBTList(nbtTagCompound.getCompoundTag(WeaponTagsHandler.FIREARM_KEY)
                            .getTagList("sProj", 10)
                    );
                } else if (nbtTagCompound.getCompoundTag(WeaponTagsHandler.FIREARM_KEY)
                        .getTagList(WeaponTagsHandler.CHAMBER, 10).tagCount() == 0) {
                    projectile = new Projectile("empty");
                    System.out.println("shit ain't loaded");
                } else {
                    projectile.fillFromNBTList(nbtTagCompound.getCompoundTag(WeaponTagsHandler.FIREARM_KEY)
                            .getTagList(WeaponTagsHandler.CHAMBER, 10)
                    );
                }
                ArrayList<String> proficiencies = new ArrayList<String>();
                if (nbtTagCompound.hasKey("proficiency")) {
                    list = nbtTagCompound.getTagList("proficiency", 8);
                    for (NBTBase entry : list) {
                        proficiencies.add(entry.toString().substring(1, entry.toString().length() - 1));
                    }
                }
                weaponTag = new Firearm("оружие", projectile);
                ((Firearm) weaponTag).fillFromNBT(nbtTagCompound.getCompoundTag(WeaponTagsHandler.FIREARM_KEY), proficiencies);
                if  (handler!=null) {
                    ((Firearm) weaponTag).fillModules(handler);
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("CREATED WEAPON: " + this.toString());

    }

    public boolean isLoaded() {
        if (!isRanged()) return false;
        if (getFirearm().isEnergy()) return true;
        System.out.println("shit's name: " + ((Firearm) weaponTag).getProjectile().getName());
        boolean result = ((Firearm) weaponTag).getProjectile().getName().equals("empty"); // костыль пиздец
        System.out.println("res: " + result);
        return !result;
    }

    /*
    @Override
    public void fillFromNBT(NBTTagCompound nbtTagCompound) throws ParseException {
        damage = nbtTagCompound.getInteger("damage");
        if (nbtTagCompound.hasKey(WeaponTagsHandler.MELEE_KEY)) {
            weaponTagName = WeaponTagsHandler.MELEE_KEY;
            weaponTag = new Melee("оружие");
            ((Melee) weaponTag).fillFromNBTNoOverride(nbtTagCompound.getCompoundTag(WeaponTagsHandler.MELEE_KEY));
        }
        else if (nbtTagCompound.hasKey(WeaponTagsHandler.FIREARM_KEY)) {
            weaponTagName = WeaponTagsHandler.FIREARM_KEY;

            Projectile projectile = new Projectile("патрон");
            projectile.fillFromNBTList(nbtTagCompound.getCompoundTag(WeaponTagsHandler.FIREARM_KEY)
                    .getTagList(WeaponTagsHandler.CHAMBER, 10)
            );
            weaponTag = new Firearm("оружие", projectile);
            ((Firearm) weaponTag).fillFromNBTNoOverride(nbtTagCompound.getCompoundTag(WeaponTagsHandler.FIREARM_KEY));
        }

        System.out.println("CREATED WEAPON: " + this.toString());
    }

     */
    @Override
    public void fillFromJson(String jsonStr) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONObject json;
        json = (JSONObject) parser.parse(jsonStr);
//        damage = ((Long) json.get("damage")).intValue();
//        weaponTag = json.get("weaponTag").toString();
//        JSONObject melee = (JSONObject) json.get("melee");
        // reach = ((Long) melee.get("reach")).intValue();

        System.out.println("CREATED WEAPON: " + this.toString());
    }

    public Firearm getFirearm() {
        if (weaponTagName.equals(WeaponTagsHandler.FIREARM_KEY))
            return (Firearm) weaponTag;
        return null;
    }

    public Melee getMelee() {
        if (weaponTagName.equals(WeaponTagsHandler.MELEE_KEY))
            return (Melee) weaponTag;
        return null;
    }

    public int getDamage() {
        if (weaponTagName.equals(WeaponTagsHandler.FIREARM_KEY))
            return ((Firearm) weaponTag).getLoadedProjectile().getDamage();
        if (weaponTag == null) {
            System.out.println("Weapon tag is null! Fuck!");
        }
        return weaponTag.getDamage();
    }

    public double getReach() {
        return weaponTag.getReach();
    }

    public ArrayList<String> getProficiencies() {
        return weaponTag.getProficiencies();
    }


    //public void setReach(int reach) {
    //     this.reach = reach;
    // }

    public boolean isMelee() {
        return weaponTagName.equals(WeaponTagsHandler.MELEE_KEY);
    }

    public boolean isMeleeCategory(MeleeCategory meleeCategory) {
        if (isRanged() && getFirearm().getLoadedProjectile().getMod().contains(meleeCategory.toString())) return true;
        return isMelee() && getMelee().getCategory().equals(meleeCategory);
    }

    public boolean isFist() {
        return getName().equals("fist");
    }

    @Override
    public boolean isRanged() {
        return weaponTagName.equals(WeaponTagsHandler.FIREARM_KEY);
    }

    @Override
    public String toString() {
        return "Weapon{" +
                "name=" + getName() +
                ", isRanged=" + isRanged() +
                ", damage=" + getDamage() +
                ", reach=" + getReach() +
                '}';
    }
}
