package ru.konungstvo.combat.equipment;

import net.minecraft.nbt.NBTTagCompound;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Shield extends Weapon {
    private int defense;
    private String type;
    private String material;

    public Shield(String name, NBTTagCompound nbtTagCompound) {
        super(name);

        if (nbtTagCompound == null) return;
        try {
            fillFromNBT(nbtTagCompound);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void fillFromNBT(NBTTagCompound nbtTagCompound) throws ParseException {
        defense = nbtTagCompound.getCompoundTag("shield").getInteger("defence");
        type = nbtTagCompound.getCompoundTag("shield").getString("category");
        material = nbtTagCompound.getCompoundTag("shield").getString("material");
    }

    @Override
    public void fillFromJson(String jsonStr) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONObject json;
        json = (JSONObject) parser.parse(jsonStr);
        JSONObject melee = (JSONObject) json.get("shield");
        defense = ((Long) melee.get("defence")).intValue();
        type = (String) melee.get("category");
        material = (String) melee.get("material");

        System.out.println("CREATED WEAPON: " + this.toString());
    }

    @Override
    public int getDamage() {
        return defense;
    }

    @Override
    public double getReach() {
        return 0;
    }

    public boolean canBeUsedActively() {
        return (!material.equals("стационарный") ||
                getDamage() != 3) && getDamage() != 4;
    }

    public int getStrengthRequirement() {
        switch (getType()) {
            case "малый":
                if (getDamage() == 4) return 1;
                return -10;

            case "средний":
                if (getDamage() == 4) return 3;
                if (getDamage() == 3) return 2;
                return -10;

            case "большой":
                if (getDamage() == 4) return 4;
                if (getDamage() == 3) return 3;
                if (getDamage() == 2) return 2;
                if (getDamage() == 1) return 1;
                return -10;

            case "стационарный":
                if (getDamage() == 4) return 5;
                if (getDamage() == 3) return 4;
                if (getDamage() == 2) return 3;
                if (getDamage() == 1) return 2;
                return -10;

            default:
                return -10;
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMaterial() {
        return material;
    }

    public int getDefense() {
        return defense;
    }
}
