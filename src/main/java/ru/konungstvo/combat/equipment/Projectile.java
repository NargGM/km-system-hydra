package ru.konungstvo.combat.equipment;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;

public class Projectile extends Equipment {
    private int damage;
    private String caliber;
    private String mod;
    private boolean infinite = false;
    private String modsplus = "";
    private int cost = -1;
    private int cooldown = -1;
    private int homemade = 0;
    private int difficulty = -666;
    private boolean prepare = false;
    //private String spellname = "spellname";
    public Projectile(String name) {
        super(name);
    }

    public void fillFromNBT(NBTTagCompound nbtTagCompound) throws ParseException {

    }

    public void fillFromNBTList(NBTTagList nbtTagList) throws ParseException {
        NBTTagCompound tagCompound = nbtTagList.getCompoundTagAt(nbtTagList.tagCount()-1);
        NBTTagCompound ammo = (NBTTagCompound) tagCompound.getTag("projectile");
        System.out.println(nbtTagList);
        System.out.println(ammo);
        System.out.println(tagCompound);
        damage = ammo.getInteger("damage");
        caliber = ammo.getString("caliber");
        mod = ammo.getString("mod");
        infinite = ammo.hasKey("infinite");
        if(ammo.hasKey("modsplus")) modsplus = ammo.getString("modsplus");
        if(ammo.hasKey("cost")) {
            cost = ammo.getInteger("cost");
        }
        if(ammo.hasKey("cooldown")) {
            cooldown = ammo.getInteger("cooldown");
        }
        if(ammo.hasKey("handmade")) homemade = ammo.getInteger("homemade");
        if (!mod.isEmpty()) mod = mod.toLowerCase();
        System.out.println("CREATED WEAPON: " + this.toString());
        if (ammo.hasKey("diff")) difficulty = ammo.getInteger("diff");
        if (ammo.hasKey("prep")) prepare = ammo.getBoolean("prep");
        //if (ammo.hasKey("spellname")) spellname = ammo.getString("spellname");

    }

    @Override
    public void fillFromJson(String jsonStr) throws ParseException {

    }

    @Override
    public int getDamage() {
        return damage;
    }

    @Override
    public double getReach() {
        return 0;
    }

    @Override
    public ArrayList<String> getProficiencies() {
        return new ArrayList<>();
    }

    public String getCaliber() {
        return caliber;
    }

    public String getMod() {
        return mod;
    }

    public boolean isInfinite() {
        return infinite;
    }

    public String getModsplus() {
        return modsplus;
    }

    public int getCost() {
        return cost;
    }

    public int getCooldown() {
        return cooldown;
    }
    public int getHomemade() {
        return homemade;
    }

    public int getDifficulty() { return difficulty; }
    public boolean needPrepare() { return prepare; }

    public void setCaliber(String caliber) {this.caliber = caliber;}
    public void setDamage(int damage) {this.damage = damage;}
    public void setMod(String mod) {this.mod = mod;}
    //public String getSpellname() { return spellname; }
}
