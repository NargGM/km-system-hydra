package ru.konungstvo.combat.equipment;

import java.io.Serializable;

public enum EnergyType implements Serializable {
    NONE("нет", 0),
    LASER("лазерное", 1),
    PLASMA("плазменное", 2)
    ;

    private String name;
    private int num;

    EnergyType(String name, int num) {
        this.name = name;
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
