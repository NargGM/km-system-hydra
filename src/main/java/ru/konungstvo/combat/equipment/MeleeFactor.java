package ru.konungstvo.combat.equipment;

public enum MeleeFactor {
    SHARP("острый");

    String desc;

    MeleeFactor(String desc) {
        this.desc = desc;
    }

    public static MeleeFactor get(String categoryStr) {
        for (MeleeFactor meleeFactor : MeleeFactor.values()) {
            if (meleeFactor.desc.equals(categoryStr)) {
                return meleeFactor;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return desc;
    }

    public String get() {
        return desc;
    }

    }
