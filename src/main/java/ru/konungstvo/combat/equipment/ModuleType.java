package ru.konungstvo.combat.equipment;

public enum ModuleType {

    OPTIC("оптика"),
    BARREL("ствол"),
    MAGAZINE("магазин"),
    STOCK("приклад"),
    UNDERBARREL("подствольник"),
    EXTRA2("допдопмодуль"),
    EXTRA("допмодуль");

    String name;
    ModuleType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
