package ru.konungstvo.combat.movement;

import ru.konungstvo.chat.message.SkillDiceMessage;

public class MovementCalculator {

    public static int getAmountOfBlocks(int diceResult) {
        switch (diceResult) {
            case -4:
                return 0;
            case -3:
                return 1;
            case -2:
                return 2;
            case -1:
                return 3;
            case 0:
                return 4;
            case 1:
                return 5;
            case 2:
                return 6;
            case 3:
                return 7;
            case 4:
                return 8;
            case 5:
                return 9;
            case 6:
                return 10;
            case 7:
                return 11;
        }
        return 0;
    }

    public static int getAmountOfBlocksFromDice(SkillDiceMessage diceMessage, String type, double traitmod) {
        System.out.println("getting blocks for " + diceMessage.getDice().getResult());
        int diceResult;
        if (type.equals("free")) {
            diceResult = diceMessage.getDice().getBase();
        } else {
            diceResult = diceMessage.getDice().getResult();
        }
        int standard = getAmountOfBlocks(diceResult);
        switch (type) {
            case "run":
                return (int) (standard * 2 * traitmod);
            case "start":
            case "walk":
                return (int) (standard * traitmod);
            case "free":
            case "crawl":
                return (int) (standard/2 * traitmod);
            case "crouch":
                return (int) (standard * 0.75 * traitmod);
            case "jump":
                return (int) Math.max(diceResult + 1, 0);
            case "jump_run":
                if (diceResult < -1) return 0;
                if (diceResult == -1) return 1;
                return (int) Math.max(diceResult + 3, 0);
        }
        return (int) (standard * traitmod);

    }
}
