package ru.konungstvo.combat.movement;

public enum ChargeHistory {
    NONE, CHARGE, CHARGE_FADING;
}
