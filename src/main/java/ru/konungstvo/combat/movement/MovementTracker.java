package ru.konungstvo.combat.movement;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemBlock;
import net.minecraft.server.management.PlayerList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntitySign;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import noppes.npcs.api.NpcAPI;
import noppes.npcs.api.wrapper.WorldWrapper;
import noppes.npcs.entity.EntityNPCInterface;
import org.lwjgl.input.Keyboard;
import ru.konungstvo.bridge.Core;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.Combat;
import ru.konungstvo.commands.executor.NextExecutor;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.gm.npcadder.NpcCommand;
import ru.konungstvo.commands.helpercommands.player.turn.PerformAction;
import ru.konungstvo.commands.helpercommands.player.turn.ToRoot;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ru.konungstvo.control.Helpers.hasSavedPlayer;

@Mod.EventBusSubscriber(modid = Core.MODID)
public class MovementTracker {
    private static List<String> suspects = new LinkedList<>();
    private static Map<String, MovementBadge> badges = new HashMap<>();
    private static final Pattern pattern = Pattern.compile("(\\[|\\{)\\w*?(\\]|\\})");

    @SideOnly(Side.SERVER)
    public static void teleportToCenter(String playerName) {

        EntityPlayerMP player;
        player = ServerProxy.getForgePlayer(playerName);

        double posX = Math.floor(player.posX) + 0.5;
        double posY = Math.floor(player.posY);
        double posZ = Math.floor(player.posZ) + 0.5;

        player.attemptTeleport(posX, posY, posZ);
        System.out.println("badges size is " + badges.size());
        //player.sendMessage(new TextComponentString("Teleported to " + posX + " " + posY + " " + posZ));
    }

    @SideOnly(Side.SERVER)
    public static void teleportToBlock(String playerName, BlockPos pos) {

        EntityPlayerMP player;
        player = ServerProxy.getForgePlayer(playerName);

        double posX = Math.floor(pos.getX()) + 0.5;
        double posY = Math.floor(pos.getY() + 1.);
        double posZ = Math.floor(pos.getZ()) + 0.5;

        System.out.println("Trying to teleport to " + posX + " " + posY + " " + posZ);

        player.attemptTeleport(posX, posY, posZ);
        //player.sendMessage(new TextComponentString("Teleported to " + posX + " " + posY + " " + posZ));

    }

    @SideOnly(Side.SERVER)
    public static void setMovementHistory(String playerName, MovementHistory movementHistory) {
        DataHolder.inst().getPlayer(playerName).setMovementHistory(movementHistory);
    }

    @SideOnly(Side.SERVER)
    @SubscribeEvent
    public static void onRightClickBlock(PlayerInteractEvent.RightClickBlock event) {
        if (suspects.contains(event.getEntity().getName())) {

            System.out.println("HERE!");
            System.out.println("entity name: " + event.getEntity().getName());
            System.out.println("pos: " + event.getPos());


            BlockPos playerPos = event.getEntityPlayer().getPosition();
            BlockPos playerStandsOn = new BlockPos(playerPos.getX(), playerPos.getY() - 1, playerPos.getZ());

            EntityLivingBase player = (EntityLivingBase) event.getEntity();
            if (player.getHeldItemMainhand().getItem() instanceof ItemBlock) return;
            System.out.println("Player position: " + player.posX + " " + player.posY + " " + player.posZ);
            System.out.println("Player on block: " + playerStandsOn);
            System.out.println("Clicked on block: " + event.getPos());

            double blockCenterX = event.getPos().getX() + 0.5;
            double blockCenterY = event.getPos().getY();
            double blockCenterZ = event.getPos().getZ() + 0.5;
            System.out.println("Center of the block clicked: " + blockCenterX + " " + blockCenterY + " " + blockCenterZ);
            double distance = MovementTracker.getDistanceBetween(
                    player.posX,
                    player.posY - 1,
                    player.posZ,
                    blockCenterX,
                    blockCenterY,
                    blockCenterZ
            );
            MovementBadge badge = badges.get(player.getName());

            System.out.println("Distance: " + distance);
            System.out.println("toCover: " + badge.getBlocksToCover());
            System.out.println("behind: " + badge.getDistanceBehind());
            if (distance > badge.getBlocksToCover() - badge.getDistanceBehind()) {

                TextComponentString mes = new TextComponentString("Этот блок находится дальше, чем вы можете пройти.");
                mes.getStyle().setColor(TextFormatting.GRAY);
                player.sendMessage(mes);
                return;
            }
            System.out.println("Suspects size is " + suspects.size());

            if (FMLCommonHandler.instance().getSide() == Side.SERVER && suspects.contains(player.getName())) {
                if(player.getRidingEntity() instanceof EntityNPCInterface) {
                    EntityNPCInterface mount = (EntityNPCInterface) player.getRidingEntity();
                    mount.wrappedNPC.setHome(event.getPos().getX(), event.getPos().getY(), event.getPos().getZ());
                    System.out.println(event.getPos().getX() + " " + event.getPos().getY() + " " + event.getPos().getZ());
                    mount.wrappedNPC.reset();
                } else {
                    teleportToBlock(player.getName(), event.getPos());
                }
            }
        }
    }

    @SideOnly(Side.SERVER)
    @SubscribeEvent
    // use PlayerTick
    public void onPlayerTick(TickEvent.PlayerTickEvent event) {
//    public static void onLivingUpdate(LivingEvent.LivingUpdateEvent event) {
//        EntityLivingBase player = event.getEntityLiving();
//        System.out.println("TEST! Caught tick event");
        try {
            EntityLivingBase player = event.player;
//        if (event.getEntityLiving() != null && event.getEntityLiving() instanceof EntityPlayer && FMLCommonHandler.instance().getSide() == Side.SERVER) {
//        System.out.println(event.type);
//        System.out.println(event.phase);
            if (event.side == Side.SERVER && event.type == TickEvent.Type.PLAYER && event.player != null) {
                if (event.player.isCreative() && !event.player.getHeldItemMainhand().isEmpty() && event.player.getHeldItemMainhand().getItem().getRegistryName() != null && event.player.getHeldItemMainhand().getItem().getRegistryName().toString().equals("minecraft:diamond_pickaxe")) {
                    return;
                }
                if (!event.player.onGround && !event.player.isElytraFlying() && !event.player.isRiding()) return;
                String removeBadge = "";
                if (suspects.contains(player.getName())) {
                    EntityNPCInterface mount = null;
                    BlockPos home = null;
                    if(player.getRidingEntity() instanceof EntityNPCInterface) {
                        mount = (EntityNPCInterface) player.getRidingEntity();
                    }
                    MovementBadge badge = badges.get(player.getName());
                    double diff;

                    if (mount == null) {
                        diff = getDistanceBetween(
                            badge.getPrevPosX(), badge.getPrevPosY(), badge.getPrevPosZ(),
                            player.posX, player.posY, player.posZ
                        );
                    } else {
                        home = new BlockPos(mount.wrappedNPC.getHomeX(), mount.wrappedNPC.getHomeY(), mount.wrappedNPC.getHomeZ());
                        diff = getDistanceBetween(
                                badge.getPrevPosX(), badge.getPrevPosY(), badge.getPrevPosZ(),
                                home.getX()+0.5, home.getY(), home.getZ()+0.5
                        );
                    }
                    //double distanceBehind = Math.round(badge.getDistanceBehind() * 100.) / 100.;
                    double distanceBehind = badge.getDistanceBehind();
                    if (diff < 0.25) return;
                    if (diff + distanceBehind < 0.9) return;
                    double newBehind = badge.getDistanceBehind() + diff;
                    badge.setDistanceBehind(newBehind);


//                if (newBehind - distanceBehind > 0.49) {
                    Message message = null;
                    if (diff > 0.0) {
//                    System.out.println("diff: " + diff);
                        //ClientProxy.deleteClickContainer("MovementCounter");
                        //ClickContainer.deleteContainer("MovementCounter", (EntityPlayerMP) player);
                        message = new Message("Блоков пройдено: " + Math.round(newBehind * 100.) / 100. + "/" + badge.getBlocksToCover() + "\n", ChatColor.COMBAT);
                        Player player1 = DataHolder.inst().getPlayer(player.getName());
                        if(player1.getSubordinate() != null) player1 = player1.getSubordinate();
                        if (player1.getMovementHistory() == MovementHistory.JUMP) {
                            MessageComponent stop = new MessageComponent("[Конец прыжка]", ChatColor.BLUE);
                            stop.setClickCommand("/" + GoExecutor.NAME + " end antiforced jump");
                            message.addComponent(stop);
                        } else {
                            MessageComponent action = new MessageComponent("[Действие] ", ChatColor.BLUE);
                            action.setClickCommand("/" + PerformAction.NAME + " from_movement");
                            MessageComponent stop = new MessageComponent("[Конец хода]", ChatColor.BLUE);
                            stop.setClickCommand("/" + GoExecutor.NAME + " end forced");
                            if (((badge.getBlocksToCover() - badge.getDistanceBehind()) >= 5.50) || badge.freely) {
                                MessageComponent charge = null;
                                if (player1.getMovementHistory() == MovementHistory.NOW) {
                                    if (player1.getChargeHistory() == ChargeHistory.NONE) {
                                        if (badge.getDistanceBehind() < 1.4) {
                                            charge = new MessageComponent("[Начать натиск] ", ChatColor.GRAY);
                                            charge.setHoverText("Нельзя начинать натиск почти не сдвинувшись с места.", TextFormatting.RED);
                                        } else {
                                            charge = new MessageComponent("[Начать натиск] ", ChatColor.RED);
                                            charge.setClickCommand("/" + GoExecutor.NAME + " end forced charge");
                                            charge.setHoverText("Требует около 6 блоков. Закончит ваш ход без траты разбега.\n" +
                                                    "На следующий ход вы сможете, после бега,\n" +
                                                    "атаковать оружием ближнего боя с бонусом.", TextFormatting.DARK_GREEN);
                                        }
                                    } else {
                                        action.setColor(ChatColor.DARK_RED);
                                        action.setHoverText("Натиск готов.\nВперёд!", TextFormatting.DARK_RED);
                                        if (badge.getDistanceBehind() < 1.4) {
                                            action.setColor(ChatColor.GRAY);
                                            action.setHoverText("Вы не получите натиск, пока не пробежите хотя бы несколько блоков.", TextFormatting.GRAY);
                                            action.setClickCommand("/" + PerformAction.NAME + " from_movement cancel_charge");
                                            charge = new MessageComponent("[Продолжить натиск] ", ChatColor.GRAY);
                                            charge.setHoverText("Нельзя продолжить натиск почти не сдвинувшись с места.", TextFormatting.RED);
                                        } else {
                                            charge = new MessageComponent("[Продолжить натиск] ", ChatColor.BLUE);
                                            charge.setClickCommand("/" + GoExecutor.NAME + " end forced charge");
                                            charge.setHoverText("Требует около 6 блоков. Закончит ваш ход без траты разбега.\n" +
                                                    "На следующий ход вы сможете, после бега,\n" +
                                                    "атаковать оружием ближнего боя с бонусом.", TextFormatting.DARK_GREEN);
                                        }
                                    }


                                }
                                message.addComponent(action);
                                if (charge != null) message.addComponent(charge);
                            }

                            message.addComponent(stop);
                        }
//                    TextComponentString panel = MessageGenerator.generate(message);
                        //ClientProxy.addClickContainer("MovementCounter", panel);
//                    System.out.println("send message with update");
                    }
                    if (home == null) {
                        badge.setPrevPosX(player.posX);
                        badge.setPrevPosY(player.posY);
                        badge.setPrevPosZ(player.posZ);
                    } else {
                        badge.setPrevPosX(home.getX() + 0.5);
                        badge.setPrevPosY(home.getY());
                        badge.setPrevPosZ(home.getZ() + 0.5);
                    }


                    if ((badge.getBlocksToCover() - badge.getDistanceBehind()) < 0.5) {
                        double blocksRemaining = Math.round((badge.getBlocksToCover() - badge.getDistanceBehind()) * 100.) / 100.;
                        TextComponentString mes = new TextComponentString("Вы завершили передвижение. Блоков осталось: " +
                                //(badge.getBlocksToCover() - distanceBehind)
//                            Math.round((badge.getBlocksToCover() - badge.getDistanceBehind()) * 100.) / 100.
                                blocksRemaining
                        );

                        TextComponentString mesToGm = new TextComponentString("[GM] " + player.getName() + " завершает передвижение, остаток: " +
                                blocksRemaining
                        );
                        mesToGm.getStyle().setColor(TextFormatting.GOLD);
                        DataHolder.inst().informMasterForCombat(player.getName(), mesToGm);

                        teleportToCenter(player.getName());
                        mes.getStyle().setColor(TextFormatting.YELLOW);
                        player.sendMessage(mes);
                        removeBadge = player.getName();
                        removeMovementBadgeFrom(removeBadge, true);

//                    setMovementHistory(player.getName(), MovementHistory.NOW);
                        // REMOVE PREVIOUS PANEL
//                    ClickContainer.deleteContainer("MovementCounter", (EntityPlayerMP) player);
                        DataHolder.inst().getPlayer(player.getName()).performCommand("/go end");

                    } else {
                        Helpers.sendClickContainerToClient(message, "MovementCounter", (EntityPlayerMP) player);
                    }

                }
                if (!removeBadge.isEmpty()) {
//                removeMovementBadgeFrom(removeBadge, true);
                }
            }
        } catch (Exception e) {
            //System.out.println(e.toString());
        }

    }


    public static double getDistanceBetween(double rightX, double righxY, double righxZ, double leftX, double leftY, double leftZ) {
        //System.out.println("Getting diff between " + rightX + ";" + righxY + ";" + righxZ + " and " + leftX + ";" + leftY + ";" + leftZ);
        return Math.sqrt(Math.pow(rightX - leftX, 2) + Math.pow(righxY - leftY, 2) + Math.pow(righxZ - leftZ, 2));
    }

    public static void addSuspect(String name) {
        suspects.add(name);
    }

    public static double dropSuspect(String name) {
        MovementBadge badge = badges.get(name);
        double blocksRemaining = -1;
        if (badge != null) {
            blocksRemaining = Math.round((badge.getBlocksToCover() - badge.getDistanceBehind()) * 100.) / 100.;
        }

        suspects.remove(name);
        return blocksRemaining;
    }


    @SideOnly(Side.CLIENT)
    public static void attachMovementBadgeToClient(String name, int blocksToCover) {
        /*
        removeMovementBadgeFrom(name);
        addSuspect(name);
        EntityPlayer player = null;
        player = Minecraft.getMinecraft().player;
        MovementBadge badge = new MovementBadge(blocksToCover, player.getPosition(), player.posX, player.posY, player.posZ);
        badges.put(name, badge);

         */
    }


    @SideOnly(Side.SERVER)
    public static void attachMovementBadgeToServer(String name, int blocksToCover, boolean freely) {
        removeMovementBadgeFrom(name, false);
        addSuspect(name);

//        Combat c = DataHolder.inst().getCombatForPlayer(name);
//        if (c != null && !DataHolder.inst().isNpc(name))
//            DataHolder.inst().getPlayer(name).unfreeze();

        EntityPlayer player = null;
        player = ServerProxy.getForgePlayer(name);
        MovementBadge badge;
        if(player.getRidingEntity() instanceof EntityNPCInterface) {
            EntityNPCInterface mount = (EntityNPCInterface) player.getRidingEntity();
            BlockPos home = new BlockPos(mount.wrappedNPC.getHomeX(), mount.wrappedNPC.getHomeY(), mount.wrappedNPC.getHomeZ());
            badge = new MovementBadge(blocksToCover, home, home.getX()+0.5, home.getY(), home.getZ()+0.5, freely);
        } else {
            badge = new MovementBadge(blocksToCover, player.getPosition(), player.posX, player.posY, player.posZ, freely);
        }
        badges.put(name, badge);
    }

    public static void removeMovementBadgeFrom(String name, boolean shouldFreeze) {
        Combat c = DataHolder.inst().getCombatForPlayer(name);
        if (shouldFreeze && c != null && !DataHolder.inst().isNpc(name) && !DataHolder.inst().getPlayer(name).hasPermission(Permission.GM))
            DataHolder.inst().getPlayer(name).freeze();
        dropSuspect(name);
        badges.remove(name);

    }
}
