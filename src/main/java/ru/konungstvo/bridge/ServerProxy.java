package ru.konungstvo.bridge;

import de.cas_ual_ty.gci.inventory.CustomInventory;
import de.cas_ual_ty.gci.inventory.capabilities.ICAPCustomInventory;
import net.mcft.copy.backpacks.api.IBackpack;
import net.mcft.copy.backpacks.misc.BackpackCapability;
import net.mcft.copy.backpacks.misc.BackpackDataItems;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.command.ICommandManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntitySign;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;

import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.event.CommandEvent;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.server.permission.DefaultPermissionLevel;
import net.minecraftforge.server.permission.PermissionAPI;

import noppes.npcs.CustomNpcs;
import noppes.npcs.api.IWorld;
import noppes.npcs.api.NpcAPI;
import noppes.npcs.api.constants.EntityType;
import noppes.npcs.api.entity.IEntity;
import noppes.npcs.api.entity.data.IData;
import noppes.npcs.api.entity.data.INPCInventory;
import noppes.npcs.api.item.IItemStack;
import noppes.npcs.api.wrapper.NPCWrapper;
import noppes.npcs.entity.EntityNPCInterface;
import ru.konungstvo.Reminder;
import ru.konungstvo.bridge.discord.DiscordBridge;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.ChatEvent;
import ru.konungstvo.chat.message.*;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.Combat;
import ru.konungstvo.combat.movement.GoExecutor;
import ru.konungstvo.combat.movement.MovementTracker;
import ru.konungstvo.commands.debug.DuelDebug;
import ru.konungstvo.commands.executor.*;
import ru.konungstvo.commands.helpercommands.PacketTestCommand;
import ru.konungstvo.commands.helpercommands.gm.npcadder.NpcCommand;
import ru.konungstvo.commands.helpercommands.modifiers.*;
import ru.konungstvo.commands.helpercommands.player.attack.*;
import ru.konungstvo.commands.helpercommands.gm.npcadder.ChooseNpcCommand;
import ru.konungstvo.commands.helpercommands.player.defense.*;
import ru.konungstvo.commands.helpercommands.player.movement.MoveExecutor;
import ru.konungstvo.commands.helpercommands.player.movement.PerformMovement;
import ru.konungstvo.commands.helpercommands.player.turn.PerformAction;
import ru.konungstvo.commands.helpercommands.player.turn.PerformShift;
import ru.konungstvo.commands.helpercommands.player.turn.ToRoot;
import ru.konungstvo.commands.helpercommands.player.turn.ToolPanel;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.Logger;
import ru.konungstvo.control.network.*;
import ru.konungstvo.kmrp_lore.helpers.*;
import ru.konungstvo.player.NPC;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.*;
import java.util.concurrent.FutureTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static commandchatcore.EventSendChatMessageToPlayer.sendChatMsg;
import static de.cas_ual_ty.gci.inventory.capabilities.CAPCustomInventoryProvider.INVENTORY_CAP;
import static net.minecraftforge.common.MinecraftForge.EVENT_BUS;

public class ServerProxy {

    private static Logger logger = new Logger("ServerProxy");
    private static final Pattern pattern = Pattern.compile("(\\[|\\{)\\w*?(\\]|\\})");
    private static final Pattern patternDifMessage = Pattern.compile("(\\[.+?:!?(ужасно|плохо|посредственно|нормально|хорошо|отлично|превосходно|легендарно)?\\])");

    public static IBlockState FIRE_BLOCK = null;

    //private int online = 0;
    //{{{--- FORGE EVENTS

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
    }

//    @SubscribeEvent(priority = EventPriority.LOWEST)
//    public void frogEvent(LivingSpawnEvent.CheckSpawn event) {
//        if (event.getEntity() instanceof EntityLivingBase) {
//            System.out.println(event.getEntityLiving().getName());
//            if (event.getEntityLiving().getName().equals("Dart Frog")) {
//                event.setResult(Event.Result.DENY);
//            }
//        }
//    }

    @SubscribeEvent(priority = EventPriority.LOWEST)
    public void frogEvent2(EntityJoinWorldEvent event) {
        if (event.getEntity() instanceof EntityLivingBase) {
            if (event.getEntity().getName().equals("Dart Frog")) {
                event.setCanceled(true);
            }
        }
    }

    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void onChatMessage(ServerChatEvent event) {
        event.setCanceled(true);
        EntityPlayerMP forgePlayer = event.getPlayer();
        // get Player object

        Player player = DataHolder.inst().getPlayer(forgePlayer.getName());

        // process message
        ChatEvent chatEvent;
        try {
            chatEvent = new ChatEvent(player, event.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            player.sendMessage(new Message(e.toString(), ChatColor.RED));
            return;
        }

        RoleplayMessage result = chatEvent.getResult();
        if (result == null) return;

        try {
            // Глобальное сообщение, отсылаем всем
            if (chatEvent.getRoleplayMessage() instanceof GlobalMessage) {
                for (Player p : DataHolder.inst().getPlayerList()) {
                    //asyncPlayerChatEvent.getRecipients().add(p);
                    p.sendMessage(result);
                }

                // Сообщение в ГМку
            } else if (chatEvent.getRoleplayMessage().getRange().toString().contains("GM")) {
                if (!hasPermission(player.getName(), Permission.GM.get())) {
                    // Отсылаем отправителю, если он не ГМ
                    player.sendMessage(result);
                }
                // Отправляем всем ГМам
                sendToAllMasters(result);

                // То же самое, но для билдеров. TODO
            } else if (chatEvent.getRoleplayMessage().getRange() == Range.CM) {
                if (!hasPermission(player.getName(), Permission.CM.get())) {
                    player.sendMessage(result);
                }
                sendToAllCraftMasters(result);

                // Все остальные рейнджи
            } else {
                if (chatEvent.getRoleplayMessage().getRange().toString().contains("STORY")) {
                    Matcher rMat = patternDifMessage.matcher(chatEvent.getRoleplayMessage().getContent());
                    if (rMat.find()) {
                        String prikol = rMat.group().replace("[", "").replace("]","");
                        String skill = prikol.split(":")[0].toLowerCase();
                        boolean musthaveskill = false;
                        boolean secret = false;
                        if (skill.startsWith("!")) {
                            skill = skill.replaceFirst("!", "");
                            musthaveskill = true;
                        }
                        if (skill.startsWith("?")) {
                            System.out.println("test");
                            skill = skill.replace("?", "");
                            secret = true;
                        }
                        String diff = prikol.split(":")[1];
                        boolean musthavelevel = false;
                        if (diff.startsWith("!")) {
                            diff = diff.replaceFirst("!", "");
                            musthavelevel = true;
                        }
                        int diffInt = DataHolder.inst().getDiceAsInteger(diff);
                        String content = rMat.replaceAll("");
                        String successMessage = "";
                        String loseMessage = "";
                        if (content.contains(";;;")) {
                            successMessage = content.split(";;;")[0].trim();
                            loseMessage = content.split(";;;")[1].trim();
                        } else {
                            successMessage = content.trim();
                        }
                        ArrayList<String> success = new ArrayList<>();
                        ArrayList<String> lose = new ArrayList<>();
                        for (Player p : findAllPlayersInRange(player, chatEvent.getRoleplayMessage().getRangeAsInt())) {
                            if (p.hasPermission(Permission.GM)) continue;
                            boolean donthaveskill = false;
                            boolean donthavelevel = false;
                            FudgeDiceMessage fdm;
                            if (p.getSkill(skill) != null) {
                                fdm = new SkillDiceMessage(p.getName(), "% " + skill, Range.NORMAL_DIE);
                                if (fdm.getDice().getBase() < diffInt) donthavelevel = true;
                            } else {
                                donthaveskill = true;
                                donthavelevel = true;
                                fdm = new FudgeDiceMessage(p.getName(), "% плохо", Range.NORMAL_DIE);
                            }
                            fdm.build();
                            if (fdm.getDice().getResult() >= diffInt && (!donthaveskill || !musthaveskill) && (!donthavelevel || !musthavelevel)) {
                                success.add(fdm.toString());
                                if (!successMessage.isEmpty()) {
                                    Message msg = new Message("", ChatColor.STORY);
                                    if (secret) {
                                        MessageComponent secretMessage = new MessageComponent("[???]");
                                        secretMessage.setColor(ChatColor.GRAY);
                                    } else {
                                        MessageComponent dice = new MessageComponent("[" + (musthaveskill ? "§l" : "") + skill.substring(0, 1).toUpperCase() + skill.substring(1) + "§2§n УСПЕХ!]");
                                        dice.setColor(ChatColor.DARK_GREEN);
                                        dice.setUnderlined(true);
                                        dice.setHoverText(new TextComponentString("§2Сложность: §e" + (musthavelevel ? "§l" : "") + diff + "\n§e" + fdm.toString() + fdm.getDice().getPercentDice().getPercent() + " " + (Math.abs(fdm.getDice().getPercentDice().get()) >= fdm.getDice().getPersentResult() && fdm.getDice().getPercentDice().get() != 0 ? (fdm.getDice().getPercentDice().get() < 0 ? "§4" : "§2") : "") + "Бросок: " + fdm.getDice().getPersentResult() + "/100§7" + fdm.getDice().getPercentDice().toString()));
                                        msg.addComponent(dice);
                                    }
                                    MessageComponent content1 = new MessageComponent(" " + chatEvent.getRoleplayMessage().getRange().getDescription() + successMessage + "§6" + chatEvent.getRoleplayMessage().getRange().getDescription(), ChatColor.STORY);
                                    msg.addComponent(content1);
                                    p.sendMessage(msg);
                                }
                            } else {
                                if ((!donthaveskill || !musthaveskill) && (!donthavelevel || !musthavelevel)) lose.add(fdm.toString());
                                else if (donthaveskill && musthaveskill) lose.add("§4" + p.getName() + " не имеет навыка!");
                                else if (donthavelevel && musthavelevel) lose.add("§4" + p.getName() + " недостаточный уровень навыка!");
                                if (!loseMessage.isEmpty()) {
                                    Message msg = new Message("", ChatColor.STORY);
                                    if (secret) {
                                        MessageComponent secretMessage = new MessageComponent("[???]");
                                        secretMessage.setColor(ChatColor.GRAY);
                                    } else {
                                        MessageComponent dice = new MessageComponent("[" + (musthaveskill ? "§l" : "") + skill.substring(0, 1).toUpperCase() + skill.substring(1) + "§4§n ПРОВАЛ!]");
                                        dice.setColor(ChatColor.DARK_RED);
                                        dice.setUnderlined(true);
                                        dice.setHoverText(new TextComponentString("§4Сложность: §e" + (musthavelevel ? "§l" : "") + diff + "\n" + ((donthaveskill && musthaveskill) ? "§4Нет навыка!" : ((donthavelevel && musthavelevel) ? "§4Недостаточный навык!" : "§e" + fdm.toString() + fdm.getDice().getPercentDice().getPercent() + " " + (Math.abs(fdm.getDice().getPercentDice().get()) >= fdm.getDice().getPersentResult() && fdm.getDice().getPercentDice().get() != 0 ? (fdm.getDice().getPercentDice().get() < 0 ? "§4" : "§2") : "") +  "Бросок: " + fdm.getDice().getPersentResult() + "/100§7" + fdm.getDice().getPercentDice().toString()))));
                                        msg.addComponent(dice);
                                    }
                                    MessageComponent content1 = new MessageComponent(" " + chatEvent.getRoleplayMessage().getRange().getDescription() + loseMessage + "§6" + chatEvent.getRoleplayMessage().getRange().getDescription(), ChatColor.STORY);
                                    msg.addComponent(content1);
                                    p.sendMessage(msg);
                                }
                            }
                        }

                        Message msg = new Message("", ChatColor.STORY);
                        MessageComponent dice = new MessageComponent("[" + (secret ? "§7§o?§e§n" : "")  + (musthaveskill ? "§l§n" : "") + skill + "§e§n:" + (musthavelevel ? "§l" : "") + diff +  "§e§l§n]", ChatColor.YELLOW);
                        MessageComponent content1 = new MessageComponent(" " + chatEvent.getRoleplayMessage().getRange().getDescription() + content.replaceFirst(";;;","§6;;;").trim() + chatEvent.getRoleplayMessage().getRange().getDescription(), ChatColor.STORY);
                        dice.setUnderlined(true);
                        StringBuilder hover = new StringBuilder();
                        if (success.size() > 0) hover.append("§2Успех: \n");
                        for (String sucstr : success) {
                            hover.append("§e" + sucstr + "\n");
                        }

                        if (lose.size() > 0) hover.append("§4Провал: \n");
                        for (String losestr : lose) {
                            hover.append("§e" + losestr + "\n");
                        }
                        dice.setHoverText(new TextComponentString(hover.toString().trim()));
                        msg.addComponent(dice);
                        msg.addComponent(content1);
                        for (Player p : findAllPlayersInRange(player, chatEvent.getRoleplayMessage().getRangeAsInt())) {
                            if (!p.hasPermission(Permission.GM)) continue;
                            p.sendMessage(msg);
                        }
                        informDistantMasters(player, msg, chatEvent.getResult().getRangeAsInt());
                    } else {
                        for (Player p : findAllPlayersInRange(player, chatEvent.getRoleplayMessage().getRangeAsInt())) {
                            p.sendMessage(result);
                        }
                        informDistantMasters(player, result, chatEvent.getResult().getRangeAsInt());
                    }
                } else {
                    if (chatEvent.getRoleplayMessage().getRange().getDistance() >= 36 && (chatEvent.getRoleplayMessage().getRange().toString().contains("SCREAM") || chatEvent.getRoleplayMessage().getRange().toString().contains("SHOUT") || chatEvent.getRoleplayMessage().getRange().toString().contains("LOUD"))) {
                        MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();;
                        ICommandManager icommandmanager = server.getCommandManager();
                        EntityPlayer player1 = ServerProxy.getForgePlayer(player.getName());
                        for (Player p : findAllPlayersInRange(player, chatEvent.getRoleplayMessage().getRangeAsInt())) {
                            if (p == null) continue;
                            if (DataHolder.inst().isNpc(p.getName())) continue;
                            if (p != player) {
                                try {
                                    String dir = "";
                                    EntityPlayer player2 = ServerProxy.getForgePlayer(p.getName());
                                    Vec3d vector = player1.getPositionVector().subtract(player2.getPositionVector());
                                    Vec3d playerDirection = player2.getLookVec();
                                    double angleDir = (Math.atan2(vector.z, vector.x) / 2 / Math.PI * 360 + 360) % 360;
                                    double angleLook = (Math.atan2(playerDirection.z, playerDirection.x) / 2 / Math.PI * 360 + 360) % 360;
                                    double angle = (angleDir - angleLook + 360) % 360;
                                    if (angle <= 22.5 || angle >= 337.5) {
                                        dir = "↑";
                                    } else if (angle <= 67.5) {
                                        dir = "↗";
                                    } else if (angle <= 112.5) {
                                        dir = "→";
                                    } else if (angle <= 157.5) {
                                        dir = "↘";
                                    } else if (angle <= 202.5) {
                                        dir = "↓";
                                    } else if (angle <= 247.5) {
                                        dir = "↙";
                                    } else if (angle <= 292.5) {
                                        dir = "←";
                                    } else if (angle <= 337.5) {
                                        dir = "↖";
                                    }

                                    if (Math.abs(player1.posY - player2.posY) >= 20) {
                                        if (player1.posY > player2.posY) {
                                            dir = dir + " ▴";
                                        } else {
                                            dir = dir + " ▾";
                                        }
                                    }

                                    icommandmanager.executeCommand(server, "title " + p.getName() + " actionbar {\"text\":\"" + dir + "\",\"bold\":true}");
                                    //p.sendMessage(dir);
                                } catch (Exception ignored) {

                                }
                            }
                            p.sendMessage(result);
                        }
                    } else {
                        for (Player p : findAllPlayersInRange(player, chatEvent.getRoleplayMessage().getRangeAsInt())) {
                            p.sendMessage(result);
                        }
                    }
                    informDistantMasters(player, result, chatEvent.getResult().getRangeAsInt());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (chatEvent.getRoleplayMessage() instanceof RadioMessage) {
            sendRadioMessage(player, result.getContent().replaceAll("&([a-z0-9])", "§$1"), result.getRange(), chatEvent.getRoleplayMessage() instanceof RadioOocMessage);
        }

        logger.info(result.toString());

    }

    @SubscribeEvent(priority = EventPriority.LOWEST)
    @SideOnly(Side.SERVER)
    public void onPlayerJoin(PlayerEvent.PlayerLoggedInEvent event) {
        EntityPlayerMP forgePlayer = (EntityPlayerMP) event.player;
        Player player = DataHolder.inst().getPlayer(forgePlayer.getName());

        String playerName = event.player.getName();
        TextComponentString message = ChatEvent.getPlayerLeave(playerName);

        // create new player, or fetch them from saved data
        if (player == null) {
            logger.debug("New player joined.");
            player = Helpers.getSavedOrNewPlayer(forgePlayer.getName());
            DataHolder.inst().addPlayer(player);

            logger.debug("Added " + player.getName() + " to Database.");
            // Consider permissions
            if (hasPermission(forgePlayer.getName(), "km.gm")) {
                player.addPermission(Permission.GM);
                player.addPermission(Permission.BD);
                player.addPermission(Permission.CM);
                player.addPermission(Permission.TELL);
                player.addPermission(Permission.RADIO);
            } else if (hasPermission(forgePlayer.getName(), "km.builder")) {
                player.addPermission(Permission.BD);
            } else if (hasPermission(forgePlayer.getName(), "km.craft")) {
                player.addPermission(Permission.CM);
            } else if (hasPermission(forgePlayer.getName(), "km.radio")) {
                player.addPermission(Permission.RADIO);
            }

            if (hasPermission(forgePlayer.getName(), "km.tell")) {
                player.addPermission(Permission.TELL);
            }

            // Execute a Reminder if existent for player (see ReminderExecutor)
            for (Reminder reminder : DataHolder.inst().getReminders()) {
                if (reminder.getPlayerName().equals(player.getName())) {
                    player.sendMessage("~ " + reminder.getContent() + "~");
                    DataHolder.inst().removeReminder(reminder);
                    break;
                }
            }
        }

        //player.performCommand("/containerpurge");
        if (DataHolder.inst().getCombatForPlayer(playerName) != null && DataHolder.inst().getCombatForPlayer(playerName).getThirdPersonBlocked() && !player.hasPermission(Permission.GM)) {
            KMPacketHandler.INSTANCE.sendTo(new ThirdPersonClientMessage(-666), (EntityPlayerMP) event.player);
        } else {
            KMPacketHandler.INSTANCE.sendTo(new ThirdPersonClientMessage(666), (EntityPlayerMP) event.player);
        }

        // Send info in discord
        SocketAddress remoteAddress = getForgePlayer(playerName).connection.getNetworkManager().getRemoteAddress();
        String ip = ((InetSocketAddress) remoteAddress).getAddress().getHostAddress();
        DiscordBridge.sendMessage("**" + playerName + "** (" + ip + ") входит в игру.");
        DiscordBridge.updateOnline(false);
        BufferedWriter bufferWriter = null;
        try {
            File file = new File("/srv/minecraft_server/logs/ipgame.log");
            try {

                FileWriter writer = new FileWriter(file, true);
                bufferWriter = new BufferedWriter(writer);
                bufferWriter.write("\n" + playerName + " " + ip);
            } catch (FileNotFoundException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
            } finally {
                if (bufferWriter != null) {
                    try {
                        bufferWriter.close();
                    } catch (IOException iOException) {
                        iOException.printStackTrace();
                    }
                }
            }
        } catch (IOException iOException) {
            iOException.printStackTrace();
        }

        //DiscordBridge.updateOnline(online + 1);
        // Send custom leave message
        /*
        for (Player p : DataHolder.getInstance().getPlayerList()) {
            getForgePlayer(p.getName()).sendMessage(message);
        }

         */

    }

    @SubscribeEvent(priority = EventPriority.LOWEST)
    @SideOnly(Side.SERVER)
    public void onPlayerLeave(PlayerEvent.PlayerLoggedOutEvent event) {
        String playerName = event.player.getName();
        TextComponentString message = ChatEvent.getPlayerLeave(playerName);

        // save player's data on leave, UNLESS they are in a combat
        if (DataHolder.inst().getCombatForPlayer(playerName) == null) {
            DataHolder.inst().removePlayer(playerName);
            try {
                Helpers.writeSerialized("saved_players", DataHolder.inst().getPlayer(playerName));
                DataHolder.inst().removePlayer(playerName);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        // Send info in discord
        try {
            System.out.println("leaving player's name: " + playerName);
            SocketAddress remoteAddress = getForgePlayer(playerName).connection.getNetworkManager().getRemoteAddress();
            String ip = ((InetSocketAddress) remoteAddress).getAddress().getHostAddress();
            DiscordBridge.sendMessage("**" + playerName + "** (" + ip + ") выходит из игры.");
        } catch (
                Exception e) {
            e.printStackTrace();
        }

        DiscordBridge.updateOnline(true);
        //DiscordBridge.updateOnline(online - 1);
        // Send custom leave message
        /*
        for (Player player : DataHolder.getInstance().getPlayerList()) {
            getForgePlayer(player.getName()).sendMessage(message);
        }

         */

    }

    @SubscribeEvent(priority = EventPriority.LOWEST)
    @SideOnly(Side.SERVER)
    public void onCommand(CommandEvent event) {
        System.out.println("TEST");
        System.out.println("t" + event.getSender().getName());
        System.out.println("t" + event.getCommand().getAliases());
        System.out.println("t" + event.getCommand().getName());
        if (event.getCommand().getName().equals("nmessage")) {
            DiscordBridge.sendMessage("<" + event.getSender().getName() + "->" + event.getParameters()[0] + "> " + String.join(" ", Arrays.copyOfRange(event.getParameters(), 1, event.getParameters().length)));
        }
    }

    @SubscribeEvent(priority = EventPriority.LOWEST)
    @SideOnly(Side.SERVER)
    public void eatEvent(LivingEntityUseItemEvent.Finish event) {
        Entity entity = event.getEntity();
        if ((!(entity instanceof EntityPlayerMP))) return;
        EntityPlayerMP playerMP = (EntityPlayerMP) entity;
        if (!ExpireDate.hasExpiration(event.getItem())) {
            playerMP.sendMessage(new TextComponentString("§4Негодно к употреблению..."));
            IItemStack iitemStack = NpcAPI.Instance().getIItemStack(event.getItem());
            playerMP.getFoodStats().setFoodLevel(Math.max(0, playerMP.getFoodStats().getFoodLevel() - (iitemStack.getFoodLevel())));
            return;
        }
        ExpireDate expireDate = new ExpireDate(event.getItem());
        if (!expireDate.neverExpired() && expireDate.getExpireDate() < getCurrentDay() && expireDate.getExpireDate() != -666) {
            IItemStack iitemStack = NpcAPI.Instance().getIItemStack(event.getItem());
            playerMP.sendMessage(new TextComponentString("§4Негодно к употреблению..."));
            playerMP.getFoodStats().setFoodLevel(Math.max(0, playerMP.getFoodStats().getFoodLevel() - (iitemStack.getFoodLevel())));
            return;
        }

        if (expireDate.getNutritionValue() != -666) {
            IItemStack iitemStack = NpcAPI.Instance().getIItemStack(event.getItem());
            if (iitemStack.getFoodLevel() > expireDate.getNutritionValue()) {
                playerMP.getFoodStats().setFoodLevel(Math.max(0, playerMP.getFoodStats().getFoodLevel() - (iitemStack.getFoodLevel() - expireDate.getNutritionValue())));
            } else if (iitemStack.getFoodLevel() < expireDate.getNutritionValue()) {
                playerMP.getFoodStats().setFoodLevel(Math.min(20, playerMP.getFoodStats().getFoodLevel() + (expireDate.getNutritionValue() - iitemStack.getFoodLevel())));
            }
        }

        if (event.getItem().hasTagCompound() && event.getItem().getTagCompound() != null)  {
            if (event.getItem().getTagCompound().hasKey("useable")) {
                if (event.getItem().getTagCompound().getCompoundTag("useable").hasKey("type")) {
                    if (event.getItem().getTagCompound().getCompoundTag("useable").getString("type").equals("mana")) {
                        if (event.getItem().getTagCompound().getCompoundTag("useable").hasKey("regen")) {
                            int regen = event.getItem().getTagCompound().getCompoundTag("useable").getInteger("regen");
                            if (DataHolder.inst().addMana(playerMP.getName(), regen)) {
                                DataHolder.inst().getPlayer(playerMP.getName()).sendMessage(new Message( "§2Восполнено §a" + regen + " маны§2. Текущий уровень маны §f" + DataHolder.inst().getMana(playerMP.getName()) + "/" + DataHolder.inst().getMaxMana(playerMP.getName()), ChatColor.DARK_GREEN));
                            }
                        }
                    } else if (event.getItem().getTagCompound().getCompoundTag("useable").getString("type").equals("energy")) {
                        if (event.getItem().getTagCompound().getCompoundTag("useable").hasKey("regen")) {
                            int regen = event.getItem().getTagCompound().getCompoundTag("useable").getInteger("regen");
                            NpcAPI.Instance().getIWorld(0).getPlayer(playerMP.getName()).addFactionPoints(3, regen);
                            DataHolder.inst().getPlayer(playerMP.getName()).sendMessage(new Message( "§6Восполнено §a" + regen + " энергии. Текущий уровень " + NpcAPI.Instance().getIWorld(0).getPlayer(playerMP.getName()).getFactionPoints(3) + ".", ChatColor.DARK_GREEN));
                        }
                    }
                }
            }
            if (event.getItem().getTagCompound().hasKey("msgOnConsume")) {
                playerMP.sendMessage(new TextComponentString(event.getItem().getTagCompound().getString("[" + event.getItem().getDisplayName() + "&f] " + "msgOnConsume")));
            }
            if (event.getItem().getTagCompound().hasKey("notifyGms")) {
                String resultStr = " x:" + playerMP.getPosition().getX() + " y:" + playerMP.getPosition().getY() + " z:" + playerMP.getPosition().getZ() + " употребил " + event.getItem().getDisplayName() + ": " + event.getItem().getTagCompound().getString("notifyGms");
                String gamemsg = "§2[Discord] [" + playerMP.getName()  + "] §6[@GameMaster " + resultStr + "§6]";
//                if(!ServerProxy.hasPermission(player.getName(), "km.gm"))
//                    player.sendMessage(new Message(gamemsg));
                ServerProxy.sendToAllMasters(new Message(gamemsg));
                String discmsg = "[Discord] [" + playerMP.getName()  + "] [" + resultStr + "]";
                DiscordBridge.sendMessageToMasters(discmsg);
            }
        }
        DataHolder.inst().updateHungerAndThirst(playerMP.getName());
    }

    @SubscribeEvent(priority = EventPriority.LOWEST)
    @SideOnly(Side.SERVER)
    public void onRightClickSign(PlayerInteractEvent.RightClickBlock event) {
        //System.out.println("TEST");
        EntityPlayerMP p = (EntityPlayerMP) event.getEntity();
        TileEntity clickedBlock = event.getWorld().getTileEntity(event.getPos());

        if(clickedBlock instanceof TileEntitySign && p.isSneaking()) {
            //System.out.println("Удача");
            TileEntitySign bs = (TileEntitySign) clickedBlock;
            ITextComponent[] tc = bs.signText;
            ArrayList<String> players = new ArrayList<>();
            for (ITextComponent tx : tc) {
                if (tx.getFormattedText().contains("Зови ГМа") || tx.getFormattedText().contains("Звать ГМа") || tx.getFormattedText().contains("Зови ГМ") || tx.getFormattedText().contains("Звать ГМ")) {
                    DataHolder.inst().getPlayer(p.getName()).performCommand("/dmsggm ЗОВУ ГМА");
                    return;
                }
                //System.out.println("Проход");
                Matcher matcher = pattern.matcher(tx.getFormattedText());
                while (matcher.find()) {
                    System.out.println(matcher.group());
                    String player = matcher.group().replace("[","").replace("]","").replace("{","").replace("}","");
                    System.out.println(player);
                    //if(event.getWorld().getMinecraftServer().getPlayerList().getPlayerByUsername(player) != null) continue;
                    //if(!playerDat.contains(player)) continue;
                    if(player.equals(p.getName())) continue;
                    players.add(player);

                }
            }
            System.out.println(players);
            if(!players.isEmpty()) {
                // CREATE CLICK PANEL
                Message message = new Message("");
                MessageComponent close = new MessageComponent("[Закрыть] ", ChatColor.GRAY);
                //close.setBold(true);
                close.setClickCommand("/" + ToRoot.NAME + " CallPlayer");
                MessageComponent start = new MessageComponent("Выберите игрока, которого вы хотите позвать в игру:\n", ChatColor.COMBAT);
                message.addComponent(start);
                for (String player : players) {
                    MessageComponent oppComponent = new MessageComponent("[" + player + "] ", ChatColor.BLUE);
                    oppComponent.setClickCommand(
//                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                            String.format("/%s %s", "dcall", player)
                    );
                    message.addComponent(oppComponent);

                }
                message.addComponent(close);

                // SEND PACKET TO CLIENT
                Helpers.sendClickContainerToClient(message, "CallPlayer", p);
//                String json = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message));
//                ClickContainerMessage result = new ClickContainerMessage("CallPlayer", json);
//                KMPacketHandler.INSTANCE.sendTo(result, p);
            }
        }
    }

    @SubscribeEvent(priority = EventPriority.LOWEST)
    @SideOnly(Side.SERVER)
    public void onRightClickRadio(PlayerInteractEvent.EntityInteract event) {
        if (!event.getEntityPlayer().getHeldItemMainhand().isEmpty()) {
            ItemStack is = event.getEntityPlayer().getHeldItemMainhand();
            if (is.hasTagCompound()) {
                if (is.getTagCompound() != null) {
                    if (is.getTagCompound().hasKey("radiomic")) {
                        if (is.getTagCompound().getCompoundTag("radiomic").hasKey("id")) {
                            String id = "";
                            if(event.getTarget() instanceof EntityNPCInterface) {
                                id = getRadiopackIdNpc((NPCWrapper) ((EntityNPCInterface) event.getTarget()).wrappedNPC); //TODO ???
                            } else if(event.getTarget() instanceof EntityPlayerMP) {
                                id = getRadiopackId(DataHolder.inst().getPlayer(event.getTarget().getName()));
                            }
                            if (id.equals("-666")) {
                                event.getEntityPlayer().sendMessage(new TextComponentString("Нельзя привязать микрофон к приемнику."));
                                return;
                            }
                            if (!id.isEmpty()) {
                                is.getTagCompound().getCompoundTag("radiomic").setString("id", id);
                                event.getEntityPlayer().sendMessage(new TextComponentString("Вы успешно привязали микрофон к id " + id));
                            }
                        }
                    }
                }
            }
        }
    }
    // FORGE EVENTS ---}}}

    // {{{--- SENDING MESSAGES


    public static void sendMessageTo(String playerName, Message message) {
        DataHolder.inst().getPlayer(playerName).sendMessage(message);
    }

    @Deprecated
    public static void informMasters(String result) {
    }

    @Deprecated
    public static void informMasters(String result, Player player) {
    }

    public static void sendToAllMasters(Message message) {
        for (Player master : DataHolder.inst().getPlayerList()) {
            if (hasPermission(master.getName(), Permission.GM.get())) {
                master.sendMessage(message);
            }
        }

        try {
            DiscordBridge.sendMessage(message.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendToAllMastersIgnoreDiscord(Message message) {
        for (Player master : DataHolder.inst().getPlayerList()) {
            if (hasPermission(master.getName(), Permission.GM.get())) {
                master.sendMessage(message);
            }
        }
    }

    public static void sendToAllCraftMasters(Message message) {
        for (Player master : DataHolder.inst().getPlayerList()) {
            if (hasPermission(master.getName(), Permission.CM.get())) {
                master.sendMessage(message);
            }
        }
    }

    @Deprecated
    public static void informMasters(Message message) {
        informMasters(message, null, true);
    }

    public static void informMasters(Message message, Player player) {
        informMasters(message, player, true);
    }

    public static void informMasters(Message message, Player player, boolean prefix) {
        Message init = new Message();
        if (prefix) {
            init = new Message("[GM] ", ChatColor.DICE);
        }
        init.addComponent(message);
        message = init;
        Player masterForCombat = null;
        if (player != null && DataHolder.inst().getMasterForPlayerInCombat(player) != null) {
            masterForCombat = DataHolder.inst().getMasterForPlayerInCombat(player);
            masterForCombat.sendMessage(message);
        }
        //System.out.println("Trying to inform master from " + player);
        try {
            for (Player master : DataHolder.inst().getPlayerList()) {
                //System.out.println("Found player " + master.getName());
                if (master == null) continue;
                if (hasPermission(master.getName(), "km.gm")) {
                    if (masterForCombat != null && master.getName().equals(masterForCombat.getName())) continue;
                    double rangePreference = master.getRangePreference();

                    EntityPlayerMP forgeMaster = getForgePlayer(master.getName());
                    double distance = 99;
                    if (player != null) {
                        distance = getDistanceBetween(player.getName(), master.getName());
                    } else if (masterForCombat != null) {
                        distance = getDistanceBetween(masterForCombat.getName(), master.getName());
                    }

                    if (distance > Range.NORMAL.getDistance()) {
                        if (rangePreference == 0 ||
                                distance <= rangePreference
                        ) {
                            master.sendMessage(message);
                        }
                    } else {
                        master.sendMessage(message);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Error while informing masters: " + e.getMessage());
            e.printStackTrace();
        }

        try {
            DiscordBridge.sendMessage(message.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void sendMessage(Player player, String message) {
        System.out.println(message);
        try {
            getForgePlayer(player.getName()).sendMessage(new TextComponentString(message));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void sendMessage(Player player, Message message) {
        if (player == null) {
            String error = TextFormatting.RED + "ERROR! Player is null while trying to send message. Report this to admin.";
            informMasters(new Message(error), null, true);
            return;
        }
        if (DataHolder.inst().isNpc(player.getName())) return;
        try {
            getForgePlayer(player.getName()).sendMessage(MessageGenerator.generate(message));
        } catch (Exception e) {
            //System.out.println("Error while sending message: " + e.getMessage());
        }
    }

    public static void sendMessage(Player executor, Range range, Message message) {
        //logger.debug("Sending roleplayMessage from " + executor.getName() + " with range " + range + " and content " + roleplayMessage);
        EntityPlayerMP player = getForgePlayer(executor.getName());
        /*
        BlockPos loc;
        if (player == null) {
            loc = executor.getLocation();
        } else {
            loc = player.getPosition();
        }

         */
        if (range != Range.NORMAL) {
            message.addComponent(new MessageComponent(" §8(" + range.getSign() + ")"));
        }
        List<Player> receivers = findAllPlayersInRange(executor, range.getDistance());
        for (Player p : receivers) {
            p.sendMessage(message);
        }

        informDistantMasters(executor, message, range.getDistance());
        DiscordBridge.sendMessage(message.toString());
    }


    public static void sendMessageFromAndInformMasters(Player player, Message message) {
        Range range = player.getDefaultRange();
        List<Player> ll1 = findAllPlayersInRange(player, range.getDistance());
        if (range != Range.NORMAL) {
            try {
                message.addComponent(new MessageComponent(" §8(" + range.getSign() + ")"));
            } catch (Exception ignored) {

            }
        }

        Player theMaster = null;
        Combat c = DataHolder.inst().getCombatForPlayer(player.getName());
        if (c != null) {
            for (Player master : DataHolder.inst().getMasters()) {
                System.out.println("master: " + master.getName());
                if (master.getAttachedCombatID() == c.getId()) {
                    EntityPlayerMP forgePlayer = ServerProxy.getForgePlayer(master.getName());
                    System.out.println("forge player is " + forgePlayer);
                    if (forgePlayer != null) {
                        theMaster = master;
                        ll1.remove(theMaster);
                        break;
                    }
                }
            }
        }

        List<Player> masters = new ArrayList<Player>();
        for (Player master : DataHolder.inst().getMasters()) {
            if (getForgePlayer(master.getName()) == null) continue;
            if (hasPermission(master.getName(), Permission.GM.get())) {
                if (master.getName().equals(player.getName())) continue;
                if (theMaster != null && master.getName().equals(theMaster.getName())) continue;
                if (ll1.contains(master)) continue;

                double rangePreference = master.getRangePreference();

                EntityPlayerMP forgeMaster = getForgePlayer(master.getName());
                double distance = getDistanceBetween(player.getName(), master.getName());
//                System.out.println("Distance is " + distance);

//                logger.debug("RangePref for " + master.getName() + " is " + rangePreference);
//                logger.debug("Distance between master and player is " + distance);
//                logger.debug("Range distance is " + rangeDistance);
                if (distance > range.getDistance()) {
//                    logger.debug("Master is out of range");
                    //if his rangePreference allows him to hear roleplayMessage
                    if (rangePreference == 0 ||                                         //if master wants to hear everything
                            distance <= rangePreference
                    ) {
                        masters.add(master);
                    }
                }

            }
        }

        if (theMaster != null) theMaster.sendMessage(message);

        for (Player p : ll1) {
            p.sendMessage(message);
        }

        if (message == null) return;
        Message masterMessage = message;
        try {
            masterMessage.dim();
        } catch (Exception ignored) {

        }
        for (Player master : masters) {
            master.sendMessage(masterMessage);
        }

        try {
            DiscordBridge.sendMessage(masterMessage.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendMessageFromAndForPlayersInCombat(Player theMaster, Message message, Combat combat, Range range) {
        List<Player> ll1 = findAllPlayersInRange(theMaster, range.getDistance());
        List<Player> ll2 = combat.getFighters();

        if (range != Range.NORMAL) {
            try {
                message.addComponent(new MessageComponent(" §8(" + range.getSign() + ")"));
            } catch (Exception ignored) {

            }
        }

        EntityPlayerMP forgePlayer = ServerProxy.getForgePlayer(theMaster.getName());
        System.out.println("forge player is " + forgePlayer);
        if (forgePlayer != null) {
            ll1.remove(theMaster);
            ll2.remove(theMaster);
        }

        List<Player> masters = new ArrayList<Player>();
        for (Player master : DataHolder.inst().getMasters()) {
            if (getForgePlayer(master.getName()) == null) continue;
            if (hasPermission(master.getName(), Permission.GM.get())) {
                if (master.getName().equals(theMaster.getName())) continue;
                if (theMaster != null && master.getName().equals(theMaster.getName())) continue;
                if (ll1.contains(master) || ll2.contains(master)) continue;

                double rangePreference = master.getRangePreference();

                EntityPlayerMP forgeMaster = getForgePlayer(master.getName());
                double distance = getDistanceBetween(theMaster.getName(), master.getName());
//                System.out.println("Distance is " + distance);

//                logger.debug("RangePref for " + master.getName() + " is " + rangePreference);
//                logger.debug("Distance between master and player is " + distance);
//                logger.debug("Range distance is " + rangeDistance);
                if (distance > range.getDistance()) {
//                    logger.debug("Master is out of range");
                    //if his rangePreference allows him to hear roleplayMessage
                    if (rangePreference == 0 ||                                         //if master wants to hear everything
                            distance <= rangePreference
                    ) {
                        masters.add(master);
                    }
                }

            }
        }

        if (theMaster != null) theMaster.sendMessage(message);

        for (Player p : ll1) {
            ll2.remove(p);
            p.sendMessage(message);
        }
        for (Player p : ll2) {
            p.sendMessage(message);
        }

        if (message == null) return;
        Message masterMessage = message;
        try {
            masterMessage.dim();
        } catch (Exception ignored) {

        }
        for (Player master : masters) {
            master.sendMessage(masterMessage);
        }

        try {
            DiscordBridge.sendMessage(masterMessage.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // find all players in range of two given players and send them messages w/o duplicates
    public static void sendMessageFromTwoSources(Player pl1, Player pl2, Range range, Message roleplayMessage) {
        List<Player> ll1 = findAllPlayersInRange(pl1, range.getDistance());
        List<Player> ll2 = findAllPlayersInRange(pl2, range.getDistance());

        if (range != Range.NORMAL) {
            try {
                roleplayMessage.addComponent(new MessageComponent(" §8(" + range.getSign() + ")"));
            } catch (Exception ignored) {

            }
        }

        for (Player p : ll1) {
            ll2.remove(p);
            p.sendMessage(roleplayMessage);
        }
        for (Player p : ll2) {
            p.sendMessage(roleplayMessage);
        }
    }

    public static void informDistantMasters(Player player, Message message, double rangeDistance) {
//        System.out.println("Trying to inform distant masters!");
        if (message == null) return;
        try {
            message.dim();
        } catch (Exception ignored) {

        }
//        System.out.println(message.toString());

        for (Player master : DataHolder.inst().getPlayerList()) {
            if (getForgePlayer(master.getName()) == null) continue;
            if (hasPermission(master.getName(), Permission.GM.get())) {
                if (master.getName().equals(player.getName())) continue;

                double rangePreference = master.getRangePreference();

                EntityPlayerMP forgeMaster = getForgePlayer(master.getName());
                double distance = getDistanceBetween(player.getName(), master.getName());
//                System.out.println("Distance is " + distance);

//                logger.debug("RangePref for " + master.getName() + " is " + rangePreference);
//                logger.debug("Distance between master and player is " + distance);
//                logger.debug("Range distance is " + rangeDistance);
                if (distance > rangeDistance) {
//                    logger.debug("Master is out of range");
                    //if his rangePreference allows him to hear roleplayMessage
                    if (rangePreference == 0 ||                                         //if master wants to hear everything
                            distance <= rangePreference
                    ) {
                        master.sendMessage(message);
                    }
                }

            }
        }

        try {
            DiscordBridge.sendMessage(message.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void sendMessageFromAndForAllPlayersInCombatAndInformMasters(Combat combat, Range range, Message roleplayMessage) {
        List<Player> ll1 = new ArrayList<>();
        for (Player player : combat.getFighters()) {
            for (Player player1 : getAllFightersInRange(player, range.getDistance())) {
                if (!ll1.contains(player1)) ll1.add(player1);
            }

        }

        if (range != Range.NORMAL) {
            try {
                roleplayMessage.addComponent(new MessageComponent(" §8(" + range.getSign() + ")"));
            } catch (Exception ignored) {

            }
        }

        Player theMaster = null;
        if (combat != null) {
            for (Player master : DataHolder.inst().getMasters()) {
                System.out.println("master: " + master.getName());
                if (master.getAttachedCombatID() == combat.getId()) {
                    EntityPlayerMP forgePlayer = ServerProxy.getForgePlayer(master.getName());
                    System.out.println("forge player is " + forgePlayer);
                    if (forgePlayer != null) {
                        theMaster = master;
                        ll1.remove(theMaster);
                        break;
                    }
                }
            }
        }

        List<Player> masters = new ArrayList<Player>();
        for (Player master : DataHolder.inst().getMasters()) {
            if (getForgePlayer(master.getName()) == null) continue;
            if (hasPermission(master.getName(), Permission.GM.get())) {
                if (ll1.contains(master)) continue;
                if (theMaster != null && master.getName().equals(theMaster.getName())) continue;

                double rangePreference = master.getRangePreference();

                EntityPlayerMP forgeMaster = getForgePlayer(master.getName());
                double distance = getDistanceBetween(theMaster.getName(), master.getName());
//                System.out.println("Distance is " + distance);

//                logger.debug("RangePref for " + master.getName() + " is " + rangePreference);
//                logger.debug("Distance between master and player is " + distance);
//                logger.debug("Range distance is " + rangeDistance);
                if (distance > range.getDistance()) {
//                    logger.debug("Master is out of range");
                    //if his rangePreference allows him to hear roleplayMessage
                    if (rangePreference == 0 ||                                         //if master wants to hear everything
                            distance <= rangePreference
                    ) {
                        masters.add(master);
                    }
                }

            }
        }

        if (theMaster != null) theMaster.sendMessage(roleplayMessage);

        for (Player p : ll1) {
            p.sendMessage(roleplayMessage);
        }


        if (roleplayMessage == null) return;
        Message masterMessage = roleplayMessage;
        try {
            masterMessage.dim();
        } catch (Exception ignored) {

        }
        for (Player master : masters) {
            master.sendMessage(masterMessage);
        }

        try {
            DiscordBridge.sendMessage(masterMessage.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void sendMessageFromTwoSourcesAndInformMasters(Player pl1, Player pl2, Range range, Message roleplayMessage) {
        List<Player> ll1 = findAllPlayersInRange(pl1, range.getDistance());
        List<Player> ll2 = findAllPlayersInRange(pl2, range.getDistance());

        if (range != Range.NORMAL) {
            try {
                roleplayMessage.addComponent(new MessageComponent(" §8(" + range.getSign() + ")"));
            } catch (Exception ignored) {

            }
        }

        Player theMaster = null;
        Combat c = DataHolder.inst().getCombatForPlayer(pl1.getName());
        if (c != null) {
            for (Player master : DataHolder.inst().getMasters()) {
                System.out.println("master: " + master.getName());
                if (master.getAttachedCombatID() == c.getId()) {
                    EntityPlayerMP forgePlayer = ServerProxy.getForgePlayer(master.getName());
                    System.out.println("forge player is " + forgePlayer);
                    if (forgePlayer != null) {
                        theMaster = master;
                        ll1.remove(theMaster);
                        ll2.remove(theMaster);
                        break;
                    }
                }
            }
        }

        List<Player> masters = new ArrayList<Player>();
        for (Player master : DataHolder.inst().getMasters()) {
            if (getForgePlayer(master.getName()) == null) continue;
            if (hasPermission(master.getName(), Permission.GM.get())) {
                if (master.getName().equals(pl1.getName())) continue;
                if (master.getName().equals(pl2.getName())) continue;
                if (theMaster != null && master.getName().equals(theMaster.getName())) continue;
                if (ll1.contains(master) || ll2.contains(master)) continue;

                double rangePreference = master.getRangePreference();

                EntityPlayerMP forgeMaster = getForgePlayer(master.getName());
                double distance = getDistanceBetween(pl1.getName(), master.getName());
                double distance2 = getDistanceBetween(pl2.getName(), master.getName());
                if (distance2 < distance) distance = distance2;
//                System.out.println("Distance is " + distance);

//                logger.debug("RangePref for " + master.getName() + " is " + rangePreference);
//                logger.debug("Distance between master and player is " + distance);
//                logger.debug("Range distance is " + rangeDistance);
                if (distance > range.getDistance()) {
//                    logger.debug("Master is out of range");
                    //if his rangePreference allows him to hear roleplayMessage
                    if (rangePreference == 0 ||                                         //if master wants to hear everything
                            distance <= rangePreference
                    ) {
                        masters.add(master);
                    }
                }

            }
        }

        if (theMaster != null) theMaster.sendMessage(roleplayMessage);

        for (Player p : ll1) {
            ll2.remove(p);
            p.sendMessage(roleplayMessage);
        }
        for (Player p : ll2) {
            p.sendMessage(roleplayMessage);
        }

        if (roleplayMessage == null) return;
        Message masterMessage = roleplayMessage;
        try {
            masterMessage.dim();
        } catch (Exception ignored) {

        }
        for (Player master : masters) {
            master.sendMessage(masterMessage);
        }

        try {
            DiscordBridge.sendMessage(masterMessage.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void sendMessageFromTwoSourcesAndInformMastersWithUniqueMessage(Player pl1, Player pl2, Range range, Message roleplayMessage, Message uniqueMessage) {
        List<Player> ll1 = findAllPlayersInRange(pl1, range.getDistance());
        List<Player> ll2 = findAllPlayersInRange(pl2, range.getDistance());

        if (range != Range.NORMAL) {
            try {
                roleplayMessage.addComponent(new MessageComponent(" §8(" + range.getSign() + ")"));
            } catch (Exception ignored) {

            }
        }

        Player theMaster = null;
        Combat c = DataHolder.inst().getCombatForPlayer(pl1.getName());
        if (c != null) {
            for (Player master : DataHolder.inst().getMasters()) {
                System.out.println("master: " + master.getName());
                if (master.getAttachedCombatID() == c.getId()) {
                    EntityPlayerMP forgePlayer = ServerProxy.getForgePlayer(master.getName());
                    System.out.println("forge player is " + forgePlayer);
                    if (forgePlayer != null) {
                        theMaster = master;
                        ll1.remove(theMaster);
                        ll2.remove(theMaster);
                        break;
                    }
                }
            }
        }

        List<Player> masters = new ArrayList<Player>();
        for (Player master : DataHolder.inst().getMasters()) {
            if (getForgePlayer(master.getName()) == null) continue;
            if (hasPermission(master.getName(), Permission.GM.get())) {
                if (master.getName().equals(pl1.getName())) continue;
                if (master.getName().equals(pl2.getName())) continue;
                if (theMaster != null && master.getName().equals(theMaster.getName())) continue;
                if (ll1.contains(master) || ll2.contains(master)) continue;

                double rangePreference = master.getRangePreference();

                EntityPlayerMP forgeMaster = getForgePlayer(master.getName());
                double distance = getDistanceBetween(pl1.getName(), master.getName());
                double distance2 = getDistanceBetween(pl2.getName(), master.getName());
                if (distance2 < distance) distance = distance2;
//                System.out.println("Distance is " + distance);

//                logger.debug("RangePref for " + master.getName() + " is " + rangePreference);
//                logger.debug("Distance between master and player is " + distance);
//                logger.debug("Range distance is " + rangeDistance);
                if (distance > range.getDistance()) {
//                    logger.debug("Master is out of range");
                    //if his rangePreference allows him to hear roleplayMessage
                    if (rangePreference == 0 ||                                         //if master wants to hear everything
                            distance <= rangePreference
                    ) {
                        masters.add(master);
                    }
                }

            }
        }

        if (theMaster != null) theMaster.sendMessage(uniqueMessage);

        for (Player p : ll1) {
            ll2.remove(p);
            p.sendMessage(roleplayMessage);
        }
        for (Player p : ll2) {
            p.sendMessage(roleplayMessage);
        }

        if (uniqueMessage == null) return;
        try {
            uniqueMessage.dim();
        } catch (Exception ignored) {

        }
        for (Player master : masters) {
            master.sendMessage(uniqueMessage);
        }

        try {
            DiscordBridge.sendMessage(uniqueMessage.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void sendRadioMessageFrom(EntityLivingBase player, String channel, String sender, String message) {
        String volume = "9";
        System.out.println("reciever1" + player.getName());
        if(player instanceof EntityNPCInterface) {
            volume = getRadiopackVolumeByChannelNpc((NPCWrapper) ((EntityNPCInterface) player).wrappedNPC,channel); //TODO ???
        } else {
            volume = getRadiopackVolumeByChannel((EntityPlayer) player, channel);
        }
        Range range = Range.QUIET;
        switch (volume) {
            case "3":
                range = Range.WHISPER;
                volume = " (тихо)";
                break;
            case "9":
                range = Range.QUIET;
                volume = " (приглушенно)";
                break;
            case "18":
                range = Range.NORMAL;
                volume = "";
                break;
            case "36":
                range = Range.LOUD;
                volume = " (громко)";
                break;
        }
        Player backpacker = DataHolder.inst().getPlayer(player.getName());
        System.out.println("reciever " + player.getName());
        System.out.println("sender " + sender);
        List<Player> ll1 = findAllPlayersInRange(player, range.getDistance());
        System.out.println("test");
        Message toBack = new Message("[§7"+channel+volume+"§f] [§a" + sender + "§f] [" + message + "]");
        System.out.println("test1");
        Message toAll = new Message("[§7Из радио "+player.getName()+volume+"§f] [§a" + sender + "§f] [" + message + "]");
        System.out.println("test2");
        if(backpacker != null) backpacker.sendMessage(toBack);
        System.out.println("test3");
        for (Player p : ll1) {
            System.out.println(p.getName());
            if (!p.isOnline()) continue;
            System.out.println(p.getName());
            if (ServerProxy.getForgePlayer(p.getName()) == null) continue;
            if(Objects.equals(p.getName(), player.getName())) continue;
            System.out.println(p.getName());
            p.sendMessage(toAll);
        }

    }

    public static void sendRadioMessageToBackpacker(Player player, String channel, String sender, String message) {
        Message toBack = new Message("[§7"+channel+"§f] [§a" + sender + "§f] [" + message + "]");
        sendToAllMasters(toBack);
        player.sendMessage(toBack);
    }

    // SENDING MESSAGES ---}}}

    // {{{--- FINDING PLAYERS IN RANGE

    public static List<Player> findAllPlayersInRange(Player source, int rangeAsInt) {
        List<Player> result = new LinkedList<>();
        for (Player player : DataHolder.inst().getPlayerList()) {
            if (getDistanceBetween(source.getName(), player.getName()) <= rangeAsInt) {
                result.add(player);
            }
        }
        return result;
    }

    public static List<Player> findAllPlayersInRange(EntityLivingBase source, int rangeAsInt) {
        List<Player> result = new LinkedList<>();
        for (Player player : DataHolder.inst().getPlayerList()) {
            if (!player.isOnline()) continue;
            System.out.println("test + " + player.getName());
            EntityPlayer forgePlayer = getForgePlayer(player.getName());
            if (forgePlayer == null) continue;
            if (MovementTracker.getDistanceBetween(source.posX, source.posY, source.posZ,
                    forgePlayer.posX, forgePlayer.posY, forgePlayer.posZ) <= rangeAsInt) {
                result.add(player);
                System.out.println("oorah");
            }
        }
        return result;
    }

    public static List<Player> findAllPlayersInRange(BlockPos blockPos, int rangeAsInt) {
        List<Player> result = new LinkedList<>();
        for (Player player : DataHolder.inst().getPlayerList()) {
            EntityPlayer forgePlayer = getForgePlayer(player.getName());
            if (MovementTracker.getDistanceBetween(blockPos.getX(), blockPos.getY(), blockPos.getZ(),
                    forgePlayer.posX, forgePlayer.posY, forgePlayer.posZ) <= rangeAsInt) {
                result.add(player);
            }
        }
        return result;
    }

    public static List<Player> getAllFightersInRange(Player source, double weaponRange) {
        List<Player> result = new ArrayList<>();

        for (Player player : DataHolder.inst().getPlayerList()) {
            if (player.equals(source)) continue;
            if (player.hasPermission(Permission.GM)) continue;

            double diff = getDistanceBetween(source, player);

            //System.out.println("Diff between " + source.getName() + " and " + player.getName() + " is " + diff);
            if (diff <= weaponRange) result.add(player);
        }
        for (NPC npc : DataHolder.inst().getNpcList()) {
            if (npc.equals(source)) continue;

            double diff = getDistanceBetween(source, npc);
            //System.out.println("Diff between " + source.getName() + " and " + npc.getName() + " is " + diff);

            if (diff <= weaponRange) result.add(npc);
        }
        return result;
    }

    public static List<Player> getAllFightersInRangeForCombat(Player source, double weaponRange) {
        List<Player> result = new ArrayList<>();
        int combatId = source.getAttachedCombatID();
        if (combatId == -1) {
            source.sendMessage(new Message("Нет боя.", ChatColor.RED));
            return result;
        }
        for (Player player : DataHolder.inst().getPlayerList()) {
            if (player.equals(source)) continue;
            if (player.hasPermission(Permission.GM)) continue;
            if (DataHolder.inst().getCombatForPlayer(player.getName()) == null) continue;
            if (DataHolder.inst().getCombatForPlayer(player.getName()).getId() != combatId) continue;
            double diff = getDistanceBetween(source, player);

            //System.out.println("Diff between " + source.getName() + " and " + player.getName() + " is " + diff);
            if (diff <= weaponRange) result.add(player);
        }
        for (NPC npc : DataHolder.inst().getNpcList()) {
            if (npc.equals(source)) continue;

            double diff = getDistanceBetween(source, npc);
            //System.out.println("Diff between " + source.getName() + " and " + npc.getName() + " is " + diff);

            if (diff <= weaponRange) result.add(npc);
        }
        return result;
    }

    public static List<Player> getVisibleTargets(Player attacker) {
        DataHolder dh = DataHolder.inst();
        List<Player> result = new ArrayList<>();
        Combat combat = dh.getCombatForPlayer(attacker.getName());

        EntityLivingBase att = ServerProxy.getForgePlayer(attacker.getName());
        if (dh.isNpc(attacker.getName())) att = dh.getNpcEntity(attacker.getName());


        for (Player p : dh.getPlayerAndNpcList()) {
            try {
                if (p.hasPermission(Permission.GM)) continue;
                if (p.getName().equals(attacker.getName())) continue;
                // IF PLAYER
                if (!dh.isNpc(p.getName())) {
                    if (att
                            .canEntityBeSeen(
                                    ServerProxy.getForgePlayer(p.getName())
                            )) {
                        result.add(p);
                    }

                    // IF NPC
                } else {
//                System.out.println(p.getName() + " is npc");
                    EntityLivingBase npc = dh.getNpcEntity(p.getName());
                    if (npc == null) {
                        System.out.println("FUCK! npc entity is null");
                    }
                    if (att
                            .canEntityBeSeen(
                                    dh.getNpcEntity(p.getName())
                            )) {
                        result.add(p);
//                    System.out.println("added " + p.getName());
                    }
                }
            } catch (NullPointerException e) { continue; };
        }
        return result;
    }



    @Deprecated
    public static double getDistanceBetween(Player pl1, Player pl2) {
        return getRoundedDistanceBetween(pl1.getName(), pl2.getName());
    }

    public static double getRoundedDistanceBetween(Player pl1, Player pl2) {
        return getRoundedDistanceBetween(pl1.getName(), pl2.getName());
    }


    public static double getHeightDiffBetween(EntityLivingBase pl1, EntityLivingBase pl2) {
        return pl1.posY - pl2.posY;
    }

    public static double getDistanceBetween(EntityLivingBase pl1, Player pl2) {
        Entity fp2 = getForgePlayer(pl2.getName());
        return MovementTracker.getDistanceBetween(
                pl1.posX, pl1.posY, pl1.posZ,
                fp2.posX, fp2.posY, fp2.posZ
        );
    }

    public static double getDistanceBetween(EntityLivingBase pl1, EntityLivingBase pl2) {
        return MovementTracker.getDistanceBetween(
                pl1.posX, pl1.posY, pl1.posZ,
                pl2.posX, pl2.posY, pl2.posZ
        );
    }

    public static double getRoundedDistanceBetweenButCountEyeHeight(EntityLivingBase pl1, EntityLivingBase pl2) {
        return MovementTracker.getDistanceBetween(
                Math.floor(pl1.posX) + 0.5, pl1.posY + pl1.getEyeHeight(), Math.floor(pl1.posZ) + 0.5,
                        Math.floor(pl2.posX) + 0.5, pl2.posY + pl2.getEyeHeight(), Math.floor(pl2.posZ) + 0.5
        );
    }

    public static double getRoundedDistanceBetweenButCountEyeHeightForOne(EntityLivingBase pl1, EntityLivingBase pl2) {
        return MovementTracker.getDistanceBetween(
                Math.floor(pl1.posX) + 0.5, pl1.posY + pl1.getEyeHeight(), Math.floor(pl1.posZ) + 0.5,
                Math.floor(pl2.posX) + 0.5, pl2.posY, Math.floor(pl2.posZ) + 0.5
        );
    }

    public static double getDistanceBetween(String name1, String name2) {
//        System.out.println("Getting distance between " + name1 + " and " + name2);
        Entity fp1 = getForgePlayer(name1);
        if (fp1 == null) {
//            System.out.println("fp1 is null");
            fp1 = DataHolder.inst().getNpcEntity(name1);
        }
        Entity fp2 = getForgePlayer(name2);
        if (fp2 == null) {
//            System.out.println("fp2 is null");
            fp2 = DataHolder.inst().getNpcEntity(name2);
        }

        if (fp1 == null || fp2 == null) {
//            System.out.println("SOMETHING STILL NULL! " + fp1 + " " + fp2);
            return 999;
        }

        // if players are in different worlds, return 0
        if (!fp1.getEntityWorld().equals(fp2.getEntityWorld())) {
            return 999;
        }

        return MovementTracker.getDistanceBetween(
                fp1.posX, fp1.posY, fp1.posZ,
                fp2.posX, fp2.posY, fp2.posZ
        );

        /*
        // get locations. If locations of NPC's and not players are requested, get them from DataHolder
        BlockPos loc1;
        if (fp1 == null) {
            loc1 = DataHolder.getInstance().getPlayer(name1).getLocation();
        } else {
            loc1 = fp1.getPosition();
        }


        BlockPos loc2;
        try {
            if (fp2 == null) {
                loc2 = DataHolder.getInstance().getPlayer(name2).getLocation();
            } else {
                loc2 = fp2.getPosition();
            }
        } catch (Exception e) {
            System.out.println("§4ПРОИЗОШЛА СТРАННАЯ ЕБАЛА!");
            System.out.println(e.getMessage());
            return 0;
        }

        return loc1.getDistance(loc2.getX(), loc2.getY(), loc2.getZ());

         */
    }

    public static double getRoundedDistanceBetween(String name1, String name2) {
//        System.out.println("Getting distance between " + name1 + " and " + name2);
        Entity fp1 = getForgePlayer(name1);
        if (fp1 == null) {
//            System.out.println("fp1 is null");
            fp1 = DataHolder.inst().getNpcEntity(name1);
        }
        Entity fp2 = getForgePlayer(name2);
        if (fp2 == null) {
//            System.out.println("fp2 is null");
            fp2 = DataHolder.inst().getNpcEntity(name2);
        }

        if (fp1 == null || fp2 == null) {
//            System.out.println("SOMETHING STILL NULL! " + fp1 + " " + fp2);
            return 999;
        }

        // if players are in different worlds, return 0
        if (!fp1.getEntityWorld().equals(fp2.getEntityWorld())) {
            return 999;
        }

        return MovementTracker.getDistanceBetween(
                Math.floor(fp1.posX) + 0.5, fp1.posY, Math.floor(fp1.posZ) + 0.5,
                Math.floor(fp2.posX) + 0.5, fp2.posY, Math.floor(fp2.posZ) + 0.5
        );

        /*
        // get locations. If locations of NPC's and not players are requested, get them from DataHolder
        BlockPos loc1;
        if (fp1 == null) {
            loc1 = DataHolder.getInstance().getPlayer(name1).getLocation();
        } else {
            loc1 = fp1.getPosition();
        }


        BlockPos loc2;
        try {
            if (fp2 == null) {
                loc2 = DataHolder.getInstance().getPlayer(name2).getLocation();
            } else {
                loc2 = fp2.getPosition();
            }
        } catch (Exception e) {
            System.out.println("§4ПРОИЗОШЛА СТРАННАЯ ЕБАЛА!");
            System.out.println(e.getMessage());
            return 0;
        }

        return loc1.getDistance(loc2.getX(), loc2.getY(), loc2.getZ());

         */
    }

    public static double getDistanceBetween(BlockPos blockPos, Player player) {
//        System.out.println("Getting distance between " + name1 + " and " + name2);
        Entity fp1 = getForgePlayer(player.getName());
        if (fp1 == null) {
//            System.out.println("fp1 is null");
            fp1 = DataHolder.inst().getNpcEntity(player.getName());
            EntityNPCInterface npc = (EntityNPCInterface) fp1;
            return MovementTracker.getDistanceBetween(
                    npc.wrappedNPC.getHomeX(), npc.wrappedNPC.getHomeY(), npc.wrappedNPC.getHomeZ(),
                    blockPos.getX(), blockPos.getY(), blockPos.getZ()
            );
        }


        if (fp1 == null) {
//            System.out.println("SOMETHING STILL NULL! " + fp1 + " " + fp2);
            return 0;
        }

        return MovementTracker.getDistanceBetween(
                fp1.posX, fp1.posY, fp1.posZ,
                blockPos.getX(), blockPos.getY(), blockPos.getZ()
        );
    }

    public static double getDistanceBetweenRounded(BlockPos blockPos, Player player) {
//        System.out.println("Getting distance between " + name1 + " and " + name2);
        Entity fp1 = getForgePlayer(player.getName());
        if (fp1 == null) {
//            System.out.println("fp1 is null");
            fp1 = DataHolder.inst().getNpcEntity(player.getName());
            EntityNPCInterface npc = (EntityNPCInterface) fp1;
            return MovementTracker.getDistanceBetween(
                    npc.wrappedNPC.getHomeX(), npc.wrappedNPC.getHomeY(), npc.wrappedNPC.getHomeZ(),
                    blockPos.getX(), blockPos.getY(), blockPos.getZ()
            );
        }


        if (fp1 == null) {
//            System.out.println("SOMETHING STILL NULL! " + fp1 + " " + fp2);
            return 0;
        }

        return MovementTracker.getDistanceBetween(
                Math.floor(fp1.posX) + 0.5, fp1.posY, Math.floor(fp1.posZ) + 0.5,
                blockPos.getX(), blockPos.getY(), blockPos.getZ()
        );
    }

    public static boolean hasPermission(String name, String permission) {
        EntityPlayerMP forgePlayer = getForgePlayer(name);
        if (forgePlayer == null) return false;
        boolean has = PermissionAPI.hasPermission(forgePlayer, permission);
        //System.out.println("Checking " + name + " for permission " + permission + ": " + has);
        return has;
    }

    public static EntityPlayerMP getForgePlayer(String name) {
//        System.out.println("getting player " + name);
        return FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUsername(name);
//            return(EntityPlayerMP) MinecraftServer.getServer().getPlayerList().getPlayer(name);
    }

    // FINDING PLAYERS IN RANGE ---}}}


    @Mod.EventHandler
    public void stopServer(FMLServerStoppingEvent event) {
        System.out.println("KMSystem [FORGE] is stopping!");
        for (Player player : DataHolder.inst().getPlayerList()) {
            player.unfreeze();
        }

    }


    @Mod.EventHandler
    public void startServer(FMLServerStartingEvent event) {
        System.out.println("KMSystem [FORGE] is starting!");
        EVENT_BUS.register(this);
        EVENT_BUS.register(new MovementTracker());


        PermissionAPI.registerNode("km.gm", DefaultPermissionLevel.OP, "Game Master permission");
        PermissionAPI.registerNode("km.tell", DefaultPermissionLevel.NONE, "Private messages permission");
        PermissionAPI.registerNode("km.builder", DefaultPermissionLevel.NONE, "Builder permission");
        //PermissionAPI.registerNode("km.radio", DefaultPermissionLevel.NONE, "Radio permission");

        KMPacketHandler.INSTANCE.registerMessage(PacketMessageHandler.class, PacketMessage.class, 1, Side.CLIENT);
        KMPacketHandler.INSTANCE.registerMessage(ClickContainerPacketMessageHandler.class, ClickContainerMessage.class, 2, Side.CLIENT);
        KMPacketHandler.INSTANCE.registerMessage(ModifierMessageHandler.class, ModifierMessage.class, 3, Side.SERVER);
        KMPacketHandler.INSTANCE.registerMessage(ClientGoMessageHandler.class, ClientGoMessage.class, 4, Side.SERVER);
        KMPacketHandler.INSTANCE.registerMessage(ModifierSyncMessageHandler.class, ModifierSyncMessage.class, 5, Side.SERVER);
        KMPacketHandler.INSTANCE.registerMessage(ThirdPersonMessage.ThirdPersonMessageHandler.class, ThirdPersonMessage.class, 6, Side.SERVER);
        KMPacketHandler.INSTANCE.registerMessage(ThirdPersonClientMessage.ThirdPersonClientMessageHandler.class, ThirdPersonClientMessage.class, 7, Side.SERVER);


        event.registerServerCommand(new UpdateExecutor());
        event.registerServerCommand(new WoundsExecutor());
        event.registerServerCommand(new TraitExecutor());
        event.registerServerCommand(new TellExecutor());
        event.registerServerCommand(new SetWoundsExecutor());
        event.registerServerCommand(new SetRangeExecutor());
        event.registerServerCommand(new ReminderExecutor());
        event.registerServerCommand(new RecursiveExecutor());
        event.registerServerCommand(new EquipExecutor());
        event.registerServerCommand(new PacketTestCommand());
        event.registerServerCommand(new ArmorMergeCommand());
        event.registerServerCommand(new AdvExecutor());
        event.registerServerCommand(new VectorTestCommand());
        event.registerServerCommand(new GiveMeArmor());

        event.registerServerCommand(new CombatExecutor());

        event.registerServerCommand(new BoomExecutor());
        event.registerServerCommand(new DecisionExecutor());
        event.registerServerCommand(new BoomDefenseExecutor());
        event.registerServerCommand(new FireExecutor());
        event.registerServerCommand(new FireDefenseExecutor());

        event.registerServerCommand(new AutoDefenseExecutor());
        event.registerServerCommand(new DefensesExecutor());
        event.registerServerCommand(new DiscordBotExecutor());
        event.registerServerCommand(new DiscordMessageExecutor());
        event.registerServerCommand(new DiscordCallExecutor());
        event.registerServerCommand(new DiscordNotifyMastersExecutor());
        event.registerServerCommand(new DiscordNotifyMastersMineExecutor());
        event.registerServerCommand(new ScriptExec());
        event.registerServerCommand(new DiscordNotifySilentExecutor());
        event.registerServerCommand(new EffectsExecutor());
        event.registerServerCommand(new ManaExecutor());
        event.registerServerCommand(new CycleTagExecutor());
        event.registerServerCommand(new WeightExecutor());
        event.registerServerCommand(new ExpExecutor());
        event.registerServerCommand(new PseudonymExecutor());
        event.registerServerCommand(new MagicExecutor());
        event.registerServerCommand(new ExamineExecutor());
        event.registerServerCommand(new CoverExecutor());
        event.registerServerCommand(new TriggerOpportunityExecutor());
        event.registerServerCommand(new ShootExecutor());

        // NPC
        event.registerServerCommand(new ChooseNpcCommand());
        event.registerServerCommand(new NpcCommand());

        // ATTACK
        event.registerServerCommand(new AttackCommand());
        event.registerServerCommand(new ActiveHandAttack());
        event.registerServerCommand(new ChooseAttackSkill());
        event.registerServerCommand(new SelectOpponentCommand());
        event.registerServerCommand(new PerformAttackCommand());
        event.registerServerCommand(new InitAttackCommand());

        // DEFENSE
        event.registerServerCommand(new SelectDefenseCommand());
        event.registerServerCommand(new PerformDefenseCommand());
        event.registerServerCommand(new SelectDefenseItem());
        event.registerServerCommand(new ActiveHandDefense());
        event.registerServerCommand(new ChooseDefenseSkill());
        event.registerServerCommand(new SurrenderExecutor());


        // MODIFIERS
        event.registerServerCommand(new ModifiersButton());
        event.registerServerCommand(new ChooseModifier());
        event.registerServerCommand(new Concentration());
        event.registerServerCommand(new DefensiveStance());
        event.registerServerCommand(new AimedMode());
        event.registerServerCommand(new SetModifier());
        event.registerServerCommand(new SyncModifiers());

        // TURN
        event.registerServerCommand(new ToolPanel());
        event.registerServerCommand(new PerformAction());
        event.registerServerCommand(new ToRoot());
        event.registerServerCommand(new NextExecutor());
        event.registerServerCommand(new QueueExecutor());
        event.registerServerCommand(new PerformShift());

        // MOVEMENT
        event.registerServerCommand(new GoExecutor());
        event.registerServerCommand(new RunExecutor());
        event.registerServerCommand(new PerformMovement());
        event.registerServerCommand(new MoveExecutor());


        // DEBUG
        event.registerServerCommand(new DuelDebug());
        event.registerServerCommand(new PurgeSkillSaverExecutor());
        event.registerServerCommand(new RemoveSubordinateExecutor());
        event.registerServerCommand(new PercentDiceExecutor());
        event.registerServerCommand(new HoverExecutor());
        event.registerServerCommand(new SubExecutor());
        event.registerServerCommand(new PurgeModifiersExecutor());
        event.registerServerCommand(new ServerPurgeContainersExecutor());

        event.registerServerCommand(new LoadCommand());
        event.registerServerCommand(new UnloadCommand());


        DataHolder.inst();
        System.out.println("DataHolder initiated");
        DiscordBridge.sendMessage("**Server is starting!**");

        if (ForgeRegistries.BLOCKS.getValue( new ResourceLocation("stewblocks", "effect_fire" )) != null) FIRE_BLOCK = ForgeRegistries.BLOCKS.getValue( new ResourceLocation("stewblocks", "effect_fire" )).getDefaultState();;


    }

    public static void sendMewtsage(String command) {
        MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
        server.futureTaskQueue.add(new FutureTask(new Runnable() {
            @Override
            public void run() {
                server.getCommandManager().executeCommand(server, "noppes script run sendmessagefrommewt " + command);
            }
        }, null));
    }

    public static void sendRadioMessage(Player player, String radiomessage, Range range, boolean ooc) {
        String id = getRadiomicId(player);
        if (id == null) {
            //TODO:send to discord here
            return;
        }
        EntityLivingBase playerBack = null;
        String channel = "";
        IWorld iWorld = NpcAPI.Instance().getIWorld(ServerProxy.getForgePlayer(player.getName()).dimension);
        IEntity[] list = iWorld.getAllEntities(EntityType.NPC);
        boolean maxLoud = false;
        int stationDistanceGood;
        int stationDistanceBad;
        int backpackDistanceGood;
        int backpackDistanceBad;
        if(range.getDistance() < Range.QUIET.getDistance() && range.getDistance() > 0) {
            stationDistanceGood = 100;
            stationDistanceBad = 1750;
            backpackDistanceGood = 25;
            backpackDistanceBad = 250;
        } else if(range.getDistance() < Range.NORMAL.getDistance()) {
            stationDistanceGood = 500;
            stationDistanceBad = 1750;
            backpackDistanceGood = 50;
            backpackDistanceBad = 250;
        } else if (range.getDistance() == Range.NORMAL.getDistance()){
            stationDistanceGood = 1000;
            stationDistanceBad = 1750;
            backpackDistanceGood = 100;
            backpackDistanceBad = 250;
        } else {
            stationDistanceGood = 1500;
            stationDistanceBad = 1750;
            backpackDistanceGood = 200;
            backpackDistanceBad = 250;
            maxLoud = true;
        }
        List<Player> ll1 = findAllPlayersInRange(player, 6);
        for (Player player1 : ll1) {
            channel = getRadiopackChannelById(player1, id);
            System.out.println("CHANNEL FOR " + player1.getName() + " " + channel);
            if (!channel.equals("")) {
                playerBack = getForgePlayer(player1.getName());
                break;
            }
        }
        System.out.println("TEST0");
        if (channel.equals("")) {
            for (IEntity iEntity : list) {
                if (iEntity.getName().contains("[Радио") || ((NPCWrapper) iEntity).getDisplay().getTitle().contains("[Радио]")) {
                    if (getDistanceBetween((EntityLivingBase) ((NPCWrapper) iEntity).getMCEntity(), getForgePlayer(player.getName())) <= 6) {
                        if (iEntity.getName().contains("[Радиостанция]")) {
                            if (!((NPCWrapper) iEntity).getDisplay().getTitle().isEmpty() && !((NPCWrapper) iEntity).getDisplay().getTitle().contains("Включено")) continue;
                        }
                        channel = getRadiopackChannelByIdNpc((NPCWrapper) iEntity, id);
                        if (!channel.equals("")) {
                            playerBack = (EntityLivingBase) ((NPCWrapper) iEntity).getMCEntity();
                            break;
                        }
                    }
                }
            }
        }

        System.out.println(playerBack);
        if (channel.equals("") || playerBack == null) {
            player.sendMessage("Не найден радиорюкзак с ID " + id + " поблизости!");
            return;
        }
        List<EntityLivingBase> allrecievers = new LinkedList<>();
        for (Player player2 : DataHolder.inst().getPlayerList()) {
            if (!player2.isOnline()) continue;
            System.out.println(player2.getName());
            if(!Objects.equals(player2.getName(), playerBack.getName()) && Objects.equals(getRadiopackChannel(player2), channel) && !player2.hasPermission(Permission.GM)) {
                allrecievers.add(getForgePlayer(player2.getName()));
            }
        }

        for (IEntity iEntity : list) {
            if (iEntity.getName().contains("[Радио") || ((NPCWrapper) iEntity).getDisplay().getTitle().contains("[Радио]")) {
                System.out.println(iEntity.getName());
                if (iEntity.getName().contains("[Радиостанция]")) {
                    if (!((NPCWrapper) iEntity).getDisplay().getTitle().isEmpty() && !((NPCWrapper) iEntity).getDisplay().getTitle().contains("Включено")) continue;
                }
                if (!Objects.equals(iEntity.getName(), playerBack.getName()) && Objects.equals(getRadiopackChannelNpc((NPCWrapper) iEntity), channel) && !Objects.equals(iEntity.getName(), player.getPseudonym())) {
                    allrecievers.add((EntityLivingBase) ((NPCWrapper) iEntity).getMCEntity());
                }

            }
        }

        String[] stations = ((String) iWorld.getStoreddata().get("stations")).split(":");

        boolean retranslated_bad = false;
        boolean retranslated_good = false;
        System.out.println("TEST1");
        for (String station : stations) {
            if (station.isEmpty()) continue;
            if (iWorld.getStoreddata().has("station" + station)) {
                if (((String) iWorld.getStoreddata().get("station" + station)).equals("offline")) continue;
                String[] posString = ((String) iWorld.getStoreddata().get("station" + station)).split(":");
                BlockPos blockPos = new BlockPos(Integer.parseInt(posString[0]), Integer.parseInt(posString[1]), Integer.parseInt(posString[2]));
                double mod = Double.parseDouble(posString[3]);
                double distance = MovementTracker.getDistanceBetween(playerBack.posX, playerBack.posY, playerBack.posZ,
                        blockPos.getX(), blockPos.getY(), blockPos.getZ());
                if (distance <= stationDistanceBad * mod) {
                    if (iWorld.getStoreddata().has("whitelist" + station)) {
                        boolean passed = false;
                        try {
                        double channelN = Double.parseDouble(channel);
                        String whitelist = (String) iWorld.getStoreddata().get("whitelist" + station);
                        if (whitelist.contains(";")) {
                            String[] whit = whitelist.split(";");
                            for (String wh : whit) {
                                String[] w = wh.split("-");
                                if (Double.parseDouble(w[0]) <= channelN && channelN <= Double.parseDouble(w[1])) {
                                    passed = true;
                                    break;
                                }
                            }
                        } else {
                            String[] w = whitelist.split("-");
                            if (Double.parseDouble(w[0]) <= channelN && channelN <= Double.parseDouble(w[1])) {
                                passed = true;
                            }
                        }
                        } catch (Exception ignored) {

                        }
                        if (!passed) continue;
                    }
                    if (distance <= (stationDistanceGood * mod)) {
                        System.out.println("RADIO connected to " + station + " distance is " + distance);
                        retranslated_good = true;
                        break;
                    } else if (distance <= stationDistanceBad * mod) {
                        retranslated_bad = true;
                    }
                }
            }
        }

        List<EntityLivingBase> received_bad = new LinkedList<>();
        List<EntityLivingBase> received_good = new LinkedList<>();
        for (EntityLivingBase receiver : allrecievers) {
            double distance = getDistanceBetween(receiver, playerBack);
            if(distance <= backpackDistanceGood) {
                if(!received_good.contains(receiver)) {
                    received_good.add(receiver);
                    System.out.println("RADIO connected to backpack" + " distance is " + distance);
                }
            } else if (distance <= backpackDistanceBad) {
                if(!received_bad.contains(receiver)) received_bad.add(receiver);
            }
        }

        System.out.println("TEST2");
        if (retranslated_bad || retranslated_good) {
            for (EntityLivingBase receiver : allrecievers) {
                if (received_good.contains(receiver)) continue;
                for (String station : stations) {
                    if (station.isEmpty()) continue;
                    if (iWorld.getStoreddata().has("station" + station)) {
                        if (((String) iWorld.getStoreddata().get("station" + station)).equals("offline")) continue;
                        String[] posString = ((String) iWorld.getStoreddata().get("station" + station)).split(":");
                        BlockPos blockPos = new BlockPos(Integer.parseInt(posString[0]), Integer.parseInt(posString[1]), Integer.parseInt(posString[2]));
                        double mod = Double.parseDouble(posString[3]);
                        double distance = MovementTracker.getDistanceBetween(receiver.posX, receiver.posY, receiver.posZ,
                                blockPos.getX(), blockPos.getY(), blockPos.getZ());
                        if (distance <= stationDistanceBad * mod) {
                            if (iWorld.getStoreddata().has("whitelist" + station)) {
                                boolean passed = false;
                                try {
                                    double channelN = Double.parseDouble(channel);
                                    String whitelist = (String) iWorld.getStoreddata().get("whitelist" + station);
                                    if (whitelist.contains(";")) {
                                        String[] whit = whitelist.split(";");
                                        for (String wh : whit) {
                                            String[] w = wh.split("-");
                                            if (Double.parseDouble(w[0]) <= channelN && channelN <= Double.parseDouble(w[1])) {
                                                passed = true;
                                                break;
                                            }
                                        }
                                    } else {
                                        String[] w = whitelist.split("-");
                                        if (Double.parseDouble(w[0]) <= channelN && channelN <= Double.parseDouble(w[1])) {
                                            passed = true;
                                        }
                                    }
                                } catch (Exception ignored) {

                                }
                                if (!passed) continue;
                            }
                            if ((distance <= (stationDistanceGood * mod)) && retranslated_good) {
                                if (!received_good.contains(receiver)) {
                                    received_good.add(receiver);
                                    System.out.println("RADIORECIEVER connected to " + station + " distance is " + distance);
                                }
                                break;
                            } else if (distance <= stationDistanceBad * mod) {
                                if (!received_bad.contains(receiver)) received_bad.add(receiver);
                            }
                        }
                    }
                }
            }



        }
        System.out.println("TEST3");
        radiomessage = radiomessage.trim();
        String rad;
        if (range != Range.NORMAL && range != Range.NORANGE) {
            if (!ooc) {
                rad = "(" + range.getDescription() + ") " + radiomessage;
            } else {
                rad = "(" + range.getDescription() + " в OOC) §d(( " + radiomessage + " §d))§f";
            }
        } else {
            if (!ooc) {
                rad = radiomessage;
            } else {
                rad = "(в OOC) §d(( " + radiomessage + " §d))§f";
            }
        }
        String name = player.getName();
        if (player.hasPermission(Permission.GM) && !player.getPseudonym().isEmpty()) name = player.getPseudonym();

        System.out.println(received_good);
        System.out.println(received_bad);
        System.out.println("TEST4");
        Player backpack = DataHolder.inst().getPlayer(playerBack.getName());

        Message toBack = new Message("[§7"+channel+"§f] [§a" + name + "§f] [" + rad + "§f]");
        sendToAllMasters(toBack);
        if (backpack != null && !backpack.hasPermission(Permission.GM)) backpack.sendMessage(toBack);
        if (player != backpack && !player.hasPermission(Permission.GM)) {
            player.sendMessage(new Message("[§7В радио "+ playerBack.getName()+"§f] [§a" + name + "§f] [" + rad + "§f]"));
        }

        for (EntityLivingBase reciever : received_good) {
            sendRadioMessageFrom(reciever, channel, name, rad);
        }
        String bad;
        if (maxLoud) {
            bad = "§7§o***сильные помехи заглушают голос***§f";
        } else {
            bad = "§7§o***голос слишком тихий, заглушен помехами***§f";
        }
        System.out.println("TEST5");
        for (EntityLivingBase reciever : received_bad) {
            if (!received_good.contains(reciever)) sendRadioMessageFrom(reciever, channel, name, bad);
        }
        DiscordBridge.sendMessage("[" + channel +"] [" + name + "] [" + rad + "]");
    }

    public static void sendRadioFromDiscordMessage(String name, String channel, String radiomessage, Range range, boolean ooc) {
        radiomessage = radiomessage.replaceAll("&([a-z0-9])", "§$1");
        IWorld iWorld = NpcAPI.Instance().getIWorld(0);
        IEntity[] list = NpcAPI.Instance().getIWorld(0).getAllEntities(EntityType.NPC);
        boolean maxLoud = false;
        int stationDistanceGood;
        int stationDistanceBad;
        if(range.getDistance() < Range.QUIET.getDistance() && range.getDistance() > 0) {
            stationDistanceGood = 100;
            stationDistanceBad = 1750;
        } else if(range.getDistance() < Range.NORMAL.getDistance()) {
            stationDistanceGood = 500;
            stationDistanceBad = 1750;
        } else if (range.getDistance() == Range.NORMAL.getDistance()){
            stationDistanceGood = 1000;
            stationDistanceBad = 1750;
        } else {
            stationDistanceGood = 1500;
            stationDistanceBad = 1750;
            maxLoud = true;
        }

        System.out.println("TEST1");

        List<EntityLivingBase> allrecievers = new LinkedList<>();
        for (Player player2 : DataHolder.inst().getPlayerList()) {
            if (!player2.isOnline()) continue;
            System.out.println(player2.getName());
            if(Objects.equals(getRadiopackChannel(player2), channel)  && !player2.hasPermission(Permission.GM)) {
                allrecievers.add(getForgePlayer(player2.getName()));
            }
        }

        System.out.println("TEST2");

        for (IEntity iEntity : list) {
            if (iEntity.getName().contains("[Радио") || ((NPCWrapper) iEntity).getDisplay().getTitle().contains("[Радио]")) {
                System.out.println(iEntity.getName());
                if (iEntity.getName().contains("[Радиостанция]")) {
                    if (!((NPCWrapper) iEntity).getDisplay().getTitle().isEmpty() && !((NPCWrapper) iEntity).getDisplay().getTitle().contains("Включено")) continue;
                }
                if (Objects.equals(getRadiopackChannelNpc((NPCWrapper) iEntity), channel) && !Objects.equals(iEntity.getName(), name)) {
                    allrecievers.add((EntityLivingBase) ((NPCWrapper) iEntity).getMCEntity());
                }

            }
        }

        String[] stations = ((String) iWorld.getStoreddata().get("stations")).split(":");

        boolean retranslated_good = true;

        List<EntityLivingBase> received_bad = new LinkedList<>();
        List<EntityLivingBase> received_good = new LinkedList<>();

        System.out.println("TEST3");

        for (EntityLivingBase receiver : allrecievers) {
            if (received_good.contains(receiver)) continue;
            for (String station : stations) {
                if (station.isEmpty()) continue;
                if (iWorld.getStoreddata().has("station" + station)) {
                    if (((String) iWorld.getStoreddata().get("station" + station)).equals("offline")) continue;
                    String[] posString = ((String) iWorld.getStoreddata().get("station" + station)).split(":");
                    BlockPos blockPos = new BlockPos(Integer.parseInt(posString[0]), Integer.parseInt(posString[1]), Integer.parseInt(posString[2]));
                    double mod = Double.parseDouble(posString[3]);
                    double distance = MovementTracker.getDistanceBetween(receiver.posX, receiver.posY, receiver.posZ,
                            blockPos.getX(), blockPos.getY(), blockPos.getZ());
                    if (distance <= stationDistanceBad * mod) {
                        if (iWorld.getStoreddata().has("whitelist" + station)) {
                            boolean passed = false;
                            try {
                                double channelN = Double.parseDouble(channel);
                                String whitelist = (String) iWorld.getStoreddata().get("whitelist" + station);
                                if (whitelist.contains(";")) {
                                    String[] whit = whitelist.split(";");
                                    for (String wh : whit) {
                                        String[] w = wh.split("-");
                                        if (Double.parseDouble(w[0]) <= channelN && channelN <= Double.parseDouble(w[1])) {
                                            passed = true;
                                            break;
                                        }
                                    }
                                } else {
                                    String[] w = whitelist.split("-");
                                    if (Double.parseDouble(w[0]) <= channelN && channelN <= Double.parseDouble(w[1])) {
                                        passed = true;
                                    }
                                }
                            } catch (Exception ignored) {

                            }
                            if (!passed) continue;
                        }
                        if ((distance <= (stationDistanceGood * mod)) && retranslated_good) {
                            if (!received_good.contains(receiver)) {
                                received_good.add(receiver);
                                System.out.println("RADIORECIEVER connected to " + station + " distance is " + distance);
                            }
                            break;
                        } else if (distance <= stationDistanceBad * mod) {
                            if (!received_bad.contains(receiver)) received_bad.add(receiver);
                        }
                    }
                }
            }
        }

        System.out.println("TEST4");

        radiomessage = radiomessage.trim();
        String rad;
        if (range != Range.NORMAL) {
            if (!ooc) {
                rad = "(" + range.getDescription() + ") " + radiomessage;
            } else {
                rad = "(" + range.getDescription() + " в OOC) §d(( " + radiomessage + " §d))§f";
            }
        } else {
            if (!ooc) {
                rad = radiomessage;
            } else {
                rad = "(в OOC) §d(( " + radiomessage + " §d))§f";
            }
        }

        System.out.println(received_good);
        System.out.println(received_bad);

        for (EntityLivingBase reciever : received_good) {
            sendRadioMessageFrom(reciever, channel, name, rad);
        }
        String bad;
        if (maxLoud) {
            bad = "§7§o***сильные помехи заглушают голос***§f";
        } else {
            bad = "§7§o***голос слишком тихий, заглушен помехами***§f";
        }
        for (EntityLivingBase reciever : received_bad) {
            if (!received_good.contains(reciever)) sendRadioMessageFrom(reciever, channel, name, bad);
        }
        System.out.println("TEST5");
        DiscordBridge.sendMessage("[" + channel +"] [" + name + "] [" + rad + "]");
        Message toBack = new Message("[§7"+channel+"§f] [§a" + name + "§f] [" + rad + "§f]");
        sendToAllMasters(toBack);
    }


    static String getRadioChannel(Player player1) {
        EntityPlayer player = ServerProxy.getForgePlayer(player1.getName());
        if (player.getHeldItemMainhand().hasTagCompound()) {
            if (player.getHeldItemMainhand().getTagCompound() != null) {
                if (player.getHeldItemMainhand().getTagCompound().hasKey("radio")) {
                    if (player.getHeldItemMainhand().getTagCompound().getCompoundTag("radio").hasKey("channel")) {
                        return player.getHeldItemMainhand().getTagCompound().getCompoundTag("radio").getString("channel");
                    }
                }
            }
        }
        if (player.getHeldItemOffhand().hasTagCompound()) {
            if (player.getHeldItemOffhand().getTagCompound() != null) {
                if (player.getHeldItemOffhand().getTagCompound().hasKey("radio")) {
                    if (player.getHeldItemOffhand().getTagCompound().getCompoundTag("radio").hasKey("channel")) {
                        return player.getHeldItemOffhand().getTagCompound().getCompoundTag("radio").getString("channel");
                    }
                }
            }
        }
        return "0.0";
    }

    static String getRadiomicId(Player player1) {
        EntityPlayer player = ServerProxy.getForgePlayer(player1.getName());
        if (player.getHeldItemMainhand().hasTagCompound()) {
            if (player.getHeldItemMainhand().getTagCompound() != null) {
                if (player.getHeldItemMainhand().getTagCompound().hasKey("radiomic")) {
                    if (player.getHeldItemMainhand().getTagCompound().getCompoundTag("radiomic").hasKey("id")) {
                        return player.getHeldItemMainhand().getTagCompound().getCompoundTag("radiomic").getString("id");
                    }
                }
            }
        }
        if (player.getHeldItemOffhand().hasTagCompound()) {
            if (player.getHeldItemOffhand().getTagCompound() != null) {
                if (player.getHeldItemOffhand().getTagCompound().hasKey("radiomic")) {
                    if (player.getHeldItemOffhand().getTagCompound().getCompoundTag("radiomic").hasKey("id")) {
                        return player.getHeldItemOffhand().getTagCompound().getCompoundTag("radiomic").getString("id");
                    }
                }
            }
        }
        return null;
    }

    static String getRadiopackChannelById(Player player1, String id) {
        try {
            EntityPlayer player = ServerProxy.getForgePlayer(player1.getName());
            ICAPCustomInventory cap = player.getCapability(INVENTORY_CAP, null);
            CustomInventory inv = cap.getInventory();
            NonNullList<ItemStack> list = inv.getStacks();
            ArrayList<ItemStack> itemStacks = new ArrayList<ItemStack>(list);
            itemStacks.add(player.getHeldItemMainhand());
            itemStacks.add(player.getHeldItemOffhand());
            for (ItemStack is : itemStacks) {
                System.out.println(is);
                if (!is.isEmpty() && is.hasTagCompound()) {
                    if (is.getTagCompound() != null) {
                        if (is.getTagCompound().hasKey("radiopack")) {
                            NBTTagCompound radio = is.getTagCompound().getCompoundTag("radiopack");
                            if (radio.hasKey("id")) {
                                if (radio.getString("id").equals(id)) {
                                    if (radio.hasKey("status")) {
                                        if (radio.getString("status").equals("on")) {
                                            if (radio.hasKey("channel")) {
                                                return radio.getString("channel");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    static String getRadiopackChannelByIdNpc(NPCWrapper npc, String id) {
        try {
            ItemStack is = npc.getInventory().getLeftHand().getMCItemStack();


                System.out.println(is);
                if (!is.isEmpty() && is.hasTagCompound()) {
                    if (is.getTagCompound() != null) {
                        if (is.getTagCompound().hasKey("radiopack")) {
                            NBTTagCompound radio = is.getTagCompound().getCompoundTag("radiopack");
                            if (radio.hasKey("id")) {
                                if (radio.getString("id").equals(id)) {
                                    if (radio.hasKey("status")) {
                                        if (radio.getString("status").equals("on")) {
                                            if (radio.hasKey("channel")) {
                                                return radio.getString("channel");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    static String getRadiopackChannel(Player player1) {
        try {
            EntityPlayer player = ServerProxy.getForgePlayer(player1.getName());
            ICAPCustomInventory cap = player.getCapability(INVENTORY_CAP, null);

            CustomInventory inv = null;
            ArrayList<ItemStack> itemStacks = new ArrayList<ItemStack>();
            if (cap != null) {
                inv = cap.getInventory();
                NonNullList<ItemStack> list = inv.getStacks();
                itemStacks = new ArrayList<ItemStack>(list);
            }

            itemStacks.add(player.getHeldItemMainhand());
            itemStacks.add(player.getHeldItemOffhand());
            for (ItemStack is : itemStacks) {
                if (!is.isEmpty() && is.hasTagCompound()) {
                    if (is.getTagCompound() != null) {
                        if (is.getTagCompound().hasKey("radiopack")) {
                            NBTTagCompound radio = is.getTagCompound().getCompoundTag("radiopack");
                            if (radio.hasKey("status")) {
                                if (radio.getString("status").equals("on")) {
                                    if (radio.hasKey("channel")) {
                                        return radio.getString("channel");
                                    }
                                }
                            }

                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    static String getRadiopackId(Player player1) {
        try {
            EntityPlayer player = ServerProxy.getForgePlayer(player1.getName());
            ICAPCustomInventory cap = player.getCapability(INVENTORY_CAP, null);
            CustomInventory inv = cap.getInventory();
            NonNullList<ItemStack> list = inv.getStacks();
            ArrayList<ItemStack> itemStacks = new ArrayList<ItemStack>(list);
            itemStacks.add(player.getHeldItemMainhand());
            itemStacks.add(player.getHeldItemOffhand());
            for (ItemStack is : itemStacks) {
                if (!is.isEmpty() && is.hasTagCompound()) {
                    if (is.getTagCompound() != null) {
                        if (is.getTagCompound().hasKey("radiopack")) {
                            NBTTagCompound radio = is.getTagCompound().getCompoundTag("radiopack");
                            if (radio.hasKey("reciever")) return "-666";
                            if (radio.hasKey("id")) {
                                return radio.getString("id");
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    static String getRadiopackVolumeByChannel(EntityPlayer player, String channel) {
        try {
            ICAPCustomInventory cap = player.getCapability(INVENTORY_CAP, null);
            CustomInventory inv = cap.getInventory();
            NonNullList<ItemStack> list = inv.getStacks();
            ArrayList<ItemStack> itemStacks = new ArrayList<ItemStack>(list);
            itemStacks.add(player.getHeldItemMainhand());
            itemStacks.add(player.getHeldItemOffhand());
            for (ItemStack is : itemStacks) {
                if (!is.isEmpty() && is.hasTagCompound()) {
                    if (is.getTagCompound() != null) {
                        if (is.getTagCompound().hasKey("radiopack")) {
                            NBTTagCompound radio = is.getTagCompound().getCompoundTag("radiopack");
                            if (radio.hasKey("status")) {
                                if (radio.getString("status").equals("on")) {
                                    if (radio.hasKey("channel")) {
                                        if (radio.getString("channel").equals(channel)) {
                                            if(radio.hasKey("volume")) return radio.getString("volume");
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "9";
    }

    static String getRadiopackChannelNpc(NPCWrapper npc) {
        try {
            ItemStack is = npc.getInventory().getLeftHand().getMCItemStack();
                if (!is.isEmpty() && is.hasTagCompound()) {
                    if (is.getTagCompound() != null) {
                        if (is.getTagCompound().hasKey("radiopack")) {
                            NBTTagCompound radio = is.getTagCompound().getCompoundTag("radiopack");
                            if (radio.hasKey("status")) {
                                if (radio.getString("status").equals("on")) {
                                    if (radio.hasKey("channel")) {
                                        return radio.getString("channel");
                                    }
                                }
                            }

                        }
                    }
                }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    static String getRadiopackIdNpc(NPCWrapper npc) {
        try {
            ItemStack is = npc.getInventory().getLeftHand().getMCItemStack();
            if (!is.isEmpty() && is.hasTagCompound()) {
                if (is.getTagCompound() != null) {
                    if (is.getTagCompound().hasKey("radiopack")) {
                        NBTTagCompound radio = is.getTagCompound().getCompoundTag("radiopack");
                        if (radio.hasKey("reciever")) return "-666";
                        if (radio.hasKey("id")) {
                            return radio.getString("id");
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    static String getRadiopackVolumeByChannelNpc(NPCWrapper npc, String channel) {
        try {
            ItemStack is = npc.getInventory().getLeftHand().getMCItemStack();
            if (!is.isEmpty() && is.hasTagCompound()) {
                if (is.getTagCompound() != null) {
                    if (is.getTagCompound().hasKey("radiopack")) {
                        NBTTagCompound radio = is.getTagCompound().getCompoundTag("radiopack");
                        if (radio.hasKey("status")) {
                            if (radio.getString("status").equals("on")) {
                                if (radio.hasKey("channel")) {
                                    if (radio.getString("channel").equals(channel)) {
                                        if(radio.hasKey("volume")) return radio.getString("volume");
                                    }
                                }
                            }
                        }

                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "9";
    }

    public static int[] countWeightNpc(String name) {
        int weight = 0;
        int psychweight = 0;
        try {
            EntityNPCInterface npc = DataHolder.inst().getNpcEntity(name);
            INPCInventory inv = npc.wrappedNPC.getInventory();
            if (inv.getRightHand() != null) {
                int [] test = countWeightItem(inv.getRightHand().getMCItemStack());
                weight += test[0];
                psychweight += test[1];
            }
            if (inv.getLeftHand() != null) {
                int [] test = countWeightItem(inv.getLeftHand().getMCItemStack());
                weight += test[0];
                psychweight += test[1];
            }
            if (inv.getProjectile() != null) {
                int [] test = countWeightItem(inv.getProjectile().getMCItemStack());
                weight += test[0];
                psychweight += test[1];
            }
            for (int i = 0; i < 4; i++) {
                if (inv.getArmor(i) != null) {
                    int [] test = countWeightItem(inv.getArmor(i).getMCItemStack());
                    weight += test[0];
                    psychweight += test[1];
                }
            }
            for (int i = 0; i < 9; i++) {
                if (inv.getDropItem(i) != null) {
                    int [] test = countWeightItem(inv.getDropItem(i).getMCItemStack());
                    weight += test[0];
                    psychweight += test[1];
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new int[] {weight, psychweight};
    }
    public static int[] countWeight(String name) {
        int weight = 0;
        int psychweight = 0;
        long mili = System.currentTimeMillis();
        EntityPlayerMP playerMP = ServerProxy.getForgePlayer(name);
        if (playerMP == null) return new int[] {0, 0};
        for (ItemStack itemStack : playerMP.inventory.mainInventory) {
            int [] test = countWeightItem(itemStack);
            weight += test[0];
            psychweight += test[1];
        }
        for (ItemStack itemStack : playerMP.inventory.armorInventory) {
            int [] test =  countWeightItem(itemStack);
            weight += test[0];
            psychweight += test[1];
        }
        for (ItemStack itemStack : playerMP.inventory.offHandInventory) {
            int [] test =  countWeightItem(itemStack);
            weight += test[0];
            psychweight += test[1];
        }
        try {
            ICAPCustomInventory cap = playerMP.getCapability(INVENTORY_CAP, null);
            CustomInventory inv = cap.getInventory();
            NonNullList<ItemStack> list = inv.getStacks();
            for (ItemStack itemStack : list) {
                int [] test =  countWeightItem(itemStack);
                weight += test[0];
                psychweight += test[1];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            IBackpack iBackpack = playerMP.getCapability(BackpackCapability.CAPABILITY, null);
            BackpackDataItems bdi = (BackpackDataItems) iBackpack.getData();
            for (int i = 0; i < bdi.getItems().getSlots(); i++) {
                weight += countOnlyWeightItem(bdi.getItems().getStackInSlot(i));
            }
        } catch (Exception ignored) {
        }
        System.out.println("Count took " + (System.currentTimeMillis() - mili));
        ArrayList<Integer> weightList = new ArrayList<>();

        return new int[] {weight, psychweight} ;
    }

    public static int countBuffsNpc(String name, String skill) {
        int buff = 0;
        try {
            EntityNPCInterface npc = DataHolder.inst().getNpcEntity(name);
            INPCInventory inv = npc.wrappedNPC.getInventory();
            if (inv.getRightHand() != null) {
                buff += countBuffItem(inv.getRightHand().getMCItemStack(), true, skill);
            }
            if (inv.getLeftHand() != null) {
                buff += countBuffItem(inv.getLeftHand().getMCItemStack(), true, skill);
            }
            if (inv.getProjectile() != null) {
                buff +=  countBuffItem(inv.getProjectile().getMCItemStack(), false, skill);
            }
            for (int i = 0; i < 4; i++) {
                if (inv.getArmor(i) != null) {
                    buff += countBuffItem(inv.getArmor(i).getMCItemStack(), true, skill);

                }
            }
            for (int i = 0; i < 9; i++) {
                if (inv.getDropItem(i) != null) {
                    buff += countBuffItem(inv.getDropItem(i).getMCItemStack(), false, skill);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return buff;
    }
    public static int countBuffs(String name, String skill) {
        int buff = 0;
        long mili = System.currentTimeMillis();
        EntityPlayerMP playerMP = ServerProxy.getForgePlayer(name);
        if (playerMP == null) return 0;
        for (ItemStack itemStack : playerMP.inventory.mainInventory) {
            if (itemStack.equals(playerMP.getHeldItemMainhand())) buff += countBuffItem(itemStack, true, skill);
            else buff += countBuffItem(itemStack, false, skill);
        }
        for (ItemStack itemStack : playerMP.inventory.armorInventory) {
            buff += countBuffItem(itemStack, true, skill);
        }
        for (ItemStack itemStack : playerMP.inventory.offHandInventory) {
            buff += countBuffItem(itemStack, true, skill);
        }
        try {
            ICAPCustomInventory cap = playerMP.getCapability(INVENTORY_CAP, null);
            CustomInventory inv = cap.getInventory();
            NonNullList<ItemStack> list = inv.getStacks();
            for (ItemStack itemStack : list) {
                buff += countBuffItem(itemStack, false, skill);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Count took " + (System.currentTimeMillis() - mili));
        ArrayList<Integer> weightList = new ArrayList<>();

        return buff;
    }

    static int countOnlyWeightItem(ItemStack itemStack) {
        int weight = 0;
        if (itemStack.isEmpty()) return 0;
        WeightHandler wh = new WeightHandler(itemStack);
        weight += wh.getWeightAnything() * itemStack.getCount();
        return weight;
    }

    static int[] countWeightItem(ItemStack itemStack) {
        int weight = 0;
        int psychweight = 0;
        if (itemStack.isEmpty()) return new int[] {0, 0};
        WeightHandler wh = new WeightHandler(itemStack);
        weight += wh.getWeightAnything() * itemStack.getCount();
        psychweight += wh.getPsychWeight() * itemStack.getCount();
        return new int[] {weight, psychweight};
    }

    static int countBuffItem(ItemStack itemStack, boolean eq, String skill) {
        int buff = 0;
        if (itemStack.isEmpty()) return 0;
        EquipmentBuffTagHandler eh = new EquipmentBuffTagHandler(itemStack);
        if (eq) buff += eh.getEqBuff(skill) * itemStack.getCount();
        else buff += eh.getInvBuff(skill) * itemStack.getCount();
        return buff;
    }

    public static int getCurrentDay() {
        try {
            IData data = NpcAPI.Instance().getIWorld(0).getStoreddata();
            if (data.has("currentDay")) {
                System.out.println("TEST SENDING PACKAGE" + data.get("currentDay"));
                return Integer.parseInt((String) data.get("currentDay"));
            } else {
                return -666;
            }
        } catch (Exception ignored) {
            System.out.println(ignored);
            return -666;
        }
    }

    public static void makeSureIsLoaded(ItemStack item) {
        if (!WeaponTagsHandler.hasWeaponTags(item)) {
            TextComponentString error = new TextComponentString("Нет огнестрельного оружия в руках.");
            error.getStyle().setColor(TextFormatting.RED);
            return;
        }

        WeaponTagsHandler weapon = new WeaponTagsHandler(item);
        if (weapon.isFull()) {
            TextComponentString error = new TextComponentString("Оружие не может вместить больше патронов.");
            error.getStyle().setColor(TextFormatting.RED);
            return;
        }

        NBTTagCompound magazineTag = weapon.getMagazine();
//            System.out.println("magazine: " + magazineTag);
        if (magazineTag == null || magazineTag.hasNoTags() || magazineTag.toString().equals("{}")) return;

        NBTTagList loadedStuff = magazineTag.getTagList(AttachmentsHandler.LOADED_PROJECTILES, 10);
        if (loadedStuff.tagCount() == 0) {
            TextComponentString error = new TextComponentString("Вставленный магазин пуст.");
            error.getStyle().setColor(TextFormatting.RED);
            return;
        }
        //AttachmentsHandler magazine = new AttachmentsHandler(weapon.getMagazine());
        System.out.println("loadedStuff: " + loadedStuff);
        weapon.loadProjectileToChamber(loadedStuff.getCompoundTagAt(loadedStuff.tagCount() - 1));
        loadedStuff.removeTag(loadedStuff.tagCount() - 1);
        magazineTag.setTag(AttachmentsHandler.LOADED_PROJECTILES, loadedStuff);
    }
}
