package ru.konungstvo.bridge;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.player.Player;
import ru.konungstvo.player.wounds.WoundPyramid;
import ru.konungstvo.player.wounds.WoundType;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * A helper class to handle communication with service_modifiers.
 * Works with wounds, armor and modifiers.
 */
public class Modifiers {
    private String modifiersURL;
    private String modifiersAuth;

    public Modifiers(String modifiersURL, String modifiersPass) {
        this.modifiersURL = modifiersURL;
        this.modifiersAuth = modifiersPass;
    }

    public int getWoundsMod(String playerName) {
        String url = modifiersURL + "character/" + playerName + "/modifiers";
        //System.out.println("url: " + url);
        JSONObject response = new JSONObject();
        try {
            response = Helpers.readJsonFromUrl(url, modifiersAuth);

        } catch (Exception e) {
            return 0;
//            throw new NetworkException("Fetching wounds:", "ERROR while fetching modifiers: " + e.getMessage());
        }
        if (response.get("wounds") == null)
            return 0;
        return Math.toIntExact((Long) response.get("wounds"));
    }

    // Negative mod from armor, used while throwing specific dice like acrobatics or reaction.
    public int getArmorMod(String name) {
        String url = modifiersURL + "character/" + name + "/modifiers";
        JSONObject responce = new JSONObject();
        try {
            responce = Helpers.readJsonFromUrl(url, modifiersAuth);
        } catch (Exception e) {
            return 0;
//            throw new NetworkException("Fetching wounds:", "ERROR while fetching armor: " + e.getMessage());
        }
        if (responce.get("armor") == null)
            return 0;
        return Math.toIntExact((Long) responce.get("armor"));
    }

    //Территория костылей
    public int getBloodloss(String name) {
        System.out.println(name);
        if (Pattern.matches(".*\\p{InCyrillic}.*", name)) {
            name = "PleaseDontUse" + transliterate(name);
        }
        System.out.println(name);
        String url = modifiersURL + "armor/" + name + "/fetch";
        System.out.println(url);
        JSONObject responce = new JSONObject();
        try {
            responce = Helpers.readJsonFromUrl(url, modifiersAuth);
        } catch (Exception e) {
            return 0;
//            throw new NetworkException("Fetching wounds:", "ERROR while fetching armor: " + e.getMessage());
        }
        if (responce.get("type") == null)
            return 0;
        return Math.toIntExact((Long) responce.get("type"));
    }

    public void updateBloodloss(String username, int bloodloss) {
        System.out.println(username);
        if (Pattern.matches(".*\\p{InCyrillic}.*", username)) {
            username = "PleaseDontUse" + transliterate(username);
        }
        System.out.println(username);
        String url = modifiersURL + "armor/"+ username +"/update";
        System.out.println(url);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", bloodloss);
        Helpers.sendJsonToUrl(jsonObject, url, modifiersAuth);
    }

    public int getMentalDebuff(String name) {
        String url = modifiersURL + "character/" + name + "/wounds";
        JSONArray response = new JSONArray();
        try {
            response = Helpers.readJsonArrayFromUrl(url, modifiersAuth);
        } catch (Exception e) {
            return 0;
            //   throw new NetworkException("Fetching wounds:", "ERROR while fetching wounds: " + e.getMessage());
        }
        int mentalDebuff = 0;
        for (Object aResponse : response) {
            JSONObject jsonObject = (JSONObject) aResponse;
            boolean almostCured = (boolean) jsonObject.get("almost_cured");
            if (((String) jsonObject.get("description")).contains("[ментальная рана]")) {
                switch (Math.toIntExact((Long) jsonObject.get("type"))) {
                    case 0:
                        mentalDebuff += WoundPyramid.SCRATCH_WEIGHT;
                        if (almostCured) mentalDebuff -= WoundPyramid.SCRATCH_WEIGHT;
                        break;
                    case 1:
                        mentalDebuff += WoundPyramid.SCRATCH_WEIGHT * 2;
                        if (almostCured) mentalDebuff -= WoundPyramid.SCRATCH_WEIGHT;
                        break;
                    case 2:
                        mentalDebuff += WoundPyramid.SCRATCH_WEIGHT * 4;
                        if (almostCured) mentalDebuff -= WoundPyramid.SCRATCH_WEIGHT * 2;
                        break;
                }
            }
        }
        return mentalDebuff;
    }
    //
    // Positive mod from armor, used in combats.
    public int getDefenseMod(String name) {
        String url = modifiersURL + "armor/" + name + "/fetch";
        JSONObject response = new JSONObject();
        try {
            response = Helpers.readJsonFromUrl(url, modifiersAuth);
        } catch (Exception e) {
            return 0;
//            throw new NetworkException("Fetching wounds:", "ERROR while fetching defense: " + e.getMessage());
        }
        long type;
        try {
            type = (long) response.get("type");
        } catch (NullPointerException e) {
            return 0;
        }
        return Math.toIntExact(type + 1);
    }

    public List<WoundType> getWounds(String name) {
        String url = modifiersURL + "character/" + name + "/wounds";
        JSONArray response = new JSONArray();
        try {
            response = Helpers.readJsonArrayFromUrl(url, modifiersAuth);
        } catch (Exception e) {
            return null;
         //   throw new NetworkException("Fetching wounds:", "ERROR while fetching wounds: " + e.getMessage());
        }

        List<WoundType> result = new ArrayList<>();
        int almostCuredBuff = 0;
        for (Object aResponse : response) {
            JSONObject jsonObject = (JSONObject) aResponse;
            boolean almostCured = (boolean) jsonObject.get("almost_cured");
            System.out.println(almostCured);

            switch (Math.toIntExact((Long) jsonObject.get("type"))) {
                case 0:
                    result.add(WoundType.SCRATCH);
                    if (almostCured) almostCuredBuff += WoundPyramid.SCRATCH_WEIGHT;
                    break;
                case 1:
                    result.add(WoundType.LIGHT);
                    if (almostCured) almostCuredBuff += WoundPyramid.LIGHT_WEIGHT - WoundPyramid.SCRATCH_WEIGHT;
                    break;
                case 2:
                    result.add(WoundType.SEVERE);
                    if (almostCured) almostCuredBuff += WoundPyramid.SEVERE_WEIGHT - WoundPyramid.LIGHT_WEIGHT;
                    break;
                case 3:
                    result.add(WoundType.CRITICAL);
                    //if (almostCured) almostCuredBuff += WoundPyramid.SCRATCH_WEIGHT;
            }
        }
        try {
            DataHolder.inst().getPlayer(name).getWoundPyramid().setAlmostCuredBuff(almostCuredBuff);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String sendWound(String username, WoundType woundType, String description) {
        String url = modifiersURL + "wound/create";

        TimeZone tz = TimeZone.getTimeZone("UTC+3");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
        df.setTimeZone(tz);

        String nowAsISO = df.format(new Date());
        String almostCuredAsISO = "";
        String curedAsISO = "";


        //ВЕРОЯТНЫЙ БАГ СО ВРЕМЕНЕМ И ЧАСОВЫМИ ПОЯСАМИ
        if (woundType == WoundType.LIGHT) {
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.DATE, 2);
            almostCuredAsISO = df.format(c.getTime());
            System.out.println(almostCuredAsISO);
            c.add(Calendar.DATE, 1);
            curedAsISO = df.format(c.getTime());
        } else if (woundType == WoundType.SCRATCH) {
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.DATE, 1);
            curedAsISO = df.format(c.getTime());
        } else if (DataHolder.inst().isNpc(username)) { //чтобы не загрязнять список ран на сервере ранами НПЦ, я думаю. Хотя я не уверен что он загрязняется и как это работает
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.DATE, 3);
            almostCuredAsISO = df.format(c.getTime());
            System.out.println(almostCuredAsISO);
            c.add(Calendar.DATE, 2);
            curedAsISO = df.format(c.getTime());
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", username);
        jsonObject.put("type", woundType.getInt());
        jsonObject.put("description", description);
        jsonObject.put("almost_cured", false);
        jsonObject.put("created_at", nowAsISO);
        if (!almostCuredAsISO.isEmpty())
            jsonObject.put("almost_cured_at", almostCuredAsISO);
        if (!curedAsISO.isEmpty())
            jsonObject.put("expire_at", curedAsISO);
        return Helpers.sendJsonToUrl(jsonObject, url, modifiersAuth);
    }

    public String updateWound(JSONObject jsonObject) {
        String url = modifiersURL + "wound/" + jsonObject.get("id") + "/update";

        jsonObject.remove("id");
        return Helpers.sendJsonToUrl(jsonObject, url, modifiersAuth);
    }

    public String removeWound(JSONObject jsonObject) {
        String url = modifiersURL + "wound/" + jsonObject.get("id") + "/delete";
        return Helpers.getUrl(url, modifiersAuth);
    }

    public List<String> getBleedingwounds(String name) {
        String url = modifiersURL + "character/" + name + "/wounds";
        JSONArray response = new JSONArray();
        try {
            response = Helpers.readJsonArrayFromUrl(url, modifiersAuth);
        } catch (Exception e) {
            return null;
            //   throw new NetworkException("Fetching wounds:", "ERROR while fetching wounds: " + e.getMessage());
        }

        List<String> result = new ArrayList<>();
        for (Object aResponse : response) {
            JSONObject jsonObject = (JSONObject) aResponse;
//            if (((String) jsonObject.get("description")).contains("[слабое кровотечение]")) result.add("[слабое кровотечение]");
//            if (((String) jsonObject.get("description")).contains("[сильное кровотечение]")) result.add("[сильное кровотечение]");
//            if (((String) jsonObject.get("description")).contains("[критическое кровотечение]")) result.add("[критическое кровотечение]");
            if (((String) jsonObject.get("description")).contains("[легкое повреждение]")) result.add("[легкое повреждение]");
            if (((String) jsonObject.get("description")).contains("[тяжелое повреждение]")) result.add("[тяжелое повреждение]");
            if (((String) jsonObject.get("description")).contains("[ментальная рана]")) result.add("[ментальная рана]");
//            if (((String) jsonObject.get("description")).contains("[горение]")) result.add("[горение]");
            if (((String) jsonObject.get("description")).contains("[оглушение:")) result.add("[оглушение:");
        }
/*        try {
            DataHolder.inst().getPlayer(name).getWoundPyramid().setAlmostCuredBuff(almostCuredBuff);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        System.out.println(result);
        return result;
    }

    public int getLeftarmInjuriesDebuff(String name) {
        String url = modifiersURL + "character/" + name + "/wounds";
        JSONArray response = new JSONArray();
        try {
            response = Helpers.readJsonArrayFromUrl(url, modifiersAuth);
        } catch (Exception e) {
            return 0;
            //   throw new NetworkException("Fetching wounds:", "ERROR while fetching wounds: " + e.getMessage());
        }
        int debuff = 0;
        for (Object aResponse : response) {
            JSONObject jsonObject = (JSONObject) aResponse;
            if (((String) jsonObject.get("description")).contains("[тяжелое повреждение левой руки")) debuff += 50;
            if (((String) jsonObject.get("description")).contains("[легкое повреждение левой руки")) debuff += 25;

        }
        System.out.println(debuff);
        return debuff;
    }

    public int getRightarmInjuriesDebuff(String name) {
        String url = modifiersURL + "character/" + name + "/wounds";
        JSONArray response = new JSONArray();
        try {
            response = Helpers.readJsonArrayFromUrl(url, modifiersAuth);
        } catch (Exception e) {
            return 0;
            //   throw new NetworkException("Fetching wounds:", "ERROR while fetching wounds: " + e.getMessage());
        }
        int debuff = 0;
        for (Object aResponse : response) {
            JSONObject jsonObject = (JSONObject) aResponse;
            if (((String) jsonObject.get("description")).contains("[тяжелое повреждение правой руки")) debuff += 50;
            if (((String) jsonObject.get("description")).contains("[легкое повреждение правой руки")) debuff += 25;

        }
        System.out.println(debuff);
        return debuff;
    }

    public int getLegsInjuriesDebuff(String name) {
        String url = modifiersURL + "character/" + name + "/wounds";
        JSONArray response = new JSONArray();
        try {
            response = Helpers.readJsonArrayFromUrl(url, modifiersAuth);
        } catch (Exception e) {
            return 0;
            //   throw new NetworkException("Fetching wounds:", "ERROR while fetching wounds: " + e.getMessage());
        }
        int debuff = 0;
        for (Object aResponse : response) {
            JSONObject jsonObject = (JSONObject) aResponse;
            if (((String) jsonObject.get("description")).contains("[тяжелое повреждение левой ноги")) debuff += 50;
            if (((String) jsonObject.get("description")).contains("[легкое повреждение левой ноги")) debuff += 25;
            if (((String) jsonObject.get("description")).contains("[тяжелое повреждение правой ноги")) debuff += 50;
            if (((String) jsonObject.get("description")).contains("[легкое повреждение правой ноги")) debuff += 25;
        }
        System.out.println(debuff);
        return debuff;
    }

    public ArrayList<String> getInjuriesList(String name) {
        String url = modifiersURL + "character/" + name + "/wounds";
        JSONArray response = new JSONArray();
        ArrayList <String> inj = new ArrayList<>();
        try {
            response = Helpers.readJsonArrayFromUrl(url, modifiersAuth);
        } catch (Exception e) {
            return inj;
            //   throw new NetworkException("Fetching wounds:", "ERROR while fetching wounds: " + e.getMessage());
        }
        int debuff = 0;
        for (Object aResponse : response) {
            JSONObject jsonObject = (JSONObject) aResponse;
            String desc = ((String) jsonObject.get("description"));
            if (desc.contains("[тяжелое повреждение левой руки")) inj.add("§4Тяжелое повреждение левой руки - §l" + ((String) jsonObject.get("description")).split("\\[тяжелое повреждение левой руки: ")[1].replaceAll("]", "") + "§4.");
            if (desc.contains("[легкое повреждение левой руки")) inj.add("§eЛегкое повреждение левой руки - §l" + ((String) jsonObject.get("description")).split("\\[легкое повреждение левой руки: ")[1].replaceAll("]", "") + "§e.");
            if (desc.contains("[тяжелое повреждение правой руки")) inj.add("§4Тяжелое повреждение правой руки - §l" + ((String) jsonObject.get("description")).split("\\[тяжелое повреждение правой руки: ")[1].replaceAll("]", "") + "§4.");
            if (desc.contains("[легкое повреждение правой руки")) inj.add("§eЛегкое повреждение правой руки - §l" + ((String) jsonObject.get("description")).split("\\[легкое повреждение правой руки: ")[1].replaceAll("]", "") + "§e.");
            if (desc.contains("[тяжелое повреждение левой ноги")) inj.add("§4Тяжелое повреждение левой ноги - §l" + ((String) jsonObject.get("description")).split("\\[тяжелое повреждение левой ноги: ")[1].replaceAll("]", "") + "§4.");
            if (desc.contains("[легкое повреждение левой ноги")) inj.add("§eЛегкое повреждение левой ноги - §l" + ((String) jsonObject.get("description")).split("\\[легкое повреждение левой ноги: ")[1].replaceAll("]", "") + "§e.");
            if (desc.contains("[тяжелое повреждение правой ноги")) inj.add("§4Тяжелое повреждение правой ноги - §l" + ((String) jsonObject.get("description")).split("\\[тяжелое повреждение правой ноги: ")[1].replaceAll("]", "") + "§4.");
            if (desc.contains("[легкое повреждение правой ноги")) inj.add("§eЛегкое повреждение правой ноги - §l" + ((String) jsonObject.get("description")).split("\\[легкое повреждение правой ноги: ")[1].replaceAll("]", "") + "§e.");
        }
        System.out.println(debuff);
        return inj;
    }

    public boolean getAndUpdateTurnSkip(String name) {
        String url = modifiersURL + "character/" + name + "/wounds";
        JSONArray response = new JSONArray();
        try {
            response = Helpers.readJsonArrayFromUrl(url, modifiersAuth);
        } catch (Exception e) {
            return false;
            //   throw new NetworkException("Fetching wounds:", "ERROR while fetching wounds: " + e.getMessage());
        }
        for (Object aResponse : response) {
            JSONObject jsonObject = (JSONObject) aResponse;
            if (((String) jsonObject.get("description")).contains("[пропуск следующего хода]")) {
                jsonObject.replace("description", ((String) jsonObject.get("description")).replace("[пропуск следующего хода]", ""));
                updateWound(jsonObject);
                return true;
            } else if (((String) jsonObject.get("description")).contains("[пропуск следующих двух ходов]")) {
                jsonObject.replace("description", ((String) jsonObject.get("description")).replace("[пропуск следующих двух ходов]", "[пропуск следующего хода]"));
                updateWound(jsonObject);
                return true;
            } else if (((String) jsonObject.get("description")).contains("[пропуск следующих трех ходов]")) {
                jsonObject.replace("description", ((String) jsonObject.get("description")).replace("[пропуск следующих трех ходов]", "[пропуск следующих двух ходов]"));
                updateWound(jsonObject);
                return true;
            }

        }

        return false;
    }

    public boolean vampHealWound(Player player, int type, String victim) {
        String url = modifiersURL + "character/" + player.getName() + "/wounds";
        JSONArray response = new JSONArray();
        try {
            response = Helpers.readJsonArrayFromUrl(url, modifiersAuth);
        } catch (Exception e) {
            return false;
            //   throw new NetworkException("Fetching wounds:", "ERROR while fetching wounds: " + e.getMessage());
        }

        for (Object aResponse : response) {
            JSONObject jsonObject = (JSONObject) aResponse;
            boolean almostCured = (boolean) jsonObject.get("almost_cured");
            if (Math.toIntExact((Long) jsonObject.get("type")) == 2) {
                if (!almostCured && type >= 2) {
                    jsonObject.replace("almost_cured", true);
                    jsonObject.replace("description", ((String) jsonObject.get("description")) + " [вылечено вампиризмом со здоровья " + victim + "]");
                    updateWound(jsonObject);
                    player.updateWounds(DataHolder.inst().getModifiers().getWounds(player.getName()));
                    return true;
                }
            }
        }
        for (Object aResponse : response) {
            JSONObject jsonObject = (JSONObject) aResponse;
            boolean almostCured = (boolean) jsonObject.get("almost_cured");
            if (Math.toIntExact((Long) jsonObject.get("type")) == 1) {
                if (!almostCured && type >= 1) {
                    jsonObject.replace("almost_cured", true);
                    jsonObject.replace("description", ((String) jsonObject.get("description")) + " [вылечено вампиризмом со здоровья " + victim + "]");
                    updateWound(jsonObject);
                    player.updateWounds(DataHolder.inst().getModifiers().getWounds(player.getName()));
                    return true;
                }
            }
        }
        for (Object aResponse : response) {
            JSONObject jsonObject = (JSONObject) aResponse;
            boolean almostCured = (boolean) jsonObject.get("almost_cured");
            if (Math.toIntExact((Long) jsonObject.get("type")) == 0) {
                if (!almostCured && type >= 0) {
                    removeWound(jsonObject);
                    player.updateWounds(DataHolder.inst().getModifiers().getWounds(player.getName()));
                    return true;
                }
            }
        }
        return false;
    }

    public void purgeWounds(Player player) {
        String url = modifiersURL + "character/" + player.getName() + "/wounds";
        JSONArray response = new JSONArray();
        try {
            response = Helpers.readJsonArrayFromUrl(url, modifiersAuth);
        } catch (Exception e) {
            return;
            //   throw new NetworkException("Fetching wounds:", "ERROR while fetching wounds: " + e.getMessage());
        }
        for (Object aResponse : response) {
            JSONObject jsonObject = (JSONObject) aResponse;
            removeWound(jsonObject);
            player.updateWounds(DataHolder.inst().getModifiers().getWounds(player.getName()));
        }
    }

    public void purgeStunWounds(Player player) {
        try {
        String url = modifiersURL + "character/" + player.getName() + "/wounds";
        JSONArray response = new JSONArray();

            response = Helpers.readJsonArrayFromUrl(url, modifiersAuth);
            for (Object aResponse : response) {
                JSONObject jsonObject = (JSONObject) aResponse;
                if (!((String) jsonObject.get("description")).contains("[оглушение") && !((String) jsonObject.get("description")).contains("[нокаут]")) continue;
                removeWound(jsonObject);
                player.updateWounds(DataHolder.inst().getModifiers().getWounds(player.getName()));
            }
        } catch (Exception ignored) {
            return;
            //   throw new NetworkException("Fetching wounds:", "ERROR while fetching wounds: " + e.getMessage());
        }

    }


    public boolean getAndUpdateStunningWounds(String name) {
        String url = modifiersURL + "character/" + name + "/wounds";
        JSONArray response = new JSONArray();
        try {
            response = Helpers.readJsonArrayFromUrl(url, modifiersAuth);
        } catch (Exception e) {
            return false;
            //   throw new NetworkException("Fetching wounds:", "ERROR while fetching wounds: " + e.getMessage());
        }
        boolean shouldUpdateWounds = false;
        for (Object aResponse : response) {
            JSONObject jsonObject = (JSONObject) aResponse;
            if (((String) jsonObject.get("description")).contains("[оглушение:")) {
                System.out.println("Оглушение найдено");
                if (((String) jsonObject.get("description")).contains("[оглушение:станет переходной через 5 ходов]")) {
                    jsonObject.replace("description", ((String) jsonObject.get("description")).replace("[оглушение:станет переходной через 5 ходов]", "[оглушение:станет переходной через 4 хода]"));
                } else if (((String) jsonObject.get("description")).contains("[оглушение:станет переходной через 4 хода]")) {
                    jsonObject.replace("description", ((String) jsonObject.get("description")).replace("[оглушение:станет переходной через 4 хода]", "[оглушение:станет переходной через 3 хода]"));
                } else if (((String) jsonObject.get("description")).contains("[оглушение:станет переходной через 3 хода]")) {
                    jsonObject.replace("description", ((String) jsonObject.get("description")).replace("[оглушение:станет переходной через 3 хода]", "[оглушение:станет переходной через 2 хода]"));
                } else if (((String) jsonObject.get("description")).contains("[оглушение:станет переходной через 2 хода]")) {
                    jsonObject.replace("description", ((String) jsonObject.get("description")).replace("[оглушение:станет переходной через 2 хода]", "[оглушение:станет переходной через ход]"));
                } else if (((String) jsonObject.get("description")).contains("[оглушение:станет переходной через ход]")) {
                    jsonObject.replace("description", ((String) jsonObject.get("description")).replace("[оглушение:станет переходной через ход]", "[оглушение]"));
                    jsonObject.replace("almost_cured", true);
                    shouldUpdateWounds = true;
                }
                updateWound(jsonObject);
            }
        }
        return shouldUpdateWounds;
    }

    //в конце хода
    public void updateBloodlossAndStunning(Player previousPlayer, String previous) {
        previousPlayer.setBloodloss(DataHolder.inst().getModifiers().getBloodloss(previous));
        List<String> proplist = DataHolder.inst().getModifiers().getBleedingwounds(previous);
        if (!previousPlayer.cantBleed()) {
            int bleeding = previousPlayer.getStrongestBleeding();
            if (bleeding != 0) {
                previousPlayer.setBloodloss(previousPlayer.getBloodloss() + bleeding);
            }
        }
        boolean stunningWoundsNeedAttention = false;
        for (String prop : proplist) {
            System.out.println(prop);
//            if (prop.equals("[слабое кровотечение]") && !previousPlayer.cantBleed()) previousPlayer.setBloodloss(previousPlayer.getBloodloss() + 5);
//            if (prop.equals("[сильное кровотечение]") && !previousPlayer.cantBleed()) previousPlayer.setBloodloss(previousPlayer.getBloodloss() + 10);
//            if (prop.equals("[критическое кровотечение]") && !previousPlayer.cantBleed()) previousPlayer.setBloodloss(previousPlayer.getBloodloss() + 20);
            //if (prop.equals("[горение]") && !previousPlayer.cantBurn()) previousPlayer.inflictDamage(3, "ожог от горения");
            if (prop.equals("[оглушение:")) stunningWoundsNeedAttention = true;
        }
        DataHolder.inst().getModifiers().updateBloodloss(previous, previousPlayer.getBloodloss());
        if (stunningWoundsNeedAttention) {
            if (DataHolder.inst().getModifiers().getAndUpdateStunningWounds(previous)) {
                previousPlayer.updateWounds(DataHolder.inst().getModifiers().getWounds(previous));
            }
        }
    }

    public static String transliterate(String message){
        char[] abcCyr =   {' ','а','б','в','г','д','е','ё', 'ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х', 'ц','ч', 'ш','щ','ъ','ы','ь','э', 'ю','я','А','Б','В','Г','Д','Е','Ё', 'Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х', 'Ц', 'Ч','Ш', 'Щ','Ъ','Ы','Ь','Э','Ю','Я','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9','-','_'};
        String[] abcLat = {" ","a","b","v","g","d","e","e","zh","z","i","y","k","l","m","n","o","p","r","s","t","u","f","h","ts","ch","sh","sch", "","i", "","e","ju","ja","A","B","V","G","D","E","E","Zh","Z","I","Y","K","L","M","N","O","P","R","S","T","U","F","H","Ts","Ch","Sh","Sch", "","I", "","E","Ju","Ja","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","0","1","2","3","4","5","6","7","8","9","-","_"};
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < message.length(); i++) {
            for (int x = 0; x < abcCyr.length; x++ ) {
                if (message.charAt(i) == abcCyr[x]) {
                    builder.append(abcLat[x]);
                }
            }
        }
        return builder.toString();
    }
}
