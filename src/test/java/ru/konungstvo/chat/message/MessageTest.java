package ru.konungstvo.chat.message;

import net.minecraft.util.text.TextComponentString;
import org.junit.Test;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.chat.range.Range;

public class MessageTest {
    @Test
    public void SimpleMessage_BuildsCorrectly() {
        RoleplayMessage roleplayMessage = new RoleplayMessage("Volpe", "Some content here", Range.SHOUT);
        roleplayMessage.build();
        TextComponentString res = MessageGenerator.generate(roleplayMessage);
        System.out.println(res.toString());
    }

}