package ru.konungstvo.bridge;

import org.junit.Test;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Player;
import ru.konungstvo.player.wounds.WoundType;

import static org.junit.Assert.*;

public class ModifiersTest {

    @Test
    public void sendWound() {
        String modifiersURL = "http://modifiers.konungstvo.ru/";
        String modifiersAuth = "modifiers:modifiers_password";
        Modifiers modifiers = new Modifiers(modifiersURL, modifiersAuth);
//        modifiers.sendWound("Defender", WoundType.SEVERE, "test2");
    }

    @Test
    public void getWounds() {
        String modifiersURL = "http://modifiers.konungstvo.ru/";
        String modifiersAuth = "modifiers:modifiers_password";
        Modifiers modifiers = new Modifiers(modifiersURL, modifiersAuth);
//        modifiers.getWounds("Defender");
    }

    @Test
    public void skillsetsTest() {
        DataHolder.inst().addPlayer(new Player("Purge"));
        DataHolder.inst().updatePlayerNewSkills("Purge");
    }

}